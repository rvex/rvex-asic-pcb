
# Input clocks. Just 25MHz for the core and 50Mhz for the HSI, so the HSI MMCM
# needs to be toned down a bunch for the emulated ASIC to work right.
create_clock -period 40.000 -name hsi_emul_backup_clk -waveform {0.000 20.000} [get_ports hsi_emul_backup_clk]
create_clock -period 20.000 -name hsi_emul_clk_p -waveform {0.000 10.000} [get_ports hsi_emul_clk_p]

# Auto-generated constraint for the clock ratio mux.
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks diff_clk_div2] -group [get_clocks -include_generated_clocks diff_clk_div4]

# The ASIC never uses the backup clock and differential clock at the same time,
# so all paths between the two are false.
set_clock_groups -asynchronous -group [get_clocks diff_clk_div*] -group [get_clocks -of_objects [get_ports hsi_emul_backup_clk]]
