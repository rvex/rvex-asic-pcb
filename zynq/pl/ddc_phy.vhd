library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity ddc_phy is
  port (
    
    -- Pads
    scl         : inout std_logic;
    sda         : inout std_logic;
    hpd         : out   std_logic;
    
    -- Control signals
    ddc_scl_i   : out std_logic;
    ddc_scl_o   : in  std_logic;
    ddc_scl_t   : in  std_logic;
    ddc_sda_i   : out std_logic;
    ddc_sda_o   : in  std_logic;
    ddc_sda_t   : in  std_logic;
    hpd_gpio    : in  std_logic_vector(0 downto 0)
    
  );
end ddc_phy;

architecture behavioral of ddc_phy is

  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of ddc_scl_i: signal is "xilinx.com:interface:iic:1.0 ddc SCL_I";
  attribute X_INTERFACE_INFO of ddc_scl_o: signal is "xilinx.com:interface:iic:1.0 ddc SCL_O";
  attribute X_INTERFACE_INFO of ddc_scl_t: signal is "xilinx.com:interface:iic:1.0 ddc SCL_T";
  attribute X_INTERFACE_INFO of ddc_sda_i: signal is "xilinx.com:interface:iic:1.0 ddc SDA_I";
  attribute X_INTERFACE_INFO of ddc_sda_o: signal is "xilinx.com:interface:iic:1.0 ddc SDA_O";
  attribute X_INTERFACE_INFO of ddc_sda_t: signal is "xilinx.com:interface:iic:1.0 ddc SDA_T";

begin
  
  scl_iobuf_inst: IOBUF port map (IO => scl, I => ddc_scl_o, T => ddc_scl_t, O => ddc_scl_i);
  sda_iobuf_inst: IOBUF port map (IO => sda, I => ddc_sda_o, T => ddc_sda_t, O => ddc_sda_i);
  
  hpd <= hpd_gpio(0);
  
end behavioral;
