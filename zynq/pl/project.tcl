
#==============================================================================
# Project configuration
#==============================================================================

# Select whether to emulate the ASIC or use the real socket.
set emulate 1

# Select whether to instantiate the ADS8885 interface.
set ads8885 1

# Select whether to instantiate video loopback stuff.
set video_loop 0


#==============================================================================
# Project creation
#==============================================================================

# Project description.
set prj_name  pl
set prj_dir   viv

# Create project.
create_project $prj_name $prj_dir -part xc7z020clg484-2
set_property target_language VHDL [current_project]

# Import HDL files.
add_files -quiet -fileset sources_1 ddc_phy.vhd

# Import constraints.
add_files -quiet -fileset constrs_1 ../xdc/pins.xdc
add_files -quiet -fileset constrs_1 $prj_name.xdc

# Import IP repository.
set_property ip_repo_paths ../ip/repo [current_project]
update_ip_catalog

# Create the block diagram.
create_bd_design "toplevel"


#==============================================================================
# Utility functions for block diagram
#==============================================================================

proc add_clkgen { name clk1_name drp } {

  set clkgen [join [list $name _clkgen] ""]
  set reset  [join [list $name _reset ] ""]

  create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.4 $clkgen
  set_property -dict [list \
    CONFIG.USE_DYN_RECONFIG $drp \
    CONFIG.AXI_DRP $drp \
  ] [get_bd_cells $clkgen]
  set_property -dict [list \
    CONFIG.PRIM_IN_FREQ.VALUE_SRC USER \
    CONFIG.OVERRIDE_MMCM {true} \
  ] [get_bd_cells $clkgen]
  set_property -dict [list \
    CONFIG.PRIM_SOURCE {No_buffer} \
    CONFIG.PRIM_IN_FREQ {50} \
    CONFIG.MMCM_COMPENSATION {INTERNAL} \
    CONFIG.MMCM_CLKIN1_PERIOD {20.000} \
    CONFIG.MMCM_DIVCLK_DIVIDE {1} \
    CONFIG.MMCM_CLKFBOUT_MULT_F {24.000} \
    CONFIG.RESET_TYPE {ACTIVE_LOW} \
    CONFIG.CLK_OUT1_PORT $clk1_name \
  ] [get_bd_cells $clkgen]

  connect_bd_net [get_bd_pins ps/FCLK_CLK0] [get_bd_pins $clkgen/clk_in1]
  if {$drp == false} {
    connect_bd_net [get_bd_pins ps/FCLK_RESET0_N] [get_bd_pins $clkgen/resetn]
  }
  create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 $reset

  connect_bd_net [get_bd_pins ps/FCLK_RESET0_N] [get_bd_pins $reset/ext_reset_in]
  connect_bd_net [get_bd_pins $clkgen/locked] [get_bd_pins $reset/dcm_locked]
  connect_bd_net [get_bd_pins $clkgen/$clk1_name] [get_bd_pins $reset/slowest_sync_clk]

}


#==============================================================================
# Create processing system and clocks
#==============================================================================

# Add PS IP block.
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 ps
set_property -dict [list \
  CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125} \
  CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
  CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} \
  CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
  CONFIG.PCW_ENET0_GRP_MDIO_IO {MIO 52 .. 53} \
  CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
  CONFIG.PCW_UART0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_UART0_UART0_IO {MIO 14 .. 15} \
  CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
  CONFIG.PCW_USE_S_AXI_GP0 {0} \
] [get_bd_cells ps]
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" Master "Disable" Slave "Disable" }  [get_bd_cells ps]

# Create clock generators.
add_clkgen axi axi false

set_property -dict [list \
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {12} \
  CONFIG.CLKOUT2_USED {true} \
  CONFIG.CLK_OUT2_PORT {refclk} \
  CONFIG.MMCM_CLKOUT1_DIVIDE {6} \
  CONFIG.MMCM_CLKIN1_PERIOD {20.000} \
  CONFIG.MMCM_DIVCLK_DIVIDE {1} \
  CONFIG.MMCM_CLKFBOUT_MULT_F {24.000} \
] [get_bd_cells axi_clkgen]


#==============================================================================
# Create HSI interface
#==============================================================================

# Create the HSI interface.
create_bd_cell -type ip -vlnv tudelft.nl:user:hsi:1.0 hsi
set_property -dict [list \
  CONFIG.C_M_AXI_TARGET_MEMORY_BASE {0x00000000} \
  CONFIG.C_M_AXI_TARGET_MEMORY_MASK {0x000FFFFF} \
  CONFIG.C_M_AXI_TARGET_PERIPH_BASE {0x00000000} \
  CONFIG.C_M_AXI_TARGET_PERIPH_MASK {0x000FFFFF} \
  CONFIG.CLK_BUF_TYPE {BUFG} \
  CONFIG.ENABLE_XCLK {false} \
] [get_bd_cells hsi]

# Connect interrupt input of HSI.
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 irq_concat
connect_bd_net [get_bd_pins irq_concat/dout] [get_bd_pins hsi/asic_irq]
set_property -dict [list CONFIG.NUM_PORTS {16}] [get_bd_cells irq_concat]
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 no_irq
set_property -dict [list \
  CONFIG.CONST_WIDTH {1} \
  CONFIG.CONST_VAL {0} \
] [get_bd_cells no_irq]
for {set i 0} {$i < 16} {incr i} {
  connect_bd_net [get_bd_pins no_irq/dout] [get_bd_pins irq_concat/In$i]
}

# Connect the external HSI ports.
make_bd_intf_pins_external [get_bd_intf_pins hsi/asic_hsi]
if {$emulate} {
  set_property name hsi_loop [get_bd_intf_ports asic_hsi_0]
} else {
  set_property name hsi_asic [get_bd_intf_ports asic_hsi_0]
}
make_bd_intf_pins_external [get_bd_intf_pins hsi/user_teensy_uart]
set_property name power    [get_bd_intf_ports user_teensy_uart_0]


#==============================================================================
# Create the memory for the r-VEX
#==============================================================================

# NOTE: of course the end goal is to, you know, use the DDR. Not to mention
# connecting it to peripherals. That's pretty much the primary function of this
# damn FPGA. But Vivado is giving me an insane amount of shit for even daring
# to consider this, so I gave up. Even the block memory is a pain in the ass
# because you can't change its size in any sane manner. Or any manner at all.
# I'm not sure. Godspeed.

# Create the memory.
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 memory
set_property -dict [ list \
  CONFIG.Memory_Type {True_Dual_Port_RAM} \
  CONFIG.Enable_32bit_Address {true} \
  CONFIG.Use_Byte_Write_Enable {true} \
  CONFIG.Byte_Size {8} \
  CONFIG.Write_Width_A {32} \
  CONFIG.Read_Width_A {32} \
  CONFIG.Write_Depth_A {32768} \
  CONFIG.Write_Width_B {32} \
  CONFIG.Read_Width_B {32} \
  CONFIG.Register_PortA_Output_of_Memory_Primitives {false} \
  CONFIG.Register_PortB_Output_of_Memory_Primitives {false} \
  CONFIG.Use_RSTA_Pin {false} \
  CONFIG.Use_RSTB_Pin {false} \
  CONFIG.use_bram_block {Stand_Alone} \
  CONFIG.EN_SAFETY_CKT {false} \
] [get_bd_cells memory]

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 memory_hsi
set_property -dict [list \
  CONFIG.SINGLE_PORT_BRAM {1} \
] [get_bd_cells memory_hsi]
connect_bd_intf_net [get_bd_intf_pins memory_hsi/BRAM_PORTA] [get_bd_intf_pins memory/BRAM_PORTA]

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 memory_debug
set_property -dict [list \
  CONFIG.SINGLE_PORT_BRAM {1} \
] [get_bd_cells memory_debug]
connect_bd_intf_net [get_bd_intf_pins memory_debug/BRAM_PORTA] [get_bd_intf_pins memory/BRAM_PORTB]


#==============================================================================
# Generate interconnect
#==============================================================================

# Connect the HSI memory bus.
create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 memory_hsi_icon
set_property -dict [list CONFIG.NUM_SI {1}] [get_bd_cells memory_hsi_icon]
connect_bd_intf_net [get_bd_intf_pins hsi/m_axi] [get_bd_intf_pins memory_hsi_icon/S00_AXI]
connect_bd_intf_net [get_bd_intf_pins memory_hsi_icon/M00_AXI] [get_bd_intf_pins memory_hsi/S_AXI]

connect_bd_net [get_bd_pins hsi/m_axi_aclk_out] [get_bd_pins hsi/m_axi_aclk]
connect_bd_net [get_bd_pins hsi/m_axi_aclk_out] [get_bd_pins memory_hsi_icon/aclk]
connect_bd_net [get_bd_pins hsi/m_axi_aclk_out] [get_bd_pins memory_hsi/s_axi_aclk]

connect_bd_net [get_bd_pins hsi/m_axi_aresetn_out] [get_bd_pins hsi/m_axi_aresetn]
connect_bd_net [get_bd_pins hsi/m_axi_aresetn_out] [get_bd_pins memory_hsi_icon/aresetn]
connect_bd_net [get_bd_pins hsi/m_axi_aresetn_out] [get_bd_pins memory_hsi/s_axi_aresetn]

# Connect the PS.
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { \
  Master      "/ps/M_AXI_GP0" \
  intc_ip     "New AXI Interconnect" \
  Clk_xbar    "/axi_clkgen/axi (100 MHz)" \
  Clk_master  "/axi_clkgen/axi (100 MHz)" \
  Clk_slave   "/axi_clkgen/axi (100 MHz)" \
} [get_bd_intf_pins memory_debug/S_AXI]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { \
  Master      "/ps/M_AXI_GP0" \
  intc_ip     "/axi_mem_intercon" \
  Clk_xbar    "Auto" \
  Clk_master  "Auto" \
  Clk_slave   "Auto" \
} [get_bd_intf_pins hsi/s_axi]

# Assign addresses.
assign_bd_address [get_bd_addr_segs {memory_hsi/S_AXI/Mem0 }]
set_property offset 0x00000000 [get_bd_addr_segs {hsi/m_axi/SEG_memory_hsi_Mem0}]

set_property range 64K [get_bd_addr_segs {hsi/m_axi/SEG_memory_hsi_Mem0}]
set_property offset 0x41000000 [get_bd_addr_segs {ps/Data/SEG_hsi_reg0}]

set_property range 64K [get_bd_addr_segs {ps/Data/SEG_memory_debug_Mem0}]
set_property offset 0x40000000 [get_bd_addr_segs {ps/Data/SEG_memory_debug_Mem0}]


#==============================================================================
# Add the ASIC emulator if requested
#==============================================================================

# Add the ASIC emulator.
if {$emulate} {
  create_bd_cell -type ip -vlnv tudelft.nl:user:asic_emulator:1.0 asic
  make_bd_intf_pins_external [get_bd_intf_pins asic/asic_hsi]
  set_property name hsi_emul [get_bd_intf_ports asic_hsi_0]
}


#==============================================================================
# Add ADC interface if requested
#==============================================================================

if {$ads8885} {
  
  create_bd_cell -type ip -vlnv tudelft.nl:user:ads8885:1.0 ads8885
  apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { \
    Master      "/ps/M_AXI_GP0" \
    intc_ip     "Auto" \
    Clk_xbar    "Auto" \
    Clk_master  "Auto" \
    Clk_slave   "Auto" \
  } [get_bd_intf_pins ads8885/s_axi]
  set_property offset 0x42000000 [get_bd_addr_segs {ps/Data/SEG_ads8885_reg0}]
  
  make_bd_intf_pins_external [get_bd_intf_pins ads8885/ads8885]
  set_property name adc [get_bd_intf_ports ads8885_0]
  
  create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 adc_dma
  set_property -dict [list \
    CONFIG.c_include_sg {0} \
    CONFIG.c_sg_include_stscntrl_strm {0} \
    CONFIG.c_include_mm2s {0} \
  ] [get_bd_cells adc_dma]
  connect_bd_intf_net [get_bd_intf_pins ads8885/m_axis] [get_bd_intf_pins adc_dma/S_AXIS_S2MM]
  connect_bd_net [get_bd_pins ads8885/m_axis_aclk] [get_bd_pins axi_clkgen/axi]
  connect_bd_net [get_bd_pins ads8885/m_axis_aresetn] [get_bd_pins axi_reset/peripheral_aresetn]
  connect_bd_net [get_bd_pins adc_dma/axi_resetn] [get_bd_pins axi_reset/peripheral_aresetn]
  connect_bd_net [get_bd_pins adc_dma/m_axi_s2mm_aclk] [get_bd_pins axi_clkgen/axi]
  connect_bd_net [get_bd_pins adc_dma/s_axi_lite_aclk] [get_bd_pins axi_clkgen/axi]
  
  apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { \
    Master      "/ps/M_AXI_GP0" \
    intc_ip     "Auto" \
    Clk_xbar    "Auto" \
    Clk_master  "Auto" \
    Clk_slave   "Auto" \
  } [get_bd_intf_pins adc_dma/S_AXI_LITE]
  set_property offset 0x42800000 [get_bd_addr_segs {ps/Data/SEG_adc_dma_Reg}]
  
  set_property -dict [list CONFIG.NUM_SI {2}] [get_bd_cells axi_mem_intercon]
  connect_bd_intf_net [get_bd_intf_pins adc_dma/M_AXI_S2MM] -boundary_type upper [get_bd_intf_pins axi_mem_intercon/S01_AXI]
  connect_bd_net [get_bd_pins axi_mem_intercon/S01_ACLK] [get_bd_pins axi_clkgen/axi]
  connect_bd_net [get_bd_pins axi_mem_intercon/S01_ARESETN] [get_bd_pins axi_reset/peripheral_aresetn]
  
  assign_bd_address [get_bd_addr_segs {memory_debug/S_AXI/Mem0 }]
  
}

#==============================================================================
# Add video loopback if requested
#==============================================================================

if {$video_loop} {
  
  # AXI GPIO for controlling the reset input of the HDMI sink PLL.
  create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 hdmi_ctrl
  apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {\
    Master      "/ps/M_AXI_GP0" \
    intc_ip     "Auto" \
    Clk_xbar    "Auto" \
    Clk_master  "Auto" \
    Clk_slave   "Auto" \
  } [get_bd_intf_pins hdmi_ctrl/S_AXI]
  set_property offset 0x43000000 [get_bd_addr_segs {ps/Data/SEG_hdmi_ctrl_Reg}]
  
  # Bit zero of GPIO handles reset of sink PLL (active-low, so set high to
  # enable source).
  create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 hdmi_ctrl_slice
  set_property -dict [list CONFIG.DIN_FROM {0} CONFIG.DIN_TO {0}] [get_bd_cells hdmi_ctrl_slice]
  connect_bd_net [get_bd_pins hdmi_ctrl/gpio_io_o] [get_bd_pins hdmi_ctrl_slice/Din]
  
  # Bit one of GPIO is connected to sink ~HPD.
  create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 hdmi_hpd_slice
  set_property -dict [list CONFIG.DIN_FROM {1} CONFIG.DIN_TO {1}] [get_bd_cells hdmi_hpd_slice]
  connect_bd_net [get_bd_pins hdmi_ctrl/gpio_io_o] [get_bd_pins hdmi_hpd_slice/Din]
  
  # Create sink.
  create_bd_cell -type ip -vlnv digilentinc.com:ip:dvi2rgb:1.7 dvi2rgb
  set_property -dict [list CONFIG.kRstActiveHigh {false}] [get_bd_cells dvi2rgb]
  connect_bd_net [get_bd_pins dvi2rgb/RefClk] [get_bd_pins axi_clkgen/refclk]
  connect_bd_net [get_bd_pins axi_reset/peripheral_aresetn] [get_bd_pins dvi2rgb/aRst_n]
  connect_bd_net [get_bd_pins hdmi_ctrl_slice/Dout] [get_bd_pins dvi2rgb/pRst_n]
  
  make_bd_intf_pins_external      [get_bd_intf_pins dvi2rgb/TMDS]
  set_property name hdmi_sink     [get_bd_intf_ports TMDS_0]
  
  # Sink control signals.
  create_bd_cell -type module -reference ddc_phy ddc_phy
  connect_bd_intf_net [get_bd_intf_pins dvi2rgb/DDC] [get_bd_intf_pins ddc_phy/ddc]
  connect_bd_net [get_bd_pins hdmi_hpd_slice/Dout] [get_bd_pins ddc_phy/hpd_gpio]
  make_bd_pins_external [get_bd_pins ddc_phy/scl]
  make_bd_pins_external [get_bd_pins ddc_phy/sda]
  make_bd_pins_external [get_bd_pins ddc_phy/hpd]
  set_property name hdmi_sink_scl [get_bd_ports scl_0]
  set_property name hdmi_sink_sda [get_bd_ports sda_0]
  set_property name hdmi_sink_hpd_n [get_bd_ports hpd_0]
  
  # Create source.
  create_bd_cell -type ip -vlnv digilentinc.com:ip:rgb2dvi:1.2 rgb2dvi
  set_property -dict [list CONFIG.kRstActiveHigh {false}] [get_bd_cells rgb2dvi]
  connect_bd_net [get_bd_pins dvi2rgb/PixelClk] [get_bd_pins rgb2dvi/PixelClk]
  connect_bd_net [get_bd_pins dvi2rgb/aPixelClkLckd] [get_bd_pins rgb2dvi/aRst_n]
  connect_bd_intf_net [get_bd_intf_pins dvi2rgb/RGB] [get_bd_intf_pins rgb2dvi/RGB]
  
  make_bd_intf_pins_external  [get_bd_intf_pins rgb2dvi/TMDS]
  set_property name hdmi_src  [get_bd_intf_ports TMDS_0]
  
}


#==============================================================================
# Finalize block diagram and project
#==============================================================================

# Regenerate layout so stuff isn't a mess.
regenerate_bd_layout

# Verify that everything is okay.
validate_bd_design

# Make the HDL wrapper for the block diagram.
make_wrapper -files [get_files *toplevel.bd] -top

# Add the wrapper file.
add_files -norecurse $prj_dir/$prj_name.srcs/sources_1/bd/toplevel/hdl/toplevel_wrapper.vhd

# Set the wrapper file to be the toplevel.
set_property top toplevel_wrapper [current_fileset]

# Update compile order, so the GUI doesn't have to do it again.
update_compile_order -fileset sources_1

# Generate the IP in the block diagram using global synthesis (there's so few
# objects with significant synthesis time that it's not worth doing
# checkpoints).
set_property synth_checkpoint_mode None [get_files *toplevel.bd]
generate_target all [get_files *toplevel.bd]
export_ip_user_files -of_objects [get_files *toplevel.bd] -no_script -sync -force -quiet
