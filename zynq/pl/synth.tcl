
open_project viv/pl.xpr

launch_runs synth_1
wait_on_run synth_1
if {[get_property PROGRESS [get_runs synth_1]] != "100%"} {
    error "ERROR: synthesis failed!"
}

launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1
if {[get_property PROGRESS [get_runs synth_1]] != "100%"} {
    error "ERROR: implementation failed!"
}

