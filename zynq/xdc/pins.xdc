
####################
# ASIC socket pins #
####################

set_property PACKAGE_PIN W6 [get_ports hsi_asic_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_clk_p]
set_property DIFF_TERM TRUE [get_ports hsi_asic_clk_p]
set_property OFFCHIP_TERM NONE [get_ports hsi_asic_clk_p]

set_property PACKAGE_PIN AB7 [get_ports hsi_asic_a2f_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_a2f_p[0]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_a2f_p[0]]

set_property PACKAGE_PIN AB2 [get_ports hsi_asic_a2f_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_a2f_p[1]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_a2f_p[1]]

set_property PACKAGE_PIN AA11 [get_ports hsi_asic_a2f_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_a2f_p[2]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_a2f_p[2]]

set_property PACKAGE_PIN AB10 [get_ports hsi_asic_a2f_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_a2f_p[3]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_a2f_p[3]]

set_property PACKAGE_PIN T4 [get_ports hsi_asic_f2a_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_f2a_p[0]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_f2a_p[0]]
set_property OFFCHIP_TERM NONE [get_ports hsi_asic_f2a_p[0]]

set_property PACKAGE_PIN AB5 [get_ports hsi_asic_f2a_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_f2a_p[1]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_f2a_p[1]]
set_property OFFCHIP_TERM NONE [get_ports hsi_asic_f2a_p[1]]

set_property PACKAGE_PIN AA12 [get_ports hsi_asic_f2a_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_f2a_p[2]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_f2a_p[2]]
set_property OFFCHIP_TERM NONE [get_ports hsi_asic_f2a_p[2]]

set_property PACKAGE_PIN AA9 [get_ports hsi_asic_f2a_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_asic_f2a_p[3]]
set_property DIFF_TERM TRUE [get_ports hsi_asic_f2a_p[3]]
set_property OFFCHIP_TERM NONE [get_ports hsi_asic_f2a_p[3]]

set_property PACKAGE_PIN AB16 [get_ports hsi_asic_aresetn_phy]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_asic_aresetn_phy]

set_property PACKAGE_PIN AA16 [get_ports hsi_asic_aresetn_core]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_asic_aresetn_core]

set_property PACKAGE_PIN AB17 [get_ports hsi_asic_backup_clk]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_asic_backup_clk]

set_property PACKAGE_PIN AA17 [get_ports hsi_asic_mode]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_asic_mode]

set_property PACKAGE_PIN AA18 [get_ports hsi_asic_ctrl_f2a]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_asic_ctrl_f2a]

set_property PACKAGE_PIN Y18 [get_ports hsi_asic_ctrl_a2f]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_asic_ctrl_a2f]


###############################
# HSI loopback FPGA-side pins #
###############################

# Note: the single-ended pins are shared with the ASIC socket pins. Basically,
# you can only use one of these at a time.

set_property PACKAGE_PIN V12 [get_ports hsi_loop_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_clk_p]
set_property DIFF_TERM TRUE [get_ports hsi_loop_clk_p]
set_property OFFCHIP_TERM NONE [get_ports hsi_loop_clk_p]

set_property PACKAGE_PIN Y4 [get_ports hsi_loop_a2f_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_a2f_p[0]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_a2f_p[0]]

set_property PACKAGE_PIN V7 [get_ports hsi_loop_a2f_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_a2f_p[1]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_a2f_p[1]]

set_property PACKAGE_PIN AA7 [get_ports hsi_loop_a2f_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_a2f_p[2]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_a2f_p[2]]

set_property PACKAGE_PIN U12 [get_ports hsi_loop_a2f_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_a2f_p[3]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_a2f_p[3]]

set_property PACKAGE_PIN R6 [get_ports hsi_loop_f2a_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_f2a_p[0]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_f2a_p[0]]
set_property OFFCHIP_TERM NONE [get_ports hsi_loop_f2a_p[0]]

set_property PACKAGE_PIN Y11 [get_ports hsi_loop_f2a_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_f2a_p[1]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_f2a_p[1]]
set_property OFFCHIP_TERM NONE [get_ports hsi_loop_f2a_p[1]]

set_property PACKAGE_PIN V10 [get_ports hsi_loop_f2a_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_f2a_p[2]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_f2a_p[2]]
set_property OFFCHIP_TERM NONE [get_ports hsi_loop_f2a_p[2]]

set_property PACKAGE_PIN W17 [get_ports hsi_loop_f2a_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_loop_f2a_p[3]]
set_property DIFF_TERM TRUE [get_ports hsi_loop_f2a_p[3]]
set_property OFFCHIP_TERM NONE [get_ports hsi_loop_f2a_p[3]]

set_property PACKAGE_PIN AB16 [get_ports hsi_loop_aresetn_phy]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_loop_aresetn_phy]

set_property PACKAGE_PIN AA16 [get_ports hsi_loop_aresetn_core]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_loop_aresetn_core]

set_property PACKAGE_PIN AB17 [get_ports hsi_loop_backup_clk]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_loop_backup_clk]

set_property PACKAGE_PIN AA17 [get_ports hsi_loop_mode]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_loop_mode]

set_property PACKAGE_PIN AA18 [get_ports hsi_loop_ctrl_f2a]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_loop_ctrl_f2a]

set_property PACKAGE_PIN Y18 [get_ports hsi_loop_ctrl_a2f]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_loop_ctrl_a2f]


###################################
# HSI loopback emulated ASIC pins #
###################################

set_property PACKAGE_PIN Y6 [get_ports hsi_emul_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_clk_p]
set_property DIFF_TERM TRUE [get_ports hsi_emul_clk_p]

set_property PACKAGE_PIN V5 [get_ports hsi_emul_a2f_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_a2f_p[0]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_a2f_p[0]]
set_property OFFCHIP_TERM NONE [get_ports hsi_emul_a2f_p[0]]

set_property PACKAGE_PIN V8 [get_ports hsi_emul_a2f_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_a2f_p[1]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_a2f_p[1]]
set_property OFFCHIP_TERM NONE [get_ports hsi_emul_a2f_p[1]]

set_property PACKAGE_PIN Y9 [get_ports hsi_emul_a2f_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_a2f_p[2]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_a2f_p[2]]
set_property OFFCHIP_TERM NONE [get_ports hsi_emul_a2f_p[2]]

set_property PACKAGE_PIN W16 [get_ports hsi_emul_a2f_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_a2f_p[3]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_a2f_p[3]]
set_property OFFCHIP_TERM NONE [get_ports hsi_emul_a2f_p[3]]

set_property PACKAGE_PIN U6 [get_ports hsi_emul_f2a_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_f2a_p[0]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_f2a_p[0]]

set_property PACKAGE_PIN W11 [get_ports hsi_emul_f2a_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_f2a_p[1]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_f2a_p[1]]

set_property PACKAGE_PIN U10 [get_ports hsi_emul_f2a_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_f2a_p[2]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_f2a_p[2]]

set_property PACKAGE_PIN W20 [get_ports hsi_emul_f2a_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports hsi_emul_f2a_p[3]]
set_property DIFF_TERM TRUE [get_ports hsi_emul_f2a_p[3]]

set_property PACKAGE_PIN AA22 [get_ports hsi_emul_aresetn_phy]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_emul_aresetn_phy]

set_property PACKAGE_PIN AB22 [get_ports hsi_emul_aresetn_core]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_emul_aresetn_core]

set_property PACKAGE_PIN Y19 [get_ports hsi_emul_backup_clk]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_emul_backup_clk]

set_property PACKAGE_PIN AA19 [get_ports hsi_emul_mode]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_emul_mode]

set_property PACKAGE_PIN AB21 [get_ports hsi_emul_ctrl_f2a]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_emul_ctrl_f2a]

set_property PACKAGE_PIN AA21 [get_ports hsi_emul_ctrl_a2f]
set_property IOSTANDARD LVCMOS25 [get_ports hsi_emul_ctrl_a2f]


#################################
# High-speed ADC (ADS8885) pins #
#################################

set_property PACKAGE_PIN A21 [get_ports adc_convst]
set_property IOSTANDARD LVCMOS33 [get_ports adc_convst]

set_property PACKAGE_PIN H22 [get_ports adc_sclk]
set_property IOSTANDARD LVCMOS33 [get_ports adc_sclk]

set_property PACKAGE_PIN G22 [get_ports adc_din]
set_property IOSTANDARD LVCMOS33 [get_ports adc_din]

set_property PACKAGE_PIN A22 [get_ports adc_dout]
set_property IOSTANDARD LVCMOS33 [get_ports adc_dout]


###########################
# Teensy interfacing pins #
###########################

set_property PACKAGE_PIN E15 [get_ports teensy_rx1]
set_property IOSTANDARD LVCMOS33 [get_ports teensy_rx1]

set_property PACKAGE_PIN E16 [get_ports teensy_cts1]
set_property IOSTANDARD LVCMOS33 [get_ports teensy_rts1]

set_property PACKAGE_PIN F16 [get_ports teensy_tx1]
set_property IOSTANDARD LVCMOS33 [get_ports teensy_tx1]

set_property PACKAGE_PIN G15 [get_ports teensy_rx2]
set_property IOSTANDARD LVCMOS33 [get_ports teensy_rx2]

set_property PACKAGE_PIN E18 [get_ports teensy_rts2]
set_property IOSTANDARD LVCMOS33 [get_ports teensy_cts2]

set_property PACKAGE_PIN F18 [get_ports teensy_tx2]
set_property IOSTANDARD LVCMOS33 [get_ports teensy_tx2]

set_property PACKAGE_PIN E19 [get_ports power_teensy_rx]
set_property IOSTANDARD LVCMOS33 [get_ports power_teensy_rx]

set_property PACKAGE_PIN G16 [get_ports power_teensy_tx]
set_property IOSTANDARD LVCMOS33 [get_ports power_teensy_tx]


#########################
# PWM audio output pins #
#########################

set_property PACKAGE_PIN D16 [get_ports audio_left]
set_property IOSTANDARD LVCMOS33 [get_ports audio_left]

set_property PACKAGE_PIN D17 [get_ports audio_right]
set_property IOSTANDARD LVCMOS33 [get_ports audio_right]


#########################################
# HDMI sink pins (compatible with PYNQ) #
#########################################

set_property PACKAGE_PIN C17 [get_ports {hdmi_sink_data_p[0]}]
set_property IOSTANDARD TMDS_33 [get_ports {hdmi_sink_data_p[0]}]

set_property PACKAGE_PIN D20 [get_ports {hdmi_sink_data_p[1]}]
set_property IOSTANDARD TMDS_33 [get_ports {hdmi_sink_data_p[1]}]

set_property PACKAGE_PIN F21 [get_ports {hdmi_sink_data_p[2]}]
set_property IOSTANDARD TMDS_33 [get_ports {hdmi_sink_data_p[2]}]

set_property PACKAGE_PIN B19 [get_ports hdmi_sink_clk_p]
set_property IOSTANDARD TMDS_33 [get_ports hdmi_sink_clk_p]

set_property PACKAGE_PIN C19 [get_ports hdmi_sink_hpd_n]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_sink_hpd_n]

set_property PACKAGE_PIN D18 [get_ports hdmi_sink_cec]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_sink_cec]

set_property PACKAGE_PIN G19 [get_ports hdmi_sink_sda]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_sink_sda]

set_property PACKAGE_PIN F19 [get_ports hdmi_sink_scl]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_sink_scl]


###########################################
# HDMI source pins (compatible with PYNQ) #
###########################################

set_property PACKAGE_PIN A18 [get_ports {hdmi_src_data_p[0]}]
set_property IOSTANDARD TMDS_33 [get_ports {hdmi_src_data_p[0]}]

set_property PACKAGE_PIN A16 [get_ports {hdmi_src_data_p[1]}]
set_property IOSTANDARD TMDS_33 [get_ports {hdmi_src_data_p[1]}]

set_property PACKAGE_PIN B21 [get_ports {hdmi_src_data_p[2]}]
set_property IOSTANDARD TMDS_33 [get_ports {hdmi_src_data_p[2]}]

set_property PACKAGE_PIN D22 [get_ports hdmi_src_clk_p]
set_property IOSTANDARD TMDS_33 [get_ports hdmi_src_clk_p]

set_property PACKAGE_PIN B15 [get_ports hdmi_src_hpd_n]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_src_hpd_n]

set_property PACKAGE_PIN C15 [get_ports hdmi_src_cec]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_src_cec]

set_property PACKAGE_PIN G20 [get_ports hdmi_src_sda]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_src_sda]

set_property PACKAGE_PIN G21 [get_ports hdmi_src_scl]
set_property IOSTANDARD LVCMOS33 [get_ports hdmi_src_scl]


#############
# PMOD pins #
#############

# Note: you can use jumper A on the TE0703 to select between 3.3V and 1.8V
# VCCIO for the PMOD connector. You could also supply your own voltage to it
# (within spec of the FPGA of course) by removing the jumper.

set_property PACKAGE_PIN T16 [get_ports pmod_1p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_1p]

set_property PACKAGE_PIN T17 [get_ports pmod_1n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_1n]

set_property PACKAGE_PIN R20 [get_ports pmod_2p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_2p]

set_property PACKAGE_PIN R21 [get_ports pmod_2n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_2n]

set_property PACKAGE_PIN M19 [get_ports pmod_3p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_3p]

set_property PACKAGE_PIN M20 [get_ports pmod_3n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_3n]

set_property PACKAGE_PIN M21 [get_ports pmod_4p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_4p]

set_property PACKAGE_PIN M22 [get_ports pmod_4n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_4n]

set_property PACKAGE_PIN P20 [get_ports pmod_5p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_5p]

set_property PACKAGE_PIN P21 [get_ports pmod_5n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_5n]

set_property PACKAGE_PIN L21 [get_ports pmod_6p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_6p]

set_property PACKAGE_PIN L22 [get_ports pmod_6n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_6n]

set_property PACKAGE_PIN J15 [get_ports pmod_7p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_7p]

set_property PACKAGE_PIN K15 [get_ports pmod_7n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_7n]

set_property PACKAGE_PIN P17 [get_ports pmod_8p]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_8p]

set_property PACKAGE_PIN P18 [get_ports pmod_8n]
set_property IOSTANDARD LVCMOS33 [get_ports pmod_8n]

