
FPGA resources
==============

This directory contains the stuff needed for the FPGA part of the board. The
`pl` directory is probably what you're looking for if you're reading this; it
contains the Vivado project (generator) for the programmable logic. The `xdc`
and `ip` directories are used by this project for the pin constraints and the
IP blocks respectively.

I suggest pretending like the Linux directory does not exist. Hell, I suggest
pretending like the ARM processing system doesn't exist.

