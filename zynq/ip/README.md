
Vivado IP repository files
==========================

This directory contains the Vivado IP repository containing the r-VEX IP blocks
(such as the HSI). These blocks must first be generated; run `make`. This
recurses into the various subdirectories of the `prj` folder, which contains the
TCL scripts to generate the IP packaging projects and to do the actual
packaging. You can customize the IP blocks and run their testbenches in modelsim
using the Makefiles in those subdirectories.

The actual IP repository is generated in the `repo` subdir, so that's what you
should be including in the Vivado PL projects.

The `src` directory contains the source files for the various IP blocks, as well
as simulation-only files that are only used by the IP block testbenches. This
includes the ASIC RTL files, with only minor modifications made to them to make
them work in simulations. ASIC errata are NOT fixed in these files, but the
problems are marked with comments.

The IP blocks are:

 - *hsi:* the interface for the ASIC. Also communicates with Teensy RX/TX2 to
   access the power management registers. The register map/description can be
   found at the top of `src/hsi/ip_hsi.vhd`.
 - *ads8885:* interfaces with the high-speed ADC. It can be accessed using an
   AXI-lite slave, and/or it can output a stream of the ADC samples. I never
   tested the latter, but it should be easy to get it up and running. The
   register map/description can be found at the top of
   `src/ads8885/ip_ads8885.vhd`.
 - *asic_emulator:* emulates the ASIC using the loopback HSI circuitry on the
   board. There are a number of limitations to this, though. Most importantly,
   it's actually a 4-issue reconfigurable core instead of the full 8-issue
   design, to make it fit on the Zynq comfortably. The ASIC errata are not
   modelled in this design, and in fact it uses VHDL from a newer version of
   the core. The scan chains are not modelled; attempting to scan will give
   undefined link behavior (because the registers are not clock-gated by the
   scan enable signal). Finally, it won't clock at 100 MHz. I know 25 MHz
   works. You need to constrain it manually in your toplevel.
 - *dvi2rgb:* taken from the PYNQ IP repository. This is the physical interface
   for using the HDMI sink (input).
 - *rgb2dvi:* taken from the PYNQ IP repository. This is the physical interface
   for using the HDMI source (output).

Missing:

 - Some UART-like output for the core. Easiest would be to route it through the
   Teensy, but it's not coded into the Arduino sketch right now, either.
 - Access to the keyboard/mouse USB ports. This would probably amount to a UART
   as well, just programmed differently in the Teensy. There is some code for
   controlling the USB ports in the Teensy already which I used to test the PCB,
   but it doesn't do much right now.
 - Audio output. This would literally just amount to PWM, so it shouldn't be too
   difficult to add. Note that I haven't tested the board though, so there could
   theoretically be soldering/circuit mistakes in there.
