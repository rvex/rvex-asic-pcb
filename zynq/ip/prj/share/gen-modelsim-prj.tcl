
onerror { quit -f -code 1 }

# Compile files based on ../hdl-list.
set a [open "../hdl-list" r]
while {[gets $a line]>=0} {
  set fields [split $line " "]
  lassign $fields mode libname fname
  if {[file isdirectory $libname] == 0} then {
    vlib -type directory $libname
  }
  vcom -quiet -93 -work $libname ../$fname
}

quit -f -code 0
