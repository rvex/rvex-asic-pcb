
# Auto-infer the IP core so we can open and modify it.
ipx::infer_core -vendor tudelft.nl -library user -taxonomy {/Debug_&_Verification/Debug} ../../repo/asic_emulator_v1_0
ipx::edit_ip_in_project -upgrade true -name asic_emulator -directory viv ../../repo/asic_emulator_v1_0/component.xml

# Open the IP core definition.
ipx::open_ipxact_file ../../repo/asic_emulator_v1_0/component.xml

# Add the repository to the path so we can set the interfaces properly.
set_property ip_repo_paths ../../repo [current_project]
update_ip_catalog

# Set description.
set_property description {r-VEX ASIC high-speed interface} [ipx::current_core]

# Infer the HSI interface.
ipx::infer_bus_interfaces tudelft:user:asic_hsi_rtl:1.0 [ipx::current_core]

# Add some information to the customization GUI.
ipgui::add_static_text -name {info} -component [ipx::current_core] -parent [ipgui::get_pagespec -name "Page 0" -component [ipx::current_core] ] -text {This unit emulates the ASIC in a way that it fits on the Zynq fabric. The following limitations apply:<ul><li>The core is 4-way 2-context instead of 8-way 4-context. So you need to use generic binaries.</li><li>The HSI does not do delay calibration and doesn't use the HSI clock period register information.</li><li>The HSI port must be connected to physical pins on the FPGA, since IO primitives are used.</li><li>It's obviously not going to clock as high as the ASIC.</li><li>Scan chains are not emulated. Attempts to scan will crash the interface (because scan enable doesn't<br>effectively clock gate the registers), and chains will always read as zero.</li></ul></p>}

# Save and update stuff.
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
update_ip_catalog -rebuild -repo_path ../../repo

