
# Auto-infer the IP core so we can open and modify it.
ipx::infer_core -vendor tudelft.nl -library user -name ads8885 -taxonomy {/Communication_&_Networking/Serial_Interfaces} ../../repo/ads8885_v1_0
ipx::edit_ip_in_project -upgrade true -name ads8885 -directory viv ../../repo/ads8885_v1_0/component.xml

# Open the IP core definition.
ipx::open_ipxact_file ../../repo/ads8885_v1_0/component.xml

# Add the repository to the path so we can set the interfaces properly.
set_property ip_repo_paths ../../repo [current_project]
update_ip_catalog

# Add constraint files.
add_files -fileset constrs_1 -norecurse ../../repo/ads8885_v1_0/ip_ads8885_ooc.xdc
set fil [get_files ../../repo/ads8885_v1_0/ip_ads8885_ooc.xdc]
set_property USED_IN {synthesis implementation} $fil

ipx::merge_project_changes files [ipx::current_core]
ipx::reorder_files ip_ads8885_ooc.xdc [ipx::get_file_groups xilinx_anylanguagesynthesis -of_objects [ipx::current_core]]
set_property USED_IN {synthesis implementation out_of_context} [ipx::get_files ip_ads8885_ooc.xdc -of_objects [ipx::get_file_groups xilinx_anylanguagesynthesis -of_objects [ipx::current_core]]]

# Set description.
set_property description {ADS8885 18-bit ADC interface.} [ipx::current_core]

# Infer the ADS SPI port interface.
ipx::infer_bus_interfaces tudelft.nl:user:ads8885_rtl:1.0 [ipx::current_core]

# Enable the trigger input signal only when the user wants at least one trigger.
set_property enablement_dependency {$trigger_width > 0} [ipx::get_ports triggers -of_objects [ipx::current_core]]
set_property driver_value 0 [ipx::get_ports triggers -of_objects [ipx::current_core]]

# Add some information to the customization GUI.
ipgui::add_static_text -name {info} -component [ipx::current_core] -parent [ipgui::get_pagespec -name "Page 0" -component [ipx::current_core] ] -text {This unit provides stream and AXI-slave readout of the ADS8885 ADC on the ASIC board.<br><br>Triggers can be used to automatically enable/disable the stream. Only one can be selected<br>at a time, but you can connect 32 signals to choose from.}
ipgui::move_text -component [ipx::current_core] -order 0 [ipgui::get_textspec -name "info" -component [ipx::current_core]] -parent [ipgui::get_pagespec -name "Page 0" -component [ipx::current_core]]
set_property display_name {Trigger count} [ipgui::get_guiparamspec -name "TRIGGER_WIDTH" -component [ipx::current_core] ]
set_property tooltip {Number of trigger input signals} [ipgui::get_guiparamspec -name "TRIGGER_WIDTH" -component [ipx::current_core] ]

# Save and update stuff.
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
update_ip_catalog -rebuild -repo_path ../../repo

