
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity ip_ads8885_iface_tb is
end ip_ads8885_iface_tb;

architecture behavioral of ip_ads8885_iface_tb is
  
  signal clk        : std_logic;
  signal reset      : std_logic;
  signal pad_din    : std_logic;
  signal pad_convst : std_logic;
  signal pad_sclk   : std_logic;
  signal pad_dout   : std_logic;
  signal osa_amount : std_logic_vector(7 downto 0);
  signal data       : std_logic_vector(25 downto 0);
  signal strobe     : std_logic;
  
  signal ads_data   : std_logic_vector(17 downto 0) := "111111111111110000";
  signal ads_sr     : std_logic_vector(17 downto 0);
  
begin
  
  uut: entity work.ip_ads8885_iface
    port map (
      clk         => clk,
      reset       => reset,
      pad_din     => pad_din,
      pad_convst  => pad_convst,
      pad_sclk    => pad_sclk,
      pad_dout    => pad_dout,
      osa_amount  => osa_amount,
      data        => data,
      strobe      => strobe
    );
  
  clk_process: process is
  begin
    clk <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
  end process;
  
  stim_proc: process is
  begin
    osa_amount <= X"03";
    reset <= '1';
    wait for 100 ns;
    wait until rising_edge(clk);
    reset <= '0';
    wait;
  end process;
  
  ads8885_proc: process (pad_convst, pad_sclk) is
  begin
    if rising_edge(pad_convst) then
      pad_dout <= 'Z';
      ads_data <= std_logic_vector(unsigned(ads_data) + 1);
      ads_sr <= ads_data;
    end if;
    if falling_edge(pad_convst) then
      pad_dout <= ads_sr(17);
      ads_sr <= ads_sr(16 downto 0) & "Z";
    end if;
    if pad_convst = '0' and falling_edge(pad_sclk) then
      pad_dout <= ads_sr(17);
      ads_sr <= ads_sr(16 downto 0) & "Z";
    end if;
  end process;
  
end behavioral;
