
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

library hsi;
use hsi.ip_hsi_simUtils_pkg.all;

library asic;
library aximem;

entity testbench is
end testbench;

architecture behavioral of testbench is
  
  -- ASIC memory remapping. The lower address space of the r-VEX is mapped
  -- into C_M_AXI_TARGET_MEMORY using write-back read- and write-allocate
  -- cache mode. The upper address space is mapped into C_M_AXI_TARGET_PERIPH
  -- using device bufferable cache mode. Out-of-range accesses return bus
  -- fault 1. AXI response codes 2 and 3 return bus fault 2 and 3
  -- respectively.
  constant C_M_AXI_TARGET_MEMORY_BASE  : std_logic_vector := X"20000000";
  constant C_M_AXI_TARGET_MEMORY_MASK  : std_logic_vector := X"0FFFFFFF";
  constant C_M_AXI_TARGET_PERIPH_BASE  : std_logic_vector := X"80000000";
  constant C_M_AXI_TARGET_PERIPH_MASK  : std_logic_vector := X"7FFFFFFF";
  
  -- "BUFG" to use global clock buffers, "BUFR" to use regional clock buffers.
  constant CLK_BUF_TYPE         : string := "BUFR";
  
  -- Whether the HSI LVDS data pairs should be inverted. This is necessary for
  -- communicating with the ASIC socket on the current PCB revision because it
  -- was significantly easier to route that way.
  constant INVERT_HSI_PAIRS     : boolean := true;
  
  -- How HSI delay calibration is done.
  --  - NONE: delay calibration procedure is skipped entirely.
  --  - ASIC: the calibration waveform is generated, but the FPGA doesn't do
  --          anything other than waiting for the lock handshake from the
  --          ASIC.
  --  - BOTH: delay calibration is done on both ASIC and FPGA.
  --  - NO_IDELAYCTRL: same as BOTH, but the IDELAYCTRL primitive is not
  --          instantiated here (and must be instantiated elsewhere).
  constant DELAY_CAL_MODE       : string := "ASIC";
  
  -- Whether or not a clock domain crossing should be inserted in the memory
  -- access path of the ASIC. If this is disabled, the latency/bandwidth is
  -- potentially improved, but m_axi_aclk MUST then use m_axi_aclk_out and
  -- m_axi_aresetn_out. It is recommended to do this if you're putting an AXI
  -- L2 cache in the path, so the CDC to the PS clock domains can be behind
  -- the cache.
  -- This must be true in the testbench unless you manually rewire the clock,
  -- since a conditional clock assignment would introduce a delta delay that
  -- might mess things up.
  constant ENABLE_XCLK          : boolean := true;
  
  -- Length of a scan chain.
  constant SCAN_LENGTH          : natural range 1 to 2048 := 2016;
  
  -- Amount to divide the core clock by when scanning.
  constant SCAN_CLK_DIV         : natural range 1 to 256 := 10;
  
  -- AXI slave interface. Must run at 100MHz! Clock is used as a reference
  -- for the MMCM and to generate the UART baudrate.
  signal s_axi_aclk             :  std_logic;
  signal s_axi_aresetn          :  std_logic;
                                
  signal s_axi_awaddr           :  std_logic_vector(14 downto 0);
  signal s_axi_awvalid          :  std_logic;
  signal s_axi_awready          :  std_logic;
                                
  signal s_axi_wdata            :  std_logic_vector(31 downto 0);
  signal s_axi_wstrb            :  std_logic_vector(3 downto 0);
  signal s_axi_wvalid           :  std_logic;
  signal s_axi_wready           :  std_logic;
                                
  signal s_axi_bresp            :  std_logic_vector(1 downto 0);
  signal s_axi_bvalid           :  std_logic;
  signal s_axi_bready           :  std_logic;
                                
  signal s_axi_araddr           :  std_logic_vector(14 downto 0);
  signal s_axi_arvalid          :  std_logic;
  signal s_axi_arready          :  std_logic;
                                
  signal s_axi_rdata            :  std_logic_vector(31 downto 0);
  signal s_axi_rresp            :  std_logic_vector(1 downto 0);
  signal s_axi_rvalid           :  std_logic;
  signal s_axi_rready           :  std_logic;
  
  -- AXI master interface.
  signal m_axi_aclk             :  std_logic;
  signal m_axi_aresetn          :  std_logic;
  
  signal m_axi_awaddr           :  std_logic_vector(31 downto 0);
  signal m_axi_awsize           :  std_logic_vector(2 downto 0);
  signal m_axi_awcache          :  std_logic_vector(3 downto 0);
  signal m_axi_awvalid          :  std_logic;
  signal m_axi_awready          :  std_logic;
  
  signal m_axi_wdata            :  std_logic_vector(31 downto 0);
  signal m_axi_wstrb            :  std_logic_vector(3 downto 0);
  signal m_axi_wvalid           :  std_logic;
  signal m_axi_wready           :  std_logic;
  
  signal m_axi_bresp            :  std_logic_vector(1 downto 0);
  signal m_axi_bvalid           :  std_logic;
  signal m_axi_bready           :  std_logic;
  
  signal m_axi_araddr           :  std_logic_vector(31 downto 0);
  signal m_axi_arsize           :  std_logic_vector(2 downto 0);
  signal m_axi_arcache          :  std_logic_vector(3 downto 0);
  signal m_axi_arvalid          :  std_logic;
  signal m_axi_arready          :  std_logic;
  
  signal m_axi_rdata            :  std_logic_vector(31 downto 0);
  signal m_axi_rresp            :  std_logic_vector(1 downto 0);
  signal m_axi_rvalid           :  std_logic;
  signal m_axi_rready           :  std_logic;
  
  -- HSI interface.
  signal asic_hsi_aresetn_phy   :  std_logic;
  signal asic_hsi_aresetn_core  :  std_logic;
  signal asic_hsi_backup_clk    :  std_logic;
  signal asic_hsi_clk_p         :  std_logic;
  signal asic_hsi_clk_n         :  std_logic;
  signal asic_hsi_f2a_p         :  std_logic_vector(3 downto 0);
  signal asic_hsi_f2a_n         :  std_logic_vector(3 downto 0);
  signal asic_hsi_a2f_p         :  std_logic_vector(3 downto 0);
  signal asic_hsi_a2f_n         :  std_logic_vector(3 downto 0);
  signal asic_hsi_mode          :  std_logic;
  signal asic_hsi_ctrl_f2a      :  std_logic;
  signal asic_hsi_ctrl_a2f      :  std_logic;
  
  -- Teensy power register interface.
  signal teensy_tx              :  std_logic;
  signal teensy_rx              :  std_logic;
  
  -- Interrupt requests for the ASIC.
  signal asic_irq               :  std_logic_vector(15 downto 0);
  
  -- Testbench signals.
  signal reset                  : std_logic;
  signal clk_enable             : boolean := true;
  
begin
  
  clk_proc: process is
  begin
    wait for 5 ns;
    s_axi_aclk <= '0';
    wait for 5 ns;
    s_axi_aclk <= '1';
    m_axi_aclk <= '0';
    wait for 5 ns;
    s_axi_aclk <= '0';
    wait for 5 ns;
    s_axi_aclk <= '1';
    m_axi_aclk <= '1';
    if not clk_enable then
      wait;
    end if;
  end process;
  
  reset_proc: process is
  begin
    s_axi_aresetn <= '0';
    m_axi_aresetn <= '0';
    reset <= '1';
    wait for 1 us;
    s_axi_aresetn <= '1';
    m_axi_aresetn <= '1';
    reset <= '0';
    wait until not clk_enable;
    s_axi_aresetn <= '0';
    m_axi_aresetn <= '0';
    reset <= '1';
    wait;
  end process;
  
  uut: entity hsi.ip_hsi
    generic map (
      C_M_AXI_TARGET_MEMORY_BASE=> C_M_AXI_TARGET_MEMORY_BASE,
      C_M_AXI_TARGET_MEMORY_MASK=> C_M_AXI_TARGET_MEMORY_MASK,
      C_M_AXI_TARGET_PERIPH_BASE=> C_M_AXI_TARGET_PERIPH_BASE,
      C_M_AXI_TARGET_PERIPH_MASK=> C_M_AXI_TARGET_PERIPH_MASK,
      CLK_BUF_TYPE              => CLK_BUF_TYPE,
      INVERT_HSI_PAIRS          => INVERT_HSI_PAIRS,
      DELAY_CAL_MODE            => DELAY_CAL_MODE,
      ENABLE_XCLK               => ENABLE_XCLK,
      SCAN_LENGTH               => SCAN_LENGTH,
      SCAN_CLK_DIV              => SCAN_CLK_DIV
    )
    port map (
      s_axi_aclk                => s_axi_aclk,
      s_axi_aresetn             => s_axi_aresetn,
      s_axi_awaddr              => s_axi_awaddr,
      s_axi_awvalid             => s_axi_awvalid,
      s_axi_awready             => s_axi_awready,
      s_axi_wdata               => s_axi_wdata,
      s_axi_wstrb               => s_axi_wstrb,
      s_axi_wvalid              => s_axi_wvalid,
      s_axi_wready              => s_axi_wready,
      s_axi_bresp               => s_axi_bresp,
      s_axi_bvalid              => s_axi_bvalid,
      s_axi_bready              => s_axi_bready,
      s_axi_araddr              => s_axi_araddr,
      s_axi_arvalid             => s_axi_arvalid,
      s_axi_arready             => s_axi_arready,
      s_axi_rdata               => s_axi_rdata,
      s_axi_rresp               => s_axi_rresp,
      s_axi_rvalid              => s_axi_rvalid,
      s_axi_rready              => s_axi_rready,
      m_axi_aclk                => m_axi_aclk,
      m_axi_aresetn             => m_axi_aresetn,
      m_axi_awaddr              => m_axi_awaddr,
      m_axi_awsize              => m_axi_awsize,
      m_axi_awcache             => m_axi_awcache,
      m_axi_awvalid             => m_axi_awvalid,
      m_axi_awready             => m_axi_awready,
      m_axi_wdata               => m_axi_wdata,
      m_axi_wstrb               => m_axi_wstrb,
      m_axi_wvalid              => m_axi_wvalid,
      m_axi_wready              => m_axi_wready,
      m_axi_bresp               => m_axi_bresp,
      m_axi_bvalid              => m_axi_bvalid,
      m_axi_bready              => m_axi_bready,
      m_axi_araddr              => m_axi_araddr,
      m_axi_arsize              => m_axi_arsize,
      m_axi_arcache             => m_axi_arcache,
      m_axi_arvalid             => m_axi_arvalid,
      m_axi_arready             => m_axi_arready,
      m_axi_rdata               => m_axi_rdata,
      m_axi_rresp               => m_axi_rresp,
      m_axi_rvalid              => m_axi_rvalid,
      m_axi_rready              => m_axi_rready,
      asic_hsi_aresetn_phy      => asic_hsi_aresetn_phy,
      asic_hsi_aresetn_core     => asic_hsi_aresetn_core,
      asic_hsi_backup_clk       => asic_hsi_backup_clk,
      asic_hsi_clk_p            => asic_hsi_clk_p,
      asic_hsi_clk_n            => asic_hsi_clk_n,
      asic_hsi_f2a_p            => asic_hsi_f2a_p,
      asic_hsi_f2a_n            => asic_hsi_f2a_n,
      asic_hsi_a2f_p            => asic_hsi_a2f_p,
      asic_hsi_a2f_n            => asic_hsi_a2f_n,
      asic_hsi_mode             => asic_hsi_mode,
      asic_hsi_ctrl_f2a         => asic_hsi_ctrl_f2a,
      asic_hsi_ctrl_a2f         => asic_hsi_ctrl_a2f,
      teensy_tx                 => teensy_tx,
      teensy_rx                 => teensy_rx,
      asic_irq                  => asic_irq
    );
  
  asic_inst: entity asic.rvsys_asic
    generic map (
      CACHE_IMPL                => 0,
      N_RX                      => 4,
      N_TX                      => 4,
      N_CK                      => 1
    )
    port map (
      pad_aresetn_core          => asic_hsi_aresetn_core,
      pad_aresetn_phy           => asic_hsi_aresetn_phy,
      pad_backup_clk            => asic_hsi_backup_clk,
      pad_Iref_tx               => (others => 'Z'),
      pad_Iref_rx               => open,
      pad_hsi_clk_p(0)          => asic_hsi_clk_p,
      pad_hsi_clk_n(0)          => asic_hsi_clk_n,
      pad_f2a_p                 => asic_hsi_f2a_n,
      pad_f2a_n                 => asic_hsi_f2a_p,
      pad_a2f_p                 => asic_hsi_a2f_n,
      pad_a2f_n                 => asic_hsi_a2f_p,
      pad_mode                  => asic_hsi_mode,
      pad_ctrl_f2a              => asic_hsi_ctrl_f2a,
      pad_ctrl_a2f              => asic_hsi_ctrl_a2f
    );
  
  teensy_block: block is
    
    -- Receive data.
    signal rx_data    : std_logic_vector(7 downto 0);
    signal rx_error   : std_logic;
    signal rx_strobe  : std_logic;
    
    -- Transmit data.
    signal tx_data    : std_logic_vector(7 downto 0);
    signal tx_strobe  : std_logic;
    signal tx_busy    : std_logic;
    
    -- Expected RX byte (don't care mask).
    signal rx_expect  : std_logic_vector(7 downto 0);
    
    -- Address and data registers for the RX logic.
    signal addr       : std_logic_vector(10 downto 0);
    signal data       : std_logic_vector(15 downto 0);
    
    -- Commands to the TX FSM.
    signal fsm_write  : std_logic;
    signal fsm_read   : std_logic;
    signal fsm_addr   : std_logic_vector(10 downto 0);
    signal fsm_data   : std_logic_vector(15 downto 0);
    
  begin
    
    uart_inst: entity hsi.ip_hsi_utils_uart
      generic map (
        F_CLK           => 100000000.0,
        F_BAUD          =>    115200.0
      )
      port map (
        reset           => reset,
        clk             => s_axi_aclk,
        clkEn           => '1',
        rx              => teensy_tx,
        tx              => teensy_rx,
        rx_data         => rx_data,
        rx_frameError   => rx_error,
        rx_strobe       => rx_strobe,
        rx_charTimeout  => open,
        tx_data         => tx_data,
        tx_strobe       => tx_strobe,
        tx_busy         => tx_busy
      );
    
    teensy_rx_proc: process (s_axi_aclk) is
      variable addr_v : std_logic_vector(10 downto 0);
      variable data_v : std_logic_vector(15 downto 0);
    begin
      if rising_edge(s_axi_aclk) then
        
        fsm_read <= '0';
        fsm_write <= '0';
        
        addr_v := addr;
        data_v := data;
        
        if rx_strobe = '1' then
          if rx_error = '1' then
            report "Teensy RX frame error" severity WARNING;
          end if;
          if is_X(rx_data) then
            report "Teensy RX undefined data" severity WARNING;
          else
            if not std_match(rx_data, rx_expect) then
              report "Teensy RX unexpected data" severity WARNING;
            end if;
              
            if rx_data(7) = '0' then
              if rx_data(6) = '0' then
                addr_v(10 downto 5) := rx_data(5 downto 0);
                rx_expect <= "01------";
              else
                addr_v(4 downto 0) := rx_data(4 downto 0);
                if rx_data(5) = '0' then
                  rx_expect <= "100-----";
                else
                  fsm_read <= '1';
                  fsm_addr <= addr_v;
                  fsm_data <= X"5432";
                  report "Teensy read reg " & rvs_hex(addr_v) severity NOTE;
                  addr_v := (others => 'U');
                  data_v := (others => 'U');
                  rx_expect <= "00------";
                end if;
              end if;
            else
              if rx_data(6) = '0' then
                if rx_data(5) = '0' then
                  data_v(15 downto 11) := rx_data(4 downto 0);
                  rx_expect <= "101-----";
                else
                  data_v(10 downto 6) := rx_data(4 downto 0);
                  rx_expect <= "11------";
                end if;
              else
                data_v(5 downto 0) := rx_data(5 downto 0);
                fsm_write <= '1';
                fsm_addr  <= addr_v;
                fsm_data  <= data_v;
                report "Teensy write reg " & rvs_hex(addr_v) & " -> " & rvs_hex(data_v) severity NOTE;
                addr_v := (others => 'U');
                data_v := (others => 'U');
                rx_expect <= "00------";
              end if;
            end if;
          end if;
          
        end if;
        
        addr <= addr_v;
        data <= data_v;
        
        if reset = '1' then
          addr <= (others => 'U');
          data <= (others => 'U');
          rx_expect <= "00------";
        end if;
        
      end if;
    end process;
    
    teensy_tx_proc: process is
    begin
      while True loop
        wait until rising_edge(s_axi_aclk) and (fsm_write = '1' or fsm_read = '1');
        if fsm_write = '1' then
          
          -- Send ack.
          wait until rising_edge(s_axi_aclk) and tx_busy = '0';
          tx_data <= "01010101";
          tx_strobe <= '1';
          wait until rising_edge(s_axi_aclk);
          tx_strobe <= '0';
          
        else
          
          -- Send data high.
          wait until rising_edge(s_axi_aclk) and tx_busy = '0';
          tx_data <= "100" & fsm_data(15 downto 11);
          tx_strobe <= '1';
          wait until rising_edge(s_axi_aclk);
          tx_strobe <= '0';
          
          -- Send data middle.
          wait until rising_edge(s_axi_aclk) and tx_busy = '0';
          tx_data <= "101" & fsm_data(10 downto 6);
          tx_strobe <= '1';
          wait until rising_edge(s_axi_aclk);
          tx_strobe <= '0';
          
          -- Send data low.
          wait until rising_edge(s_axi_aclk) and tx_busy = '0';
          tx_data <= "11" & fsm_data(5 downto 0);
          tx_strobe <= '1';
          wait until rising_edge(s_axi_aclk);
          tx_strobe <= '0';
          
        end if;
      end loop;
    end process;
    
  end block;
  
  rvex_mst_inst: entity aximem.AxiSlaveMock
    generic map (
      DATA_WIDTH  => 32,
      ADDR_WIDTH  => 32,
      LEN_WIDTH   => 8,
      ID_WIDTH    => 1,
      SREC_FILE   => "../test-progs/mem.srec",
      SREC_OFFSET => X"00000000" & C_M_AXI_TARGET_MEMORY_BASE
    )
    port map (
      clk         => m_axi_aclk,
      reset       => reset,
      
      awid        => "0",
      awaddr      => m_axi_awaddr,
      awlen       => X"00",
      awsize      => m_axi_awsize,
      awburst     => "01",
      awvalid     => m_axi_awvalid,
      awready     => m_axi_awready,
      
      wdata       => m_axi_wdata,
      wstrb       => m_axi_wstrb,
      wlast       => '1',
      wvalid      => m_axi_wvalid,
      wready      => m_axi_wready,
      
      bid         => open,
      bresp       => m_axi_bresp,
      bvalid      => m_axi_bvalid,
      bready      => m_axi_bready,
      
      arid        => "0",
      araddr      => m_axi_araddr,
      arlen       => X"00",
      arsize      => m_axi_arsize,
      arburst     => "01",
      arvalid     => m_axi_arvalid,
      arready     => m_axi_arready,
      
      rid         => open,
      rdata       => m_axi_rdata,
      rresp       => m_axi_rresp,
      rlast       => open,
      rvalid      => m_axi_rvalid,
      rready      => m_axi_rready
    );
  
  axi_cmd_gen: process is
    
    procedure handshake(
      signal ours   : out std_logic;
      signal theirs : in std_logic
    ) is
    begin
      wait until rising_edge(s_axi_aclk);
      ours <= '1';
      while true loop
        wait until rising_edge(s_axi_aclk);
        exit when theirs = '1';
      end loop;
      ours <= '0';
    end procedure;
    
    procedure axi_write(
      base    : in  std_logic_vector(31 downto 0);
      offs    : in  natural;
      data    : in  std_logic_vector(31 downto 0);
      swap    : in  boolean := false;
      strobe  : in  std_logic_vector(3 downto 0) := "1111"
    ) is
    begin
      
      -- Send address.
      s_axi_awaddr <= std_logic_vector(resize(unsigned(base) + offs, 15));
      handshake(s_axi_awvalid, s_axi_awready);
      
      -- Send data.
      if swap then
        s_axi_wdata(31 downto 24) <= data( 7 downto  0);
        s_axi_wdata(23 downto 16) <= data(15 downto  8);
        s_axi_wdata(15 downto  8) <= data(23 downto 16);
        s_axi_wdata( 7 downto  0) <= data(31 downto 24);
        s_axi_wstrb(3) <= strobe(3);
        s_axi_wstrb(2) <= strobe(2);
        s_axi_wstrb(1) <= strobe(1);
        s_axi_wstrb(0) <= strobe(0);
      else
        s_axi_wdata <= data;
        s_axi_wstrb <= strobe;
      end if;
      handshake(s_axi_wvalid, s_axi_wready);
      
      -- Wait for OK.
      handshake(s_axi_bready, s_axi_bvalid);
      
    end procedure;
    
    procedure axi_read(
      base    : in  std_logic_vector(31 downto 0);
      offs    : in  natural;
      data    : out std_logic_vector(31 downto 0);
      swap    : in  boolean := false
    ) is
    begin
      
      -- Send address.
      s_axi_araddr <= std_logic_vector(resize(unsigned(base) + offs, 15));
      handshake(s_axi_arvalid, s_axi_arready);
      
      -- Wait for OK.
      handshake(s_axi_rready, s_axi_rvalid);
      if swap then
        data(31 downto 24) := s_axi_rdata( 7 downto  0);
        data(23 downto 16) := s_axi_rdata(15 downto  8);
        data(15 downto  8) := s_axi_rdata(23 downto 16);
        data( 7 downto  0) := s_axi_rdata(31 downto 24);
      else
        data := s_axi_rdata;
      end if;
      
    end procedure;
    
    procedure hsi_reset(
      ifsel   : in  std_logic;
      ratio   : in  std_logic
    ) is
      variable data : std_logic_vector(31 downto 0);
    begin
      
      data := "01000000"&"00000000"&"00000000"&"00000000"; -- stop clock
      axi_write(X"00003C00", 0, data);
      
      wait for 1 us;
      
      data := "00100000"&"00000000"&"00000000"&"00000000"; -- reset
      data(25) := ifsel;
      data(24) := ratio;
      axi_write(X"00003C00", 0, data);
      
      wait for 1 us;
      
      data := "01100000"&"00000000"&"00000000"&"00000000"; -- start clock
      axi_write(X"00003C00", 0, data);
      
      wait for 5 us;
      
    end procedure;
    
    procedure wait_done_dbg is
      variable data : std_logic_vector(31 downto 0);
    begin
      while true loop
        wait for 250 us;
        
        -- Do to a problem in the general purpose register file of the ASIC, we
        -- need to halt the core before we can read from the debug bus.
        -- Otherwise the address of general purpose register port 0 gets messed
        -- up for one instruction every debug bus read.
        
        -- Send break command to DCR.
        axi_write(X"00004230", 0, X"09000000");
        
        -- Wait for core to halt. We can either just wait (but we need to wait
        -- long enough for the core to finish halting, especially in reliable
        -- UART mode) or we can busy-wait on the idle flag in the HSI status
        -- register.
        --wait for 25 us;
        while true loop
          wait for 1 us;
          axi_read (X"00003800", 0, data); -- read idle flag
          exit when data(16) = '1';        -- idle is set
        end loop;
        
        -- Read the done flag from DCR.
        axi_read (X"00004230", 0, data);        -- read done flag
        exit when data(31 downto 16) /= X"DEAD" and data(31) = '1'; -- done is set
        
        -- Done flag not set yet, send resume command to DCR.
        axi_write(X"00004230", 0, X"04000000");
        
      end loop;
      report "Debug bus reports that core is done" severity NOTE;
    end procedure;
    
    procedure wait_done_status is
      variable data : std_logic_vector(31 downto 0);
    begin
      while true loop
        wait for 10 us;
        axi_read (X"00003800", 0, data); -- read done flag
        exit when data(20) = '1';        -- done is set
      end loop;
      report "HSI status register reports that core is done" severity NOTE;
    end procedure;
    
    procedure update_mmcm_reg(
      reg     : in natural;
      mask    : in std_logic_vector(15 downto 0);
      value   : in std_logic_vector(15 downto 0)
    ) is
      variable data : std_logic_vector(31 downto 0);
    begin
      axi_read(X"00003000", reg*4, data); -- CLKOUT0 (layout ClkReg1) -> HSI clock
      data(31 downto 16) := X"0000";
      data(15 downto 0) := (data(15 downto 0) and mask) or value;
      axi_write(X"00003000", reg*4, data);
    end procedure;
    
    variable data : std_logic_vector(31 downto 0);
    
  begin
    clk_enable <= true;
    
    s_axi_awaddr    <= (others => '0');
    s_axi_awvalid   <= '0';
    s_axi_wdata     <= X"00000000";
    s_axi_wstrb     <= "0000";
    s_axi_wvalid    <= '0';
    s_axi_bready    <= '0';
    s_axi_araddr    <= (others => '0');
    s_axi_arvalid   <= '0';
    s_axi_rready    <= '0';
    
    wait until rising_edge(s_axi_aclk) and reset = '0';
    
    wait for 30 us;
    
    -- Test reading from MMCM.
    report "Testing reads from MMCM" severity NOTE;
    for i in 6 to 26 loop
      axi_read(X"00003000", i*4, data);
      report "MMCM reg " & integer'image(i) & " is " & rvs_hex(data(15 downto 0), 4) severity NOTE;
    end loop;
    axi_read(X"00003000", 40*4, data);
    report "MMCM reg 40 is " & rvs_hex(data(15 downto 0), 4) severity NOTE;
    axi_read(X"00003000", 78*4, data);
    report "MMCM reg 78 is " & rvs_hex(data(15 downto 0), 4) severity NOTE;
    axi_read(X"00003000", 79*4, data);
    report "MMCM reg 79 is " & rvs_hex(data(15 downto 0), 4) severity NOTE;
    
    -- Test writing to MMCM. Change the dividers to 4 and 8 for 150/300MHz.
    update_mmcm_reg(40, X"0000", X"FFFF"); -- Power register (layout PowerReg)
    update_mmcm_reg(10, X"1000", "000"&"0"&"000010"&"000010"); -- HSI internal clock
    update_mmcm_reg(11, X"FC00", "00000000"&"0"&"0"&"000000");
    update_mmcm_reg(12, X"1000", "000"&"0"&"000010"&"000010"); -- HSI output clock
    update_mmcm_reg(13, X"FC00", "00000000"&"0"&"0"&"000001");
    update_mmcm_reg(14, X"1000", "000"&"0"&"000010"&"000010"); -- HSI input clock
    update_mmcm_reg(15, X"FC00", "00000000"&"0"&"0"&"000010");
    update_mmcm_reg(16, X"1000", "000"&"0"&"000100"&"000100"); -- core clock
    update_mmcm_reg(17, X"FC00", "00000000"&"0"&"0"&"000000");
    data := (others => 'U');
    axi_write(X"00003800", 0, data); -- Clear MMCM reset
    
    -- Test accessing the Teensy.
    report "Testing write to Teensy" severity NOTE;
    data := X"12345678";
    axi_write(X"00000000", 837*4, data);
    report "Wrote " & rvs_hex(data(15 downto 0), 4) & " to reg 0x345" severity NOTE;
    report "Testing read from Teensy" severity NOTE;
    axi_read(X"00000000", 837*4, data);
    report "Teensy reg 0x345 is " & rvs_hex(data(15 downto 0), 4) severity NOTE;
    
    -- 1:4 HSI.
    wait for 10 us;
    report "Starting core in 1:4 HSI" severity NOTE;
    hsi_reset('1', '1');
    wait_done_status;
    
    -- 1:2 HSI.
    wait for 10 us;
    report "Starting core in 1:2 HSI" severity NOTE;
    hsi_reset('1', '0');
    wait_done_status;
    
    -- High-speed UART.
    wait for 10 us;
    report "Starting core in fast UART mode" severity NOTE;
    hsi_reset('0', '1');
    
    -- Make sure the UART keeps working if we enable/disable the clock a few
    -- times.
    for i in 0 to 99 loop
      data := "01000000"&"00000000"&"00000000"&"00000000"; -- stop clock
      axi_write(X"00003C00", 0, data);
      wait for 1 us;
      data := "01100000"&"00000000"&"00000000"&"00000000"; -- restart clock
      axi_write(X"00003C00", 0, data);
      wait for 1 us;
    end loop;
    
    -- Wait for completion in high-speed UART mode.
    wait_done_status;
    
    -- Try doing a scan chain access. We unfortunately can't do this while
    -- running, because the simulation model flipflops aren't scan-aware, so
    -- scan clock cycles are interpreted as normal clock cycles. But we can
    -- test the mockup scan chain if we don't care about messing up the ASIC
    -- simulation model internal state.
    wait for 10 us;
    report "Let's try to access the scan chain" severity NOTE;
    data := "01000000"&"00000000"&"00000000"&"00000000"; -- stop clock
    axi_write(X"00003C00", 0, data);
    data := "11000000"&"00000000"&"00000000"&"00000100"; -- read scan chain 4
    axi_write(X"00003C00", 0, data);
    loop
      wait for 10 us;
      axi_read(X"00003C00", 0, data);
      exit when data(31 downto 29) = "000";
    end loop;
    
    axi_read(X"00002400", 5*4, data); -- read word 5
    report "Scan chain 4 word 5 was " & rvs_hex(data) & ", setting to 0xDEADC0DE" severity NOTE;
    axi_write(X"00002400", 5*4, X"DEADC0DE"); -- set word 5 to 0xDEADC0DE
    
    data := "11100000"&"00000000"&"00000000"&"00000100"; -- write scan chain 4
    axi_write(X"00003C00", 0, data);
    loop
      wait for 10 us;
      axi_read(X"00003C00", 0, data);
      exit when data(31 downto 29) = "000";
    end loop;
    wait for 10 us;
    data := "01100000"&"00000000"&"00000000"&"00000000"; -- restart clock
    axi_write(X"00003C00", 0, data);
    
    -- Reliable UART.
    wait for 10 us;
    report "Starting core in reliable UART mode" severity NOTE;
    hsi_reset('0', '0');
    
    -- Make sure the UART keeps working if we enable/disable the clock a few
    -- times.
    for i in 0 to 99 loop
      data := "01000000"&"00000000"&"00000000"&"00000000"; -- stop clock
      axi_write(X"00003C00", 0, data);
      wait for 1 us;
      data := "01100000"&"00000000"&"00000000"&"00000000"; -- restart clock
      axi_write(X"00003C00", 0, data);
      wait for 1 us;
    end loop;
    
    -- Wait for completion in reliable UART mode.
    wait_done_status;
    
    clk_enable <= false;
    wait;
  end process;
  
  asic_irq <= (others => '0');
  
end behavioral;

