
# Auto-infer the IP core so we can open and modify it.
ipx::infer_core -vendor tudelft.nl -library user -name hsi -taxonomy /Communication_&_Networking/Serial_Interfaces ../../repo/hsi_v1_0
ipx::edit_ip_in_project -upgrade true -name hsi -directory viv ../../repo/hsi_v1_0/component.xml

# Set some project properties. The IP doesn't care about this; this is just
# quality-of-life stuff to test stuff in the IP project.
set_property part xc7z012sclg485-2 [current_project]
set_property target_language VHDL [current_project]

# Open the IP core definition.
ipx::open_ipxact_file ../../repo/hsi_v1_0/component.xml

# Add the repository to the path so we can set the interfaces properly.
set_property ip_repo_paths ../../repo [current_project]
update_ip_catalog

set ip [ipx::current_core]

# Add XDC files (loosely based on UG1119).
add_files -fileset constrs_1 -norecurse ../../repo/hsi_v1_0/ip_hsi_ooc.xdc
set fil [get_files ../../repo/hsi_v1_0/ip_hsi_ooc.xdc]
set_property USED_IN {synthesis implementation} $fil

add_files -fileset constrs_1 -norecurse ../../repo/hsi_v1_0/ip_hsi.xdc
set fil [get_files ../../repo/hsi_v1_0/ip_hsi.xdc]
set_property USED_IN {synthesis implementation} $fil
set_property PROCESSING_ORDER {LATE} $fil

ipx::merge_project_changes files [ipx::current_core]
ipx::reorder_files ip_hsi_ooc.xdc [ipx::get_file_groups xilinx_anylanguagesynthesis -of_objects [ipx::current_core]]
set_property USED_IN {synthesis implementation out_of_context} [ipx::get_files ip_hsi_ooc.xdc -of_objects [ipx::get_file_groups xilinx_anylanguagesynthesis -of_objects [ipx::current_core]]]
ipx::reorder_files ip_hsi.xdc [ipx::get_file_groups xilinx_anylanguagesynthesis -of_objects [ipx::current_core]]

# Set description.
set_property description {r-VEX ASIC high-speed interface} $ip

# Configure the master clock outputs properly; they're not correctly inferred.
ipx::infer_bus_interface m_axi_aclk_out xilinx.com:signal:clock_rtl:1.0 $ip

ipx::infer_bus_interface m_axi_aresetn_out xilinx.com:signal:reset_rtl:1.0 $ip
ipx::add_bus_parameter POLARITY [ipx::get_bus_interfaces m_axi_aresetn_out -of_objects $ip]
set_property value ACTIVE_LOW [ipx::get_bus_parameters POLARITY -of_objects [ipx::get_bus_interfaces m_axi_aresetn_out -of_objects $ip]]

set_property enablement_dependency {$ENABLE_XCLK = 0} [ipx::get_ports m_axi_aresetn_out -of_objects $ip]
set_property enablement_dependency {$ENABLE_XCLK = 0} [ipx::get_ports m_axi_aclk_out -of_objects $ip]

# Infer the external HSI interface.
ipx::infer_bus_interfaces tudelft:user:asic_hsi_rtl:1.0 $ip
ipx::infer_bus_interfaces tudelft.nl:user:teensy_uart_rtl:1.0 $ip

# Make the configuration GUI nice.
set page [ipgui::get_pagespec -name "Page 0" -component $ip]

# Address space panel.
set pane [ipgui::add_group -name {address_space} -component $ip -display_name {Address space}]
ipgui::move_group -component $ip -order 0 $pane -parent $page
ipgui::add_static_text -name {address_space_hint} -component $ip -parent $pane -text {The r-VEX address space is divided into two regions:<ul><li>0x00000000..[memory mask]: system memory, cached, remapped to [memory base]</li><li>0x80000000..0x80000000+[peripheral mask]: system memory, uncached, remapped to [peripheral base]</li></ul>The values in square brackets can be changed below. Out-of-range accesses report a fault to the r-VEX.}

set ob [ipgui::get_guiparamspec -name "C_M_AXI_TARGET_MEMORY_BASE" -component $ip]
set_property display_name {Memory base} $ob
set_property tooltip {Memory base address in AXI space} $ob
ipgui::move_param -component $ip -order 1 $ob -parent $pane

set ob [ipgui::get_guiparamspec -name "C_M_AXI_TARGET_MEMORY_MASK" -component $ip]
set_property display_name {Memory mask} $ob
set_property tooltip {Memory address mask} $ob
ipgui::move_param -component $ip -order 2 $ob -parent $pane

set ob [ipgui::get_guiparamspec -name "C_M_AXI_TARGET_PERIPH_BASE" -component $ip]
set_property display_name {Peripheral base} $ob
set_property tooltip {Peripheral base address in AXI space} $ob
ipgui::move_param -component $ip -order 3 $ob -parent $pane

set ob [ipgui::get_guiparamspec -name "C_M_AXI_TARGET_PERIPH_MASK" -component $ip]
set_property display_name {Peripheral mask} $ob
set_property tooltip {Peripheral address mask} $ob
ipgui::move_param -component $ip -order 4 $ob -parent $pane

# Clock domain crossing panel.
set pane [ipgui::add_group -name {xclk} -component $ip -display_name {AXI master clock domain}]
ipgui::move_group -component $ip -order 1 $pane -parent $page
ipgui::add_static_text -name {xclk_hint} -component $ip -parent $pane -text {To minimize latency and throughput of the HSI memory access bus, it is recommended to instantiate a level-2<br>cache of some kind, and have it run in the core clock domain. As in, m_axi_aclk and m_axi_aresetn and the cache clock/reset<br>should be connected to the m_axi_aclk_out and m_axi_aresetn_out pins. In that case, the internal clock domain crossing<br>can be disabled below.}

set ob [ipgui::get_guiparamspec -name "ENABLE_XCLK" -component $ip]
set_property display_name {Enable clock domain crossing} $ob
set_property tooltip {Enable clock domain crossing} $ob
ipgui::move_param -component $ip -order 1 $ob -parent $pane

# HSI configuration panel.
set pane [ipgui::add_group -name {hsi} -component $ip -display_name {HSI configuration}]
ipgui::move_group -component $ip -order 2 $pane -parent $page

set ob [ipgui::get_guiparamspec -name "DELAY_CAL_MODE" -component $ip]
set_property display_name {Delay calibration mode} $ob
set_property tooltip {Delay calibration mode} $ob
set_property widget {comboBox} $ob
ipgui::move_param -component $ip -order 0 $ob -parent $pane
set ob [ipx::get_user_parameters DELAY_CAL_MODE -of_objects $ip]
set_property value_validation_type pairs $ob
set_property value_validation_pairs {{Disabled} NONE {Only on ASIC} ASIC} $ob

set ob [ipgui::get_guiparamspec -name "SCAN_CLK_DIV" -component $ip]
set_property display_name {Scan clock division} $ob
set_property tooltip {Determines the scan clock with respect to the core clock.} $ob
ipgui::move_param -component $ip -order 1 $ob -parent $pane

set ob [ipgui::get_guiparamspec -name "SCAN_LENGTH" -component $ip]
set_property display_name {Scan chain length} $ob
set_property tooltip {The number of bits in each scan chain.} $ob
ipgui::move_param -component $ip -order 2 $ob -parent $pane

set ob [ipgui::get_guiparamspec -name "INVERT_HSI_PAIRS" -component $ip]
set_property display_name {Invert HSI LVDS data pairs} $ob
set_property tooltip {Invert HSI LVDS data pairs} $ob
ipgui::move_param -component $ip -order 3 $ob -parent $pane

set ob [ipgui::get_guiparamspec -name "CLK_BUF_TYPE" -component $ip]
set_property display_name {Clock buffer type} $ob
set_property tooltip {Clock buffer type} $ob
set_property widget {comboBox} $ob
ipgui::move_param -component $ip -order 4 $ob -parent $pane
set ob [ipx::get_user_parameters CLK_BUF_TYPE -of_objects $ip]
set_property value_validation_type pairs $ob
set_property value_validation_pairs {{BUFR (local to clock region)} BUFR {BUFG (global)} BUFG} $ob

# Save and update stuff.
ipx::create_xgui_files $ip
ipx::update_checksums $ip
ipx::save_core $ip
update_ip_catalog -rebuild -repo_path ../../repo


