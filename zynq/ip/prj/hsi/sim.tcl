
# Give simulate command.
vsim -t ps -novopt xil_defaultlib.testbench +notimingchecks

# Don't shut down modelsim if there is some error.
onerror {resume}

# Change radix to Hexadecimal.
radix hex

# Supress spam.
set NumericStdNoWarnings 1
set StdArithNoWarnings 1

# Add interesting signals.
add log sim:/testbench/*
add log sim:/testbench/uut/*
add log -r sim:/testbench/uut/hsi_inst/*

add wave -divider HSI

add wave -group ASIC     -label "aresetn_phy"      "sim:/testbench/asic_hsi_aresetn_phy"
add wave -group ASIC     -label "mode"             "sim:/testbench/asic_hsi_mode"
add wave -group ASIC     -label "aresetn_core"     "sim:/testbench/asic_hsi_aresetn_core"
add wave -group ASIC     -label "diff_clk"         "sim:/testbench/asic_hsi_clk_p"
add wave -group ASIC     -label "backup_clk"       "sim:/testbench/asic_hsi_backup_clk"
add wave -group ASIC     -label "ctrl F2A = calib" "sim:/testbench/asic_hsi_ctrl_f2a"
add wave -group ASIC     -label "ctrl A2F = lock"  "sim:/testbench/asic_hsi_ctrl_a2f"
add wave -group ASIC     -label "HSI F2A pair 0"   "sim:/testbench/asic_hsi_f2a_n(0)"
add wave -group ASIC     -label "HSI F2A pair 1"   "sim:/testbench/asic_hsi_f2a_n(1)"
add wave -group ASIC     -label "HSI F2A pair 2"   "sim:/testbench/asic_hsi_f2a_n(2)"
add wave -group ASIC     -label "HSI F2A pair 3"   "sim:/testbench/asic_hsi_f2a_n(3)"
add wave -group ASIC     -label "HSI A2F pair 0"   "sim:/testbench/asic_hsi_a2f_n(0)"
add wave -group ASIC     -label "HSI A2F pair 1"   "sim:/testbench/asic_hsi_a2f_n(1)"
add wave -group ASIC     -label "HSI A2F pair 2"   "sim:/testbench/asic_hsi_a2f_n(2)"
add wave -group ASIC     -label "HSI A2F pair 3"   "sim:/testbench/asic_hsi_a2f_n(3)"

add wave -divider "Debug command bus"

add wave                           -label "Clock"        sim:/testbench/s_axi_aclk
add wave                           -label "Reset"        sim:/testbench/s_axi_aresetn
add wave -group "Debug write cmd"  -label "Address"      sim:/testbench/s_axi_awaddr
add wave -group "Debug write cmd"  -label "Valid"        sim:/testbench/s_axi_awvalid
add wave -group "Debug write cmd"  -label "Ready"        sim:/testbench/s_axi_awready
add wave -group "Debug write data" -label "Data"         sim:/testbench/s_axi_wdata
add wave -group "Debug write data" -label "Strobe"       sim:/testbench/s_axi_wstrb
add wave -group "Debug write data" -label "Valid"        sim:/testbench/s_axi_wvalid
add wave -group "Debug write data" -label "Ready"        sim:/testbench/s_axi_wready
add wave -group "Debug write resp" -label "Result"       sim:/testbench/s_axi_bresp
add wave -group "Debug write resp" -label "Valid"        sim:/testbench/s_axi_bvalid
add wave -group "Debug write resp" -label "Ready"        sim:/testbench/s_axi_bready
add wave -group "Debug read cmd"   -label "Address"      sim:/testbench/s_axi_araddr
add wave -group "Debug read cmd"   -label "Valid"        sim:/testbench/s_axi_arvalid
add wave -group "Debug read cmd"   -label "Ready"        sim:/testbench/s_axi_arready
add wave -group "Debug read resp"  -label "Data"         sim:/testbench/s_axi_rdata
add wave -group "Debug read resp"  -label "Result"       sim:/testbench/s_axi_rresp
add wave -group "Debug read resp"  -label "Valid"        sim:/testbench/s_axi_rvalid
add wave -group "Debug read resp"  -label "Ready"        sim:/testbench/s_axi_rready

add wave -divider "r-VEX memory bus"

add wave                           -label "Clock"        sim:/testbench/m_axi_aclk
add wave                           -label "Reset"        sim:/testbench/m_axi_aresetn
add wave -group "r-VEX write cmd"  -label "Address"      sim:/testbench/m_axi_awaddr
add wave -group "r-VEX write cmd"  -label "Beat size"    sim:/testbench/m_axi_awsize
add wave -group "r-VEX write cmd"  -label "Valid"        sim:/testbench/m_axi_awvalid
add wave -group "r-VEX write cmd"  -label "Ready"        sim:/testbench/m_axi_awready
add wave -group "r-VEX write data" -label "Data"         sim:/testbench/m_axi_wdata
add wave -group "r-VEX write data" -label "Strobe"       sim:/testbench/m_axi_wstrb
add wave -group "r-VEX write data" -label "Valid"        sim:/testbench/m_axi_wvalid
add wave -group "r-VEX write data" -label "Ready"        sim:/testbench/m_axi_wready
add wave -group "r-VEX write resp" -label "Result"       sim:/testbench/m_axi_bresp
add wave -group "r-VEX write resp" -label "Valid"        sim:/testbench/m_axi_bvalid
add wave -group "r-VEX write resp" -label "Ready"        sim:/testbench/m_axi_bready
add wave -group "r-VEX read cmd"   -label "Address"      sim:/testbench/m_axi_araddr
add wave -group "r-VEX read cmd"   -label "Beat size"    sim:/testbench/m_axi_arsize
add wave -group "r-VEX read cmd"   -label "Valid"        sim:/testbench/m_axi_arvalid
add wave -group "r-VEX read cmd"   -label "Ready"        sim:/testbench/m_axi_arready
add wave -group "r-VEX read resp"  -label "Data"         sim:/testbench/m_axi_rdata
add wave -group "r-VEX read resp"  -label "Result"       sim:/testbench/m_axi_rresp
add wave -group "r-VEX read resp"  -label "Valid"        sim:/testbench/m_axi_rvalid
add wave -group "r-VEX read resp"  -label "Ready"        sim:/testbench/m_axi_rready

# Run simulation.
run -all

