
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library streams;
use streams.Streams.all;
use streams.Utils.all;
use streams.Memory.all;

entity AxiSlaveMockAddrGen is
  generic (
    
    -- Bus metrics.
    ADDR_WIDTH                  : natural := 32;
    LEN_WIDTH                   : natural := 8;
    ID_WIDTH                    : natural := 2
    
  );
  port (
    
    -- Global signals.
    clk                         : in  std_logic;
    reset                       : in  std_logic;
    
    -- AXI command stream.
    aid                         : in  std_logic_vector(ID_WIDTH-1 downto 0);
    aaddr                       : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
    alen                        : in  std_logic_vector(LEN_WIDTH-1 downto 0);
    asize                       : in  std_logic_vector(2 downto 0);
    aburst                      : in  std_logic_vector(1 downto 0);
    avalid                      : in  std_logic;
    aready                      : out std_logic;
    
    -- Address output stream.
    xid                         : out std_logic_vector(ID_WIDTH-1 downto 0);
    xaddr                       : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    xsize                       : out std_logic_vector(2 downto 0);
    xlast                       : out std_logic;
    xvalid                      : out std_logic;
    xready                      : in  std_logic
    
  );
end AxiSlaveMockAddrGen;

architecture Behavioral of AxiSlaveMockAddrGen is
begin
  
  process is
    variable remain     : natural;
    variable addr_cur   : unsigned(ADDR_WIDTH-1 downto 0);
    variable addr_inc   : natural;
    variable addr_mask  : unsigned(ADDR_WIDTH-1 downto 0);
  begin
    
    -- Reset values.
    aready <= '0';
    xid    <= (others => '0');
    xaddr  <= (others => '0');
    xsize  <= (others => '0');
    xlast  <= '0';
    xvalid <= '0';
    
    transfer: loop
      
      -- Wait for request.
      request_wait: loop
        wait until rising_edge(clk);
        exit transfer when reset = '1';
        exit request_wait when avalid = '1';
      end loop;
      
      -- Save ID and size.
      xid <= aid;
      xsize <= asize;
      
      -- Figure out burst stuff.
      remain := to_integer(unsigned(alen)) + 1;
      addr_cur := unsigned(aaddr);
      addr_inc := to_integer(unsigned(asize));
      if aburst = "00" then
        addr_mask := (others => '0');
      elsif aburst = "01" then
        addr_mask := (others => '1');
      elsif alen = std_logic_vector(to_unsigned(2-1, LEN_WIDTH)) then
        if addr_inc = 0 then
          addr_mask := (others => '0');
        else
          addr_mask := (addr_inc downto 0 => '1', others => '0');
        end if;
      elsif alen = std_logic_vector(to_unsigned(4-1, LEN_WIDTH)) then
        addr_mask := (addr_inc + 1 downto 0 => '1', others => '0');
      elsif alen = std_logic_vector(to_unsigned(8-1, LEN_WIDTH)) then
        addr_mask := (addr_inc + 2 downto 0 => '1', others => '0');
      elsif alen = std_logic_vector(to_unsigned(16-1, LEN_WIDTH)) then
        addr_mask := (addr_inc + 3 downto 0 => '1', others => '0');
      else
        addr_mask := (others => '1');
      end if;
      addr_inc := 2**addr_inc;
      
      -- Acknowledge the request.
      aready <= '1';
      wait until rising_edge(clk);
      exit transfer when reset = '1';
      aready <= '0';
      
      beat: loop
        
        -- If we don't have any more beats to do, get the next transfer.
        next transfer when remain = 0;
        
        -- Set up the next beat.
        xaddr <= std_logic_vector(addr_cur);
        if remain = 1 then
          xlast <= '1';
        else
          xlast <= '0';
        end if;
        
        -- Wait for the beat to be acknowledged.
        xvalid <= '1';
        beat_ready_wait: loop
          wait until rising_edge(clk);
          exit transfer when reset = '1';
          exit beat_ready_wait when xready = '1';
        end loop;
        xvalid <= '0';
        
        -- Compute the next address.
        addr_cur := ((addr_cur + addr_inc) and addr_mask)
                 or (addr_cur and not addr_mask);
        remain := remain - 1;
        
      end loop;
      
    end loop;
  end process;
  
end Behavioral;

