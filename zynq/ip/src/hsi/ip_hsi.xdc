
# Add the max_delay defs for the AXI to MMCM-generated clock crossings.
set_max_delay -from [get_clocks -of_objects [get_ports s_axi_aclk]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT0]] 10.0
set_max_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT0]] -to [get_clocks -of_objects [get_ports s_axi_aclk]] 10.0
set_max_delay -from [get_clocks -of_objects [get_ports s_axi_aclk]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT1]] 10.0
set_max_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT1]] -to [get_clocks -of_objects [get_ports s_axi_aclk]] 10.0
set_max_delay -from [get_clocks -of_objects [get_ports s_axi_aclk]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT2]] 10.0
set_max_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT2]] -to [get_clocks -of_objects [get_ports s_axi_aclk]] 10.0
set_max_delay -from [get_clocks -of_objects [get_ports s_axi_aclk]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT3]] 10.0
set_max_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT3]] -to [get_clocks -of_objects [get_ports s_axi_aclk]] 10.0
set_max_delay -from [get_clocks -of_objects [get_ports s_axi_aclk]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT4]] 10.0
set_max_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT4]] -to [get_clocks -of_objects [get_ports s_axi_aclk]] 10.0

# The user should be allowed to vary the phase of the HSI input clock between
# 0 and 180 degrees. It's configured to 180 by default, so the setup path is
# already properly constrained. The hold path isn't. So we need a min_delay.    v- HSI in                                                              v- internal HSI
set_min_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT3]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT1]]  0.0

# The user should be allowed to change the phase and duty of the HSI output
# clock to whatever. Fortunately, the only signal here is the clock enable,
# which we don't really care about.                                             v- core                                                                v- HSI out
set_min_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT4]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT2]]  0.0
set_max_delay -from [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT4]] -to [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT2]] 10.0

# If the core clock and master AXI clock are different, we're synthesizing with
# a CDC (or should be if the used connected us correctly, anyway). In that
# case, add a max_delay for the CDC.
# TODO: this is nog legal in XDC files, and TCL files don't get scoped
# properly. The user will have to add these constraints manually in the
# set clk_m_axi   [get_clocks -of_objects [get_ports m_axi_aclk]]
# set clk_core    [get_clocks -of_objects [get_pins clkgen_block.mmcm_inst/CLKOUT4]]
# instantiating project if the CDC is used.
# if {$clk_m_axi != $clk_core} {
#   set_max_delay -from $clk_m_axi -to $clk_core 10.0
#   set_max_delay -from $clk_core -to $clk_m_axi 10.0
# }

