library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

-- TODO: this does NOT do delay calibration.

entity ip_hsi_hsi_phy_rx is
  generic (
    
    -- TTD parameters for compatibility with the mock version; ignored.
    TTD_UNIT_DELAY              : time := 120 ps;
    TTD_NUM_BITS                : natural := 6;
    
    -- Number of differential pairs.
    N                           : natural := 4;
    
    -- Whether to enable delay calibration.
    DELAY_CAL                   : boolean := false;
    
    -- Whether to invert the LVDS pairs.
    INVERT_HSI_PAIRS            : boolean := false
    
  );
  port (
    
    -- System control.
    aresetn_phy                 : in  std_logic;
    clk_hsi_in                  : in  std_logic;
    clk_hsi                     : in  std_logic;
    
    -- calib is asserted high while the transmitters are generating a 10101010
    -- pattern to do delay calibration. Aside from potentially doing some small
    -- adjustments to compensate for temperature effects, the delay may not
    -- change significantly while calib_hsi is low.
    calib                       : in  std_logic;
    
    -- Lock is asserted high when all receivers have locked on.
    lock                        : out std_logic;
    
    -- Clock period register contents. Can be between assertion of aresetn_phy
    -- and lock
    hsi_period                  : in  std_logic_vector(7 downto 0);
    
    -- Data input.
    pad2phy_p                   : in  std_logic_vector(N-1 downto 0);
    pad2phy_n                   : in  std_logic_vector(N-1 downto 0);
    
    -- Synchronized data output.
    phy2dll_x                   : out std_logic_vector(N-1 downto 0);
    phy2dll_y                   : out std_logic_vector(N-1 downto 0)
    
  );
end ip_hsi_hsi_phy_rx;

architecture Behavioral of ip_hsi_hsi_phy_rx is
  
  -- Lock signal from each receiver.
  signal lock_int               : std_logic_vector(N-1 downto 0);
  
begin
  
  sig_gen: for i in 0 to N-1 generate
    
    -- The DDR data signal coming from the IBUF.
    signal d                    : std_logic;
    -- Delayed data from delay calibration.
    signal d_delay  : std_logic;
    
    -- The positive-edge-aligned parallelized DDR data signals.
    signal x, y                 : std_logic;
    
  begin
    
    -- Instantiate the LVDS IBUF.
    IBUFDS_inst : IBUFDS
      generic map (
        DIFF_TERM     => true,
        IBUF_LOW_PWR  => false,
        IOSTANDARD    => "LVDS_25"
      )
      port map (
        O  => d,
        I  => pad2phy_p(i),
        IB => pad2phy_n(i)
      );
    
    -- Per-channel automatic delay calibration is not currently implemented.
    assert not DELAY_CAL report "Delay calibration is not implemented!" severity FAILURE;
    lock_int(i) <= '1';
    d_delay <= d;
    
    -- Without delay calibration, the receiver is just an IDDR.
    iddr_inst : iddr
      generic map (
        DDR_CLK_EDGE  => "SAME_EDGE",
        INIT_Q1       => '0',
        INIT_Q2       => '0',
        SRTYPE        => "SYNC"
      )
      port map (
        Q1 => y,
        Q2 => x,
        C  => clk_hsi_in,
        CE => '1',
        D  => d_delay,
        R  => '0',
        S  => '0'
      );
    
    -- Invert the data for PCB routing reasons, and register it for timing
    -- reasons.
    reg_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        if INVERT_HSI_PAIRS then
          phy2dll_x(i) <= not x;
          phy2dll_y(i) <= not y;
        else
          phy2dll_x(i) <= x;
          phy2dll_y(i) <= y;
        end if;
      end if;
    end process;
    
  end generate;
  
  -- Merge the lock signals.
  lock_proc: process (lock_int) is
    variable lock_v             : std_logic;
  begin
    lock_v := lock_int(0);
    for i in 1 to N-1 loop
      lock_v := lock_v and lock_int(i);
    end loop;
    lock <= lock_v;
  end process;
  
end Behavioral;

