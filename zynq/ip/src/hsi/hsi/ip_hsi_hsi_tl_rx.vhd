library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.ip_hsi_hsi_pkg.all;

entity ip_hsi_hsi_tl_rx is
  port (
    
    -- System control.
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- Data outputs to HSI data link layer.
    dll2tl_data                 : in  std_logic_vector(31 downto 0);
    dll2tl_first                : in  std_logic;
    dll2tl_valid                : in  std_logic;
    
    -- Bus slave request.
    tlrx2tlbs_req               : out hsi_bus_req_type;
    
    -- Bus master response.
    tlrx2tlbm_resp              : out hsi_bus_resp_type;
    
    -- GPIO data.
    tl2gpio                     : out std_logic_vector(27 downto 0)
    
  );
end ip_hsi_hsi_tl_rx;

architecture Behavioral of ip_hsi_hsi_tl_rx is
  signal header                 : std_logic_vector(31 downto 0);
  signal beat, beat_next        : std_logic_vector(2 downto 0);
  signal is_gpio                : std_logic;
begin
  
  -- Registers.
  reg_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      
      -- Always store the first word in a packet in the header register.
      if dll2tl_valid = '1' and dll2tl_first = '1' then
        header <= dll2tl_data;
      end if;
      
      -- Update the beat counter when we receive a word.
      if reset_core = '1' then
        beat <= "000";
      elsif dll2tl_valid = '1' then
        beat <= beat_next;
      end if;
      
      -- Infer the GPIO state register.
      if reset_core = '1' then
        tl2gpio <= (others => '0');
      elsif is_gpio = '1' then
        tl2gpio <= dll2tl_data(27 downto 0);
      end if;
      
    end if;
  end process;
  
  -- Decode words in packets combinatorially.
  decode_proc: process (
    dll2tl_valid, dll2tl_first, dll2tl_data, header, beat
  ) is
  begin
    
    -- Write enable signal for the GPIO register.
    is_gpio <= '0';
    
    -- Bus slave request defaults to minimize logic.
    tlrx2tlbs_req.valid <= '0';
    tlrx2tlbs_req.wr    <= not dll2tl_first;
    tlrx2tlbs_req.mask  <= dll2tl_data(19 downto 16);
    tlrx2tlbs_req.burst <= '0';
    if dll2tl_first = '1' then
      tlrx2tlbs_req.addr(31 downto 5) <= dll2tl_data(26 downto 0);
      tlrx2tlbs_req.addr(4 downto 2) <= dll2tl_data(29 downto 27);
    else
      tlrx2tlbs_req.addr(31 downto 5) <= header(26 downto 0);
      tlrx2tlbs_req.addr(4 downto 2) <= header(29 downto 27);
    end if;
    tlrx2tlbs_req.data <= dll2tl_data;
    
    -- Bus master response defaults to minimize logic.
    tlrx2tlbm_resp.valid <= '0';
    tlrx2tlbm_resp.wr    <= header(26) and not dll2tl_first;
    tlrx2tlbm_resp.fault <= header(25) and not dll2tl_first;
    tlrx2tlbm_resp.burst <= header(27) and not dll2tl_first;
    tlrx2tlbm_resp.last  <= beat(0) and beat(1) and beat(2);
    tlrx2tlbm_resp.data  <= dll2tl_data;
    if header(27) = '1' then
      tlrx2tlbm_resp.data(0) <= '0';
    end if;
    
    -- Beat counter resets unless otherwise specified.
    beat_next <= "000";
    
    -- Parse words/packets.
    if dll2tl_first = '1' then
      
      -- Parse single-word packets.
      if dll2tl_data(31 downto 27) = "00001" then
        
        -- 248-bit read request to slave.
        tlrx2tlbs_req.valid <= dll2tl_valid;
        tlrx2tlbs_req.wr    <= '0';
        tlrx2tlbs_req.burst <= '1';
        tlrx2tlbs_req.addr(31 downto 5) <= dll2tl_data(26 downto 0);
        tlrx2tlbs_req.addr(4 downto 2) <= "000";
        
      elsif dll2tl_data(31 downto 25) = "0001010" then
        
        -- Write acknowledge response to master.
        tlrx2tlbm_resp.valid <= dll2tl_valid;
        tlrx2tlbm_resp.wr    <= '1';
        tlrx2tlbm_resp.fault <= '0';
        tlrx2tlbm_resp.burst <= '0';
        
      elsif dll2tl_data(31 downto 28) = "0010" then
        
        -- GPIO update.
        is_gpio <= dll2tl_valid;
        
      elsif dll2tl_data(31 downto 30) = "01" then
        
        -- 32-bit read request to slave.
        tlrx2tlbs_req.valid <= dll2tl_valid;
        tlrx2tlbs_req.wr    <= '0';
        tlrx2tlbs_req.burst <= '0';
        tlrx2tlbs_req.addr(31 downto 5) <= dll2tl_data(26 downto 0);
        tlrx2tlbs_req.addr(4 downto 2) <= dll2tl_data(29 downto 27);
        
      end if;
      
    else
      
      -- Parse multi-word packets.
      if header(31 downto 26) = "000100" then
        
        -- Read ack/fault response to master.
        tlrx2tlbm_resp.valid <= dll2tl_valid;
        tlrx2tlbm_resp.wr    <= '0';
        tlrx2tlbm_resp.fault <= header(25);
        tlrx2tlbm_resp.burst <= '0';
        tlrx2tlbm_resp.data  <= dll2tl_data;
        
      elsif header(31 downto 26) = "000101" then
        
        -- Write fault response to master.
        tlrx2tlbm_resp.valid <= dll2tl_valid;
        tlrx2tlbm_resp.wr    <= '1';
        tlrx2tlbm_resp.fault <= header(25);
        tlrx2tlbm_resp.burst <= '0';
        tlrx2tlbm_resp.data  <= dll2tl_data;
        
      elsif header(31 downto 25) = "0001100" then
        
        -- 248-bit read response.
        tlrx2tlbm_resp.valid <= dll2tl_valid and dll2tl_data(0);
        tlrx2tlbm_resp.wr    <= '0';
        tlrx2tlbm_resp.fault <= '0';
        tlrx2tlbm_resp.burst <= '1';
        tlrx2tlbm_resp.last  <= beat(0) and beat(1) and beat(2);
        tlrx2tlbm_resp.data  <= dll2tl_data(31 downto 1) & "0";
        
        -- Count the burst beats so we know when we get the last one.
        if dll2tl_data(0) = '1' then
          beat_next <= std_logic_vector(unsigned(beat) + 1);
        else
          beat_next <= beat;
        end if;
        
      elsif header(31 downto 30) = "10" then
        
        -- 32-bit write request to slave.
        tlrx2tlbs_req.valid <= dll2tl_valid;
        tlrx2tlbs_req.wr    <= '1';
        tlrx2tlbs_req.mask  <= "1111";
        tlrx2tlbs_req.burst <= '0';
        tlrx2tlbs_req.addr(31 downto 5) <= header(26 downto 0);
        tlrx2tlbs_req.addr(4 downto 2) <= header(29 downto 27);
        tlrx2tlbs_req.data <= dll2tl_data;
        
      elsif header(31 downto 30) = "11" then
        
        -- 8-bit or 16-bit write request to slave.
        tlrx2tlbs_req.valid <= dll2tl_valid;
        tlrx2tlbs_req.wr    <= '1';
        tlrx2tlbs_req.mask  <= dll2tl_data(19 downto 16);
        tlrx2tlbs_req.burst <= '0';
        tlrx2tlbs_req.addr(31 downto 5) <= header(26 downto 0);
        tlrx2tlbs_req.addr(4 downto 2) <= header(29 downto 27);
        tlrx2tlbs_req.data <= dll2tl_data(15 downto 0) & dll2tl_data(15 downto 0);
        
      end if;
      
    end if;
  end process;
  
end Behavioral;

