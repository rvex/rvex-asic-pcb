library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ip_hsi_hsi_dll_tx is
  generic (
    
    -- Number of differential pairs.
    N                           : natural := 8;
    
    -- Number of input bits. Must be a multiple of 8*N.
    NUM_BITS                    : natural := 32
    
  );
  port (
    
    -- System control.
    reset_hsi                   : in  std_logic;
    clk_hsi                     : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- Word stream input in clk_core domain.
    tl2dll_data                 : in  std_logic_vector(NUM_BITS-1 downto 0);
    tl2dll_first                : in  std_logic;
    tl2dll_valid                : in  std_logic;
    dll2tl_ack                  : out std_logic;
    
    -- Data outputs. X during clk_hsi high, Y during clk_hsi low.
    dll2phy_X                   : out std_logic_vector(N-1 downto 0);
    dll2phy_Y                   : out std_logic_vector(N-1 downto 0)
    
  );
end ip_hsi_hsi_dll_tx;

architecture Behavioral of ip_hsi_hsi_dll_tx is
  
  -- Enable signal from the clock crossing that indicates that the PHY has
  -- consumed a flit's worth of data.
  signal serialize_shift        : std_logic;
  
  -- Data stream in the HSI clock domain. consume may only be asserted once
  -- every core clock period.
  signal data                   : std_logic_vector(N*8-1 downto 0);
  signal first                  : std_logic;
  signal valid                  : std_logic;
  signal consume                : std_logic;
  
begin
  
  serialize_block: block is
    
    -- Serialization ratio.
    constant RATIO            : natural := NUM_BITS / (N*8);
    
    -- Data shift register.
    signal data_sr            : std_logic_vector(NUM_BITS+N*8-1 downto 0);
    
    -- Valid bit shift register.
    signal valid_sr           : std_logic_vector(RATIO downto 0);
    
  begin
    
    -- The LSB of the shift register corresponds to the input of the first
    -- register, i.e. the output of a nonexistant register. A nonexistant
    -- register of course never contains valid data so valid is always 0. The
    -- data register is don't care.
    valid_sr(0) <= '0';
    data_sr(N*8-1 downto 0) <= (others => '0');
    
    -- Infer the shift registers.
    reg_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_hsi = '1' then
          valid_sr(RATIO downto 1) <= (others => '0');
        else
          if serialize_shift = '1' or valid_sr(RATIO) = '0' then
            if valid_sr(RATIO-1) = '1' then
              
              -- We're shifting the current contents of the shift register out
              -- to the PHY.
              data_sr(NUM_BITS+N*8-1 downto N*8) <= data_sr(NUM_BITS-1 downto 0);
              valid_sr(RATIO downto 1) <= valid_sr(RATIO-1 downto 0);
              first <= '0';
              
            else
              
              -- We're loading new data into the shift register.
              data_sr(NUM_BITS+N*8-1 downto N*8) <= tl2dll_data;
              valid_sr(RATIO downto 1) <= (others => tl2dll_valid);
              first <= tl2dll_first;
              
            end if;
          end if;
        end if;
      end if;
    end process;
    
    -- Acknowledge when we're taking in new data.
    dll2tl_ack <= tl2dll_valid
              and (not reset_hsi)
              and (serialize_shift or not valid_sr(RATIO))
              and (not valid_sr(RATIO-1));
    
    -- Output the data word and valid bit to the transmit state machine.
    data <= data_sr(NUM_BITS+N*8-1 downto NUM_BITS);
    valid <= valid_sr(RATIO);
    
  end block;
  
  -- The clock domain crossing from hsi to core containing flow control info is
  -- done as follows:
  --                _____
  -- consume >---\ \     \
  --              ) )     )--.                _____
  --          .--/ /_____/   o-------------\ \     \
  --          |   .-----.    |    .-----.   ) )     )---> serialize_shift
  --          '---|Q   D|<---o--->|D   Q|--/ /_____/
  --           HSI|>r   |     core|>r   |
  --              '-----'         '-----'
  --
  -- This assumes that the clocks are synchronous and that the timing tools are
  -- aware of that. The tools should constrain the setup paths to the minimum
  -- time from a clk_hsi edge to a clk_core edge.
  xclk_block: block is
    
    -- Sync toggles in the HSI clock domain whenever a data word is consumed.
    -- sync_rc follows sync in the core clock domain, allowing edges to be
    -- detected.
    signal sync, sync_rh, sync_rc : std_logic;
    
  begin
    
    sync_reg_hsi_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        if reset_hsi = '1' then
          sync_rh <= '0';
        else
          sync_rh <= sync;
        end if;
      end if;
    end process;
    
    sync <= sync_rh xor consume;
    
    sync_reg_core_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_hsi = '1' then
          sync_rc <= '0';
        else
          sync_rc <= sync;
        end if;
      end if;
    end process;
    
    serialize_shift <= sync xor sync_rc;
    
  end block;
  
  -- Transmiter state machine.
  transmitter_block: block is
    
    -- Holding registers for the data signal.
    signal data_A               : std_logic_vector(N-1 downto 0);
    signal data_B               : std_logic_vector(N-1 downto 0);
    signal data_C               : std_logic_vector(N-1 downto 0);
    signal data_D               : std_logic_vector(N-1 downto 0);
    signal data_E               : std_logic_vector(N-1 downto 0);
    signal data_F               : std_logic_vector(N-1 downto 0);
    signal data_G               : std_logic_vector(N-1 downto 0);
    signal data_H               : std_logic_vector(N-1 downto 0);
    
    -- FSM state.
    signal state, state_next    : std_logic_vector(3 downto 0);
    
  begin
    
    -- Infer the state register and data holding registers.
    reg_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        if reset_hsi = '1' then
          state <= (others => '0');
        else
          if consume = '1' then
            for i in 0 to N-1 loop
              data_A(i) <= data(i*8+7);
              data_B(i) <= data(i*8+6);
              data_C(i) <= data(i*8+5);
              data_D(i) <= data(i*8+4);
              data_E(i) <= data(i*8+3);
              data_F(i) <= data(i*8+2);
              data_G(i) <= data(i*8+1);
              data_H(i) <= data(i*8+0);
            end loop;
          end if;
          state <= state_next;
        end if;
      end if;
    end process;
    
    -- Combinatorial logic.
    comb_proc: process (
      data_A, data_B, data_C, data_D, data_E, data_F, data_G, data_H,
      first, valid, state
    ) is
      
      -- Flits must look like this:
      --   X | 1 C D G H
      --   Y | A B E F
      -- or
      --   X |   A B E F
      --   Y | 1 C D G H
      --
      -- A zero must be inserted between packets; flits that together form a
      -- packet may not have a zero in between.
      --
      -- The states are:
      --   S | 0 1 2 3 4 5 6 7 8 9 A B C D E F
      --  ---+---------------------------------
      --   X | 0 1 C D G H - - H A B E F 0 - -
      --   Y | 0 A B E F 0 - - 1 C D G H 1 - -
      --
      -- Single-flit packets can be sent at max rate using states 12345 or 567
      -- repeating. A multi-flit packet can be sent at max rate using states
      -- 12456 repeating. 0 is the idle state.
    begin
      
      case state is
        
        when "0000" =>
          dll2phy_X <= (others => '0');
          dll2phy_y <= (others => '0');
          consume <= valid;
          if valid = '1' and first = '1' then
            state_next <= "0001";
          elsif valid = '1' then
            report "Data underflow: packet interrupted." severity warning;
            state_next <= "0000";
          else
            state_next <= "0000";
          end if;
        
        when "0001" =>
          dll2phy_X <= (others => '1');
          dll2phy_y <= data_A;
          consume <= '0';
          state_next <= "0010";
        
        when "0010" =>
          dll2phy_X <= data_C;
          dll2phy_y <= data_B;
          consume <= '0';
          state_next <= "0011";
        
        when "0011" =>
          dll2phy_X <= data_D;
          dll2phy_y <= data_E;
          consume <= '0';
          state_next <= "0100";
        
        when "0100" =>
          dll2phy_X <= data_G;
          dll2phy_y <= data_F;
          consume <= '0';
          if valid = '1' and first = '1' then
            state_next <= "0101";
          elsif valid = '1' then
            state_next <= "1000";
          else
            state_next <= "0101";
          end if;
        
        when "0101" | "0110" | "0111" =>
          dll2phy_X <= data_H;
          dll2phy_y <= (others => '0');
          consume <= valid;
          if valid = '1' and first = '1' then
            state_next <= "0001";
          elsif valid = '1' then
            report "Data underflow: packet interrupted." severity warning;
            state_next <= "0000";
          else
            state_next <= "0000";
          end if;
        
        when "1000" =>
          dll2phy_X <= data_H;
          dll2phy_y <= (others => '1');
          consume <= '1';
          state_next <= "1001";
        
        when "1001" =>
          dll2phy_X <= data_A;
          dll2phy_y <= data_C;
          consume <= '0';
          state_next <= "1010";
        
        when "1010" =>
          dll2phy_X <= data_B;
          dll2phy_y <= data_D;
          consume <= '0';
          state_next <= "1011";
        
        when "1011" =>
          dll2phy_X <= data_E;
          dll2phy_y <= data_G;
          consume <= '0';
          state_next <= "1100";
        
        when "1100" =>
          dll2phy_X <= data_F;
          dll2phy_y <= data_H;
          consume <= valid;
          if valid = '1' and first = '1' then
            state_next <= "1101";
          elsif valid = '1' then
            state_next <= "0001";
          else
            state_next <= "0000";
          end if;
          
        when others =>
          dll2phy_X <= (others => '0');
          dll2phy_y <= (others => '1');
          consume <= '0';
          state_next <= "1001";
        
      end case;
    end process;
    
  end block;
  
end Behavioral;

