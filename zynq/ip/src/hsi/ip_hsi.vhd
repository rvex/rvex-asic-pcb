
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_misc.ALL;
use ieee.numeric_std.ALL;

library unisim;
use unisim.vcomponents.all;

library unimacro;
use unimacro.vcomponents.all;

library work;
use work.ip_hsi_common_pkg.all;
use work.ip_hsi_utils_pkg.all;
use work.ip_hsi_bus_pkg.all;

-- THIS INTERFACE IS ENDIANLESS FOR 32-BIT ACCESSES AND LITTLE-ENDIAN FOR OTHER
-- ACCESSES. This is the default for AXI and will work correctly with the PS,
-- but rvsrv and the r-VEX itself may need some byteswapping here and there.
-- 
--          .---.---.---.---.---.---.---.---.---.---.---.---.---.---.---.
--          |14 :13 :12 |11 :10 : 9 : 8 | 7 : 6 : 5 : 4 | 3 : 2 : 1 : 0 |
-- .--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------.-----------------.------------.
-- | 0x0000 | 0 : 0 : Register                                  : 0 : 0 | Power control registers             |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x2000 | 0 : 1 : 0 | Scan chain    | Field                 : 0 : 0 | Scan chain block RAM                |                 | Always     |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 | accessible |
-- | 0x3000 | 0 : 1 : 1 | 0 : ~ : ~ | Register                  : 0 : 0 | MMCM dynamic reconfiguration port   | See below       |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x3800 | 0 : 1 : 1 | 1 : 0 : ~ : ~ | ~ : ~ : ~ : ~ | Reg.  : 0 : 0 | HSI status/MMCM restart registers   |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |------------|
-- | 0x3C00 | 0 : 1 : 1 | 1 : 1 : ~ : ~ | ~ : ~ : ~ : ~ | ~ : ~ : 0 : 0 | HSI control register                |                 | Need lock  |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|-----------------|------------|
-- | 0x4000 | 1 : 0 : ~ | ~ : ~ : 0 : 0 | Register              : Byte  | ASIC global control registers       |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x4100 | 1 : 0 : ~ | 0 : 0 : 0 : 1 | Register              : Byte  | ASIC context 0 general purpose      |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x4200 | 1 : 0 : ~ | 0 : 0 : 1 : Register                  : Byte  | ASIC context 0 control registers    |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x4500 | 1 : 0 : ~ | 0 : 1 : 0 : 1 | Register              : Byte  | ASIC context 1 general purpose      |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------| Refer to r-VEX  |            |
-- | 0x4600 | 1 : 0 : ~ | 0 : 1 : 1 : Register                  : Byte  | ASIC context 1 control registers    | user manual     |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x4900 | 1 : 0 : ~ | 1 : 0 : 0 : 1 | Register              : Byte  | ASIC context 2 general purpose      |                 | Only with  |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 | functional |
-- | 0x4A00 | 1 : 0 : ~ | 1 : 0 : 1 : Register                  : Byte  | ASIC context 2 control registers    |                 | HSI        |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x4D00 | 1 : 0 : ~ | 1 : 1 : 0 : 1 | Register              : Byte  | ASIC context 3 general purpose      |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x4E00 | 1 : 0 : ~ | 1 : 1 : 1 : Register                  : Byte  | ASIC context 3 control registers    |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|-----------------|            |
-- | 0x6000 | 1 : 1 : ~ | ~ : 0 : ~ : ~ | ~ : ~ : ~ : Register  : Byte  | ASIC interrupt controller global    |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x6400 | 1 : 1 : ~ | ~ : 1 : ~ : ~ | ~ : 0 : 0 : Register  : Byte  | ASIC interrupt controller context 0 |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------| Refer to        |            |
-- | 0x6420 | 1 : 1 : ~ | ~ : 1 : ~ : ~ | ~ : 0 : 1 : Register  : Byte  | ASIC interrupt controller context 1 | periph_irq.vhd  |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x6440 | 1 : 1 : ~ | ~ : 1 : ~ : ~ | ~ : 1 : 0 : Register  : Byte  | ASIC interrupt controller context 2 |                 |            |
-- |--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------|                 |            |
-- | 0x6460 | 1 : 1 : ~ | ~ : 1 : ~ : ~ | ~ : 1 : 1 : Register  : Byte  | ASIC interrupt controller context 3 |                 |            |
-- '--------|---:---:---|---:---:---:---|---:---:---:---|---:---:---:---|-------------------------------------'-----------------'------------'
--          |14 :13 :12 |11 :10 : 9 : 8 | 7 : 6 : 5 : 4 | 3 : 2 : 1 : 0 |
--          '---'---'---'---'---'---'---'---'---'---'---'---'---'---'---'
-- 
-- Power control registers (0x0000):
--   Accessing these registers causes a UART transaction with the Teensy. Keep
--   in mind that this is slow. There's a timeout of 2ms in case the Teensy is
--   not responding, upon which 0xDEADCODE will be returned for reads (there is
--   no mechanism to detect a failed write from the AXI bus other than reading
--   the register again after the write). The Teensy registers are 16-bit in
--   size, so the timeout can always be distinguished from an actual value.
-- 
--   The first 1024 registers contain the first 256 voltage/current samples
--   after powering up. SS in the address indicates the sample.
--     0x0SS0: sample channel.
--      - 0x0000 -> sample invalid
--      - 0x0040 -> 2.5V digital (VDDIO)
--      - 0x0041 -> 1.2V digital (VDDCORE)
--      - 0x0042 -> 2.5V analog (LVDS)
--      - 0x0043 -> 1.2V analog (LVDS)
--     0x0SS4: microseconds since power enable asserted.
--     0x0SS8: current measurement in 10uA increments (assuming 1R shunt). This
--       value is signed, but must be sign-extended by the processor as the
--       interface hardware is not aware of this.
--     0x0SSC: voltage measurement in 0.5mV increments.
--
--   The next 8 registers contain the latest measurements:
--     0x1000: 2.5V digital (VDDIO) current in 10uA increments (signed)
--     0x1004: 1.2V digital (VDDCORE) current in 10uA increments (signed)
--     0x1008: 2.5V analog (VDDIO) current in 10uA increments (signed)
--     0x100C: 1.2V analog (LVDS) current in 10uA increments (signed)
--     0x1010: 2.5V digital (LVDS) voltage in 0.5mV increments
--     0x1014: 1.2V digital (VDDCORE) voltage in 0.5mV increments
--     0x1018: 2.5V analog (LVDS) voltage in 0.5mV increments
--     0x101C: 1.2V analog (LVDS) voltage in 0.5mV increments
-- 
--   Then there's some misc. status registers:
--     0x1020: overcurrent protection status. If nonzero, OCP kicked in.
--       Power cycle the board or cycle the power enable register to clear
--       this (this register is read-only).
--     0x1024: power currently enabled status, ignoring the overcurrent
--       protection status. That is, if overcurrent is detected this register
--       is NOT cleared. This register is pretty much just a read-only mirror
--       of 0x1028.
-- 
--   Up to this point everything was read-only. The remaining registers are
--   read-write control registers. The most important one:
--     0x1028: enable register. Write 1 to enable power to the ASIC, write 0
--       to disable power.
-- 
--   The next 4 registers trim the ASIC supply voltages. WARNING: WHILE THE
--   HARDWARE IS FAIRLY LIMITED, IT MAY BE POSSIBLE TO DAMAGE THE ASIC BY
--   OVERVOLTING. The Teensy will update this register to the value actually
--   set by the DAC when it updates the DAC (range check and roundoff error).
--   The supported range for the 2.5V rails is around 1.7V to 2.75V, the range
--   for the 1.2V rails is around 0.65V to 1.4V. The register resolution is
--   0.5mV.
--     0x102C: requested voltage for 2.5V digital (VDDIO).
--     0x1030: requested voltage for 1.2V digital (VDDCORE).
--     0x1034: requested voltage for 2.5V analog (LVDS).
--     0x1038: requested voltage for 1.2V analog (LVDS).
--
--   The final 8 registers control the LVDS current references. The resolution
--   is 0.1uA.
--     0x103C: requested sink current for A2F pair 0. 350uA (3500) nominal.
--     0x1040: requested sink current for A2F pair 1. 350uA (3500) nominal.
--     0x1044: requested sink current for A2F pair 2. 350uA (3500) nominal.
--     0x1048: requested sink current for A2F pair 3. 350uA (3500) nominal.
--     0x104C: requested source current for A2F pair 0. 500uA (5000) nominal.
--     0x1050: requested source current for A2F pair 1. 500uA (5000) nominal.
--     0x1054: requested source current for A2F pair 2. 500uA (5000) nominal.
--     0x1058: requested source current for A2F pair 3. 500uA (5000) nominal.
-- 
-- Scan chain block RAM (0x2000):
--   These block RAMs are used as a buffer for the scan chain data of the ASIC.
--   Each scan chain is 2016 bits long (63 32-bit words). 14 of these scan
--   chains exist. The scan chains are read/written from high to low address
--   and from MSB to LSB. This makes the first four scan chains correspond to
--   the general purpose register files of contexts 0..3, starting at $r0.1
--   because $r0.0 does not exist in the ASIC:
--
--     0x2000: context 0 $r0.1
--     0x2004: context 0 $r0.2
--     ......: ....... . ......
--     0x20FC: context 0 $r0.63
--     0x20FF: (unused)
--     0x2100: context 1 $r0.1
--     0x2104: context 1 $r0.2
--     ......: ....... . ......
--     0x21FC: context 1 $r0.63
--     0x21FF: (unused)
--     0x2200: context 2 $r0.1
--     0x2204: context 2 $r0.2
--     ......: ....... . ......
--     0x22FC: context 2 $r0.63
--     0x22FF: (unused)
--     0x2300: context 3 $r0.1
--     0x2304: context 3 $r0.2
--     ......: ....... . ......
--     0x23FC: context 3 $r0.63
--     0x23FF: (unused)
--
--   Keep in mind that this region is only a buffer; you need to explicitly
--   give scan read/write commands using the HSI control register to transfer
--   between this buffer and the ASIC.
-- 
--   Refer to the asic/scanchain directory for the contents of the other scan
--   chains.
-- 
-- MMCM dynamic reconfiguration port (0x3000):
--   These addresses map to the DRP port of the MMCM2_ADV block that generates
--   the core and HSI clocks. Note that "core clock" in this context doesn't
--   necessarily mean the ASIC r-VEX clock; that is only the case in UART mode,
--   when the ASIC is clocked by the backup clock. In HSI mode the HSI clock
--   determines the ASIC r-VEX clock, being either 2x or 4x the core frequency
--   depending on how the HSI is initialized (ratio bit). In HSI mode the core
--   clock must be 1/2 or 1/4 the HSI clock, irrespective of the selected
--   ratio.
--   
--   The MMCM reset must be asserted for writes to the DRP to be allowed. The
--   interface does this automatically when the first write is performed, but
--   the clock must be released manually. This is done by writing to the
--   status/MMCM restart register (the value is ignored).
--   
--   The clocks are connected to the MMCM as follows:
--     CLKOUT0: IDELAYCTRL reference clock (must be 190-210MHz).
--     CLKOUT1: internal HSI clock: this is used internally and for the F2A
--              pins.
--     CLKOUT2: output HSI clock: this is used for the HSI clock output. This
--              can be phase-shifted to manually move the F2A pin sampling
--              position to the center of the eye.
--     CLKOUT3: input HSI clock: this is used to sample the A2F pins. It can
--              be phase-shifted up to 180 degrees (this is the default for
--              timing constraint reasons) to manually move the A2F pin
--              sampling position to the center of the eye.
--     CLKOUT4: FPGA core clock.
--   The MMCM is initialized with a 1.2GHz VCO. Refer to XAP888 for the
--   register layout. Note that while the MMCM registers are only 16-bit wide,
--   the addressing is done on 32-bit word basis, and only the lower 16 bits
--   of the bus read/write data are used.
--   
--   For example, if we want 75Mhz core and 300MHz HSI (with default VCO):
--    - period = desired HSI clock period in VCO cycles = 4
--    - ratio = HSI to core clock ratio = 4
--    - ophase = HSI output clock phase in 1/8 VCO, between 0 and period*4 = 8
--    - iphase = HSI input clock phase in 1/8 VCO, between 0 and period*4 = 16
--    - Set power register:
--        *0x30A0 = 0xFFFF;
--    - Configure the new HSI internal clock:
--        *0x3028 = (*0x3028 & 0x1000) | (2 << 0)   // low time  = (period + 1) / 2
--                                     | (2 << 6);  // high time = period / 2
--        *0x302C = (*0x302C & 0xFC00) | (0 << 7);  // edge      = period % 2
--    - Configure the new HSI output clock with 90 degrees phase:
--        *0x3030 = (*0x3030 & 0x1000) | (2 << 0)   // low time  = (period + 1) / 2
--                                     | (2 << 6)   // high time = period / 2
--                                     | (0 << 13); // phase in 1/8 vco = ophase % 8
--        *0x3034 = (*0x3034 & 0xFC00) | (1 << 0)   // phase in vco periods = ophase / 8
--                                     | (0 << 7);  // edge      = period % 2
--    - Configure the new HSI input clock with 180 degrees phase:
--        *0x3038 = (*0x3038 & 0x1000) | (2 << 0)   // low time  = (period + 1) / 2
--                                     | (2 << 6)   // high time = period / 2
--                                     | (0 << 13); // phase in 1/8 vco = iphase % 8
--        *0x303C = (*0x303C & 0xFC00) | (2 << 0)   // phase in vco periods = iphase / 8
--                                     | (0 << 7);  // edge      = period % 2
--    - Set the new core clock divider:
--        *0x3040 = (*0x3040 & 0x1000) | (8 << 0)   // low time  = period * ratio / 2
--                                     | (8 << 6);  // high time = period * ratio / 2
--        *0x3044 = (*0x3044 & 0xFC00) | (0 << 7);  // edge      = 0
--    - Restart the MMCM:
--        *0x3800 = 0;
--    - Wait for MMCM lock:
--        while (*0x3800 & 0x80000000);
--   
--   For fine frequency control you'll need to change the VCO frequency using
--   the fractional divider, and adjust the reference clock division fractional
--   divider accordingly.
-- 
-- Write to HSI status/MMCM restart register (0x3800):
--   The MMCM reset is released. Write data is ignored.
-- 
-- Read from HSI status/MMCM restart register (0x3800):
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |L|X|C|H|M| | | | Done  | Idle  ||      Configuration word       |
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   L: HSI/core MMCM and IDELAYCTRL (if instantiated) locked
--   X: HSI control logic reset
--   C: ASIC core reset
--   H: HSI interface reset
--   M: MMCM reset
--   Done: which contexts are done (reported through HSI GPIO channel)
--   Idle: which contexts are idle (reported through HSI GPIO channel)
--   Configuration word: the lower 16 bits of the ASIC configuration word
--     (reported through HSI GPIO channel)
--
-- Read from clock monitor register 1..3 (0x3804..0x380C):
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |       AXI clock monitor       ||       Core clock monitor      | 0x3804
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |       AXI clock monitor       ||       HSI clock monitor       | 0x3808
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |       AXI clock monitor       ||200MHz reference clock monitor | 0x380C
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Each of the monitor values are gray-code counters that increment when
--   their respective clock has gone through 16384 cycles. The software reading
--   the registers should thus do a gray to binary conversion. The intended way
--   to measure the clocks is to read each of the registers twice with a
--   ~1-second delay. The AXI clock can then be derived approximately, and the
--   ratios between the clocks can be derived +/-17 kHz.
--
-- Write to HSI control register (0x3C00):
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |0|0|1|C|B|A|I|R| | | | | | | | || | | | | | | | | HSI clk period| Reset
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is stopped. Resets ASIC using ifsel=I and
--   ratio=R. Sample UART on edge A, setup UART on edge B (ctrl f2a) and
--   C (aresetn_phy). Returns the ASIC with the clock stopped.
--   
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |0|1|0| | | | | | | | | | | | | || | | | | | | | | | | | | | | | | Pause
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is running. Stops the ASIC clock.
--   
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |0|1|1| | | | | | | | | | | | | || | | | | | | | | | | | | | | | | Continue
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is stopped. Starts the ASIC clock.
--   
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |1|0|0| | | | | | | |                 Step count                 | Step
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is stopped. Sends <step count> clock cycles.
--   
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |1|0|1| | | | | | |E|0|0|0|0|0|0||0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0| Wait
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is stopped, and should only be done after
--   writing to chain 0x80 to select event. Sends clock cycles until the
--   event flag equals E. Can be aborted by writing this command again
--   with E in the other state so it matches what the ASIC is outputting.
--   
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |1|1|0| | | | | | | | | | | | | || | | | | | | | |     Chain     | Read
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is stopped. Reads the given scan chain to
--   memory.
--   
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |1|1|1| | | | | | | | | | | | | || | | | | | | | |     Chain     | Write
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Only allowed when clock is stopped. Writes the given scan chain from
--   memory.
-- 
-- Read from HSI control register (0x3C00):
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   | Cmd |C|B|A|I|R|K|E|                 Step count                 | Status
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Cmd: the code for the currently running command, or 000 if idle.
--   C: falling edge setup for aresetn_phy in UART mode
--   B: falling edge setup for ctrl_f2a in UART mode
--   A: falling edge sample for ctrl_a2f in UART mode
--   I: ifsel; 1=HSI, 0=UART
--   R: ratio; 1=1:2/fast UART, 0=1:4/reliable UART
--   K: whether the ASIC clock is currently running
--   E: event flag state to wait for
--   Step count: steps remaining if stepping, steps done while waiting for
--     event during or after wait command, undefined otherwise
-- 
--   Note: if there is no HSI lock or the control logic is otherwise under
--   reset, this register reads as 0xDEADC0DE. 


entity ip_hsi is
  generic (
    
    -- ASIC memory remapping. The lower address space of the r-VEX is mapped
    -- into C_M_AXI_TARGET_MEMORY using write-back read- and write-allocate
    -- cache mode. The upper address space is mapped into C_M_AXI_TARGET_PERIPH
    -- using device bufferable cache mode. Out-of-range accesses return bus
    -- fault 1. AXI response codes 2 and 3 return bus fault 2 and 3
    -- respectively.
    C_M_AXI_TARGET_MEMORY_BASE  : std_logic_vector := X"20000000";
    C_M_AXI_TARGET_MEMORY_MASK  : std_logic_vector := X"0FFFFFFF";
    C_M_AXI_TARGET_PERIPH_BASE  : std_logic_vector := X"40000000";
    C_M_AXI_TARGET_PERIPH_MASK  : std_logic_vector := X"1FFFFFFF";
    
    -- "BUFG" to use global clock buffers, "BUFR" to use regional clock buffers.
    CLK_BUF_TYPE                : string := "BUFR";
    
    -- Whether the HSI LVDS data pairs should be inverted. This is necessary for
    -- communicating with the ASIC socket on the current PCB revision because it
    -- was significantly easier to route that way.
    INVERT_HSI_PAIRS            : boolean := true;
    
    -- How HSI delay calibration is done.
    --  - NONE: delay calibration procedure is skipped entirely.
    --  - ASIC: the calibration waveform is generated, but the FPGA doesn't do
    --          anything other than waiting for the lock handshake from the
    --          ASIC.
    --  - BOTH: delay calibration is done on both ASIC and FPGA.
    --  - NO_IDELAYCTRL: same as BOTH, but the IDELAYCTRL primitive is not
    --          instantiated here (and must be instantiated elsewhere).
    DELAY_CAL_MODE              : string := "ASIC";
    
    -- Whether or not a clock domain crossing should be inserted in the memory
    -- access path of the ASIC. If this is disabled, the latency/bandwidth is
    -- potentially improved, but m_axi_aclk MUST then use m_axi_aclk_out and
    -- m_axi_aresetn_out. It is recommended to do this if you're putting an AXI
    -- L2 cache in the path, so the CDC to the PS clock domains can be behind
    -- the cache.
    ENABLE_XCLK                 : boolean := true;
    
    -- Length of a scan chain.
    SCAN_LENGTH                 : integer range 1 to 2048 := 2016;
    
    -- Amount to divide the core clock by when scanning.
    SCAN_CLK_DIV                : integer range 1 to 256 := 10
    
  );
  port (
    
    -- AXI slave interface. Must run at 100MHz! Clock is used as a reference
    -- for the MMCM and to generate the UART baudrate.
    s_axi_aclk                  : in  std_logic;
    s_axi_aresetn               : in  std_logic;
    
    s_axi_awaddr                : in  std_logic_vector(14 downto 0);
    s_axi_awvalid               : in  std_logic;
    s_axi_awready               : out std_logic;
    
    s_axi_wdata                 : in  std_logic_vector(31 downto 0);
    s_axi_wstrb                 : in  std_logic_vector(3 downto 0);
    s_axi_wvalid                : in  std_logic;
    s_axi_wready                : out std_logic;
    
    s_axi_bresp                 : out std_logic_vector(1 downto 0);
    s_axi_bvalid                : out std_logic;
    s_axi_bready                : in  std_logic;
    
    s_axi_araddr                : in  std_logic_vector(14 downto 0);
    s_axi_arvalid               : in  std_logic;
    s_axi_arready               : out std_logic;
    
    s_axi_rdata                 : out std_logic_vector(31 downto 0);
    s_axi_rresp                 : out std_logic_vector(1 downto 0);
    s_axi_rvalid                : out std_logic;
    s_axi_rready                : in  std_logic;
    
    -- AXI master interface.
    m_axi_aclk                  : in  std_logic;
    m_axi_aresetn               : in  std_logic;
    
    m_axi_awaddr                : out std_logic_vector(31 downto 0);
    m_axi_awsize                : out std_logic_vector(2 downto 0);
    m_axi_awcache               : out std_logic_vector(3 downto 0);
    m_axi_awvalid               : out std_logic;
    m_axi_awready               : in  std_logic;
    
    m_axi_wdata                 : out std_logic_vector(31 downto 0);
    m_axi_wstrb                 : out std_logic_vector(3 downto 0);
    m_axi_wvalid                : out std_logic;
    m_axi_wready                : in  std_logic;
    
    m_axi_bresp                 : in  std_logic_vector(1 downto 0);
    m_axi_bvalid                : in  std_logic;
    m_axi_bready                : out std_logic;
    
    m_axi_araddr                : out std_logic_vector(31 downto 0);
    m_axi_arsize                : out std_logic_vector(2 downto 0);
    m_axi_arcache               : out std_logic_vector(3 downto 0);
    m_axi_arvalid               : out std_logic;
    m_axi_arready               : in  std_logic;
    
    m_axi_rdata                 : in  std_logic_vector(31 downto 0);
    m_axi_rresp                 : in  std_logic_vector(1 downto 0);
    m_axi_rvalid                : in  std_logic;
    m_axi_rready                : out std_logic;
    
    -- AXI master interface clock/reset output in case ENABLE_XCLK is set to
    -- false.
    m_axi_aclk_out              : out std_logic;
    m_axi_aresetn_out           : out std_logic;
    
    -- HSI interface.
    asic_hsi_aresetn_phy        : out std_logic;
    asic_hsi_aresetn_core       : out std_logic;
    asic_hsi_backup_clk         : out std_logic;
    asic_hsi_clk_p              : out std_logic;
    asic_hsi_clk_n              : out std_logic;
    asic_hsi_f2a_p              : out std_logic_vector(3 downto 0);
    asic_hsi_f2a_n              : out std_logic_vector(3 downto 0);
    asic_hsi_a2f_p              : in  std_logic_vector(3 downto 0);
    asic_hsi_a2f_n              : in  std_logic_vector(3 downto 0);
    asic_hsi_mode               : out std_logic;
    asic_hsi_ctrl_f2a           : out std_logic;
    asic_hsi_ctrl_a2f           : in  std_logic;
    
    -- Teensy power register interface.
    teensy_tx                   : out std_logic;
    teensy_rx                   : in  std_logic;
    
    -- Interrupt requests for the ASIC.
    asic_irq                    : in  std_logic_vector(15 downto 0)
    
  );
end ip_hsi;

architecture Behavioral of ip_hsi is
  
  -- MMCM dynamic reconfiguration port.
  signal mmcm_reset           : std_logic;
  signal mmcm_en              : std_logic;
  signal mmcm_we              : std_logic;
  signal mmcm_addr            : std_logic_vector(6 downto 0);
  signal mmcm_di              : std_logic_vector(15 downto 0);
  signal mmcm_rdy             : std_logic;
  signal mmcm_do              : std_logic_vector(15 downto 0);
  
  -- HSI clock stuff.
  signal mmcm_locked          : std_logic;
  signal clk_hsi              : std_logic;
  signal clk_hsi_out          : std_logic;
  signal clk_hsi_in           : std_logic;
  signal clk_core             : std_logic;
  signal reset_hsi            : std_logic;
  signal reset_core           : std_logic;
  signal reset_ctrl           : std_logic;
  
  -- Clock monitors. These are 16-bit gray-code counters that increment every
  -- 16384th clock cycle of their monitored clock. They can be read out as
  -- status registers to allow the actual clocks to be derived from software.
  -- This is mainly for debugging/verifying the MMCM configuration.
  signal s_axi_aclk_mon       : std_logic_vector(15 downto 0);
  signal clk_core_mon         : std_logic_vector(15 downto 0);
  signal clk_hsi_mon          : std_logic_vector(15 downto 0);
  signal clk_ref_mon          : std_logic_vector(15 downto 0);
  
  -- HSI GPIO signals.
  signal hsi_gpio_configWord  : std_logic_vector(15 downto 0);
  signal hsi_gpio_idle        : std_logic_vector(3 downto 0);
  signal hsi_gpio_done        : std_logic_vector(3 downto 0);
  
  -- Debug bus to HSI control register, clk_core domain.
  signal hsi_ctrl_m2s         : bus_mst2slv_type;
  signal hsi_ctrl_s2m         : bus_slv2mst_type;
  
  -- Debug bus to HSI bridge, clk_core domain.
  signal hsi_debug_f2a        : bus_mst2slv_type;
  signal hsi_debug_a2f        : bus_slv2mst_type;
  
  -- Memory/peripheral bus from HSI bridge, clk_core domain.
  signal hsi_bus_a2f          : bus_mst2slv_type;
  signal hsi_bus_f2a          : bus_slv2mst_type;
  
  -- HSI control signals.
  signal aresetn_core         : std_logic;
  
  -- Scan chain memory, HSI side (clk_core domain).
  signal scan_init            : std_logic;
  signal scan_chain           : std_logic_vector(3 downto 0);
  signal scan_a2f             : std_logic;
  signal scan_a2f_valid       : std_logic;
  signal scan_f2a             : std_logic;
  signal scan_next            : std_logic;
  
  -- Scan chain memory, AXI side (s_axi_aclk domain).
  signal scan_axi_ena         : std_logic;
  signal scan_axi_wstrb       : std_logic_vector(3 downto 0);
  signal scan_axi_addr        : std_logic_vector(9 downto 0);
  signal scan_axi_wdata       : std_logic_vector(31 downto 0);
  signal scan_axi_rdata       : std_logic_vector(31 downto 0);
  signal scan_axi_read_ack    : std_logic;
  
  -- Whether the HSI bridge is currently ready for debug bus accesses.
  signal hsi_bridge_if_up     : std_logic;
  
  -- Teensy interface. cmd is a strobe signal sent to start a command, along
  -- with addr, write, and wdata (if write is high). addr and wdata must stay
  -- valid throughout the transaction. The Teensy interface block responds with
  -- a strobe on ack with valid rdata if the command was a read, or with a
  -- strobe on error if a communication error or ~2ms timeout occurs.
  signal teensy_cmd           : std_logic;
  signal teensy_addr          : std_logic_vector(10 downto 0);
  signal teensy_write         : std_logic;
  signal teensy_wdata         : std_logic_vector(15 downto 0);
  signal teensy_ack           : std_logic;
  signal teensy_rdata         : std_logic_vector(15 downto 0);
  signal teensy_error         : std_logic;
  
begin
  
  ------------------------------------------------------------------------------
  -- AXI slave process
  ------------------------------------------------------------------------------
  axi_slave_block: block is
    
    -- FSM state.
    type state_t is (AS_IDLE, AS_READ, AS_RRESP, AS_WRITE, AS_GETWDATA, AS_BRESP);
    signal state  : state_t;
    
    -- Selected device.
    type dev_t is (DEV_MMCM, DEV_POWER, DEV_SCAN, DEV_STAT, DEV_CTRL, DEV_BRIDGE);
    signal dev    : dev_t;
    
    -- Selected address.
    signal addr   : std_logic_vector(14 downto 0);
    
    -- Clock domain crossing signals.
    signal cdc_reset        : std_logic;
    signal cdc_axi_inCtrl   : std_logic;                     -- AXI domain is in control of the CDC
    signal cdc_axi_release  : std_logic;                     -- AXI domain is releasing control so core domain can handle command
    signal cdc_axi_wmask    : std_logic_vector(3 downto 0);  -- Write mask; read if zero
    signal cdc_axi_wdata    : std_logic_vector(31 downto 0); -- Write data; unused for reads
    signal cdc_axi_ctrl     : std_logic;                     -- Access control register instead of bridge
    signal cdc_axi_addr     : std_logic_vector(13 downto 2); -- Bridge address; unused for control register
    signal cdc_axi_rdata    : std_logic_vector(31 downto 0); -- Read data
    signal cdc_hsi_inCtrl   : std_logic;                     -- Command is valid in the HSI domain
    signal cdc_hsi_inCtrl_r : std_logic;                     -- Command has been valid for at least one cycle
    signal cdc_hsi_release  : std_logic;                     -- HSI domain is has finished command
    signal cdc_hsi_wmask    : std_logic_vector(3 downto 0);  -- Write mask; read if zero
    signal cdc_hsi_wdata    : std_logic_vector(31 downto 0); -- Write data; unused for reads
    signal cdc_hsi_ctrl     : std_logic;                     -- Access control register instead of bridge
    signal cdc_hsi_addr     : std_logic_vector(13 downto 2); -- Bridge address; unused for control register
    signal cdc_hsi_rdata    : std_logic_vector(31 downto 0); -- Read data
    signal cdc_hsi_rdata_r  : std_logic_vector(31 downto 0); -- Registered read data for the CDC
    
  begin
    
    -- Clocked control process.
    axi_slave_proc: process (s_axi_aclk, s_axi_aresetn) is
      variable state_v  : state_t;
      variable dev_v    : dev_t;
      variable addr_v   : std_logic_vector(14 downto 0);
    begin
      
      -- Clock.
      if rising_edge(s_axi_aclk) then
        
        -- Load current state into variables for convenience.
        state_v  := state;
        dev_v    := dev;
        addr_v   := addr;
        
        -- Set defaults.
        s_axi_awready   <= '0';
        s_axi_wready    <= '0';
        s_axi_arready   <= '0';
        mmcm_en         <= '0';
        mmcm_we         <= '0';
        cdc_axi_release <= '0';
        scan_axi_ena    <= '0';
        scan_axi_wstrb  <= "0000";
        teensy_cmd      <= '0';
        
        case state is
          
          when AS_IDLE =>
            
            -- Handle reads.
            if s_axi_arvalid = '1' then
              s_axi_arready <= '1';
              addr_v := s_axi_araddr;
              state_v := AS_READ;
            end if;
          
            -- Handle writes.
            if s_axi_awvalid = '1' then
              s_axi_awready <= '1';
              addr_v := s_axi_awaddr;
              state_v := AS_GETWDATA;
            end if;
            
            -- Figure out the device that belongs to the selected address.
            if addr_v(14) = '1' then
              dev_v := DEV_BRIDGE;
            elsif addr_v(13) = '0' then
              dev_v := DEV_POWER;
            elsif addr_v(12) = '0' then
              dev_v := DEV_SCAN;
            elsif addr_v(11) = '0' then
              dev_v := DEV_MMCM;
            elsif addr_v(10) = '0' then
              dev_v := DEV_STAT;
            else
              dev_v := DEV_CTRL;
            end if;
            
            -- Issue reads to the devices.
            if state_v = AS_READ then case dev_v is
              
              when DEV_MMCM =>
                -- Read from the MMCM DRP.
                mmcm_en         <= '1';
                mmcm_addr       <= addr_v(8 downto 2);
              
              when DEV_POWER =>
                -- Send the read command to the Teensy over the UART.
                teensy_cmd      <= '1';
                teensy_addr     <= addr_v(12 downto 2);
                teensy_write    <= '0';
                
              when DEV_SCAN =>
                -- Read from the scan chain block RAM.
                scan_axi_ena    <= '1';
                scan_axi_wstrb  <= "0000";
                scan_axi_addr   <= addr_v(11 downto 2);
              
              when DEV_STAT =>
                -- Read status signals immediately.
                s_axi_rdata <= (others => '0');
                s_axi_rresp <= "00";
                s_axi_rvalid <= '1';
                state_v := AS_RRESP;
                
                case addr_v(3 downto 2) is
                  
                  when "01" =>
                    -- Core clock monitor.
                    s_axi_rdata(31 downto 16) <= s_axi_aclk_mon;
                    s_axi_rdata(15 downto  0) <= clk_core_mon;
                  
                  when "10" =>
                    -- HSI clock monitor.
                    s_axi_rdata(31 downto 16) <= s_axi_aclk_mon;
                    s_axi_rdata(15 downto  0) <= clk_hsi_mon;
                  
                  when "11" =>
                    -- 200MHz reference clock monitor.
                    s_axi_rdata(31 downto 16) <= s_axi_aclk_mon;
                    s_axi_rdata(15 downto  0) <= clk_ref_mon;
                  
                  when others =>
                    -- "GPIO" signals transferred over the HSI.
                    s_axi_rdata(15 downto 0)  <= hsi_gpio_configWord;
                    s_axi_rdata(19 downto 16) <= hsi_gpio_idle;
                    s_axi_rdata(23 downto 20) <= hsi_gpio_done;
                    
                    -- Clock/reset status.
                    s_axi_rdata(27) <= mmcm_reset;
                    s_axi_rdata(28) <= reset_hsi;
                    s_axi_rdata(29) <= reset_core;
                    s_axi_rdata(30) <= reset_ctrl;
                    s_axi_rdata(31) <= mmcm_locked;
                  
                end case;
                
              when DEV_CTRL =>
                -- Give the read control register command.
                cdc_axi_wmask   <= "0000";
                cdc_axi_ctrl    <= '1';
                cdc_axi_release <= '1';
              
              when DEV_BRIDGE =>
                
                -- Give the read from bridge command.
                cdc_axi_wmask   <= "0000";
                cdc_axi_ctrl    <= '0';
                cdc_axi_addr    <= addr_v(13 downto 2);
                cdc_axi_release <= '1';
                
            end case; end if;
            
          when AS_READ =>
            
            -- Wait for read data from the devices.
            case dev_v is
              
              when DEV_MMCM =>
                if mmcm_rdy = '1' then
                  s_axi_rdata <= X"0000" & mmcm_do;
                  s_axi_rresp <= "00";
                  s_axi_rvalid <= '1';
                  state_v := AS_RRESP;
                end if;
              
              when DEV_POWER =>
                if teensy_ack = '1' or teensy_error = '1' then
                  if teensy_error = '1' then
                    s_axi_rdata <= X"DEADC0DE";
                  else
                    s_axi_rdata <= X"0000" & teensy_rdata;
                  end if;
                  s_axi_rresp <= "00";
                  s_axi_rvalid <= '1';
                  state_v := AS_RRESP;
                end if;
              
              when DEV_SCAN =>
                if scan_axi_read_ack = '1' then
                  s_axi_rdata <= scan_axi_rdata;
                  s_axi_rresp <= "00";
                  s_axi_rvalid <= '1';
                  state_v := AS_RRESP;
                end if;
              
              when DEV_CTRL =>
                if cdc_axi_release = '0' and cdc_axi_inCtrl = '1' then
                  s_axi_rdata <= cdc_axi_rdata;
                  s_axi_rresp <= "00";
                  s_axi_rvalid <= '1';
                  state_v := AS_RRESP;
                end if;
              
              when DEV_BRIDGE =>
                if cdc_axi_release = '0' and cdc_axi_inCtrl = '1' then
                  s_axi_rdata <= cdc_axi_rdata;
                  s_axi_rresp <= "00";
                  s_axi_rvalid <= '1';
                  state_v := AS_RRESP;
                end if;
              
              when others =>
                s_axi_rdata <= scan_axi_rdata;
                s_axi_rresp <= "11";
                s_axi_rvalid <= '1';
                state_v := AS_RRESP;
              
            end case;
            
          when AS_RRESP =>
            
            -- Wait for read response channel ready.
            if s_axi_rready = '1' then
              s_axi_rvalid <= '0';
              state_v := AS_IDLE;
            end if;
            
          when AS_GETWDATA =>
            
            -- Get write data.
            if s_axi_wvalid = '1' then
              s_axi_wready <= '1';
              state_v := AS_WRITE;
            end if;
            
            -- Issue writes to the devices.
            if state_v = AS_WRITE then case dev_v is
              
              when DEV_MMCM =>
                -- Write to the MMCM DRP.
                mmcm_reset      <= '1';
                mmcm_en         <= '1';
                mmcm_we         <= '1';
                mmcm_addr       <= addr_v(8 downto 2);
                mmcm_di         <= s_axi_wdata(15 downto 0);
              
              when DEV_POWER =>
                -- Send the write command to the Teensy over the UART.
                teensy_cmd      <= '1';
                teensy_addr     <= addr_v(12 downto 2);
                teensy_write    <= '1';
                teensy_wdata    <= s_axi_wdata(15 downto 0);
                
              when DEV_SCAN =>
                -- Write to the scan chain block RAM.
                scan_axi_ena    <= '1';
                scan_axi_addr   <= addr_v(11 downto 2);
                scan_axi_wstrb  <= s_axi_wstrb;
                scan_axi_wdata  <= s_axi_wdata;
              
              when DEV_STAT =>
                -- Clear the MMCM reset flag.
                mmcm_reset      <= '0';
              
              when DEV_CTRL =>
                -- Give the write control register command.
                cdc_axi_wmask   <= s_axi_wstrb;
                cdc_axi_wdata   <= s_axi_wdata;
                cdc_axi_ctrl    <= '1';
                cdc_axi_release <= '1';
              
              when DEV_BRIDGE =>
                -- Give the write to bridge command.
                cdc_axi_wmask   <= s_axi_wstrb;
                cdc_axi_wdata   <= s_axi_wdata;
                cdc_axi_ctrl    <= '0';
                cdc_axi_addr    <= addr_v(13 downto 2);
                cdc_axi_release <= '1';
              
            end case; end if;
          
          when AS_WRITE =>
            
            -- Wait for write response from the devices.
            case dev_v is
              
              when DEV_MMCM =>
                if mmcm_rdy = '1' then
                  s_axi_bresp <= "00";
                  s_axi_bvalid <= '1';
                  state_v := AS_BRESP;
                end if;
              
              when DEV_POWER =>
                if teensy_ack = '1' or teensy_error = '1' then
                  s_axi_bresp <= "00";
                  s_axi_bvalid <= '1';
                  state_v := AS_BRESP;
                end if;
              
              when DEV_CTRL =>
                if cdc_axi_release = '0' and cdc_axi_inCtrl = '1' then
                  s_axi_bresp <= "00";
                  s_axi_bvalid <= '1';
                  state_v := AS_BRESP;
                end if;
              
              when DEV_BRIDGE =>
                if cdc_axi_release = '0' and cdc_axi_inCtrl = '1' then
                  s_axi_bresp <= "00";
                  s_axi_bvalid <= '1';
                  state_v := AS_BRESP;
                end if;
              
              when others =>
                s_axi_bresp <= "00";
                s_axi_bvalid <= '1';
                state_v := AS_BRESP;
              
            end case;
            
          when AS_BRESP =>
            
            -- Wait for write response channel ready.
            if s_axi_bready = '1' then
              s_axi_bvalid <= '0';
              state_v := AS_IDLE;
            end if;
            
        end case;
        
        -- Save the state.
        state <= state_v;
        dev   <= dev_v;
        addr  <= addr_v;
        
      end if;
      
      -- Handle asynchronous reset.
      if s_axi_aresetn = '0' then
        
        -- Reset state.
        state <= AS_IDLE;
        
        -- Reset output ports.
        s_axi_awready   <= '0';
        s_axi_wready    <= '0';
        s_axi_bresp     <= "00";
        s_axi_bvalid    <= '0';
        s_axi_arready   <= '0';
        s_axi_rdata     <= X"00000000";
        s_axi_rresp     <= "00";
        s_axi_rvalid    <= '0';
        mmcm_reset      <= '0';
        mmcm_en         <= '0';
        mmcm_we         <= '0';
        cdc_axi_release <= '0';
        scan_axi_ena    <= '0';
        scan_axi_wstrb  <= "0000";
        teensy_cmd      <= '0';
        
      end if;
      
    end process;
    
    -- Reset the clock domain crossing when the core MMCM is not locked or the
    -- AXI bus is resetting.
    cdc_reset <= not (s_axi_aresetn and mmcm_locked);
    
    -- CDC synchronization unit for commands to the HSI bridge and HSI control
    -- register.
    cdc_inst: entity work.ip_hsi_utils_sync
      generic map (
        NUM_STAGES    => 2
      )
      port map (
        reset         => cdc_reset,
        
        a_clk         => s_axi_aclk,
        a_inControl   => cdc_axi_inCtrl,
        a_release     => cdc_axi_release,
        
        b_clk         => clk_core,
        b_inControl   => cdc_hsi_inCtrl,
        b_release     => cdc_hsi_release
        
      );
    
    -- HSI bridge/control synchronization registers.
    cdc_hsi_regs: process (clk_core, cdc_reset) is
    begin
      if rising_edge(clk_core) then
        cdc_hsi_wmask <= cdc_axi_wmask;
        cdc_hsi_wdata <= cdc_axi_wdata;
        cdc_hsi_ctrl  <= cdc_axi_ctrl;
        cdc_hsi_addr  <= cdc_axi_addr;
        cdc_hsi_inCtrl_r <= cdc_hsi_inCtrl;
        if cdc_hsi_release = '1' then
          cdc_hsi_rdata_r <= cdc_hsi_rdata;
        end if;
      end if;
      if cdc_reset = '1' then
        cdc_hsi_wmask <= "0000";
        cdc_hsi_wdata <= X"00000000";
        cdc_hsi_ctrl  <= '0';
        cdc_hsi_addr  <= (others => '0');
        cdc_hsi_inCtrl_r <= '0';
        cdc_hsi_rdata_r <= X"DEADC0DE";
      end if;
    end process;
    cdc_axi_regs: process (s_axi_aclk, cdc_reset) is
    begin
      if rising_edge(s_axi_aclk) then
        cdc_axi_rdata <= cdc_hsi_rdata_r;
      end if;
      if cdc_reset = '1' then
        cdc_axi_rdata <= X"DEADC0DE";
      end if;
    end process;
    
    -- Combinatorial r-VEX bus stuff in the core domain.
    rvex_bus_proc: process (
      cdc_hsi_inCtrl, cdc_hsi_inCtrl_r, reset_ctrl, hsi_bridge_if_up,
      cdc_hsi_wmask, cdc_hsi_wdata, cdc_hsi_ctrl, cdc_hsi_addr,
      hsi_ctrl_s2m, hsi_debug_a2f
    ) is
    begin
      
      -- Set default bus values.
      hsi_ctrl_m2s <= BUS_MST2SLV_IDLE;
      hsi_ctrl_m2s.writeMask <= "1111";
      hsi_ctrl_m2s.writeData <= cdc_hsi_wdata;
      
      hsi_debug_f2a <= BUS_MST2SLV_IDLE;
      hsi_debug_f2a.writeMask <= cdc_hsi_wmask;
      hsi_debug_f2a.writeData <= cdc_hsi_wdata;
      hsi_debug_f2a.address(13 downto 2) <= cdc_hsi_addr;
      
      cdc_hsi_rdata <= X"DEADC0DE";
      cdc_hsi_release <= '0';
      
      if cdc_hsi_inCtrl = '1' then
        if cdc_hsi_ctrl = '1' then
          
          -- Issue commands to control register.
          if cdc_hsi_wmask = "1111" then
            hsi_ctrl_m2s.writeEnable <= '1';
          else
            hsi_ctrl_m2s.readEnable <= '1';
          end if;
          
          -- If the control register is not ready for commands, don't even issue
          -- the command.
          if cdc_hsi_inCtrl_r = '0' and reset_ctrl = '1' then
            hsi_ctrl_m2s.writeEnable <= '0';
            hsi_ctrl_m2s.readEnable <= '0';
            cdc_hsi_rdata <= X"DEADC0DE";
            cdc_hsi_release <= '1';
          end if;
          
          -- Handle response from control register. If the bus releases busy
          -- without asserting ack (this is illegal) return with a fault marker.
          -- This happens if the bus is reset unexpectedly.
          if cdc_hsi_inCtrl_r = '1' and (hsi_ctrl_s2m.busy = '0' or reset_ctrl = '1') then
            hsi_ctrl_m2s.writeEnable <= '0';
            hsi_ctrl_m2s.readEnable <= '0';
            if hsi_ctrl_s2m.ack = '1' then
              cdc_hsi_rdata <= hsi_ctrl_s2m.readData;
            else
              cdc_hsi_rdata <= X"DEADC0DE";
            end if;
            cdc_hsi_release <= '1';
          end if;
          
        else
          
          -- Issue commands to bridge.
          if cdc_hsi_wmask /= "0000" then
            hsi_debug_f2a.writeEnable <= '1';
          else
            hsi_debug_f2a.readEnable <= '1';
          end if;
          
          -- If the bridge is not ready for commands, don't even issue the
          -- command.
          if cdc_hsi_inCtrl_r = '0' and hsi_bridge_if_up = '0' then
            hsi_debug_f2a.writeEnable <= '0';
            hsi_debug_f2a.readEnable <= '0';
            cdc_hsi_rdata <= X"DEADC0DE";
            cdc_hsi_release <= '1';
          end if;
          
          -- Handle response from bridge. If the bus releases busy without
          -- asserting ack (this is illegal) return with a fault marker. This
          -- happens if the bus is reset unexpectedly. Also, if the bridge
          -- somehow becomes unready during the command, we also cancel,
          -- violating the r-VEX bus spec. This should never happen in practice,
          -- though, since the only proper way to disable the interface is
          -- through a bus command which in the end goes through the same
          -- AXI-lite interface.
          if cdc_hsi_inCtrl_r = '1' and (hsi_debug_a2f.busy = '0' or hsi_bridge_if_up = '0') then
            hsi_debug_f2a.writeEnable <= '0';
            hsi_debug_f2a.readEnable <= '0';
            if hsi_debug_a2f.ack = '1' then
              cdc_hsi_rdata <= hsi_debug_a2f.readData;
            else
              cdc_hsi_rdata <= X"DEADC0DE";
            end if;
            cdc_hsi_release <= '1';
          end if;
          
        end if;
      end if;
      
    end process;
    
  end block;
  
  ------------------------------------------------------------------------------
  -- AXI master process
  ------------------------------------------------------------------------------
  -- TODO: this is currently really slow. It's adding lots of latency to the
  -- already slow HSI for pretty much no reason, especially for instruction
  -- accesses, which could be bursted. What we want here is a burst-aware AXI
  -- bridge in the core clock domain, connected to a system cache IP block or
  -- something.
  axi_master_block: block is
    
    -- FSM state.
    type state_t is (AS_IDLE, AS_READ, AS_WRITE);
    signal state  : state_t;
    
    -- Whether the address/data/response channel beats are currently pending. 
    signal addr_pend    : std_logic;
    signal data_pend    : std_logic;
    signal resp_pend    : std_logic;
    
    -- The HSI to memory bus in the AXI clock domain.
    signal axi_bus_a2f  : bus_mst2slv_type;
    signal axi_bus_f2a  : bus_slv2mst_type;
    
  begin
    
    -- Clocked control process.
    axi_master_proc: process (m_axi_aclk, m_axi_aresetn) is
      type dev_t is (DEV_MEM, DEV_PERIPH, DEV_OOR);
      variable dev : dev_t;
      variable addr_pend_v : std_logic;
      variable data_pend_v : std_logic;
      variable resp_pend_v : std_logic;
    begin
      
      if rising_edge(m_axi_aclk) then
        
        -- Set output defaults.
        axi_bus_f2a.ack     <= '0';
        axi_bus_f2a.fault   <= '0';
        
        -- The r-VEX bus busy signal is just a register and an OR gate, except
        -- when ack is asserted. So when we acknowledge we also override busy
        -- low.
        axi_bus_f2a.busy <= bus_requesting(axi_bus_a2f);
        
        -- Load pend state.
        addr_pend_v := addr_pend;
        data_pend_v := data_pend;
        resp_pend_v := resp_pend;
        
        -- Handle states.
        case state is
          
          when AS_IDLE =>
            
            -- Decode the incoming address.
            if axi_bus_a2f.address(31) = '0' then
              if (axi_bus_a2f.address and not C_M_AXI_TARGET_MEMORY_MASK) = X"00000000" then
                dev := DEV_MEM;
              else
                dev := DEV_OOR;
              end if;
            else
              if (axi_bus_a2f.address and not C_M_AXI_TARGET_PERIPH_MASK) = X"80000000" then
                dev := DEV_PERIPH;
              else
                dev := DEV_OOR;
              end if;
            end if;
            
            -- Translate the address and set cache mode.
            if dev = DEV_MEM then
              m_axi_araddr  <= (axi_bus_a2f.address and C_M_AXI_TARGET_MEMORY_MASK) or C_M_AXI_TARGET_MEMORY_BASE;
              m_axi_arcache <= "1111";
              m_axi_awaddr  <= (axi_bus_a2f.address and C_M_AXI_TARGET_MEMORY_MASK) or C_M_AXI_TARGET_MEMORY_BASE;
              m_axi_awcache <= "1111";
            elsif dev = DEV_PERIPH then
              m_axi_araddr  <= (axi_bus_a2f.address and C_M_AXI_TARGET_PERIPH_MASK) or C_M_AXI_TARGET_PERIPH_BASE;
              m_axi_arcache <= "0001";
              m_axi_awaddr  <= (axi_bus_a2f.address and C_M_AXI_TARGET_PERIPH_MASK) or C_M_AXI_TARGET_PERIPH_BASE;
              m_axi_awcache <= "0001";
            end if;
            
            -- Handle read/write commands.
            if dev /= DEV_OOR then
              if axi_bus_a2f.readEnable = '1' then
                
                -- Issue AXI command.
                m_axi_arvalid <= '1';
                addr_pend_v   := '1';
                
                -- Wait for the response.
                m_axi_rready  <= '1';
                resp_pend_v   := '1';
                state         <= AS_READ;
                
              elsif axi_bus_a2f.writeEnable = '1' then
                
                -- Issue AXI command.
                m_axi_awvalid <= '1';
                addr_pend_v   := '1';
                
                -- Send the write data.
                m_axi_wdata   <= axi_bus_a2f.writeData;
                m_axi_wstrb   <= axi_bus_a2f.writeMask;
                m_axi_wvalid  <= '1';
                data_pend_v   := '1';
                
                -- Wait for the response.
                m_axi_bready  <= '1';
                resp_pend_v   := '1';
                state         <= AS_WRITE;
                
              end if;
            else
              
              -- Acknowledge immediately with a fault.
              if bus_requesting(axi_bus_a2f) = '1' then
                axi_bus_f2a.busy      <= '0';
                axi_bus_f2a.ack       <= '1';
                axi_bus_f2a.fault     <= '1';
                axi_bus_f2a.readData  <= X"DEADC0DE";
              end if;
              
            end if;
          
          when AS_READ =>
            
            -- Handle read address channel.
            if m_axi_arready = '1' then
              m_axi_arvalid <= '0';
              addr_pend_v := '0';
            end if;
            
            -- Handle read channel. 
            if m_axi_rvalid = '1' then
              m_axi_rready <= '0';
              resp_pend_v := '0';
              
              -- Acknowledge r-VEX bus.
              axi_bus_f2a.busy  <= '0';
              axi_bus_f2a.ack   <= '1';
              axi_bus_f2a.fault <= m_axi_rresp(1);
              if m_axi_rresp(1) = '1' then
                axi_bus_f2a.readData <= X"0000000"&"00" & m_axi_rresp;
              else
                axi_bus_f2a.readData <= m_axi_rdata;
              end if;
              
            end if;
            
            -- Return to idle when all handshakes complete.
            if addr_pend_v = '0' and resp_pend_v = '0' then
              state <= AS_IDLE;
            end if;
          
          when AS_WRITE =>
            
            -- Handle write address channel.
            if m_axi_awready = '1' then
              m_axi_awvalid <= '0';
              addr_pend_v := '0';
            end if;
            
            -- Handle write data channel.
            if m_axi_wready = '1' then
              m_axi_wvalid <= '0';
              data_pend_v := '0';
            end if;
            
            -- Handle read channel. 
            if m_axi_bvalid = '1' then
              m_axi_bready <= '0';
              resp_pend_v := '0';
              
              -- Acknowledge r-VEX bus.
              axi_bus_f2a.busy     <= '0';
              axi_bus_f2a.ack      <= '1';
              axi_bus_f2a.fault    <= m_axi_bresp(1);
              axi_bus_f2a.readData <= X"0000000"&"00" & m_axi_bresp;
              
            end if;
            
            -- Return to idle when all handshakes complete.
            if addr_pend_v = '0' and data_pend_v = '0' and resp_pend_v = '0' then
              state <= AS_IDLE;
            end if;
            
        end case;
        
        -- Save pend states.
        addr_pend <= addr_pend_v;
        data_pend <= data_pend_v;
        resp_pend <= resp_pend_v;
        
      end if;
      
      -- Handle reset.
      if m_axi_aresetn = '0' then
        
        -- Reset bus registers.
        m_axi_awvalid <= '0';
        m_axi_wvalid  <= '0';
        m_axi_bready  <= '0';
        m_axi_arvalid <= '0';
        m_axi_rready  <= '0';
        
        -- Initialize r-VEX bus record.
        axi_bus_f2a <= BUS_SLV2MST_IDLE;
        
        -- Reset state.
        state <= AS_IDLE;
        addr_pend <= '0';
        data_pend <= '0';
        resp_pend <= '0';
        
      end if;
      
    end process;
    
    -- The master AXI bus always does 32-bit word accesses.
    m_axi_awsize <= "010";
    m_axi_arsize <= "010";
    
    -- Instantiate CDC...
    with_cdc_gen: if ENABLE_XCLK generate
      
      -- Reset signal for the CDC.
      signal cdc_reset    : std_logic;
      
    begin
      
      -- Reset the clock domain crossing when the core MMCM is not locked or the
      -- AXI bus is resetting.
      cdc_reset <= reset_core or not (m_axi_aresetn and mmcm_locked);
      
      cdc_inst: entity work.ip_hsi_bus_crossClock
        port map (
          reset       => cdc_reset,
          mst_clk     => clk_core,
          mst2crclk   => hsi_bus_a2f,
          crclk2mst   => hsi_bus_f2a,
          slv_clk     => m_axi_aclk,
          crclk2slv   => axi_bus_a2f,
          slv2crclk   => axi_bus_f2a
        );
    end generate;
    
    -- ... or don't instantiate it.
    without_cdc_gen: if not ENABLE_XCLK generate
    begin
      axi_bus_a2f <= hsi_bus_a2f;
      hsi_bus_f2a <= axi_bus_f2a;
    end generate;
    
    -- Forward the core clock and an appropriate reset for the AXI IP to use
    -- when the internal CDC is disabled.
    m_axi_aclk_out    <= clk_core;
    m_axi_aresetn_out <= s_axi_aresetn and mmcm_locked;
    
  end block;
  
  ------------------------------------------------------------------------------
  -- Clock and reset generation
  ------------------------------------------------------------------------------
  clkgen_block: block is
    signal clk_fb             : std_logic;
    signal clk_hsi_prebuf     : std_logic;
    signal clk_hsi_out_prebuf : std_logic;
    signal clk_hsi_in_prebuf  : std_logic;
    signal clk_core_prebuf    : std_logic;
    signal clk_ref_prebuf     : std_logic;
    signal clk_ref            : std_logic;
    signal mmcm_rst           : std_logic;
    signal mmcm_lock          : std_logic;
    signal reset_core_sr_a    : std_logic;
    signal reset_core_sr_b    : std_logic;
    signal reset_core_sr_c    : std_logic;
    signal reset_hsi_sr       : std_logic;
    signal reset_ctrl_sr      : std_logic;
    signal mon_clk_v          : std_logic_vector(3 downto 0);
    signal mon_out_v          : std_logic_vector(63 downto 0);
  begin
    
    -- MMCMs have an active-high reset.
    mmcm_rst <= mmcm_reset or not s_axi_aresetn;
    
    -- Instantiate MMCM.
    mmcm_inst: mmcme2_adv
      generic map (
        CLKIN1_PERIOD     => 10.0,      -- Clock input is 100MHz.
        CLKFBOUT_MULT_F   => 12.0,      -- Multiply by 12 to get 1.2GHz VCO.
        DIVCLK_DIVIDE     => 1,         -- No master division.
        CLKOUT0_DIVIDE_F  => 6.0,       -- Reference clock is 200 MHz.
        CLKOUT1_DIVIDE    => 6,         -- HSI internal clock is 200 MHz.
        CLKOUT2_DIVIDE    => 6,         -- HSI output clock is 200 MHz.
        CLKOUT2_PHASE     => 90.0,      -- Output clock is phase-shifted by 90 degrees.
        CLKOUT3_DIVIDE    => 6,         -- HSI input clock is 200 MHz.
        CLKOUT3_PHASE     => 180.0,     -- Input clock is phase-shifted by 180 degrees.
        CLKOUT4_DIVIDE    => 12,        -- Core clock is 100 MHz.
        COMPENSATION      => "INTERNAL" -- I think this is right...
      )
      port map (
        
        -- Control signals.
        PWRDWN            => '0',
        RST               => mmcm_rst,
        LOCKED            => mmcm_lock,
      
        -- Clock input.
        CLKIN1            => s_axi_aclk,
        CLKIN2            => '0',
        CLKINSEL          => '1',
        
        -- Feedback network.
        CLKFBOUT          => clk_fb,
        CLKFBIN           => clk_fb,
        
        -- Clock outputs.
        CLKOUT0           => clk_ref_prebuf,
        CLKOUT1           => clk_hsi_prebuf,
        CLKOUT2           => clk_hsi_out_prebuf,
        CLKOUT3           => clk_hsi_in_prebuf,
        CLKOUT4           => clk_core_prebuf,
        
        -- Dynamic reconfiguration port.
        DCLK              => s_axi_aclk,
        DEN               => mmcm_en,
        DWE               => mmcm_we,
        DADDR             => mmcm_addr,
        DI                => mmcm_di,
        DRDY              => mmcm_rdy,
        DO                => mmcm_do,
        
        -- Phase shift system, unused.
        PSCLK             => '0',
        PSEN              => '0',
        PSINCDEC          => '0'
        
      );
    
    -- Instantiate clock buffers.
    bufg_gen: if CLK_BUF_TYPE /= "BUFR" generate
    begin
      ref_buf_inst:  BUFG port map (I => clk_ref_prebuf,      O => clk_ref);
      hsi_buf_inst:  BUFG port map (I => clk_hsi_prebuf,      O => clk_hsi);
      hsio_buf_inst: BUFG port map (I => clk_hsi_out_prebuf,  O => clk_hsi_out);
      hsii_buf_inst: BUFG port map (I => clk_hsi_in_prebuf,   O => clk_hsi_in);
      core_buf_inst: BUFG port map (I => clk_core_prebuf,     O => clk_core);
    end generate;
    bufr_gen: if CLK_BUF_TYPE = "BUFR" generate
    begin
      ref_buf_inst:  BUFR port map (I => clk_ref_prebuf,      CE => '1', CLR => '0', O => clk_ref);
      hsi_buf_inst:  BUFR port map (I => clk_hsi_prebuf,      CE => '1', CLR => '0', O => clk_hsi);
      hsio_buf_inst: BUFR port map (I => clk_hsi_out_prebuf,  CE => '1', CLR => '0', O => clk_hsi_out);
      hsii_buf_inst: BUFR port map (I => clk_hsi_in_prebuf,   CE => '1', CLR => '0', O => clk_hsi_in);
      core_buf_inst: BUFR port map (I => clk_core_prebuf,     CE => '1', CLR => '0', O => clk_core);
    end generate;
    
    -- Generate IDELAYCTRL if we need one.
    delayctrl_gen: if DELAY_CAL_MODE = "BOTH" generate
      signal delay_rst  : std_logic;
      signal delay_lock : std_logic;
    begin
      
      -- Generate reset signal.
      delay_rst <= mmcm_rst or not mmcm_lock;
      
      -- Instantiate the IDELAYCTRL.
      delayctrl_inst: IDELAYCTRL
        port map (
          refclk  => clk_ref,
          rst     => delay_rst,
          rdy     => delay_lock
        );
      
      -- The MMCM lock flag is set only if both the MMCM and the IDELAYCTRL are
      -- locked.
      mmcm_locked <= mmcm_lock and delay_lock;
      
    end generate;
    no_delayctrl_gen: if DELAY_CAL_MODE /= "BOTH" generate
    begin
      mmcm_locked <= mmcm_lock;
    end generate;
    
    -- Core reset logic, same as in the ASIC.
    reset_core_proc: process (aresetn_core, clk_core) is
    begin
      if aresetn_core = '0' then
        reset_core_sr_a <= '1';
        reset_core_sr_b <= '1';
        reset_core_sr_c <= '1';
        reset_core <= '1';
      elsif rising_edge(clk_core) then
        reset_core_sr_a <= '0';
        reset_core_sr_b <= reset_core_sr_a;
        reset_core_sr_c <= reset_core_sr_b;
        reset_core <= reset_core_sr_c;
      end if;
    end process;
    
    -- HSI reset logic, same as in the ASIC.
    reset_hsi_proc: process (aresetn_core, clk_hsi) is
    begin
      if aresetn_core = '0' then
        reset_hsi_sr <= '1';
        reset_hsi <= '1';
      elsif rising_edge(clk_hsi) then
        reset_hsi_sr <= '0';
        reset_hsi <= reset_hsi_sr;
      end if;
    end process;
    
    -- Control unit reset generator.
    reset_ctrl_proc: process (s_axi_aresetn, mmcm_locked, clk_core) is
    begin
      if s_axi_aresetn = '0' or mmcm_locked = '0' then
        reset_ctrl_sr <= '1';
        reset_ctrl <= '1';
      elsif rising_edge(clk_core) then
        reset_ctrl_sr <= '0';
        reset_ctrl <= reset_ctrl_sr;
      end if;
    end process;
    
    -- Clock monitors.
    mon_clk_v(0) <= s_axi_aclk;
    mon_clk_v(1) <= clk_core;
    mon_clk_v(2) <= clk_hsi;
    mon_clk_v(3) <= clk_ref;
    
    clk_mon_gen: for i in 0 to 3 generate
      signal prescale : unsigned(13 downto 0);
      signal counter  : unsigned(15 downto 0);
      signal gray     : std_logic_vector(15 downto 0);
    begin
      
      reg_proc: process (mon_clk_v(i), s_axi_aresetn) is
        variable prescale_v : unsigned(13 downto 0);
      begin
        if rising_edge(mon_clk_v(i)) then
          prescale_v := prescale + 1;
          if prescale(13) = '1' and prescale_v(13) = '0' then
            counter <= counter + 1;
          end if;
          prescale <= prescale_v;
          gray <= std_logic_vector(counter) xor ("0" & std_logic_vector(counter(15 downto 1)));
        end if;
        if s_axi_aresetn = '0' then
          prescale <= (others => '0');
          counter  <= (others => '0');
          gray     <= (others => '0');
        end if;
      end process;
      
      mon_out_v(i*16+15 downto i*16) <= gray;
      
    end generate;
    
    s_axi_aclk_mon <= mon_out_v(15 downto  0);
    clk_core_mon   <= mon_out_v(31 downto 16);
    clk_hsi_mon    <= mon_out_v(47 downto 32);
    clk_ref_mon    <= mon_out_v(63 downto 48);
    
  end block;
  
  ------------------------------------------------------------------------------
  -- HSI logic
  ------------------------------------------------------------------------------
  hsi_logic_block: block is
    signal hsi_gpio_unused  : std_logic_vector(27 downto 24);
    
    -- Whether delay calibration should be enabled.
    constant DELAY_CAL      : boolean := (DELAY_CAL_MODE = "BOTH")
                                      or (DELAY_CAL_MODE = "NO_IDELAYCTRL");
    constant SKIP_CALIB     : boolean := (DELAY_CAL_MODE = "NONE");
    
    -- Interconnect between the I/Os and the internal logic.
    signal aresetn_phy      : std_logic;
    signal mode             : std_logic;
    signal diff_clk_ena     : std_logic;
    signal diff_clk_q       : std_logic;
    signal f2a_x, f2a_y     : std_logic_vector(3 downto 0);
    signal a2f_x, a2f_y     : std_logic_vector(3 downto 0);
    signal backup_clk_ena   : std_logic;
    signal backup_clk_high  : std_logic;
    signal backup_clk_q     : std_logic;
    signal ctrl_f2a         : std_logic;
    signal ctrl_a2f         : std_logic;
    
    -- HSI control signals.
    signal lock             : std_logic;
    signal calib            : std_logic;
    signal ifsel            : std_logic;
    signal ratio            : std_logic;
    signal uart_sample_edge : std_logic;
    signal uart_setup_edge  : std_logic_vector(1 downto 0);
    
    -- UART signals.
    signal uart_clk_ena     : std_logic;
    signal uart_rx          : std_logic;
    signal uart_rx_data     : std_logic_vector(31 downto 0);
    signal uart_rx_first    : std_logic;
    signal uart_rx_valid    : std_logic;
    signal uart_tx          : std_logic_vector(1 downto 0);
    signal uart_tx_data     : std_logic_vector(31 downto 0);
    signal uart_tx_first    : std_logic;
    signal uart_tx_valid    : std_logic;
    signal uart_tx_ack      : std_logic;
    
  begin
    
    -- Instantiate the FPGA control logic.
    ctrl_logic_inst: entity work.ip_hsi_hsi_fpga_ctrl
      generic map (
        SCAN_LENGTH               => SCAN_LENGTH,
        SCAN_PRESCALE             => SCAN_CLK_DIV - 1,
        SKIP_CALIB                => SKIP_CALIB
      )
      port map (
        
        -- System control.
        clk                       => clk_core,
        reset                     => reset_ctrl,
        
        -- Command interface.
        bus2fsm                   => hsi_ctrl_m2s,
        fsm2bus                   => hsi_ctrl_s2m,
        
        -- ASIC interface pins.
        aresetn_phy               => aresetn_phy,
        aresetn_core              => aresetn_core,
        mode                      => mode,
        ctrl_a2f                  => ctrl_a2f,
        ctrl_f2a                  => ctrl_f2a,
        
        -- Clock enable signals.
        diff_clk_ena              => diff_clk_ena,
        backup_clk_ena            => backup_clk_ena,
        backup_clk_high           => backup_clk_high,
        uart_clk_ena              => uart_clk_ena,
        
        -- Interface modes.
        ifsel                     => ifsel,
        ratio                     => ratio,
        uart_sample_edge          => uart_sample_edge,
        uart_setup_edge           => uart_setup_edge,
        
        -- HSI calibration signals for the FPGA side.
        calib                     => calib,
        lock                      => lock,
        
        -- Signals to UART receiver and transmiter.
        uart_rx                   => uart_rx,
        uart_tx                   => uart_tx,
        
        -- Scan chain memory interface.
        scan_init                 => scan_init,
        scan_chain                => scan_chain,
        scan_a2f                  => scan_a2f,
        scan_a2f_valid            => scan_a2f_valid,
        scan_f2a                  => scan_f2a,
        scan_next                 => scan_next,
        
        -- Whether the HSI bridhe interface is currently up and running.
        bridge_if_up              => hsi_bridge_if_up
        
      );
    
    -- Instantiate the logic that's common to the ASIC and FPGA sides.
    common_logic_inst: entity work.ip_hsi_hsi_common
      generic map (
        N_RX                      => 4,
        N_TX                      => 4,
        SIDE                      => "FPGA"
      )
      port map (
        
        -- System control.
        reset_hsi                 => reset_hsi,
        reset_core                => reset_core,
        clk_hsi                   => clk_hsi,
        clk_core                  => clk_core,
        ifsel                     => ifsel,
        
        -- Data inputs and outputs to HSI PHY.
        rx_x                      => a2f_x,
        rx_y                      => a2f_y,
        tx_x                      => f2a_x,
        tx_y                      => f2a_y,
        
        -- UART interface.
        uart_rx_data              => uart_rx_data,
        uart_rx_first             => uart_rx_first,
        uart_rx_valid             => uart_rx_valid,
        uart_tx_data              => uart_tx_data,
        uart_tx_first             => uart_tx_first,
        uart_tx_valid             => uart_tx_valid,
        uart_tx_ack               => uart_tx_ack,
        
        -- To r-VEX bus master interfave.
        bus_master_m2s            => hsi_debug_f2a,
        bus_master_s2m            => hsi_debug_a2f,
        
        -- To r-VEX bus slave interfave.
        bus_slave_m2s             => hsi_bus_a2f,
        bus_slave_s2m             => hsi_bus_f2a,
        
        -- GPIO data.
        gpio_in(27 downto 16)     => X"000",
        gpio_in(15 downto 0)      => asic_irq,
        gpio_out(27 downto 24)    => hsi_gpio_unused,
        gpio_out(23 downto 20)    => hsi_gpio_idle,
        gpio_out(19 downto 16)    => hsi_gpio_done,
        gpio_out(15 downto 0)     => hsi_gpio_configWord
        
      );
    
    -- Instantiate the backup UART receiver and transmitter.
    uart_rx_inst: entity work.ip_hsi_hsi_uart_fpga_rx
      port map (
        reset_core                => reset_core,
        clk_core                  => clk_core,
        clkEn_core                => uart_clk_ena,
        uart_rx                   => uart_rx,
        uart_mode                 => ratio,
        uart_sample_edge          => uart_sample_edge,
        uart2tl_data              => uart_rx_data,
        uart2tl_first             => uart_rx_first,
        uart2tl_valid             => uart_rx_valid
      );
    
    uart_tx_inst: entity work.ip_hsi_hsi_uart_fpga_tx
      port map (
        reset_core                => reset_core,
        clk_core                  => clk_core,
        clkEn_core                => uart_clk_ena,
        uart_tx                   => uart_tx,
        uart_mode                 => ratio,
        uart_setup_edge           => uart_setup_edge,
        tl2uart_data              => uart_tx_data,
        tl2uart_first             => uart_tx_first,
        tl2uart_valid             => uart_tx_valid,
        uart2tl_ack               => uart_tx_ack
      );
    
    -- Instantiate the reset pads.
    aresetn_phy_pad: OBUF port map (I => aresetn_phy, O => asic_hsi_aresetn_phy);
    aresetn_core_pad: OBUF port map (I => aresetn_core, O => asic_hsi_aresetn_core);
    
    -- Instantiate the mode pad.
    mode_pad: OBUF port map (I => mode, O => asic_hsi_mode);
    
    -- Instantiate the differential clock transmitter.
    diff_clk_ddr: ODDR
      port map (
        Q  => diff_clk_q,
        C  => clk_hsi_out,
        CE => mmcm_locked,
        D1 => diff_clk_ena,
        D2 => '0',
        R  => '0',
        S  => '0'
      );
    diff_clk_pad: OBUFDS
      generic map (
        IOSTANDARD => "LVDS_25"
      )
      port map (
        I  => diff_clk_q,
        O  => asic_hsi_clk_p,
        OB => asic_hsi_clk_n
      );
    
    -- Instantiate the HSI receivers.
    hsi_a2f_inst: entity work.ip_hsi_hsi_phy_rx
      generic map (
        N                         => 4,
        DELAY_CAL                 => DELAY_CAL,
        INVERT_HSI_PAIRS          => INVERT_HSI_PAIRS
      )
      port map (
        aresetn_phy               => aresetn_phy,
        clk_hsi_in                => clk_hsi_in,
        clk_hsi                   => clk_hsi,
        calib                     => calib,
        lock                      => lock,
        hsi_period                => "00000000",
        pad2phy_p                 => asic_hsi_a2f_p,
        pad2phy_n                 => asic_hsi_a2f_n,
        phy2dll_x                 => a2f_x,
        phy2dll_y                 => a2f_y
      );
    
    -- Instantiate the HSI transmitters.
    hsi_f2a_inst: entity work.ip_hsi_hsi_phy_tx
      generic map (
        N                         => 4,
        INVERT_HSI_PAIRS          => INVERT_HSI_PAIRS
      )
      port map (
        reset_hsi                 => reset_hsi,
        calib_hsi                 => calib,
        clk_hsi                   => clk_hsi,
        dll2phy_x                 => f2a_x,
        dll2phy_y                 => f2a_y,
        phy2pad_p                 => asic_hsi_f2a_p,
        phy2pad_n                 => asic_hsi_f2a_n
      );
    
    -- Instantiate the backup clock pad.
    backup_clk_ddr: ODDR
      port map (
        Q  => backup_clk_q,
        C  => clk_core,
        CE => '1',
        D1 => backup_clk_ena,
        D2 => backup_clk_high,
        R  => '0',
        S  => '0'
      );
    backup_clk_pad: OBUF
      port map (
        I  => backup_clk_q,
        O  => asic_hsi_backup_clk
      );
    
    -- Instantiate the control pads.
    ctrl_f2a_pad: OBUF
      port map (
        I => ctrl_f2a,
        O => asic_hsi_ctrl_f2a
      );
    ctrl_a2f_pad: IBUF
      port map (
        I => asic_hsi_ctrl_a2f,
        O => ctrl_a2f
      );
    
  end block;
  
  ------------------------------------------------------------------------------
  -- Scan chain memory
  ------------------------------------------------------------------------------
  scan_memory_block: block is
    
    -- Scan bit address.
    signal scan_addr  : std_logic_vector(14 downto 0);
    
  begin
    
    -- Generate the read ack signal. This is just the enable signal delayed by
    -- one cycle, because that's exactly the timing of the read data.
    read_ack_proc: process (s_axi_aclk, s_axi_aresetn) is
    begin
      if rising_edge(s_axi_aclk) then
        scan_axi_read_ack <= scan_axi_ena;
      end if;
      if s_axi_aresetn = '0' then
        scan_axi_read_ack <= '0';
      end if;
    end process;
    
    -- Instantiate the block RAM using the unimacro. We happen to need exactly
    -- a 36kb block RAM, so... Nice.
    bram_inst: bram_tdp_macro
      generic map (
        BRAM_SIZE           => "36Kb",
        DEVICE              => "7SERIES",
        SIM_COLLISION_CHECK => "GENERATE_X_ONLY",
        READ_WIDTH_A        => 32,
        WRITE_WIDTH_A       => 32,
        READ_WIDTH_B        => 1,
        WRITE_WIDTH_B       => 1
      )
      port map (
        clka    => s_axi_aclk,
        rsta    => '0',
        regceb  => '1',
        ena     => scan_axi_ena,
        wea     => scan_axi_wstrb,
        addra   => scan_axi_addr,
        dia     => scan_axi_wdata,
        doa     => scan_axi_rdata,
        
        clkb    => clk_core,
        rstb    => '0',
        regcea  => '1',
        enb     => '1',
        web(0)  => scan_a2f_valid,
        addrb   => scan_addr,
        dib(0)  => scan_a2f,
        dob(0)  => scan_f2a
      );
    
    -- Generate the HSI-side address for the block RAM.
    addr_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if scan_init = '1' then
          scan_addr(14 downto 11) <= scan_chain;
          scan_addr(10 downto 0) <= "11111011111"; -- start at bit 2015 and count down
        elsif scan_next = '1' then
          scan_addr(10 downto 0) <= std_logic_vector(unsigned(scan_addr(10 downto 0)) - 1);
        end if;
      end if;
    end process;
    
  end block;
  
  ------------------------------------------------------------------------------
  -- Teensy interface
  ------------------------------------------------------------------------------
  teensy_interface_block: block is
    
    -- UART synchronous reset.
    signal reset            : std_logic;
    
    -- Receive data.
    signal rx_data          : std_logic_vector(7 downto 0);
    signal rx_error         : std_logic;
    signal rx_strobe        : std_logic;
    
    -- Transmit data.
    signal tx_data          : std_logic_vector(7 downto 0);
    signal tx_strobe        : std_logic;
    signal tx_busy          : std_logic;
    
    -- State machine state.
    type state_t is (
      
      -- Idle state.
      TS_IDLE,
      
      -- Read states.
      TS_READ_ADDR_H,  -- About to TX 0b00AAAAAA (address bit 10 downto 5).
      TS_READ_ADDR_L,  -- About to TX 0b011AAAAA (address bit 4 downto 0).
      TS_READ_DATA_H,  -- Expecting   0b100DDDDD (read data bit 15 downto 11).
      TS_READ_DATA_M,  -- Expecting   0b101DDDDD (read data bit 10 downto 6).
      TS_READ_DATA_L,  -- Expecting   0b11DDDDDD (read data bit 5 downto 0).
      
      -- Write states.
      TS_WRITE_ADDR_H, -- About to TX 0b00AAAAAA (address bit 10 downto 5).
      TS_WRITE_ADDR_L, -- About to TX 0b010AAAAA (address bit 4 downto 0).
      TS_WRITE_DATA_H, -- About to TX 0b100DDDDD (write data bit 15 downto 11).
      TS_WRITE_DATA_M, -- About to TX 0b101DDDDD (write data bit 10 downto 6).
      TS_WRITE_DATA_L, -- About to TX 0b11DDDDDD (write data bit 5 downto 0).
      TS_WRITE_ACK     -- Expecting   0b01010101
      
    );
    signal state              : state_t;
    
    -- Next value for the state registers.
    signal state_next         : state_t;
    signal teensy_rdata_next  : std_logic_vector(15 downto 0);
    signal teensy_ack_next    : std_logic;
    signal teensy_error_next  : std_logic;
    
    -- Timeout counter. Increment to 0x30000 = 196608 = almost 2ms at 100MHz
    -- clock when running.
    signal timeout_cnt        : unsigned(17 downto 0);
    
  begin
    
    -- Generate a synchronous reset for the UART IP.
    reset_proc: process (s_axi_aclk) is
    begin
      if rising_edge(s_axi_aclk) then
        reset <= not s_axi_aresetn;
      end if;
    end process;
    
    -- Instantiate the UART block.
    uart_inst: entity work.ip_hsi_utils_uart
      generic map (
        F_CLK           => 100000000.0,
        F_BAUD          =>    115200.0
      )
      port map (
        reset           => reset,
        clk             => s_axi_aclk,
        clkEn           => '1',
        rx              => teensy_rx,
        tx              => teensy_tx,
        rx_data         => rx_data,
        rx_frameError   => rx_error,
        rx_strobe       => rx_strobe,
        rx_charTimeout  => open,
        tx_data         => tx_data,
        tx_strobe       => tx_strobe,
        tx_busy         => tx_busy
      );
    
    -- State machine register process.
    reg_proc: process (s_axi_aclk) is
    begin
      if rising_edge(s_axi_aclk) then
        
        -- Generate state registers.
        state         <= state_next;
        teensy_rdata  <= teensy_rdata_next;
        teensy_ack    <= teensy_ack_next;
        teensy_error  <= teensy_error_next;
        
        -- Handle timeouts.
        if state = TS_IDLE then
          timeout_cnt <= (others => '0');
        else
          timeout_cnt <= timeout_cnt + 1;
          if timeout_cnt(17 downto 16) = "11" then
            state         <= TS_IDLE;
            timeout_cnt   <= (others => '0');
            teensy_ack    <= '0';
            teensy_error  <= '1';
          end if;
        end if;
        
        -- Handle resets.
        if reset = '1' then
          state         <= TS_IDLE;
          timeout_cnt   <= (others => '0');
          teensy_ack    <= '0';
          teensy_error  <= '0';
        end if;
        
      end if;
    end process;
    
    -- State machine combinatorial process.
    comb_proc: process (
      state, teensy_cmd, teensy_write, teensy_rdata, teensy_addr, teensy_wdata,
      tx_busy, rx_strobe, rx_error, rx_data
    ) is
    begin
      
      -- Defaults for registers that don't change every cycle.
      state_next        <= state;
      teensy_rdata_next <= teensy_rdata;
      
      -- Disable strobe signals by default.
      teensy_ack_next   <= '0';
      teensy_error_next <= '0';
      tx_strobe         <= '0';
      tx_data           <= (others => '0');
      
      -- Process the state machine.
      case state is
        
        when TS_IDLE =>
          if teensy_cmd = '1' then
            if teensy_write = '1' then
              state_next <= TS_WRITE_ADDR_H;
            else
              state_next <= TS_READ_ADDR_H;
            end if;
          end if;
        
        when TS_READ_ADDR_H =>
          tx_data <= "00" & teensy_addr(10 downto 5);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_READ_ADDR_L;
          end if;
        
        when TS_READ_ADDR_L =>
          tx_data <= "011" & teensy_addr(4 downto 0);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_READ_DATA_H;
          end if;
        
        when TS_READ_DATA_H =>
          if rx_strobe = '1' then
            if rx_data(7 downto 5) = "100" and rx_error = '0' then
              teensy_rdata_next(15 downto 11) <= rx_data(4 downto 0);
              state_next <= TS_READ_DATA_M;
            else
              teensy_error_next <= '1';
              state_next <= TS_IDLE;
            end if;
          end if;
          
        when TS_READ_DATA_M =>
          if rx_strobe = '1' then
            if rx_data(7 downto 5) = "101" and rx_error = '0' then
              teensy_rdata_next(10 downto 6) <= rx_data(4 downto 0);
              state_next <= TS_READ_DATA_L;
            else
              teensy_error_next <= '1';
              state_next <= TS_IDLE;
            end if;
          end if;
          
        when TS_READ_DATA_L =>
          if rx_strobe = '1' then
            if rx_data(7 downto 6) = "11" and rx_error = '0' then
              teensy_rdata_next(5 downto 0) <= rx_data(5 downto 0);
              teensy_ack_next <= '1';
            else
              teensy_error_next <= '1';
            end if;
            state_next <= TS_IDLE;
          end if;
          
        when TS_WRITE_ADDR_H =>
          tx_data <= "00" & teensy_addr(10 downto 5);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_WRITE_ADDR_L;
          end if;
        
        when TS_WRITE_ADDR_L =>
          tx_data <= "010" & teensy_addr(4 downto 0);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_WRITE_DATA_H;
          end if;
          
        when TS_WRITE_DATA_H =>
          tx_data <= "100" & teensy_wdata(15 downto 11);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_WRITE_DATA_M;
          end if;
          
        when TS_WRITE_DATA_M =>
          tx_data <= "101" & teensy_wdata(10 downto 6);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_WRITE_DATA_L;
          end if;
          
        when TS_WRITE_DATA_L =>
          tx_data <= "11" & teensy_wdata(5 downto 0);
          if tx_busy = '0' then
            tx_strobe <= '1';
            state_next <= TS_WRITE_ACK;
          end if;
        
        when TS_WRITE_ACK =>
          if rx_strobe = '1' then
            if rx_data = "01010101" and rx_error = '0' then
              teensy_ack_next <= '1';
            else
              teensy_error_next <= '1';
            end if;
            state_next <= TS_IDLE;
          end if;
        
      end case;
      
    end process;
    
  end block;
  
end Behavioral;
