
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity ip_ads8885_iface is
  port (
    
    -- 100MHz clock and active-high reset.
    clk         : in    std_logic;
    reset       : in    std_logic;
    
    -- Connection to the ADC.
    pad_din     : out   std_logic;
    pad_convst  : out   std_logic;
    pad_sclk    : out   std_logic;
    pad_dout    : inout std_logic;
    
    -- Oversample count, diminished-one.
    osa_amount  : in    std_logic_vector(7 downto 0);
    
    -- Data output. This is always valid; the strobe signal only indicates that
    -- the value changed.
    data        : out   std_logic_vector(25 downto 0);
    strobe      : out   std_logic
    
  );
end ip_ads8885_iface;

architecture behavioral of ip_ads8885_iface is
  
  -- Pattern generator signals.
  signal count          : std_logic_vector(8 downto 0);
  signal convst         : std_logic; -- convst setup column + 1 delay
  signal sclk           : std_logic; -- sclk setup column + 1 delay
  signal shift          : std_logic; -- shift actual column
  
  -- Dedicated routing signals between I/O buffers and I/O registers.
  signal pad_convst_i   : std_logic; -- convst actual column
  signal pad_sclk_i     : std_logic; -- sclk actual column
  signal pad_dout_i     : std_logic; -- dout actual column
  
  -- Data output of the ADC (our data input).
  signal dout           : std_logic; -- dout sample column
  
  -- Oversampling shift register and vote output. This is for the sampling of
  -- the ADC serial data output pin, not to be confused with the ADC sample
  -- oversampling.
  signal osa_sr         : std_logic_vector(3 downto 0);
  signal osa_vote       : std_logic; -- osa_vote column
  
  -- Data shift register.
  signal data_sr        : std_logic_vector(16 downto 0);
  
  -- Raw data and valid strobe.
  signal raw_data       : signed(17 downto 0);
  signal raw_data_valid : std_logic; -- valid actual column
  
  -- Resampling logic.
  signal osa_valid      : std_logic;
  signal osa_count      : std_logic_vector(7 downto 0);
  signal osa_adder      : signed(25 downto 0);
  signal osa_accum      : signed(25 downto 0);
  
begin
  
  -- Serial clock to ADS8885 is 12.5MHz. That's 1440 ns for the transaction.
  -- We need at least 1300ns for the conversion. That's 2740ns total, leading
  -- to a maximum samplerate of ~365kHz. Let's just round that down to 333kHz,
  -- giving a quiet time of 260ns. Counter width is 9, reload is 299. The
  -- waveform is:
  --
  -- .------------------.-------.-----------.-----------.-----------.-----.-----------.-----------.
  -- | Counter register | Phase | convst .  | sclk --.  | dout --.  |osa_ | shift -.  | valid -.  |
  -- |                  |       |  ^     v  |  ^     v  |  ^     v  |vote |  ^     v  |  ^     v  |
  -- |                  |       | Set.: Act.| Set.: Act.| Act.:Smpl.|     | Set.: Act.| Set.: Act.|
  -- |==================|=======|=====:=====|=====:=====|=====:=====|=====|=====:=====|=====:=====|
  -- | 100101011 = 299  | Quiet |     :     |     :     |     :     |     |     :     |     :     |
  -- | 100101010 = 298  |       |     :     |     :     |     :     |     |     :     |     :     |
  -- | ................ | ..... | ... : ... | ... : ... | ... : ... | ... | ... : ... | ... : ... |
  -- | 100010111 = 278  |       |     :     |     :     |     :     |     |     :     |     :     |
  -- | 100010110 = 278  |       |     :     |     :     |     :     |     |     :     |     :     |
  -- | 100010101 = 277  |       |  1  :     |     :     |     :     |     |     :     |     :     |
  -- | 100010100 = 276  |_______|     :     |     :     |     :     |     |     :     |     :     |
  -- | 100010011 = 275  | Conv. |     :  1  |     :     |     :     |     |     :     |     :     |
  -- | 100010010 = 274  |       |     :  1  |     :     |     :     |     |     :     |     :     |
  -- | ................ | ..... | ... : ... | ... : ... | ... : ... | ... | ... : ... | ... : ... |
  -- | 010010101 = 149  |       |     :  1  |     :     |     :     |     |     :     |     :     |
  -- | 010010100 = 148  |       |     :  1  |     :     |     :     |     |     :     |     :     |
  -- | 010010011 = 147  |       |  0  :  1  |     :     |     :     |     |     :     |     :     |
  -- | 010010010 = 146  |_______|     :  1  |     :     |     :     |     |     :     |     :     |
  -- | 010010001 = 145  | Acqui.|     :     |     :     | \   :     |     |  -  :     |     :     |
  -- | 010010000 = 144  |       |     :     |     :     | /\  :     |     |     :  -  |     :     |
  -- | 010001111 = 143  |       |     :     |  1  :     | \/\ :     |     |     :     |     :     |
  -- | 010001110 = 142  |       |     :     |  1  :     | D17 :     |     |     :     |     :     |
  -- | 010001101 = 141  |       |     :     |  1  :  1  | D17 : D17 |     |     :     |     :     |
  -- | 010001100 = 140  |       |     :     |  1  :  1  | D17 : D17 |     |     :     |     :     |
  -- | 010001011 = 139  |       |     :     |     :  1  | D17 : D17 |     |     :     |     :     |
  -- | 010001010 = 138  |       |     :     |     :  1  | D17 : D17 |     |     :     |     :     |
  -- | 010001001 = 137  |       |     :     |     :     | \/\ : D17 |     |  1  :     |     :     |
  -- | 010001000 = 136  |       |     :     |     :     | /\/ :     | D17 |     :  1  |     :     |
  -- | 010000111 = 135  |       |     :     |  1  :     | \/\ :     |     |     :     |     :     |
  -- | 010000110 = 134  |       |     :     |  1  :     | D16 :     |     |     :     |     :     |
  -- | 010000101 = 133  |       |     :     |  1  :  1  | D16 : D16 |     |     :     |     :     |
  -- | 010000100 = 132  |       |     :     |  1  :  1  | D16 : D16 |     |     :     |     :     |
  -- | 010000011 = 131  |       |     :     |     :  1  | D16 : D16 |     |     :     |     :     |
  -- | 010000010 = 130  |       |     :     |     :  1  | D16 : D16 |     |     :     |     :     |
  -- | 010000001 = 129  |       |     :     |     :     | \/\ : D16 |     |  1  :     |     :     |
  -- | 010000000 = 128  |       |     :     |     :     | /\/ :     | D16 |     :  1  |     :     |
  -- | ................ | ..... | ... : ... | ... : ... | ... : ... | ... | ... : ... | ... : ... |
  -- | 000000111 =   7  |       |     :     |  1  :     | \/  :     |     |     :     |     :     |
  -- | 000000110 =   6  |       |     :     |  1  :     | D0  :     |     |     :     |     :     |
  -- | 000000101 =   5  |       |     :     |  1  :  1  | D0  : D0  |     |     :     |     :     |
  -- | 000000100 =   4  |       |     :     |  1  :  1  | D0  : D0  |     |     :     |     :     |
  -- | 000000011 =   3  |       |     :     |     :  1  | D0  : D0  |     |     :     |     :     |
  -- | 000000010 =   2  |_______|     :     |     :  1  | D0  : D0  |     |     :     |     :     |
  -- | 000000001 =   1  | Quiet |     :     |     :     | \/  : D0  |     |  -  :     |  1  :     |
  -- | 000000000 =   0  |       |     :     |     :     |     :     | D0  |     :  -  |     :  1  |
  -- '------------------'-------'-----------'-----------'-----------'-----'-----------'-----------'
  --
  -- The "set" columns represent the outputs of the clocked register, which
  -- means their actual values get delayed by one cycle, as shown in the
  -- "act" columns. The pad outputs have an extra delay cycle because of the
  -- I/O pad register. The "smpl" column of DOUT represents the output of
  -- said I/O register. The DOUT register gets shifted into a 4-bit shift
  -- register, which, together with the latest value, gets voted on, and is
  -- subsequently put in yet another register, of which the state is shown
  -- in the OSA vote column. OSA vote is input to a 17-bit shift register,
  -- which, together with the latest OSA vote, is assigned to the data output.
  --
  pattern_gen_proc: process (clk) is
  begin
    if rising_edge(clk) then
      
      -- Counter.
      if count = "000000000" then
        count <= "100101011";
      else
        count <= std_logic_vector(unsigned(count)-1);
      end if;
      
      -- Conversion start.
      if count = "100010101" then
        convst <= '1';
      end if;
      if count = "010010011" then
        convst <= '0';
      end if;
      
      -- SPI clock.
      sclk <= '0';
      if unsigned(count) < "010010000" then
        sclk <= count(2);
      end if;
      
      -- Shift and valid.
      shift <= '0';
      raw_data_valid <= '0';
      if count(2 downto 0) = "001" then
        shift <= '1';
        if count(8 downto 3) = "000000" then
          raw_data_valid <= '1';
        end if;
      end if;
      
      -- Handle reset.
      if reset = '1' then
        count           <= "100101011";
        convst          <= '0';
        sclk            <= '0';
        shift           <= '0';
        raw_data_valid  <= '0';
      end if;
      
    end if;
  end process;
  
  -- Instantiate the I/O buffers.
  din_pad_inst:    OBUF   port map (I => '1',          O => pad_din);
  convst_pad_inst: OBUF   port map (I => pad_convst_i, O => pad_convst);
  sclk_pad_inst:   OBUF   port map (I => pad_sclk_i,   O => pad_sclk);
  dout_pad_inst:   IBUF   port map (I => pad_dout,     O => pad_dout_i);
  dout_pad_keeper: KEEPER port map (O => pad_dout);
  
  -- Instantiate the I/O registers. By instantiating these we basically don't
  -- have to worry about timing constraints, because we'll always get the DDR
  -- register timing.
  convst_pad_reg:  ODDR generic map (DDR_CLK_EDGE => "SAME_EDGE")
                        port map (C => clk, CE => '1', R => reset, S => '0',
                                  D1 => convst, D2 => convst, Q => pad_convst_i);
  sclk_pad_reg:    ODDR generic map (DDR_CLK_EDGE => "SAME_EDGE")
                        port map (C => clk, CE => '1', R => reset, S => '0',
                                  D1 => sclk, D2 => sclk, Q => pad_sclk_i);
  dout_pad_reg:    IDDR generic map (DDR_CLK_EDGE => "SAME_EDGE")
                        port map (C => clk, CE => '1', R => reset, S => '0',
                                  D => pad_dout_i, Q1 => dout, Q2 => open);
  
  -- Instantiate oversampling logic for noise/glitch immunity.
  osa_proc: process (clk) is
    constant VOTE_LOOKUP  : std_logic_vector(31 downto 0) := X"FEE8E880";
    variable votes        : std_logic_vector(4 downto 0);
  begin
    if rising_edge(clk) then
      osa_sr(0) <= dout;
      osa_sr(3 downto 1) <= osa_sr(2 downto 0);
      votes(4 downto 1) := osa_sr(3 downto 0);
      votes(0) := dout;
      osa_vote <= VOTE_LOOKUP(to_integer(unsigned(votes)));
    end if;
  end process;
  
  -- Instantiate the data shift register.
  data_shift_proc: process (clk) is
  begin
    if rising_edge(clk) then
      if shift = '1' then
        data_sr(0) <= osa_vote;
        data_sr(16 downto 1) <= data_sr(15 downto 0);
      end if;
    end if;
  end process;
  
  -- Combine the shift register and current data.
  data_combine_proc: process (osa_vote, data_sr) is
    variable raw_data_v : std_logic_vector(17 downto 0);
  begin
    raw_data_v(0) := osa_vote;
    raw_data_v(17 downto 1) := data_sr(16 downto 0);
    raw_data <= signed(raw_data_v);
  end process;
  
  -- Accumulate the specified number of samples to increase resolution/reduce
  -- noise and lower the samplerate. This is basically resampling with a crappy
  -- box window.
  resample_proc: process (clk) is
  begin
    if rising_edge(clk) then
      
      -- Internal strobe signal.
      strobe <= '0';
      
      -- Handle new raw data.
      if raw_data_valid = '1' then
        if osa_count = X"00" then
          
          -- Save the current adder value as the new cooked sample. Strobe if
          -- this is not the first (incomplete) sample.
          strobe <= osa_valid;
          data <= std_logic_vector(osa_adder);
          
          -- Initialize the OSA logic for the next N samples.
          osa_count <= osa_amount;
          osa_accum <= (others => '0');
          osa_valid <= '1';
          
        else
          
          -- Update the OSA logic.
          osa_count <= std_logic_vector(unsigned(osa_count) - 1);
          osa_accum <= osa_adder;
          
        end if;
      end if;
      
      -- Handle reset.
      if reset = '1' then
        osa_count <= (others => '0');
        osa_accum <= (others => '0');
        osa_valid <= '0';
        data      <= (others => '0');
        strobe    <= '0';
      end if;
      
    end if;
  end process;
  
  -- Generate the accumulator itself.
  osa_adder <= osa_accum + resize(raw_data, 26);
  
end behavioral;

