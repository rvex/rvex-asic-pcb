
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

-- Stream data format:
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |                  Signed ADC data                   |5|4|3|2|1|F|
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   ADC data: value of the ADC accumulator. The amount of accumulated 18-bit
--     samples is <resample-register>+1 (i.e., there is no hardware divider or
--     rightshifter).
--   5..2: trigger information, selected using trigsel register 5 through 2.
--   1: primary trigger. Because the primary trigger indicates whether the
--     stream should be active or not (active high), this is always high, with
--     the exception of one extra word sent when the trigger goes low. This
--     word also has the AXI stream last flag set. So really, this is just
--     !last.
--   F: first word flag.
--
-- Register 0:
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |                  Signed ADC data                   |5|4|3|2|1|O|
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   ADC data: the latest ADC sample. This also updates if the stream is
--     inactive.
--   5..1: trigger values, same as in the stream output.
--   O: indicates that there has been a data overrun. Writing to this register
--     clears the flag.
--
-- Register 1:
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   |                         Trigger inputs                         |
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   This just mirrors the status of the "triggers" input signal. Read-only.
--
-- Register 2:
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   | | | | | | | | | | | | | | | | || | | | | | | | |   Resample    |
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   This register determines the samplerate and resolution of the ADC data.
--   The maximum samplerate of 333 1/3 kHz is divided by (resample+1), while
--   the raw ADC data is effectively multiplied by (resample+1). Internally
--   the system is just summing (resample+1) consequitive ADC samples before
--   passing the data to the rest of the system, so this is a simple box
--   resampling filter.
--
-- Register 3 through 7:
--   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
--   | | | | | | | | | | | | | | | | || | | | | | | | | |I|E|   Mux   |
--   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
--   Mux: selects between the up to 32 input trigger signals.
--   E: gates the trigger signal. That is, if this is low, the trigger input
--     always appears to be low.
--   I: XOR'd with the output of the trigger mux and the enable gate.
--   The selected trigger signals are put into the stream data word (register
--   3 selects bit 1, register 4 selects bit 2, etc.), and the status of the
--   register 3 trigger output determines whether the stream is active or not
--   (active high).

entity ip_ads8885 is
  generic (
    
    -- Number of trigger inputs.
    TRIGGER_WIDTH               : integer range 0 to 32 := 0
    
  );
  port (
    
    -- AXI slave interface. Must run at 100MHz!
    s_axi_aclk                  : in  std_logic;
    s_axi_aresetn               : in  std_logic;
    
    s_axi_awaddr                : in  std_logic_vector(4 downto 0);
    s_axi_awvalid               : in  std_logic;
    s_axi_awready               : out std_logic;
    
    s_axi_wdata                 : in  std_logic_vector(31 downto 0);
    s_axi_wvalid                : in  std_logic;
    s_axi_wready                : out std_logic;
    
    s_axi_bresp                 : out std_logic_vector(1 downto 0);
    s_axi_bvalid                : out std_logic;
    s_axi_bready                : in  std_logic;
    
    s_axi_araddr                : in  std_logic_vector(4 downto 0);
    s_axi_arvalid               : in  std_logic;
    s_axi_arready               : out std_logic;
    
    s_axi_rdata                 : out std_logic_vector(31 downto 0);
    s_axi_rresp                 : out std_logic_vector(1 downto 0);
    s_axi_rvalid                : out std_logic;
    s_axi_rready                : in  std_logic;
    
    -- Stream output. Must be the same clock as s_axi_aclk!
    m_axis_aclk                 : in  std_logic;
    m_axis_aresetn              : in  std_logic;
    
    m_axis_tdata                : out std_logic_vector(31 downto 0);
    m_axis_tlast                : out std_logic;
    m_axis_tvalid               : out std_logic;
    m_axis_tready               : in  std_logic;
    
    -- Trigger sources.
    triggers                    : in  std_logic_vector(TRIGGER_WIDTH-1 downto 0);
    
    -- ADS8885 interface.
    ads8885_din                 : out std_logic;
    ads8885_convst              : out std_logic;
    ads8885_sclk                : out std_logic;
    ads8885_dout                : inout std_logic
    
  );
end ip_ads8885;

architecture Behavioral of ip_ads8885 is
  
  -- Registered reset signal.
  signal reset_sr     : std_logic;
  signal reset        : std_logic;
  
  -- AXI slave FSM state.
  type state_t is (AS_IDLE, AS_RRESP, AS_GETWDATA, AS_BRESP);
  signal state        : state_t;
  
  -- AXI slave selected address.
  signal addr         : std_logic_vector(4 downto 0);
  
  -- Overrun flag.
  signal overrun_flag : std_logic;
  
  -- Resample control register.
  signal resample     : std_logic_vector(7 downto 0);
  
  -- ADC data and strobe.
  signal adc_data     : std_logic_vector(25 downto 0);
  signal adc_strobe   : std_logic;
  
  -- Trigger select registers. 0-31 are the inputs, 32-63 is always low. Bit 6
  -- inverts the trigger. The stream output is active when trigger 1 is high,
  -- triggers 2 through 5 are just output as part of the data word for
  -- identification.
  subtype trigsel_t is std_logic_vector(6 downto 0);
  type trigsel_at is array (natural range <>) of trigsel_t;
  signal trigsel      : trigsel_at(5 downto 1);
  
  -- Registered and extended copy of the triggers input.
  signal triggers_int : std_logic_vector(31 downto 0);
  
  -- Trigger mux outputs.
  signal trigmux      : std_logic_vector(5 downto 1);
  
  -- Asserted for one cycle when there is incoming data while the stream
  -- register has not yet been emptied.
  signal overrun      : std_logic;
  
  -- Strobe signal indicating that there is an ADC strobe and the value needs
  -- to be sent out to the stream sink.
  signal push         : std_logic;
  
  -- Stream packet state.
  signal first        : std_logic;
  signal busy         : std_logic;
  
  -- Internal copy of the stream valid output.
  signal m_axis_tvalid_int : std_logic;
  
begin
  
  -- Generate reset.
  reset_proc: process (s_axi_aclk, s_axi_aresetn) is
  begin
    if rising_edge(s_axi_aclk) then
      reset_sr <= '0';
      reset <= reset_sr;
    end if;
    if s_axi_aresetn = '0' then
      reset_sr <= '1';
      reset <= '1';
    end if;
  end process;
  
  -- AXI slave process.
  axi_slave_proc: process (s_axi_aclk) is
  begin
    
    -- Clock.
    if rising_edge(s_axi_aclk) then
      
      -- Set defaults.
      s_axi_awready   <= '0';
      s_axi_wready    <= '0';
      s_axi_arready   <= '0';
      
      -- Overrun flag.
      if overrun = '1' then
        overrun_flag <= '1';
      end if;
      
      case state is
        
        when AS_IDLE =>
          
          -- Handle reads immediately.
          if s_axi_arvalid = '1' then
            s_axi_arready <= '1';
            s_axi_rdata <= (others => '0');
            
            case s_axi_araddr(4 downto 2) is
              
              when "001" => -- Trigger inputs.
                s_axi_rdata <= triggers_int;
              
              when "010" => -- Resample register.
                s_axi_rdata(resample'range) <= resample;
              
              when "011" => -- Primary trigger, bit 1 in stream.
                s_axi_rdata(trigsel_t'range) <= trigsel(1);
              
              when "100" => -- Secondary trigger, bit 2 in stream.
                s_axi_rdata(trigsel_t'range) <= trigsel(2);
              
              when "101" => -- Secondary trigger, bit 3 in stream.
                s_axi_rdata(trigsel_t'range) <= trigsel(3);
              
              when "110" => -- Secondary trigger, bit 4 in stream.
                s_axi_rdata(trigsel_t'range) <= trigsel(4);
              
              when "111" => -- Secondary trigger, bit 5 in stream.
                s_axi_rdata(trigsel_t'range) <= trigsel(5);
              
              when others => -- Current output.
                s_axi_rdata(31 downto 6) <= adc_data;
                s_axi_rdata(5 downto 1) <= trigmux;
                s_axi_rdata(0) <= overrun_flag;
              
            end case;
            
            s_axi_rresp <= "00";
            s_axi_rvalid <= '1';
            state <= AS_RRESP;
            
          end if;
        
          -- Handle writes.
          if s_axi_awvalid = '1' then
            s_axi_awready <= '1';
            addr <= s_axi_awaddr;
            state <= AS_GETWDATA;
          end if;
          
        when AS_RRESP =>
          
          -- Wait for read response channel ready.
          if s_axi_rready = '1' then
            s_axi_rvalid <= '0';
            state <= AS_IDLE;
          end if;
          
        when AS_GETWDATA =>
          
          -- Handle writes.
          if s_axi_wvalid = '1' then
            s_axi_wready <= '1';
            
            case addr(4 downto 2) is
              
              when "000" => -- Clear overrun flag.
                overrun_flag <= '0';
              
              when "010" => -- Resample register.
                resample <= s_axi_wdata(resample'range);
              
              when "011" => -- Primary trigger, bit 1 in stream.
                trigsel(1) <= s_axi_wdata(trigsel_t'range);
              
              when "100" => -- Secondary trigger, bit 2 in stream.
                trigsel(2) <= s_axi_wdata(trigsel_t'range);
              
              when "101" => -- Secondary trigger, bit 3 in stream.
                trigsel(3) <= s_axi_wdata(trigsel_t'range);
              
              when "110" => -- Secondary trigger, bit 4 in stream.
                trigsel(4) <= s_axi_wdata(trigsel_t'range);
              
              when "111" => -- Secondary trigger, bit 5 in stream.
                trigsel(5) <= s_axi_wdata(trigsel_t'range);
              
              when others =>
                null;
              
            end case;
            
            s_axi_bresp <= "00";
            s_axi_bvalid <= '1';
            state <= AS_BRESP;
            
          end if;
          
        when AS_BRESP =>
          
          -- Wait for write response channel ready.
          if s_axi_bready = '1' then
            s_axi_bvalid <= '0';
            state <= AS_IDLE;
          end if;
          
      end case;
      
      -- Handle reset.
      if reset = '1' then
        
        -- Reset state.
        state <= AS_IDLE;
        
        -- Reset output ports.
        s_axi_awready   <= '0';
        s_axi_wready    <= '0';
        s_axi_bresp     <= "00";
        s_axi_bvalid    <= '0';
        s_axi_arready   <= '0';
        s_axi_rdata     <= X"00000000";
        s_axi_rresp     <= "00";
        s_axi_rvalid    <= '0';
        
        -- Reset control registers.
        overrun_flag    <= '0';
        resample        <= (others => '0');
        trigsel         <= (others => (others => '0'));
        
      end if;
      
    end if;
    
  end process;
  
  -- Instantiate the ADC interface.
  adc_iface_inst: entity work.ip_ads8885_iface
    port map (
      clk         => s_axi_aclk,
      reset       => reset,
      pad_din     => ads8885_din,
      pad_convst  => ads8885_convst,
      pad_sclk    => ads8885_sclk,
      pad_dout    => ads8885_dout,
      osa_amount  => resample,
      data        => adc_data,
      strobe      => adc_strobe
    );
  
  -- Multiplex the triggers.
  trigger_proc: process (s_axi_aclk) is
  begin
    if rising_edge(s_axi_aclk) then
      triggers_int <= (others => '0');
      triggers_int(triggers'range) <= triggers;
      for i in trigmux'range loop
        trigmux(i) <= (triggers_int(to_integer(unsigned(trigsel(i)(4 downto 0)))) and trigsel(i)(5)) xor trigsel(i)(6);
      end loop;
      if reset = '1' then
        triggers_int <= (others => '0');
        trigmux <= (others => '0');
      end if;
    end if;
  end process;
  
  -- Work out if we have incoming data that needs to be pushed to the stream.
  push <= adc_strobe and (busy or trigmux(1));
  
  -- Handle the stream.
  stream_proc: process (s_axi_aclk) is
  begin
    if rising_edge(s_axi_aclk) then
      overrun <= '0';
      if m_axis_tvalid_int = '1' then
        if m_axis_tready = '1' then
          
          -- Data acknowledged. Free the output register.
          m_axis_tvalid_int <= '0';
          
        end if;
        if push = '1' then
          
          -- There is a new data word, but the output register still in use.
          overrun <= '1';
          
        end if;
      elsif push = '1' then
        
        -- Save new data word.
        m_axis_tdata(31 downto 6) <= adc_data;
        m_axis_tdata(5 downto 1)  <= trigmux;
        m_axis_tdata(0)           <= first;
        m_axis_tlast              <= not trigmux(1);
        m_axis_tvalid_int         <= '1';
        
        -- The next word won't be the first.
        first <= '0';
        
        -- If trigmux is low, we're signalling this is the last word in the
        -- packet and can now stop being busy.
        busy <= trigmux(1);
        
      end if;
      
      -- Handle trigger rising edge.
      if trigmux(1) = '1' and busy = '0' then
        first <= '1';
        busy <= '1';
      end if;
      
      -- Handle reset.
      if reset = '1' then
        
        -- Stream output signals.
        m_axis_tdata <= (others => '0');
        m_axis_tlast <= '0';
        m_axis_tvalid_int <= '0';
        
        -- Packet state registers.
        first <= '0';
        busy <= '0';
        
        -- Overrun flag output to AXI slave process.
        overrun <= '0';
        
      end if;
    end if;
  end process;
  
  m_axis_tvalid <= m_axis_tvalid_int;
  
end Behavioral;
