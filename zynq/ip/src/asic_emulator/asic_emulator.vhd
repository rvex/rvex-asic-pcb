-- r-VEX processor
-- Copyright (C) 2008-2016 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2016 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--use IEEE.math_real.all;

library work;
use work.common_pkg.all;
use work.utils_pkg.all;
use work.bus_pkg.all;
use work.core_pkg.all;
use work.cache_pkg.all;

--=============================================================================
-- ASIC toplevel. Pads are mapped to ports of this entity to match how Xilinx's
-- tools work. May need a wrapper of some kind for the ASIC tools.
-------------------------------------------------------------------------------
entity asic_emulator is
--=============================================================================
  port (
    
    -- Core reset signal. This is basically the master reset; if this is
    -- asserted low, everything except the HSI PHY delay calibration is reset.
    -- Note that the core uses a synchronous reset though, so it won't reset
    -- until it gets its first clock somehow.
    asic_hsi_aresetn_core       : in  std_logic;
    
    -- PHY reset signal and UART receive pin B. If asserted low while core
    -- reset is also asserted low, the HSI PHY delay calibration logic is reset
    -- asynchronously in addition to all the other systems. While core reset is
    -- high, this is disconnected from the PHY reset and used as an additional
    -- FPGA to ASIC UART data pin.
    asic_hsi_aresetn_phy        : in  std_logic;
    
    -- Backup clock. This is used in UART mode as a backup to the HSI
    -- interface, in case there is a problem with the PHY or DLL.
    asic_hsi_backup_clk         : in  std_logic;
    
    -- HSI clock pads. The core clock is derived from this when running in HSI
    -- mode: either the core gets the same clock, or the core clock gets HSI
    -- divided by 2.
    asic_hsi_clk_p              : in  std_logic;
    asic_hsi_clk_n              : in  std_logic;
    
    -- HSI receiver pads.
    asic_hsi_f2a_p              : in  std_logic_vector(3 downto 0);
    asic_hsi_f2a_n              : in  std_logic_vector(3 downto 0);
    
    -- HSI transmitter pads.
    asic_hsi_a2f_p              : out std_logic_vector(3 downto 0);
    asic_hsi_a2f_n              : out std_logic_vector(3 downto 0);
    
    -- Mode selection pin. This pin does the following things:
    --  - falling edge of aresetn_phy while aresetn_core is low: low selects
    --    backup interface, high selects HSI.
    --  - rising edge of aresetn_phy while aresetn_core is low: low selects
    --    1:2 clock in HSI mode or reliable UART mode, high selects 1:4 clock
    --    in HSI mode or fast UART mode.
    --  - while aresetn_core is high: low for normal operation, high for scan
    --    chain access.
    --  - rising edge of mode pin while aresetn_core is low: shift ctrl F2A
    --    value into HSI clock period register LSB.
    --  - rising edge of mode pin while aresetn_core is high: shift ctrl F2A
    --    value into scan chain register LSB.
    asic_hsi_mode               : in  std_logic;
    
    -- FPGA to ASIC control pin. This pin does the following things:
    --  - rising edge of mode pin while aresetn_core is low: shifted into HSI
    --    clock period register LSB.
    --  - rising edge of mode pin while aresetn_core is high: shifted into scan
    --    chain register LSB.
    --  - mode pin high: scan chain input.
    --  - mode pin low, HSI mode: active-high calibration request.
    --  - mode pin low, UART mode: primary UART data (aresetn_phy is
    --    secondary).
    asic_hsi_ctrl_f2a           : in  std_logic;
    
    -- ASIC to FPGA control pin. This pin does the following things:
    --  - mode pin high: scan chain output.
    --  - mode pin low, HSI mode: active-high lock output.
    --  - mode pin low, UART mode: UART data.
    asic_hsi_ctrl_a2f           : out std_logic
    
  );
end asic_emulator;

--=============================================================================
architecture Behavioral of asic_emulator is
--=============================================================================
  
  -- Number of differential pairs. Each must be 1, 2 or 4.
  constant N_RX : natural := 4;
  constant N_TX : natural := 4;
  
  -- Number of clocks, may differ for each design
  constant N_CK : natural := 1;
  
  component hsi_asic is
    generic (
      
      -- Number of differential pairs. Each must be 1, 2 or 4.
      N_RX                        : natural := 4;
      N_TX                        : natural := 4;
      
      -- Number of clocks, may differ for each design
      N_CK                        : natural := 1
      
    );
    port (
      
      -- System control for the core.
      reset                       : out std_logic;
      clk                         : out std_logic;
      
      -- Reset for the memory. Released 8 cycles before the core reset.
      reset_mem                   : out std_logic;
      
      -- Scan chain interface. When scan_enable is high, ALL flipflops in the
      -- design must be disabled or part of a scan chain. Disabling can be done
      -- by gating the clock as well. scan_enable is only allowed to switch while
      -- clk is stopped low. It is assumed that there is a shift register from
      -- scan_out to scan_in clocked on the positive edge of clk. All shift
      -- registers need to be the same length.
      scan_enable                 : out std_logic;
      scan_in                     : out std_logic_vector(255 downto 0);
      scan_out                    : in  std_logic_vector(255 downto 0);
      
      -- Memory bus to FPGA.
      core2mem                    : in  bus_mst2slv_type;
      mem2core                    : out bus_slv2mst_type;
      
      -- Debug bus from FPGA.
      dbg2core                    : out bus_mst2slv_type;
      core2dbg                    : in  bus_slv2mst_type;
      
      -- GPIO-like stuff transferred over the HSI interface.
      irq                         : out std_logic_vector(15 downto 0);
      done                        : in  std_logic_vector(3 downto 0);
      idle                        : in  std_logic_vector(3 downto 0);
      configWord                  : in  std_logic_vector(31 downto 0);
      
      -- Core reset signal. This is basically the master reset; if this is
      -- asserted low, everything except the HSI PHY delay calibration is reset.
      -- Note that the core uses a synchronous reset though, so it won't reset
      -- until it gets its first clock somehow.
      pad_aresetn_core            : in  std_logic;
      
      -- PHY reset signal and UART receive pin B. If asserted low while core
      -- reset is also asserted low, the HSI PHY delay calibration logic is reset
      -- asynchronously in addition to all the other systems. While core reset is
      -- high, this is disconnected from the PHY reset and used as an additional
      -- FPGA to ASIC UART data pin.
      pad_aresetn_phy             : in  std_logic;
      
      -- Backup clock. This is used in UART mode as a backup to the HSI
      -- interface, in case there is a problem with the PHY or DLL.
      pad_backup_clk              : in  std_logic;
      
      -- HSI clock pads. The core clock is derived from this when running in HSI
      -- mode: either the core gets the same clock, or the core clock gets HSI
      -- divided by 2.
      pad_hsi_clk_p               : in  std_logic_vector(N_CK-1 downto 0);
      pad_hsi_clk_n               : in  std_logic_vector(N_CK-1 downto 0);
      
      -- Reference current pads.
      pad_Iref_tx                 : in  std_logic_vector(N_TX-1 downto 0);
      pad_Iref_rx                 : out std_logic_vector(N_RX-1 downto 0);
      
      -- HSI receiver pads.
      pad_f2a_p                   : in  std_logic_vector(N_RX-1 downto 0);
      pad_f2a_n                   : in  std_logic_vector(N_RX-1 downto 0);
      
      -- HSI transmitter pads.
      pad_a2f_p                   : out std_logic_vector(N_TX-1 downto 0);
      pad_a2f_n                   : out std_logic_vector(N_TX-1 downto 0);
      
      -- Mode selection pin. This pin does the following things:
      --  - falling edge of aresetn_phy while aresetn_core is low: low selects
      --    backup interface, high selects HSI.
      --  - rising edge of aresetn_phy while aresetn_core is low: low selects
      --    1:2 clock in HSI mode or reliable UART mode, high selects 1:4 clock
      --    in HSI mode or fast UART mode.
      --  - while aresetn_core is high: low for normal operation, high for scan
      --    chain access.
      --  - rising edge of mode pin while aresern_core is low: shift ctrl F2A
      --    value into HSI clock period register LSB.
      --  - rising edge of mode pin while aresern_core is high: shift ctrl F2A
      --    value into scan chain register LSB.
      pad_mode                    : in  std_logic;
      
      -- FPGA to ASIC control pin. This pin does the following things:
      --  - rising edge of mode pin while aresetn_core is low: shifted into HSI
      --    clock period register LSB.
      --  - rising edge of mode pin while aresetn_core is high: shifted into scan
      --    chain register LSB.
      --  - mode pin high: scan chain input.
      --  - mode pin low, HSI mode: active-high calibration request.
      --  - mode pin low, UART mode: primary UART data (aresetn_phy is
      --    secondary).
      pad_ctrl_f2a                : in  std_logic;
      
      -- ASIC to FPGA control pin. This pin does the following things:
      --  - mode pin high: scan chain output.
      --  - mode pin low, HSI mode: active-high lock output.
      --  - mode pin low, UART mode: UART data.
      pad_ctrl_a2f                : out std_logic
      
    );
  end component;
  
  function RCFG return rvex_generic_config_type is
    variable cfg : rvex_generic_config_type := RVEX_DEFAULT_CONFIG;
  begin
    cfg.numLanesLog2            := 2; -- 3 in ASIC
    cfg.numLaneGroupsLog2       := 1; -- 2 in ASIC
    cfg.numContextsLog2         := 1; -- 2 in ASIC
    cfg.genBundleSizeLog2       := 3;
    cfg.bundleAlignLog2         := 0;
    cfg.multiplierLanes         := 2#11111111#;
    cfg.faddLanes               := 2#00000000#;
    cfg.fcompareLanes           := 2#00000000#;
    cfg.fconvfiLanes            := 2#00000000#;
    cfg.fconvifLanes            := 2#00000000#;
    cfg.fmultiplyLanes          := 2#00000000#;
    cfg.memLaneRevIndex         := 1;
    cfg.numBreakpoints          := 1;
    cfg.forwarding              := true;
    cfg.traps                   := 2;
    cfg.limmhFromNeighbor       := true;
    cfg.limmhFromPreviousPair   := false;
    cfg.reg63isLink             := false;
    cfg.cregStartAddress        := X"FFFFFC00";
    cfg.resetVectors            := (others => (others => '0'));
    cfg.unifiedStall            := false;
    cfg.gpRegImpl               := RVEX_GPREG_IMPL_MEM; -- Weird ASIC stuff in ASIC
    cfg.traceEnable             := false;
    cfg.perfCountSize           := 4;
    cfg.cachePerfCountEnable    := true;
    cfg.stallInactive           := true;
    cfg.enablePowerLatches      := true;
    return cfg;
  end RCFG;
  
  -- System control for the core.
  signal reset                  : std_logic;
  signal clk                    : std_logic;
  
  -- Reset for the memory. Released 8 cycles before the core reset.
  signal reset_mem              : std_logic;
  
  -- Scan chain interface. When scan_enable is high, ALL flipflops in the
  -- design must be disabled or part of a scan chain. Disabling can be done
  -- by gating the clock as well. scan_enable is only allowed to switch while
  -- clk is stopped low. It is assumed that there is a shift register from
  -- scan_out to scan_in clocked on the positive edge of clk. All shift
  -- registers need to be the same length.
  signal scan_enable            : std_logic;
  signal scan_in                : std_logic_vector(255 downto 0);
  signal scan_out               : std_logic_vector(255 downto 0);
  
  -- Memory bus to FPGA.
  signal core2mem               : bus_mst2slv_type;
  signal mem2core               : bus_slv2mst_type;
  
  -- Debug bus from FPGA.
  signal dbg2core               : bus_mst2slv_type;
  signal core2dbg               : bus_slv2mst_type;
  
  -- GPIO-like stuff transferred over the HSI interface.
  signal irq                    : std_logic_vector(15 downto 0);
  signal done                   : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal idle                   : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal configWord             : std_logic_vector(31 downto 0);
  
--=============================================================================
begin -- architecture
--=============================================================================
  
  hsi_inst: hsi_asic
    generic map (
      
      -- Number of differential pairs. Each must be 1, 2 or 4.
      N_RX                      => N_RX,
      N_TX                      => N_TX,
    
      -- Number of clocks, may differ for each design
      N_CK                      => N_CK
      
    )
    port map (
      reset                     => reset,
      clk                       => clk,
      reset_mem                 => reset_mem,
      scan_enable               => scan_enable,
      scan_in                   => scan_in,
      scan_out                  => scan_out,
      core2mem                  => core2mem,
      mem2core                  => mem2core,
      dbg2core                  => dbg2core,
      core2dbg                  => core2dbg,
      irq                       => irq,
      done(3 downto 2)          => "00",
      done(1 downto 0)          => done,
      idle(3 downto 2)          => "11",
      idle(1 downto 0)          => idle,
      configWord                => configWord,
      
      pad_aresetn_core          => asic_hsi_aresetn_core,
      pad_aresetn_phy           => asic_hsi_aresetn_phy,
      pad_backup_clk            => asic_hsi_backup_clk,
      pad_hsi_clk_p(0)          => asic_hsi_clk_p,
      pad_hsi_clk_n(0)          => asic_hsi_clk_n,
      pad_Iref_tx               => (others => '0'),
      pad_Iref_rx               => open,
      pad_f2a_p                 => asic_hsi_f2a_p,
      pad_f2a_n                 => asic_hsi_f2a_n,
      pad_a2f_p                 => asic_hsi_a2f_p,
      pad_a2f_n                 => asic_hsi_a2f_n,
      pad_mode                  => asic_hsi_mode,
      pad_ctrl_f2a              => asic_hsi_ctrl_f2a,
      pad_ctrl_a2f              => asic_hsi_ctrl_a2f
      
    );
  
  rvsys_sairq_inst: entity work.rvsys_sairq
    generic map (
      
      -- Core configuration. Must be equal to the configuration presented to the
      -- rvex core connected to the cache.
      RCFG                      => RCFG,
      
      -- Cache configuration.
      CCFG                      => (
        instrCacheLinesLog2     => 8,
        dataCacheLinesLog2      => 9
      ),
      
      -- Number of external interrupts to service. Min 1, max 31. The timer
      -- interrupt is shared with interrupt 1.
      IRQ_NUM_IRQ               => 16,
      
      -- Number of bits in the timer value. 0 for timer disabled. Min 2, max 32
      -- for an actual timer.
      IRQ_TIMER_BITS            => 32,
      
      -- Enables (1) or disables (0) configurable interrupt priorities. That is,
      -- this controls the existence of the PRIOn registers and a double-width
      -- priority decoder.
      IRQ_CONFIG_PRIO_ENABLE    => 1,
      
      -- Enables (1) or disables (0) interrupt nesting support. That is, this
      -- controls the existence of the LEVELn registers and the interrupt level
      -- comparator.
      IRQ_NESTING_ENABLE        => 1,
      
      -- Enables (1) or disables (0) breakpoint broadcasting (i.e. stopping cores
      -- and/or the timer in response to a breakpoint in another core). That is,
      -- this controls the existence of the BRBROn registers and the broadcasting
      -- logic.
      IRQ_BREAKPOINT_BROADCASTING => 1,
      
      -- Enables (1) or disables (0) configurable reset vectors. That is, this
      -- controls the existence of the RVECTn registers. When disabled, the
      -- reset vector output is always zero for each context.
      IRQ_CONFIG_RVECT_ENABLE   => 1,
      
      -- Enables (1) or disables (0) inserting a register in the interrupt
      -- request path from the controller to the processor. This can be used to
      -- break the critical path if it ends up here or decrease the strain on
      -- the router a bit at the cost of an extra cycle's worth of interrupt
      -- latency.
      IRQ_OUTPUT_REGISTER       => 0,
      
      -- Platform version tag. This is put in the global control registers of the
      -- processor.
      PLATFORM_TAG              => X"52564153494331",
      
      -- Register consistency check configuration (see core.vhd).
      RCC_RECORD                => "",
      RCC_CHECK                 => "",
      RCC_CTXT                  => 0
      
    )
    port map (
      reset                     => reset,
      clk                       => clk,
      clken                     => '1',
      rv2mem                    => core2mem,
      mem2rv                    => mem2core,
      dbg2rv                    => dbg2core,
      rv2dbg                    => core2dbg,
      irq2rv                    => irq,
      done                      => done,
      idle                      => idle,
      configWord                => configWord
    );
  
  -- TODO
  --reset_mem
  --scan_enable
  --scan_in
  scan_out <= (others => '0');
  
end Behavioral;

