library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hsi_uart_asic_rx is
  port (
    
    -- System control.
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- Data inputs.
    uart_rx                     : in  std_logic_vector(1 downto 0);
    
    -- Mode input: 0=reliable, 1=fast.
    uart_mode                   : in  std_logic;
    
    -- Word stream output.
    uart2tl_data                : out std_logic_vector(31 downto 0);
    uart2tl_first               : out std_logic;
    uart2tl_valid               : out std_logic
    
  );
end hsi_uart_asic_rx;

architecture Behavioral of hsi_uart_asic_rx is
  
  -- Conditioned RX signals.
  signal rx_cond                : std_logic_vector(1 downto 0);
  
  -- Whether a start bit or stop bit is currently present and valid on the
  -- rx_cond signal.
  signal start_bit              : std_logic;
  signal stop_bit               : std_logic;
  
  -- Whether any valid bit is currently present on the rx_cond signal, be it
  -- start, stop, data, or idle. This is basically a strobe version of the
  -- recovered bit clock.
  signal valid_bit              : std_logic;
  
begin
  
  -- Sampler and majority voter for each pin.
  sample_gen: for i in 0 to 1 generate
    signal shift_r              : std_logic_vector(3 downto 0);
  begin
    
    -- Sampling and shift register process.
    sample_shift_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_core = '1' then
          shift_r <= (others => '1');
        else
          shift_r <= shift_r(2 downto 0) & uart_rx(i);
        end if;
      end if;
    end process;
    
    -- Majority voter/data selection process.
    majority_proc: process (shift_r, uart_mode) is
    begin
      if uart_mode = '1' then
        
        -- Fast mode: just select the first register.
        rx_cond(i) <= shift_r(0);
        
      else
        
        -- Reliable mode: enable majority voting.
        rx_cond(i) <= (shift_r(1) and shift_r(2))
                   or (shift_r(1) and shift_r(3))
                   or (shift_r(2) and shift_r(3));
        
      end if;
    end process;
    
  end generate;
  
  -- Frame detection logic block.
  frame_detect_block: block is
    signal frame_busy           : std_logic;
  begin
    
    -- Detect start bits.
    start_bit <= (not frame_busy) and (not rx_cond(0));
    
    -- Remember if we're currently in the middle of a frame, so we know when to
    -- disable the start bit detection logic.
    frame_busy_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_core = '1' then
          frame_busy <= '0';
        else
          if start_bit = '1' then
            frame_busy <= '1';
          elsif stop_bit = '1' then
            frame_busy <= '0';
          end if;
        end if;
      end if;
    end process;
    
  end block;
  
  -- Bit clock recovery logic block.
  bit_clk_recovery_block: block is
    
    -- Bit clock counter.
    signal counter              : unsigned(3 downto 0);
    
    -- Set when the counter is zero.
    signal zero                 : std_logic;
    
  begin
    
    -- Infer the cycle counter for the reliable UART mode, where one bit time
    -- is 7 clock cycles.
    counter_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_core = '1' then
          counter <= "0000";
        else
          if start_bit = '1' then
            counter <= "1001"; -- 9
          elsif counter = "0000" then
            counter <= "0110"; -- 6
          else
            counter <= counter - 1;
          end if;
        end if;
      end if;
    end process;
    
    -- Recovered clock signal.
    zero <= '1' when counter = "0000" else '0';
    
    -- In reliable mode, a bit is valid when it's a start bit or when our
    -- counter is zero. In fast mode, it's always valid.
    valid_bit <= start_bit or zero or uart_mode;
    
  end block;
  
  -- Parallelization logic block.
  parallelizer_block: block is
    signal shift_a              : std_logic_vector(17 downto 0);
    signal shift_b              : std_logic_vector(14 downto 0);
  begin
    
    -- Shift register process.
    shift_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_core = '1' then
          shift_a <= (others => '1');
          shift_b <= (others => '1');
        else
          
          -- Shift the shift register if we have a valid bit.
          if valid_bit = '1' then
            if start_bit = '1' or shift_a(16) = '0' then
              shift_a(17 downto 1) <= (others => '1');
              shift_a(0) <= rx_cond(0);
            else
              shift_a(16 downto 1) <= shift_a(15 downto 0);
              shift_a(0) <= rx_cond(0);
            end if;
            shift_a(17) <= shift_a(16);
            shift_b(14 downto 1) <= shift_b(13 downto 0);
            shift_b(0) <= rx_cond(1);
          end if;
          
          -- If we receive a start bit while the previous start bit is still in
          -- position 33 of the shift register, the next frame is a continuation
          -- of this packet.
          if start_bit = '1' then
            uart2tl_first <= shift_a(17);
          end if;
        end if;
      end if;
    end process;
    
    -- rx_cond is the stop bit when the start bit has shifted to position 32.
    stop_bit <= not shift_a(16) and valid_bit;
    
    -- rx_cond is the last data bit (LSB) when the start bit has shifted to
    -- position 31.
    uart2tl_valid <= valid_bit and shift_a(16) and not shift_a(15);
    uart2tl_data(31 downto 17) <= shift_a(14 downto 0);
    uart2tl_data(16) <= rx_cond(0);
    uart2tl_data(15 downto 1) <= shift_b(14 downto 0);
    uart2tl_data(0) <= rx_cond(1);
    
  end block;
  
end Behavioral;

