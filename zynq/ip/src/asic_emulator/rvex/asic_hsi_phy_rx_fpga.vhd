library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

-- TODO: this does NOT do delay calibration.

entity hsi_phy_rx is
  generic (
    
    -- TTD parameters for compatibility with the mock version; ignored.
    TTD_UNIT_DELAY              : time := 120 ps;
    TTD_NUM_BITS                : natural := 6;
    
    -- Number of differential pairs.
    N                           : natural := 4
    
  );
  port (
    
    -- System control.
    aresetn_phy                 : in  std_logic;
    clk_hsi                     : in  std_logic;
    
    -- calib is asserted high while the transmitters are generating a 10101010
    -- pattern to do delay calibration. Aside from potentially doing some small
    -- adjustments to compensate for temperature effects, the delay may not
    -- change significantly while calib_hsi is low.
    calib                       : in  std_logic;
    
    -- Lock is asserted high when all receivers have locked on.
    lock                        : out std_logic;
    
    -- Clock period register contents. Can be between assertion of aresetn_phy
    -- and lock
    hsi_period                  : in  std_logic_vector(7 downto 0);
    
    -- Data input.
    pad2phy_p                   : in  std_logic_vector(N-1 downto 0);
    pad2phy_n                   : in  std_logic_vector(N-1 downto 0);
    
    -- Synchronized data output.
    phy2dll_x                   : out std_logic_vector(N-1 downto 0);
    phy2dll_y                   : out std_logic_vector(N-1 downto 0)
    
  );
end hsi_phy_rx;

architecture Behavioral of hsi_phy_rx is
  
  -- Lock signal from each receiver.
  signal lock_int               : std_logic_vector(N-1 downto 0);
  
begin
  sig_gen: for i in 0 to N-1 generate
    
    -- The DDR data signal coming from the IBUF.
    signal d                    : std_logic;
    -- Delayed data from delay calibration.
    signal d_delay  : std_logic;
    
    -- The positive-edge-aligned parallelized DDR data signals.
    signal x, y                 : std_logic;
    
  begin
    
    -- Instantiate the LVDS IBUF.
    IBUFDS_inst : IBUFDS
      generic map (
        DIFF_TERM     => true,
        IBUF_LOW_PWR  => false,
        IOSTANDARD    => "LVDS_25"
      )
      port map (
        O  => d,
        I  => pad2phy_p(i),
        IB => pad2phy_n(i)
      );
    
--     -- Mock the delay calibration for now. Correct reception of the sync signal
--     -- is properly checked though.
--     lock_mock_block: block is
--       signal lock_delay   : unsigned(7 downto 0);
--       signal xr, yr       : std_logic;
--     begin
--       process (clk_hsi, aresetn_phy) is
--       begin
--         if aresetn_phy = '0' then
--           lock_delay <= (others => '0');
--           xr <= '0';
--           yr <= '0';
--           lock_int(i) <= '0';
--         elsif rising_edge(clk_hsi) then
--           xr <= x;
--           yr <= y;
--           if calib = '1' and lock_int(i) = '0' then
--             
--             -- During calibration, we should be receiving a DDR 10101010 pattern.
--             -- That means x and y should remain constant and should be each
--             -- others complement. Every cycle we increment an 8-bit counter if
--             -- that is the case; otherwise we clear it. When the counter reaches
--             -- 255 we assert our lock signal.
--             if (xr = x) and (yr = y) and (x = not y) and (not (is_x(x) or is_x(y))) then
--               if lock_delay /= "11111111" then
--                 lock_delay <= lock_delay + 1;
--               else
--                 lock_int(i) <= '1';
--               end if;
--             else
--               lock_delay <= (others => '0');
--             end if;
--           end if;
--         end if;
--       end process;
--     end block;
--     
--     -- Delay HSI clock by a quarter clock period. This ensures sampling in the
--     -- data 'eye'. This kinda fakes the delay calibration.
--     d_delay <= d after 0.625 ns;
    
    lock_int(i) <= '1';
    d_delay <= d;
    
    -- Without delay calibration, the receiver is just an IDDR.
    iddr_inst : iddr
      generic map (
        DDR_CLK_EDGE  => "SAME_EDGE",
        INIT_Q1       => '0',
        INIT_Q2       => '0',
        SRTYPE        => "SYNC"
      )
      port map (
        Q1 => y,
        Q2 => x,
        C  => clk_hsi,
        CE => '1',
        D  => d_delay,
        R  => '0',
        S  => '0'
      );
    
    -- Invert the data for PCB routing reasons, and register it for timing
    -- reasons.
    reg_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        phy2dll_x(i) <= not x;
        phy2dll_y(i) <= not y;
      end if;
    end process;
    
  end generate;
  
  -- Merge the lock signals.
  lock_proc: process (lock_int) is
    variable lock_v             : std_logic;
  begin
    lock_v := lock_int(0);
    for i in 1 to N-1 loop
      lock_v := lock_v and lock_int(i);
    end loop;
    lock <= lock_v;
  end process;
  
end Behavioral;

