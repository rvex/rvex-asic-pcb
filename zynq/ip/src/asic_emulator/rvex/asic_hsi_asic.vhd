library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.bus_pkg.all;

-- Used for set/reset register synthesis
library synopsys;
use synopsys.attributes.all;

entity hsi_asic is
  generic (
    
    -- Number of differential pairs. Each must be 1, 2 or 4.
    N_RX                        : natural := 4;
    N_TX                        : natural := 4;
    
    -- Number of clocks, may differ for each design
    N_CK                        : natural := 1
    
  );
  port (
    
    -- System control for the core.
    reset                       : out std_logic;
    clk                         : out std_logic;
    
    -- Reset for the memory. Released 8 cycles before the core reset.
    reset_mem                   : out std_logic;
    
    -- Scan chain interface. When scan_enable is high, ALL flipflops in the
    -- design must be disabled or part of a scan chain. Disabling can be done
    -- by gating the clock as well. scan_enable is only allowed to switch while
    -- clk is stopped low. It is assumed that there is a shift register from
    -- scan_out to scan_in clocked on the positive edge of clk. All shift
    -- registers need to be the same length.
    scan_enable                 : out std_logic;
    scan_in                     : out std_logic_vector(255 downto 0);
    scan_out                    : in  std_logic_vector(255 downto 0);
    
    -- Memory bus to FPGA.
    core2mem                    : in  bus_mst2slv_type;
    mem2core                    : out bus_slv2mst_type;
    
    -- Debug bus from FPGA.
    dbg2core                    : out bus_mst2slv_type;
    core2dbg                    : in  bus_slv2mst_type;
    
    -- GPIO-like stuff transferred over the HSI interface.
    irq                         : out std_logic_vector(15 downto 0);
    done                        : in  std_logic_vector(3 downto 0);
    idle                        : in  std_logic_vector(3 downto 0);
    configWord                  : in  std_logic_vector(31 downto 0);
    
    -- Core reset signal. This is basically the master reset; if this is
    -- asserted low, everything except the HSI PHY delay calibration is reset.
    -- Note that the core uses a synchronous reset though, so it won't reset
    -- until it gets its first clock somehow.
    pad_aresetn_core            : in  std_logic;
    
    -- PHY reset signal and UART receive pin B. If asserted low while core
    -- reset is also asserted low, the HSI PHY delay calibration logic is reset
    -- asynchronously in addition to all the other systems. While core reset is
    -- high, this is disconnected from the PHY reset and used as an additional
    -- FPGA to ASIC UART data pin.
    pad_aresetn_phy             : in  std_logic;
    
    -- Backup clock. This is used in UART mode as a backup to the HSI
    -- interface, in case there is a problem with the PHY or DLL.
    pad_backup_clk              : in  std_logic;
    
    -- HSI clock pads. The core clock is derived from this when running in HSI
    -- mode: either the core gets the same clock, or the core clock gets HSI
    -- divided by 2.
    pad_hsi_clk_p               : in  std_logic_vector(N_CK-1 downto 0);
    pad_hsi_clk_n               : in  std_logic_vector(N_CK-1 downto 0);
    
    -- Reference current pads.
    pad_Iref_tx                 : in  std_logic_vector(N_TX-1 downto 0);
    pad_Iref_rx                 : out std_logic_vector(N_RX-1 downto 0);
    
    -- HSI receiver pads.
    pad_f2a_p                   : in  std_logic_vector(N_RX-1 downto 0);
    pad_f2a_n                   : in  std_logic_vector(N_RX-1 downto 0);
    
    -- HSI transmitter pads.
    pad_a2f_p                   : out std_logic_vector(N_TX-1 downto 0);
    pad_a2f_n                   : out std_logic_vector(N_TX-1 downto 0);
    
    -- Mode selection pin. This pin does the following things:
    --  - falling edge of aresetn_phy while aresetn_core is low: low selects
    --    backup interface, high selects HSI.
    --  - rising edge of aresetn_phy while aresetn_core is low: low selects
    --    1:2 clock in HSI mode or reliable UART mode, high selects 1:4 clock
    --    in HSI mode or fast UART mode.
    --  - while aresetn_core is high: low for normal operation, high for scan
    --    chain access.
    --  - rising edge of mode pin while aresern_core is low: shift ctrl F2A
    --    value into HSI clock period register LSB.
    --  - rising edge of mode pin while aresern_core is high: shift ctrl F2A
    --    value into scan chain register LSB.
    pad_mode                    : in  std_logic;
    
    -- FPGA to ASIC control pin. This pin does the following things:
    --  - rising edge of mode pin while aresetn_core is low: shifted into HSI
    --    clock period register LSB.
    --  - rising edge of mode pin while aresetn_core is high: shifted into scan
    --    chain register LSB.
    --  - mode pin high: scan chain input.
    --  - mode pin low, HSI mode: active-high calibration request.
    --  - mode pin low, UART mode: primary UART data (aresetn_phy is
    --    secondary).
    pad_ctrl_f2a                : in  std_logic;
    
    -- ASIC to FPGA control pin. This pin does the following things:
    --  - mode pin high: scan chain output.
    --  - mode pin low, HSI mode: active-high lock output.
    --  - mode pin low, UART mode: UART data.
    pad_ctrl_a2f                : out std_logic
    
  );
end hsi_asic;

architecture Behavioral of hsi_asic is
  
  -- Interconnect between the I/Os and the internal logic.
  signal aresetn_phy_uart       : std_logic;
  signal aresetn_phy_local      : std_logic;
  signal aresetn_core           : std_logic;
  signal mode                   : std_logic;
  signal diff_clk               : std_logic;
  signal f2a_x, f2a_y           : std_logic_vector(N_RX-1 downto 0);
  signal a2f_x, a2f_y           : std_logic_vector(N_TX-1 downto 0);
  signal backup_clk             : std_logic;
  signal ctrl_f2a               : std_logic;
  signal ctrl_a2f               : std_logic;
  
  -- System control signals.
  signal reset_hsi_local        : std_logic;
  signal reset_mem_local        : std_logic;
  signal reset_core_local       : std_logic;
  signal clk_hsi_local          : std_logic;
  signal clk_core_local         : std_logic;
  signal ifsel                  : std_logic;
  signal ratio                  : std_logic;
  signal calib                  : std_logic;
  signal hsi_period             : std_logic_vector(7 downto 0);
  
  -- Scan chain signals.
  signal scan_enable_local      : std_logic;
  signal scan_out_reg           : std_logic;
  signal scan_sel               : std_logic_vector(7 downto 0);
  
  -- Buffer tree outputs.
  signal reset_core             : std_logic;
  signal reset_hsi              : std_logic;
  signal aresetn_phy            : std_logic;
  signal clk_core               : std_logic;
  signal clk_hsi                : std_logic;
  
  -- Multiplexed output signals.
  signal lock                   : std_logic;
  signal uart_tx                : std_logic;
  
  -- UART signals.
  signal uart_rx_data           : std_logic_vector(31 downto 0);
  signal uart_rx_first          : std_logic;
  signal uart_rx_valid          : std_logic;
  signal uart_tx_data           : std_logic_vector(31 downto 0);
  signal uart_tx_first          : std_logic;
  signal uart_tx_valid          : std_logic;
  signal uart_tx_ack            : std_logic;
  
  -- GPIOs received from the FPGA that are unused.
  signal gpio_unused            : std_logic_vector(27 downto 16);
  
  -- Used for set/reset register synthesis
  -- use sync_set_reset for synchronous set/reset
  -- use async_set_reset for asynchronous set/reset
  -- note that "true" makes the set/reset active high
  -- combining active high and active low set/reset is bad practice
  attribute sync_set_reset of reset_core: signal is "true";
  attribute sync_set_reset of reset_core_local: signal is "true";
  attribute sync_set_reset of reset_hsi: signal is "true";
  attribute sync_set_reset of reset_hsi_local: signal is "true";
  attribute sync_set_reset of reset_mem: signal is "true";
  attribute sync_set_reset of reset_mem_local: signal is "true";
  
begin

  -----------------------------------------------------------------------------
  -- Physical interface between Core/HSI logic and outside world
  -----------------------------------------------------------------------------
  hsi_phy_inst: entity work.hsi_phy
    generic map (
      N_RX                      => N_RX,
      N_TX                      => N_TX,
      N_CK                      => N_CK
    )
    port map (
      -- Core reset signal. 
      pad_aresetn_core          => pad_aresetn_core,
      aresetn_core              => aresetn_core,
      
      -- PHY reset signal and UART receive pin B. 
      pad_aresetn_phy           => pad_aresetn_phy,
      aresetn_phy_uart          => aresetn_phy_uart,
      
      -- Reset signal for the physical interface before the core.
      aresetn_phy_local         => aresetn_phy_local,
      aresetn_phy               => aresetn_phy,
      
      -- Backup clock.
      pad_backup_clk            => pad_backup_clk,
      backup_clk                => backup_clk,
      
      -- HSI clock pads. 
      pad_hsi_clk_p             => pad_hsi_clk_p,
      pad_hsi_clk_n             => pad_hsi_clk_n,
      diff_clk                  => diff_clk,
    
      -- Reference current pads.
      pad_Iref_tx               => pad_Iref_tx,
      pad_Iref_rx               => pad_Iref_rx,
      
      -- HSI receiver pads.
      pad_f2a_p                 => pad_f2a_p,
      pad_f2a_n                 => pad_f2a_n,
      
      -- HSI transmitter pads.
      pad_a2f_p                 => pad_a2f_p,
      pad_a2f_n                 => pad_a2f_n,
      
      -- Mode selection pin. 
      pad_mode                  => pad_mode,
      mode                      => mode,
      
      -- FPGA to ASIC control pin. 
      pad_ctrl_f2a              => pad_ctrl_f2a,
      
      -- ASIC to FPGA control pin. 
      pad_ctrl_a2f              => pad_ctrl_a2f,
      
      -- Interconnect between the I/Os and the internal logic.
      f2a_x                     => f2a_x,
      f2a_y                     => f2a_y,
      
      a2f_x                     => a2f_x,
      a2f_y                     => a2f_y,
      
      ctrl_f2a                  => ctrl_f2a,
      ctrl_a2f                  => ctrl_a2f,
      
      -- System control signals.
      calib                     => calib,
      lock                      => lock,
      hsi_period                => hsi_period,
    
      -- Buffer tree outputs.
      reset_hsi                 => reset_hsi,
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core
    );

  
  -----------------------------------------------------------------------------
  -- Instantiate reset and clock logic
  -----------------------------------------------------------------------------
  -- Reset logic. Both resets are asserted asynchronously whenever the input
  -- reset is asserted, but they release synchronous to the core clock. The
  -- HSI reset is released before the core clock.
  core_reset_logic: block is
    signal reset_sr : std_logic_vector(9 downto 0);
  begin
    reset_shift_reg: process (aresetn_core, clk_core_local) is
    begin
      if aresetn_core = '0' then
        reset_sr <= (others => '1');
      elsif rising_edge(clk_core_local) then
        reset_sr <= reset_sr(8 downto 0) & "0";
      end if;
    end process;
    reset_mem_local <= reset_sr(1);
    reset_core_local <= reset_sr(9);
  end block;
  hsi_reset_logic: block is
    signal reset_sr : std_logic;
  begin
    reset_shift_reg: process (aresetn_core, clk_hsi_local) is
    begin
      if aresetn_core = '0' then
        reset_sr <= '1';
        reset_hsi_local <= '1';
      elsif rising_edge(clk_hsi_local) then
        reset_sr <= '0';
        reset_hsi_local <= reset_sr;
      end if;
    end process;
  end block;
  
  -- Clock logic + buffer trees. See overview.[dia|pdf] for more information.
  clk_logic_inst: entity work.clk_asic
    port map (
      diff_clk                  => diff_clk,
      backup_clk                => backup_clk,
      ifsel                     => ifsel,
      ratio                     => ratio,
      reset_core_local          => reset_core_local,
      reset_mem_local           => reset_mem_local,
      reset_hsi_local           => reset_hsi_local,
      aresetn_phy_local         => aresetn_phy_local,
      scan_enable_local         => scan_enable_local,
      
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core,
      clk_hsi_local             => clk_hsi_local,
      clk_core_local            => clk_core_local,
      reset_core                => reset_core,
      reset_mem                 => reset_mem,
      reset_hsi                 => reset_hsi,
      aresetn_phy               => aresetn_phy,
      scan_enable               => scan_enable
    );
  
  -- Multiplex the backup TX, scan out, and HSI lock outputs.
  ctrl_a2f <= scan_out_reg when mode = '1'
         else lock         when ifsel = '1'
         else uart_tx;
  
  -- Output the core reset/clk.
  clk <= clk_core;
  reset <= reset_core;
  
  -----------------------------------------------------------------------------
  -- Control signal decoding logic
  -----------------------------------------------------------------------------
  -- Determine when to activate the PHY initialization logic.
  calib_ena_proc: process (clk_hsi, aresetn_phy) is
  begin
    if aresetn_phy = '0' then
      calib <= '0';
    elsif rising_edge(clk_hsi) then
      calib <= ctrl_f2a and ifsel;
    end if;
  end process;
  
  -- Decode the ifsel and ratio signals from the mode and aresetn_phy pins.
  ifsel_sel: process (aresetn_phy) is
  begin
    if falling_edge(aresetn_phy) then
      ifsel <= mode;
    end if;
  end process;
  ratio_sel: process (aresetn_phy) is
  begin
    if rising_edge(aresetn_phy) then
      ratio <= mode;
    end if;
  end process;
  
  -- Generate the HSI period shift register.
  hsi_period_proc: process (aresetn_phy, mode) is
  begin
    if aresetn_phy = '0' then
      hsi_period <= (others => '0');
    elsif rising_edge(mode) then
      hsi_period(7 downto 1) <= hsi_period(6 downto 0);
      hsi_period(0) <= ctrl_f2a;
    end if;
  end process;
  
  
  
  
  
  -----------------------------------------------------------------------------
  -- Instantiate scan chain logic
  -----------------------------------------------------------------------------
  -- The scan output is driven on the falling edge of the clock, in
  -- compliance with SPI mode 0. This also does the chain multiplexing.
  scan_out_reg_proc: process (clk_core) is
  begin
    if falling_edge(clk_core) then
      scan_out_reg <= scan_out(to_integer(unsigned(scan_sel)));
    end if;
  end process;
  
  -- Scan chains that are unused must feed back into themselves. Only the
  -- selected scan chain takes its input from the pin.
  scan_mux_proc: process (scan_out, scan_sel, ctrl_f2a) is
  begin
    for i in 0 to 255 loop
      if to_integer(unsigned(scan_sel)) = i then
        scan_in(i) <= ctrl_f2a;
      else
        scan_in(i) <= scan_out(i);
      end if;
    end loop;
  end process;
  
  -- Scan selection shift register. The shift register is clocked by the mode
  -- pad and takes its input from the calib/rx pad. In this way, it can be
  -- loaded without an active clock. This is necessary, because an active clock
  -- while mode is low would advance the state, and an active clock while mode
  -- is high would shift the scan chains.
  scan_sel_proc: process (mode) is
  begin
    if rising_edge(mode) then
      scan_sel(7 downto 1) <= scan_sel(6 downto 0);
      scan_sel(0) <= ctrl_f2a;
    end if;
  end process;
  
  -- The scan enable signal is just connected directly to the mode pin.
  scan_enable_local <= mode;
  
  -----------------------------------------------------------------------------
  -- Instantiate the UART receiver and transmitter
  -----------------------------------------------------------------------------
  uart_rx_inst: entity work.hsi_uart_asic_rx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      uart_rx(1)                => aresetn_phy_uart,
      uart_rx(0)                => ctrl_f2a,
      uart_mode                 => ratio,
      uart2tl_data              => uart_rx_data,
      uart2tl_first             => uart_rx_first,
      uart2tl_valid             => uart_rx_valid
    );
  
  uart_tx_inst: entity work.hsi_uart_asic_tx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      uart_tx                   => uart_tx,
      uart_mode                 => ratio,
      tl2dll_data               => uart_tx_data,
      tl2dll_first              => uart_tx_first,
      tl2dll_valid              => uart_tx_valid,
      dll2tl_ack                => uart_tx_ack
    );
  
  -----------------------------------------------------------------------------
  -- Instantiate the HSI stuff common to ASIC and FPGA
  -----------------------------------------------------------------------------
  hsi_common_inst: entity work.hsi_common
    generic map (
      N_RX                      => N_RX,
      N_TX                      => N_TX,
      SIDE                      => "ASIC"
    )
    port map (
      
      -- System control.
      reset_hsi                 => reset_hsi,
      reset_core                => reset_core,
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core,
      ifsel                     => ifsel,
      
      -- Data inputs and outputs to HSI PHY.
      rx_x                      => f2a_x,
      rx_y                      => f2a_y,
      tx_x                      => a2f_x,
      tx_y                      => a2f_y,
      
      -- UART interface.
      uart_rx_data              => uart_rx_data,
      uart_rx_first             => uart_rx_first,
      uart_rx_valid             => uart_rx_valid,
      uart_tx_data              => uart_tx_data,
      uart_tx_first             => uart_tx_first,
      uart_tx_valid             => uart_tx_valid,
      uart_tx_ack               => uart_tx_ack,
      
      -- To r-VEX bus master interfave.
      bus_master_m2s            => core2mem,
      bus_master_s2m            => mem2core,
      
      -- To r-VEX bus slave interfave.
      bus_slave_m2s             => dbg2core,
      bus_slave_s2m             => core2dbg,
      
      -- GPIO data.
      gpio_in(27 downto 24)     => "0000",
      gpio_in(23 downto 20)     => idle,
      gpio_in(19 downto 16)     => done,
      gpio_in(15 downto 0)      => configWord(15 downto 0),
      gpio_out(27 downto 16)    => gpio_unused,
      gpio_out(15 downto 0)     => irq
      
    );
  
end Behavioral;
