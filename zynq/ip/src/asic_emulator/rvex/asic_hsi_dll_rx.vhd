library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hsi_dll_rx is
  generic (
    
    -- Number of differential pairs. Must be a power of two.
    N                           : natural := 8;
    
    -- Number of output bits. Must be a multiple of 8*N.
    NUM_BITS                    : natural := 32
    
  );
  port (
    
    -- System control.
    reset_hsi                   : in  std_logic;
    clk_hsi                     : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- Data inputs. X must be before Y (stream is X0 Y0 X1 Y1 X2 Y2 etc.).
    phy2dll_X                   : in  std_logic_vector(N-1 downto 0);
    phy2dll_Y                   : in  std_logic_vector(N-1 downto 0);
    
    -- Word stream output in clk_core domain.
    dll2tl_data                 : out std_logic_vector(NUM_BITS-1 downto 0);
    dll2tl_first                : out std_logic;
    dll2tl_valid                : out std_logic
    
  );
end hsi_dll_rx;

architecture Behavioral of hsi_dll_rx is
  
  -- Used in the AND gate expression for ready and first.
  constant ONES                 : std_logic_vector(N-1 downto 0) := (others => '1');
  
  -- Ready signal from each channel.
  signal ready_vect             : std_logic_vector(N-1 downto 0);
  
  -- First-flit signal from each channel.
  signal first_vect             : std_logic_vector(N-1 downto 0);
  
  -- Data output signals from each channel.
  signal data_vect              : std_logic_vector(N*8-1 downto 0);
  
  -- Combined ready and first signals using a wide AND gate.
  signal ready, first           : std_logic;
  
  -- Ready, first, and data synchronized to the core clock domain.
  signal data_vect_core         : std_logic_vector(N*8-1 downto 0);
  signal ready_core, first_core : std_logic;
  
begin
  
  -- Instantiate the reception state machine for each differential pair.
  receiver_gen: for i in N-1 downto 0 generate
    
    -- Input signals and delayed versions.
    signal x, y, y_r            : std_logic;
    
    -- Swapped input signals and shift register.
    signal u, v                 : std_logic_vector(3 downto 0);
    
    -- FSM state.
    signal state, state_next    : std_logic_vector(3 downto 0);
    
    -- Swap control signal.
    -- Low: X | - A B E F   High: X | 1 C D G H
    --      Y | 1 C D G H         Y | A B E F -
    signal swap                 : std_logic;
    
    -- This signal marks that there was a break in the stream before the packet
    -- that is currently being received.
    signal first, first_r       : std_logic;
    
    -- Enable signal for the output register.
    signal ena                  : std_logic;  
    
  begin
    
    -- Assign the input signals to our local signals.
    x <= phy2dll_X(i);
    y <= phy2dll_Y(i);
    
    -- Instantiate the swap multiplexers.
    u(0) <= y_r when swap = '1' else x;
    v(0) <= x when swap = '1' else y;
    
    -- Generate the registers.
    reg_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        if reset_hsi = '1' then
          
          -- Receiver FSM reset state.
          state <= "0000";
          ready_vect(i) <= '0';
          
        else
          
          -- Receiver FSM state register.
          state <= state_next;
          
          -- Delay registers.
          y_r <= y;
          u(3 downto 1) <= u(2 downto 0);
          v(3 downto 1) <= v(2 downto 0);
          
          -- Output registers.
          if ena = '1' then
            ready_vect(i) <= '1';
            first_vect(i) <= first_r;
            data_vect(i*8+7) <= u(3);
            data_vect(i*8+6) <= u(2);
            data_vect(i*8+5) <= v(3);
            data_vect(i*8+4) <= v(2);
            data_vect(i*8+3) <= u(1);
            data_vect(i*8+2) <= u(0);
            data_vect(i*8+1) <= v(1);
            data_vect(i*8+0) <= v(0);
            first_r <= '0';
          elsif ready = '1' then
            
            -- When all receivers have been ready for one cycle, reset the ready
            -- registers.
            ready_vect(i) <= '0';
            
          end if;
          
          -- Create the "first" SR flipflop.
          if first = '1' then
            first_r <= '1';
          end if;
          
        end if;
      end if;
    end process;
    
    -- Instantiate the state machine logic.
    fsm_proc: process (x, y, state) is
    begin
      case state is
        
        -- Idle state.
        when "0000" =>
          swap <= '1';  -- Don't care.
          first <= '1'; -- Don't care.
          ena <= '0';
          if x = '1' then
            state_next <= "0001";
          elsif y = '1' then
            state_next <= "1001";
          else
            state_next <= "0000";
          end if;
        
        -- Phase AC of swapped flit after break.
        when "0001" =>
          swap <= '1';
          first <= '1';
          ena <= '0';
          state_next <= "0011";
        
        -- Phase AC of swapped flit immediately following previous flit.
        when "0010" =>
          swap <= '1';
          first <= '0';
          ena <= '0';
          state_next <= "0011";
        
        -- Phase BD of swapped flit.
        when "0011" =>
          swap <= '1';
          first <= '0';
          ena <= '0';
          state_next <= "0100";
        
        -- Phase EG of swapped flit.
        when "0100" =>
          swap <= '1';
          first <= '0';
          ena <= '0';
          state_next <= "0101";
        
        -- Phase FH of swapped flit.
        when "0101" | "0110" | "0111" =>
          swap <= '1';
          first <= '0'; -- Don't care.
          ena <= '1';
          if y = '1' then
            state_next <= "1010";
          else
            state_next <= "0000";
          end if;
        
        -- Slip state.
        when "1000" =>
          swap <= '0';  -- Don't care.
          first <= '0';
          ena <= '0';
          if x = '1' then
            state_next <= "0010";
          elsif y = '1' then
            state_next <= "1001";
          else
            state_next <= "0000";
          end if;
        
        -- Phase AC of aligned flit after break.
        when "1001" =>
          swap <= '0';
          first <= '1';
          ena <= '0';
          state_next <= "1011";
        
        -- Phase AC of aligned flit immediately following previous flit.
        when "1010" =>
          swap <= '0';
          first <= '0';
          ena <= '0';
          state_next <= "1011";
        
        -- Phase BD of aligned flit.
        when "1011" =>
          swap <= '0';
          first <= '0';
          ena <= '0';
          state_next <= "1100";
        
        -- Phase EG of aligned flit.
        when "1100" =>
          swap <= '0';
          first <= '0';
          ena <= '0';
          state_next <= "1101";
        
        -- Phase FH of aligned flit.
        when others =>
          swap <= '0';
          first <= '0'; -- Don't care.
          ena <= '1';
          state_next <= "1000";
        
      end case;
    end process;
    
  end generate;
  
  -- Generate the wide AND gate for ready and first.
  ready <= '1' when ready_vect = ONES else '0';
  first <= '1' when first_vect = ONES else '0';
  
  -- Clock crossing logic. This assumes that the clocks are synchronous and
  -- that the timing tools are aware of that. The tools should constrain the
  -- setup paths to the minimum time from a clk_hsi edge to a clk_core edge.
  -- The logic is:
  --                  _____
  -- ready >--o----\ \     \
  --          |     ) )     )--.                _____
  --          | .--/ /_____/   o-------------\ \     \
  --          | |   .-----.    |    .-----.   ) )     )---> valid_core
  --          | '---|Q   D|<---o--->|D   Q|--/ /_____/
  --          |  HSI|>r   |     core|>r   |
  --          |     '-----'         '-----'
  --          o----------------.
  --          |     .-----.    |
  --          '-----|E    |    |
  -- data_vect,  HSI|>r   |    v
  -- first >====O==>|D   Q|===|0\                           data_core
  --            |   '-----'   |  |========================> first_core
  --            '=============|1/
  --
  -- Note that there is a direct path from every input to every output.
  -- data_vect is driven directly by registers, ready and first go through an
  -- AND gate with as many inputs as there are differential pairs and then to
  -- a register. The outputs are registered immediately in the parallelizer
  -- block below. Thus, the longest path is either wide-AND -> xor -> xor,
  -- or wide-AND -> mux.
  xclk_block: block is
    
    -- Holding register for data_vect and first. When ready is high, the
    -- unregistered versions are selected, when ready is low, these are.
    signal data_vect_r            : std_logic_vector(N*8-1 downto 0);
    signal first_r                : std_logic;
    
    -- Sync toggles in the HSI clock domain whenever a new data word is ready.
    -- sync_rc follows sync in the core clock domain, allowing edges to be
    -- detected.
    signal sync, sync_rh, sync_rc : std_logic;
    
  begin
    
    -- We don't want to unnecessarily increase the minimum time that the data
    -- output is stable to the maximum period of clk_core as that would decrease
    -- the phase shift tolerance. But we don't want to insert another register
    -- either if we can help it. So we use a holding register as follows.
    holding_reg_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        if ready = '1' then
          data_vect_r <= data_vect;
          first_r <= first;
        end if;
      end if;
    end process;
    
    -- Output first and data, selecting between the new version and the
    -- registered version based on whether the former is valid.
    data_vect_core  <= data_vect when ready = '1' else data_vect_r;
    first_core      <= first when ready = '1' else first_r;
    
    -- Generate the synchronization logic for the ready signal.
    sync_reg_hsi_proc: process (clk_hsi) is
    begin
      if rising_edge(clk_hsi) then
        if reset_hsi = '1' then
          sync_rh <= '0';
        else
          sync_rh <= sync;
        end if;
      end if;
    end process;
    
    sync <= sync_rh xor ready;
    
    sync_reg_core_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_hsi = '1' then
          sync_rc <= '0';
        else
          sync_rc <= sync;
        end if;
      end if;
    end process;
    
    ready_core <= sync xor sync_rc;
    
  end block;
  
  -- This block handles parallelization in case the output bit count is larger
  -- than what is provided in one set of flits.
  parallizer_gen: if NUM_BITS > N*8 generate
    
    -- Parallelization ratio.
    constant RATIO              : natural := NUM_BITS / (N*8);
    
    -- Parallelization shift register.
    signal data_para            : std_logic_vector(NUM_BITS-1 downto 0);
    
    -- Shift register for the valid signal. This essentially behaves like a
    -- modulo RATIO counter. It is reset when flits with the "first" flag are
    -- received to achieve synchronization.
    signal valid_para           : std_logic_vector(RATIO-1 downto 0);
    
    -- Holding register for the "first" flag.
    signal first_reg            : std_logic;
    
    -- Registered version of the ready signal.
    signal ready_reg            : std_logic;
    
  begin
    
    reg_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_hsi = '1' then
          first_reg <= '0';
          ready_reg <= '0';
          valid_para <= (others => '0');
        else
          ready_reg <= ready_core;
          if ready_core = '1' then
            
            -- Generate the data shift register.
            data_para(NUM_BITS-1 downto N*8) <= data_para(NUM_BITS-N*8-1 downto 0);
            data_para(N*8-1 downto 0) <= data_vect_core;
            
            -- Generate the first and valid logic.
            if first_core = '1' then
              valid_para(RATIO-1 downto 1) <= (others => '0');
              valid_para(0) <= '1';
              first_reg <= '1';
            else
              valid_para(RATIO-1 downto 1) <= valid_para(RATIO-2 downto 0);
              valid_para(0) <= valid_para(RATIO-1);
              if valid_para(RATIO-1) = '1' then
                first_reg <= '0';
              end if;
            end if;
          end if;
        end if;
      end if;
    end process;
    
    dll2tl_data   <= data_para;
    dll2tl_first  <= first_reg;
    dll2tl_valid  <= valid_para(RATIO-1) and ready_reg;
    
  end generate;
  
  -- If we don't need a parallelizer, simply generate output registers.
  no_parallelizer_gen: if NUM_BITS <= N*8 generate
  begin
    out_reg_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_hsi = '1' then
          dll2tl_valid  <= '0';
        else
          dll2tl_data   <= data_vect_core;
          dll2tl_first  <= first_core;
          dll2tl_valid  <= ready_core;
        end if;
      end if;
    end process;
  end generate;
  
end Behavioral;

