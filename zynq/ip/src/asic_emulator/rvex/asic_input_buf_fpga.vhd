library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

entity input_buf is
  port (
    pad   : in  std_logic;
    int   : out std_logic
  );
end input_buf;

architecture Behavioral of input_buf is
begin
  
  ibuf_inst: IBUF
    generic map (
      IBUF_LOW_PWR => true,
      IOSTANDARD => "LVCMOS25"
    )
    port map (
      I => pad,
      O => int
    );
  
end Behavioral;

