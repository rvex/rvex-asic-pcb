library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.hsi_pkg.all;

entity hsi_tl_tx is
  generic (
    
    -- Size of the shift register for the bus response. This must be as large
    -- as the maximum amount of beats in a burst transfer.
    RESP_SR_LEN                 : natural := 8
    
  );
  port (
    
    -- System control.
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- Data outputs to HSI data link layer.
    tl2dll_data                 : out std_logic_vector(31 downto 0);
    tl2dll_first                : out std_logic;
    tl2dll_valid                : out std_logic;
    dll2tl_ack                  : in  std_logic;
    
    -- Bus master request.
    tlbm2tltx_req               : in  hsi_bus_req_type;
    tltx2tlbm_ack               : out std_logic;
    
    -- Bus slave response.
    tlbs2tltx_resp              : in  hsi_bus_resp_type;
    
    -- GPIO data.
    gpio2tl                     : in  std_logic_vector(27 downto 0);
    
    -- Sync request. When asserted, sync words 
    sync_req                    : in  std_logic
    
  );
end hsi_tl_tx;

architecture Behavioral of hsi_tl_tx is
  
  -- Signals from bus master.
  signal bm_req                 : hsi_bus_req_type;
  signal bm_ack                 : std_logic;
  
  -- Signals from bus slave.
  signal bs_resp                : hsi_bus_resp_type;
  signal bs_ack                 : std_logic;
  
  -- GPIO signals.
  signal gpio_r                 : std_logic_vector(27 downto 0);
  signal gpio_changed           : std_logic;
  signal gpio_ack               : std_logic;
  
  -- FSM state.
  type fsm_type is (
    S_READY,   -- Line is ready for a new packet.
    S_BM_WORD, -- A bus master 32-bit write packet is in progress.
    S_BM_PART, -- A bus master 16-bit or 8-bit write packet is in progress.
    S_BS       -- A bus slave multi-word packet is in progress.
  );
  signal state, state_r         : fsm_type;
  
begin
  
  -- Connect the bus master stuff directly.
  bm_req <= tlbm2tltx_req;
  tltx2tlbm_ack <= bm_ack;
  
  -- There is no flow control in the r-VEX bus responses, so it's possible for
  -- the r-VEX bus to return data faster than the PHY can transmit it.
  -- Therefore we need a buffer. We only ever need to buffer the maximum burst
  -- length, so fortunately this FIFO is only one deep on the ASIC side (as it
  -- does not need to support bursts).
  bs_response_fifo_block: block is
    
    -- Shift register signals.
    signal sr, sr_r             : hsi_bus_resp_array(RESP_SR_LEN-1 downto 0);
    
  begin
    
    -- Shift when data is being consumed.
    reg_proc: process (clk_core) is
    begin
      if rising_edge(clk_core) then
        if reset_core = '1' then
          for i in RESP_SR_LEN-1 downto 0 loop
            sr_r(i).valid <= '0';
          end loop;
        elsif bs_ack = '1' then
          for i in RESP_SR_LEN-2 downto 0 loop
            sr_r(i) <= sr(i+1);
          end loop;
          sr_r(RESP_SR_LEN-1).valid <= '0';
        else
          sr_r <= sr;
        end if;
      end if;
    end process;
    
    -- Override the output and register inputs with incoming data
    -- combinatorially to save a latency cycle.
    comb_proc: process (sr_r, tlbs2tltx_resp) is
    begin
      sr <= sr_r;
      if tlbs2tltx_resp.valid = '1' then
        if sr_r(0).valid = '0' then
          sr(0) <= tlbs2tltx_resp;
        end if;
        for i in RESP_SR_LEN-1 downto 1 loop
          if sr_r(i).valid = '0' and sr_r(i-1).valid = '1' then
            sr(i) <= tlbs2tltx_resp;
          end if;
        end loop;
      end if;
    end process;
    
    -- Output the front of the FIFO.
    bs_resp <= sr(0);
    
  end block;
  
  -- Detect changes in the GPIO signal to determine when to send updates. The
  -- "changed" register is set during a reset
  gpio_reg_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      gpio_r <= gpio2tl;
      if reset_core = '1' or gpio_r /= gpio2tl then
        gpio_changed <= '1';
      elsif gpio_ack = '1' then
        gpio_changed <= '0';
      end if;
    end if;
  end process;
  
  -- State machine register.
  fsm_reg_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' or gpio_r /= gpio2tl then
        state_r <= S_READY;
      elsif dll2tl_ack = '1' then
        state_r <= state;
      end if;
    end if;
  end process;
  
  -- State machine combinatorial logic.
  fsm_comb_proc: process (
    state_r, bm_req, bs_resp, gpio_r, gpio_changed, sync_req, dll2tl_ack, 
    reset_core
  ) is
  begin
    
    -- Set acknowledge bits low unless otherwise specified.
    bm_ack <= '0';
    bs_ack <= '0';
    gpio_ack <= '0';
    
    -- Set next state to ready unless otherwise specified.
    state <= S_READY;
    
    -- Set default value of the to-be-sent packet to sync or invalid depending
    -- on whether sync words are requested.
    if sync_req = '1' then
      tl2dll_valid <= '1';
      tl2dll_first <= '1';
      tl2dll_data  <= X"33333333";
    else
      tl2dll_valid <= '0';
      tl2dll_first <= '1';
      tl2dll_data  <= X"00000000";
    end if;
    
    -- Use a priority encoder to determine what we should do next.
    if state_r = S_BS then
      
      -- Bus slave read data/fault.
      tl2dll_valid <= '1';
      tl2dll_first <= '0';
      tl2dll_data <= bs_resp.data;
      
      if bs_resp.valid = '0' then
        
        -- Waiting for the next beat in the burst.
        tl2dll_data(0) <= '0';
        state <= S_BS;
        
      elsif bs_resp.burst = '0' then
        
        -- 32-bit read data or fault code.
        bs_ack <= dll2tl_ack;
        
      elsif bs_resp.last = '0' then
        
        -- First or intermediate beat in 248-bit read burst.
        tl2dll_data(0) <= '1';
        bs_ack <= dll2tl_ack;
        state <= S_BS;
        
      else
        
        -- Last beat in 248-bit read burst.
        tl2dll_data(0) <= '1';
        bs_ack <= dll2tl_ack;
        
      end if;
      
    elsif state_r = S_BM_WORD then
      
      -- Bus master request 32-bit write data.
      tl2dll_valid <= '1';
      tl2dll_first <= '0';
      tl2dll_data <= bm_req.data;
      bm_ack <= dll2tl_ack;
      
    elsif state_r = S_BM_PART then
      
      -- Bus master request 16-bit or 8-bit write data.
      tl2dll_valid <= '1';
      tl2dll_first <= '0';
      if bm_req.mask(3) = '1' then
        tl2dll_data(15 downto 0) <= bm_req.data(31 downto 16);
      else
        tl2dll_data(15 downto 0) <= bm_req.data(15 downto 0);
      end if;
      tl2dll_data(19 downto 16) <= bm_req.mask;
      bm_ack <= dll2tl_ack;
      
    elsif bs_resp.valid = '1' then
      
      -- Bus slave response header.
      tl2dll_valid <= '1';
      tl2dll_first <= '1';
      tl2dll_data(31 downto 28) <= "0001";
      tl2dll_data(27) <= bs_resp.burst;
      tl2dll_data(26) <= bs_resp.wr;
      tl2dll_data(25) <= bs_resp.fault;
      if bs_resp.wr = '1' and bs_resp.fault = '0' then
        bs_ack <= dll2tl_ack;
      else
        state <= S_BS;
      end if;
      
    elsif bm_req.valid = '1' then
      
      -- Bus master request header.
      tl2dll_valid <= '1';
      tl2dll_first <= '1';
      tl2dll_data(26 downto 0) <= bm_req.addr(31 downto 5);
      
      if bm_req.wr = '1' then
        if bm_req.mask = "1111" then
          
          -- 32-bit write header.
          tl2dll_data(31 downto 27) <= "10" & bm_req.addr(4 downto 2);
          state <= S_BM_WORD;
          
        else
          
          -- 16-bit or 8-bit write header.
          tl2dll_data(31 downto 27) <= "11" & bm_req.addr(4 downto 2);
          state <= S_BM_PART;
          
        end if;
      elsif bm_req.burst = '1' then
        
        -- 248-bit read header.
        tl2dll_data(31 downto 27) <= "00001";
        bm_ack <= dll2tl_ack;
        
      else
        
        -- 32-bit read header.
        tl2dll_data(31 downto 27) <= "01" & bm_req.addr(4 downto 2);
        bm_ack <= dll2tl_ack;
        
      end if;
      
    elsif gpio_changed = '1' and reset_core = '0' then
      
      -- GPIO data.
      tl2dll_valid <= '1';
      tl2dll_first <= '1';
      tl2dll_data  <= "0010" & gpio_r;
      gpio_ack <= dll2tl_ack;
      
    end if;
    
  end process;
  
end Behavioral;

