library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.hsi_pkg.all;

library work;
use work.bus_pkg.all;

entity hsi_tl_bs is
  generic (
    SIDE                        : string
  );
  port (
    
    -- System control.
    reset_bus                   : in  std_logic := '0';
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- r-VEX bus interface.
    tl2bs                       : out bus_mst2slv_type;
    bs2tl                       : in  bus_slv2mst_type;
    
    -- Bus slave request.
    tlrx2tlbs_req               : in  hsi_bus_req_type;
    
    -- Bus slave response. The request is buffered in the transmitter, so no
    -- flow control is necessary.
    tlbs2tltx_resp              : out hsi_bus_resp_type;
    
    -- Asserted for one cycle when a request packet is received while we're
    -- still working on the previous request.
    error                       : out std_logic
    
  );
end hsi_tl_bs;

architecture Behavioral of hsi_tl_bs is
  signal bus_req                : hsi_bus_req_type;
  signal addr_next              : std_logic_vector(4 downto 2);
  signal continue               : std_logic;
  signal cancelled              : std_logic;
begin
  
  -- State registers and logic.
  reg_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' then
        if SIDE = "ASIC" then
          bus_req.valid <= '0';
          cancelled <= '0';
        elsif bus_req.valid = '1' then
          cancelled <= '1';
        end if;
        error <= '0';
      else
        error <= '0';
        
        if bus_req.valid = '0' then
          
          -- Start a bus transfer is we receive a request packet.
          if tlrx2tlbs_req.valid = '1' then
            bus_req <= tlrx2tlbs_req;
            cancelled <= '0';
          end if;
          
        else
          
          -- Can't handle packets while already busy.
          if tlrx2tlbs_req.valid = '1' then
            error <= '1';
          end if;
          
          -- Update the state.
          bus_req.valid <= continue;
          cancelled <= cancelled and continue;
          bus_req.addr(4 downto 2) <= addr_next;
          
        end if;
      end if;
      if reset_bus = '1' then
        bus_req.valid <= '0';
        cancelled <= '0';
      end if;
    end if;
  end process;
  
  -- Combinatorial state logic.
  comb_proc: process (bus_req, bs2tl) is
  begin
    
    -- Maintain the current state unless otherwise specified.
    continue <= bus_req.valid;
    addr_next <= bus_req.addr(4 downto 2);
    
    -- Handle bus acknowledgements.
    if bs2tl.ack = '1' then
      
      -- Don't request a new transfer if we're not bursting or if we just did
      -- the last burst.
      if bus_req.burst = '0' or bus_req.addr(4 downto 2) = "111" then
        continue <= '0';
      else
        continue <= '1';
      end if;
      
      -- If we're bursting, update the beat counter.
      if bus_req.burst = '1' then
        addr_next <= std_logic_vector(unsigned(bus_req.addr(4 downto 2)) + 1);
      end if;
      
    end if;
    
  end process;
  
  -- Bus request driving logic.
  bus_req_proc: process (bus_req, continue, addr_next) is
  begin
    tl2bs <= BUS_MST2SLV_IDLE;
    tl2bs.address(31 downto 5) <= bus_req.addr(31 downto 5);
    tl2bs.address(4 downto 2) <= addr_next;
    tl2bs.address(1 downto 0) <= "00";
    tl2bs.readEnable <= continue and not bus_req.wr;
    tl2bs.writeEnable <= continue and bus_req.wr;
    tl2bs.writeMask <= bus_req.mask;
    tl2bs.writeData <= bus_req.data;
    tl2bs.flags.burstEnable <= bus_req.burst;
    if bus_req.burst = '1' and addr_next = "000" then
      tl2bs.flags.burstStart <= '1';
    else
      tl2bs.flags.burstStart <= '0';
    end if;
  end process;
  
  -- Response packet request logic.
  tlbs2tltx_resp.valid  <= bs2tl.ack and not cancelled;
  tlbs2tltx_resp.wr     <= bus_req.wr;
  tlbs2tltx_resp.fault  <= bs2tl.fault;
  tlbs2tltx_resp.burst  <= bus_req.burst;
  tlbs2tltx_resp.last   <= bus_req.burst and bus_req.addr(2)
                           and bus_req.addr(3) and bus_req.addr(4);
  tlbs2tltx_resp.data   <= bs2tl.readData;
  
end Behavioral;

