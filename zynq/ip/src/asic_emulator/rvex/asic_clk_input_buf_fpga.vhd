library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

entity clk_input_buf is
  port (
    pad   : in  std_logic;
    int   : out std_logic
  );
end clk_input_buf;

architecture Behavioral of clk_input_buf is
begin
  
  ibufg_inst: IBUFG
    generic map (
      IBUF_LOW_PWR => true,
      IOSTANDARD => "LVCMOS25"
    )
    port map (
      I => pad,
      O => int
    );
  
end Behavioral;

