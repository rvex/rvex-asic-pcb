#!/bin/sh

# If a `system.bit` file is found in the home directory of root, load that into
# the FPGA (so we don't have to repackage BOOT.BIN every time).
if [ -f /home/root/system.bit ]; then
  dd if=/home/root/system.bit of=/dev/xdevcfg
fi

# Start rvsrv.
/sbin/rvsrv -m/dev/mem:0x20000000:0x60000000
