
# Source Vivado environment.
source /opt/Xilinx/Vivado/2017.4/settings64.sh

# Make bare Vivado PL project for the board, synthesize, and generate.
vivado -mode batch -source vivado.tcl -nojournal -nolog

# Source Petalinux environment.
source /opt/petalinux/settings.sh
source /opt/petalinux/components/yocto/source/aarch64/site-config-aarch64-xilinx-linux

# Create Petalinux project.
petalinux-create --type project --template zynq --name test_boot
cd test_boot
petalinux-util --webtalk off

# Configure Petalinux.
petalinux-config --get-hw-description=../clean/clean.sdk

# MANUAL CONFIGURATION IN KCONFIG:
# - Put device tree blob on SD card so we can modify it:
#   Subsystem AUTO Hardware Settings
#    '-> Advanced bootable images storage Settings
#         '-> dtb image settings
#              '-> image storage media -> primary sd
#
# - Get rootfs from second partition:
#   Image Packaging Configuration
#    '-> Root filesystem type -> SD Card

# Build Petalinux.
petalinux-build

# Create BOOT.BIN.
petalinux-package --boot --format BIN --fsbl ./images/linux/zynq_fsbl.elf --fpga ../clean/clean.runs/impl_1/toplevel_wrapper.bit --u-boot --force

# Done with Petalinux, cd back to linux directory.
cd ..

# Modify the device tree to reserve memory for the PL.
dtc -I dtb -O dts test_boot/images/linux/system.dtb > system.dts

read -p "Manual configuraion required, see $0. Hell, you shouldn't be running this script like this anyway."
# MANUALLY MODIFY SYSTEM.DTS:
# Replace the following (it's at the end of the file):
# 
#   memory {
#     device_type = "memory";
#     reg = <0x0 0x40000000>;
#   };
# 
# with:
# 
#   memory@00000000 {
#     device_type = "memory";
#     reg = <0x0 0x40000000>;
#   };
# 
#   reserved-memory {
#     #address-cells = <0x1>;
#     #size-cells = <0x1>;
#     ranges;
# 
#     fabric@20000000 {
#       reg = <0x20000000 0x20000000>;
#     };
#   };

dtc -I dts -O dtb system.dts > system.dtb

# Build rvsrv.
make -C rvsrv

# Format uSD card.
# Replace /dev/sdX with card device name! Also note that these commands need
# sudo/root!
dd if=/dev/zero of=/dev/sdX bs=1024 count=1
fdisk /dev/sdX

# MANUAL CONFIGURATION IN FDISK:
#   Welcome to fdisk (util-linux 2.29.2).                                                                                                                                                                   
#   Changes will remain in memory only, until you decide to write them.
#   Be careful before using the write command.
# 
#   Device does not contain a recognized partition table.
#   Created a new DOS disklabel with disk identifier 0xbebfc507.
# 
#   Command (m for help): p
#   Disk test: 8 GiB, 8589934592 bytes, 16777216 sectors
#   Units: sectors of 1 * 512 = 512 bytes
#   Sector size (logical/physical): 512 bytes / 512 bytes
#   I/O size (minimum/optimal): 512 bytes / 512 bytes
#   Disklabel type: dos
#   Disk identifier: 0xbebfc507
# 
#   Command (m for help): x
# 
#   Expert command (m for help): h
#   Number of heads (1-256, default 255): 255
# 
#   Expert command (m for help): s
#   Number of sectors (1-63, default 63): 63
# 
#   Expert command (m for help): c
#   Number of cylinders (1-1048576, default 1044):
# 
#   Expert command (m for help): r
# 
#   Command (m for help): n
#   Partition type
#       p   primary (0 primary, 0 extended, 4 free)
#       e   extended (container for logical partitions)
#   Select (default p):
# 
#   Using default response p.
#   Partition number (1-4, default 1):
#   First sector (2048-16777215, default 2048):
#   Last sector, +sectors or +size{K,M,G,T,P} (2048-16777215, default 16777215): +200M
# 
#   Created a new partition 1 of type 'Linux' and of size 200 MiB.
# 
#   Command (m for help): t
#   Selected partition 1
#   Partition type (type L to list all types): c
#   Changed type of partition 'Linux' to 'W95 FAT32 (LBA)'.
# 
#   Command (m for help): n
#   Partition type
#       p   primary (1 primary, 0 extended, 3 free)
#       e   extended (container for logical partitions)
#   Select (default p):
# 
#   Using default response p.
#   Partition number (2-4, default 2):
#   First sector (411648-16777215, default 411648):
#   Last sector, +sectors or +size{K,M,G,T,P} (411648-16777215, default 16777215): 
# 
#   Created a new partition 2 of type 'Linux' and of size 7.8 GiB.
# 
#   Command (m for help): t
#   Partition number (1,2, default 2):
#   Partition type (type L to list all types): 83
# 
#   Changed type of partition 'Linux' to 'Linux'.
# 
#   Command (m for help): p
#   Disk test: 8 GiB, 8589934592 bytes, 16777216 sectors
#   Units: sectors of 1 * 512 = 512 bytes
#   Sector size (logical/physical): 512 bytes / 512 bytes
#   I/O size (minimum/optimal): 512 bytes / 512 bytes
#   Disklabel type: dos
#   Disk identifier: 0xbebfc507
# 
#   Device     Boot  Start      End  Sectors  Size Id Type
#   /dev/sdf1         2048   411647   409600  200M  c W95 FAT32 (LBA)
#   /dev/sdf2       411648 15523839 15112192  7.2G 83 Linux
# 
#   Command (m for help): w
#   The partition table has been altered.
#   Syncing disks.

mkfs.vfat -F 32 -n boot /dev/sdX1
mkfs.ext4 -L root /dev/sdX2

# The partition table should now be:
# Device     Boot   Start      End  Sectors  Size Id Type
# /dev/sdX1          2048  1050623  1048576  512M  c W95 FAT32 (LBA)
# /dev/sdX2       1050624 15523839 14473216  6.9G 83 Linux

# Mount the new partitions.
mkdir -p mnt/boot
mount /dev/sdX1 `pwd`/mnt/boot
mkdir -p mnt/root
mount /dev/sdX2 `pwd`/mnt/root

# Copy the bootloader files.
cp -t mnt/boot test_boot/images/linux/BOOT.BIN test_boot/images/linux/image.ub test_boot/images/linux/system.dtb

# To use the Petalinux rootfs, use the following. You can also use a different
# ARM-HF rootfs though, such as:
# https://rcn-ee.com/rootfs/eewiki/minfs/ubuntu-16.04.2-minimal-armhf-2017-06-18.tar.xz
sudo tar -xzvf test_boot/images/linux/rootfs.tar.gz -C mnt/root

# Copy rvsrv.
cp -t mnt/root/sbin rvsrv/rvsrv

# Copy init script.
cp -t mnt/root/etc/init.d rvsrv/rvsrv-init.sh
chmod 0755 mnt/root/etc/init.d/rvsrv-init.sh
chown root:root mnt/root/etc/init.d/rvsrv-init.sh

# Unmount.
umount `pwd`/mnt/boot
umount `pwd`/mnt/root
rmdir mnt/boot
rmdir mnt/root
rmdir mnt

# Once you have the board up and running, you need to run:
#   update-rc.d rvsrv-init.sh defaults
# to make init.d run the rvsrv-init.sh script.
