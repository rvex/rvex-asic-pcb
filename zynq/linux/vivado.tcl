# Tested using Vivado 2017.4

# Create project
create_project clean clean -part xc7z020clg484-2
set_property target_language VHDL [current_project]

# Create block diagram
create_bd_design "toplevel"

# Processing system IP
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 ps
set_property -dict [list \
  CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125} \
  CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
  CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} \
  CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
  CONFIG.PCW_ENET0_GRP_MDIO_IO {MIO 52 .. 53} \
  CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
  CONFIG.PCW_UART0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_UART0_UART0_IO {MIO 14 .. 15} \
  CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
  CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39}\
] [get_bd_cells ps]

# Block RAM + controller for testing the interface
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 bram
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 bram_ctrl
set_property -dict [list \
  CONFIG.PROTOCOL {AXI4LITE} \
  CONFIG.SINGLE_PORT_BRAM {1} \
  CONFIG.ECC_TYPE {0} \
] [get_bd_cells bram_ctrl]

# Automation to hook everything up
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" Master "Disable" Slave "Disable" }  [get_bd_cells ps]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/ps/M_AXI_GP0" intc_ip "New AXI Interconnect" Clk_xbar "Auto" Clk_master "Auto" Clk_slave "Auto" }  [get_bd_intf_pins bram_ctrl/S_AXI]
apply_bd_automation -rule xilinx.com:bd_rule:bram_cntlr -config {BRAM "Auto" }  [get_bd_intf_pins bram_ctrl/BRAM_PORTA]
regenerate_bd_layout

# Generate and wrap everything up
validate_bd_design
set_property synth_checkpoint_mode None [get_files clean/clean.srcs/sources_1/bd/toplevel/toplevel.bd]
generate_target all [get_files clean/clean.srcs/sources_1/bd/toplevel/toplevel.bd]
make_wrapper -files [get_files clean/clean.srcs/sources_1/bd/toplevel/toplevel.bd] -top
add_files -norecurse clean/clean.srcs/sources_1/bd/toplevel/hdl/toplevel_wrapper.vhd

# Synthesize etc.
launch_runs impl_1 -to_step write_bitstream -jobs 4
wait_on_run impl_1

# Export to SDK
file mkdir clean/clean.sdk
file copy -force clean/clean.runs/impl_1/toplevel_wrapper.sysdef clean/clean.sdk/toplevel_wrapper.hdf
