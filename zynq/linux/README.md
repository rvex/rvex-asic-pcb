
How to get Linux working on the TE0720
======================================

Step 0: don't do it if you don't have to. Seriously. I gave up after weeks of
random problems and being teased by *almost* working systems.

This is sort of just an archive of the stuff I did. So don't expect anything,
because it didn't work properly for me in the end. You might want to just start
from scratch. My basic idea anyway was to use a design with just a RAM block in
the block diagram to build Linux (I really couldn't care less about whether the
kernel is aware of what is in the PL - in fact I prefer it not to be so it
leaves it alone - but without that it doesn't enable some or other thing that
causes bus accesses to the PL to fail) and just program in a bitstream every
time (as part of the boot sequence) so I don't have to make a new kernel every
goddamn time. The problems I faced in the end was that I couldn't get the damn
ARM to access the PL like it would access peripherals; everything was always
cached, writeback-buffered and all. That's when I gave up and wrote `actrl` to
access the bus through JTAG instead.

If you're really stubborn, the steps to build *something* are detailed in the
`steps` file, which is *almost* a shell script, but not quite. It assumes you're
using Vivado 2017.4 and Petalinux 2017.4. I would strongly advise running the
lines step by step in a terminal until everything works, but at the very least
you should change some of the commands based on your system, so read through the
whole file. You can also skip steps depending on what you're doing using the
files in the `binaries` directory. But again, no guarantees whatsoever about
those binaries.
