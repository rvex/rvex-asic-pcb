#!/usr/bin/env python3

import sys, struct

with open(sys.argv[1], 'rb') as i:
  with open(sys.argv[2], 'wb') as o:
    while True:
      d = i.read(4)
      if len(d) < 4:
        break
      o.write(struct.pack('>i', *struct.unpack('<i', d)))
