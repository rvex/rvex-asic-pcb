#include "rvex.h"

#define RVEX_STDOUT_BUF_LEN 256
static int rvex_stdout_buf[RVEX_STDOUT_BUF_LEN/4] = {0};
static int rvex_stdout_ptr = 0;

/**
 * Prints a character to whatever platform the program is compiled for, if the
 * platform supports an output stream. Prototype conforms to the <stdio.h>
 * method.
 */
int putchar(int character) {
  if (rvex_stdout_ptr < RVEX_STDOUT_BUF_LEN-1) {
    ((char*)rvex_stdout_buf)[rvex_stdout_ptr ^ 3] = character;
    rvex_stdout_ptr++;
    ((char*)rvex_stdout_buf)[rvex_stdout_ptr ^ 3] = 0;
  }
  return 0;
}

/**
 * Same as putchar, but prints a null-terminated string. Prototype conforms to
 * the <stdio.h> method.
 */
int puts(const char *str) {
  while (*str) putchar((int)(*str++));
  return 0;
}

/**
 * Prints the string presented to it to the standard output of the platform,
 * and in addition reports success or failure, if supported by the platform.
 */
int rvex_succeed(const char *str) {
  return puts(str);
}
int rvex_fail(const char *str) {
  return puts(str);
}

/**
 * Reads a character from whatever input stream the platform has available,
 * waiting until one is available. Prototype conforms to the <stdio.h> method.
 */
int getchar(void) {
  while (1);
  return 0;
}

void interrupt(int id) {
}
