
r-VEX test software
===================

This is a VERY bare-bones system to compile `ucbqsort-fast` on the
(FPGA-emulated) ASIC and eventually run it.

***... unfortunately...*** it doesn't work, and I don't have time to debug it
anymore. The emulator is reporting illegal instruction/LIMMH forward traps in
the first instruction regardless of whether I byteswap the instructions or not.
but it's executing all-zero instructions as expected. That is, it runs what's
effectively NOPs until it reaches the end of the block RAM, at which point an
instruction fetch fault is triggered because the ASIC bus interconnect doesn't
just mirror everything like the r-VEX bus is usually configured to do, at which
point it jumps back to zero because the panic handler address has never been
written.

I'm not sure if the byteswap is necessary. I don't think it should be, but who
knows? Endianness is really confusing.

Anyway, program execution *does* work in the HSI IP block simulation
(`zynq/ip/prj/hsi`), and the physical HSI link seems to work just fine from
FPGA pin to FPGA pin (otherwise you wouldn't be able to access the emulator at
all), so it can't be *that* broken.

One thing that I thought of is that maybe the link reset isn't propagating to
the core correctly, because the link may not be running at the time. I'm not
sure about this because I haven't checked yet, but that is the next thing I
*would* check. Maybe that's breaking things when combined with a cache flush
command or something (as in, that the instruction cache isn't properly\
invalidated).

Failing that, a proper simulation of the entire Vivado block diagram may be in
order, or at least parts of it, but I've never done that before and I'm
definitely not going to figure it out in my last few hours as a TU Delft
employee.

Another thing I figured out last-minute is that it's not actually very nice that
the actrl `asic init` command reads the version registers of the core to check
connectivity, because of the ASIC bug that accessing the debug bus causes
undefined behavior in lanegroup 0 register accesses (see `asic` directory readme
file for a list of workarounds) and the fact that the r-VEX starts running
immediately after the link is brought up. So right now you have to initialize
the link, stop the r-VEX, upload the code, flush the caches, and finally reset
the r-VEX again.

The cache flush command, by the way, is handled by `rvsys_sairq.vhd` and is
documented there. It's basically just a matter of writing -1 to `CR_AFF` to
flush everything. I can't really test it with the simulator, but you should be
able to read the line valid bits through the scan chains when you're working
with the actual ASIC.

In the end, I'm not even sure if this bug is even present outside of the
emulator. So maybe it won't even affect you. In any case, good luck.

