
# Figure out command line.
if {[llength $argv] != 1} {
  puts "rvd protocol implementation for JTAG access through xsdb!"
  puts "You should pass a port for me to listen on using the command line."
  puts "For instance: xsdb-rvsrv 21079"
  puts "NOTE: this uses AXI-default endianless transactions for as far as that"
  puts "is possible. In practice, it means that what you read and write here"
  puts "through rvd is exactly what an r-VEX would see. But in memory the bytes"
  puts "within a 32-bit word are actually swapped, so while 32-bit word accesses"
  puts "from the ARM work as expected, byte accesses will not. So accessing"
  puts "registers from the ARM is fine, but reading r-VEX strings for instance"
  puts "requires arcane pointer magic."
  exit 2
}
set server_port [lindex $argv 0]

# Start Xilinx hw_server and connect to it.
conn

# Connect to the APU target intelligently.
set targets [split [targ] \n]
set ok 0
foreach target $targets {
  set a [regexp -inline -all -- {\S+} $target]
  if {[lindex $a 1] eq {APU}} {
    targ [lindex $a 0]
    puts "Connected to target [lindex $a 0]: [lindex $a 1]"
    set ok 1
    break
  }
}
if {!$ok} {
  puts "APU target not found! Cable connected to JTAG port? Board powered?"
  exit 1
}

# Handle incoming data.
proc handleComm {chan addr port} {
  if {[eof $chan]} {
    close $chan
    puts "Client $addr:$port closed connection"
    return
  }
  
  set cmd {}
  set x {bla}
  while {1} {
    set x [read $chan 1]
    if {($x eq {})} {
      puts "Client $addr:$port closed connection"
      close $chan
      return
    }
    if {($x eq ";")} {
      break
    }
    append cmd $x
  }
  
  
  set args [split "$cmd" {,}]
  set cmd [lindex $args 0]
  
  if {[string compare -nocase $cmd "read"] == 0} {
    
    # Read command! Argument 1 is the hex starting address, argument 2 is
    # decimal count.
    set hex_start [lindex $args 1]
    scan [lindex $args 1] %x start
    set count [lindex $args 2]
    
    # Debuggy message.
    puts "$addr:$port wants to read $count bytes from address 0x$hex_start onwards"
    
    # We need to byteswap for rvd... ugh. We should probably just read words
    # for the whole range we need, then work everything else out later.
    set aligned_start [expr $start & ~3]
    set aligned_end [expr ($start + $count + 3) & ~3]
    set word_count [expr ($aligned_end - $aligned_start) / 4]
    
    # Perform the read command.
    if {[catch {set data [mrd -force -val $aligned_start $word_count]} errmsg]} {
      set errmsg [regsub -all {[\s,]+} $errmsg {-}]
      puts "ERROR: $errmsg"
      puts $chan "Error,$cmd,$errmsg;"
      return
    }
    
    # Work out the response.
    set resp "OK,Read,OK,"
    append resp [lindex $args 1] "," [lindex $args 2] ","
    
    for {set i 0} {$i < $count} {incr i} {
      set j [expr $i + ($start & 3)]
      set sh [expr (3 - ($j & 3)) * 8]
      set idx [expr $j / 4]
      set byte [expr ([lindex $data $idx] >> $sh) & 255]
      append resp [format {%02X} $byte]
    }
    append resp ";"
    puts $chan $resp
    
  } elseif {[string compare -nocase $cmd "write"] == 0} {
    
    # Write command! Argument 1 is the hex starting address, argument 2 is
    # the decimal count, argument 3 is concatenated hex bytes.
    set hex_start [lindex $args 1]
    scan [lindex $args 1] %x start
    set count [lindex $args 2]
    set data [lindex $args 3]
    
    # Debuggy message.
    puts "$addr:$port wants to write $count bytes starting at address 0x$hex_start"
    
    # Write the data.
    if {[catch {
      set didx 0
      set end [expr $start + $count]
      for {set ptr $start} {$ptr < $end} {} {
        
        # Try to do word accesses if we can. The edges are done with byte
        # accesses.
        if {($ptr & 3) || ($ptr + 4 > $end)} {
          
          # Byte access.
          scan [string range $data $didx [expr $didx + 1]] %x value
          
          # We must swap the bytes around for little/no-endian.
          set real_ptr [expr $ptr ^ 3]
          mwr -force -size b $real_ptr $value
          #puts "Byte! $value -> $real_ptr"
          
          # Increment pointers.
          set ptr [expr $ptr + 1]
          set didx [expr $didx + 2]
          
        } else {
          
          # Word access.
          scan [string range $data $didx [expr $didx + 7]] %x value
          
          # Write the word.
          mwr -force -size w $ptr $value
          #puts "Word! $value -> $ptr"
          
          # Increment pointers.
          set ptr [expr $ptr + 4]
          set didx [expr $didx + 8]
          
        }
        
      }
    } errmsg]} {
      set errmsg [regsub -all {[\s,]+} $errmsg {-}]
      puts "ERROR: $errmsg"
      puts $chan "Error,$cmd,$errmsg;"
      return
    }
    
    puts $chan "OK,$cmd,OK,$hex_start,$count;"
    
  } else {
    
    puts $chan "Error,$cmd,UnsupportedCommand;"
    
  }
  
}

# Handle incoming connections.
proc accept {chan addr port} {
  puts "Accepted connection from $addr:$port"
  fconfigure $chan -buffering line
  fileevent $chan readable [list handleComm $chan $addr $port]
}

socket -server accept $server_port
puts "Listening for (py)rvd connections on port $server_port!"
vwait forever

