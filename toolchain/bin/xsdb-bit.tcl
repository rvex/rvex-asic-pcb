
# Figure out command line.
if {[llength $argv] != 1} {
  puts "One-liner to upload a new bitstream to the Zynq!"
  puts "Use me like this: xsdb-bit <bitstream.bit>"
  exit 2
}
set bitfile [lindex $argv 0]

# Start Xilinx hw_server and connect to it.
conn

# Connect to the APU target intelligently.
set targets [split [targ] \n]
set ok 0
foreach target $targets {
  set a [regexp -inline -all -- {\S+} $target]
  if {[lindex $a 1] eq {xc7z020}} {
    targ [lindex $a 0]
    puts "Connected to target [lindex $a 0]: [lindex $a 1]"
    set ok 1
    break
  }
}
if {!$ok} {
  puts "xc7z020 target not found! Cable connected to JTAG port? Board powered?"
  exit 1
}

fpga $bitfile
