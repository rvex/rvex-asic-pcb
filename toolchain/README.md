
Toolchain builder
=================

This directory builds a compiler/assembler toolchain for the r-VEX. For it to
work, this repository should be cloned into the `platforms` directory of the
following repository: https://bitbucket.org/rvex/rvex . Note that this
repository has a number of package requirements listed in its toplevel readme
file; you need to install all the listed required files. Once that's done,
simply run `make` and go grab a coffee.

If the toolchain builder gives you a stupid amount of trouble, you could also
grab precompiled binaries from r-VEX release 4.2. Just copy the contents of
`rvex-release-4.2/tools/open64/issue8` into a new directory named `prefix` here.
Then copy the contents of the `bin` and `lib` folders into `prefix/bin` and
`prefix/lib`, and use the `sourceme` file in this directory instead of calling
the one in `prefix` directly.

If you change the memory layout of the PL, you might want to change the
`config.ini` file here accordingly and recompile. The relevant values are marked
with comments. `CTRL_BASE_DBG` is not that important since it's overridden by
`rvd.map` anyway if you call `rvd` correctly, but it should be set to the
address of the HSI IP block plus 0x4000. You must also keep `rvd.map` in sync,
and if you change the location of the HSI from the default 0x41000000 you must
run `actrl` with the `--addr` flag. You're probably better off just keeping it
at 0x41000000 though, so you don't have to worry about all this.

If you're using the precompiled binaries you can't do the above. Instead, you'll
have to pass your own linker script to the compiler using the `-x` flag. The
default linker scripts are listed in `prefix/rvex-elf32/lib/ldscripts`, but
unfortunately the linker has these files baked into it, so just changing these
doesn't do anything.
