
import time
from actrl.commands import *

__all__ = ['read_clock_div', 'read_vco_freq', 'read_clock_freq', 'clock_status', 'register_cmds']

def read_clock_div(regs, name):
    """Reads information about a clock divider. name must be 'DIV', 'FB',
    'REF', 'HSI1', 'HSI2', 'HSI3', or 'CORE'. The return value is a tuple of
    the division value, the phase in degrees, and the duty cycle (high time)
    in percent."""
    
    def rd(rname):
        reg = regs.CLOCK.get(name + '_' + rname)
        if reg is not None:
            return reg.read()
        return 0
    
    phase   = rd('PHASE')
    high    = rd('HIGH')
    low     = rd('LOW')
    f_frac  = rd('F_FRAC')
    f_en    = rd('F_EN')
    f_mux_f = rd('F_MUX_F')
    f_wf_f  = rd('F_WF_F')
    f_wf_r  = rd('F_WF_R')
    edge    = rd('EDGE')
    bypass  = rd('BYPASS')
    delay   = rd('DELAY')
    
    if bypass == 1:
        
        # Counters bypassed. (At least I THINK this is what NO_COUNT does;
        # I'm not actually sure.)
        div = 1
        phase = 0
        duty = 50.0
        
    elif f_en == 1:
        
        # Best effort only; this only supports 0 phase configurations and
        # doesn't know what to do if the values are sort of misconfigured.
        
        # Determine odd_and_frac (see reference design etc., everything
        # depends on this.)
        odd_and_frac = f_frac + 8 * edge
        
        #print('name', name)
        #print('  odd_and_frac', odd_and_frac)
        
        # The low and high time values are fucking weird in fractional
        # mode. Compensate.
        even_part_high = high + (odd_and_frac < 9)
        even_part_low  = low  + (odd_and_frac < 10)
        if even_part_high != even_part_low:
            print('  Note: clock %s fractional circuitry is misconfigured; low/high mismatch' % name)
            return None
        #print('  even_part_low', even_part_low)
        div_x8   = 16 * even_part_low + 8 * edge + f_frac
        #print('  div_x8', div_x8)
        div      = div_x8 / 8
        div_int  = div_x8 // 8
        div_frac = div_x8 % 8
        
        # Check the F_WF_* registers for weirdness (whatever they are...)
        expected_f_wf_f = int(((odd_and_frac > 1) and (odd_and_frac < 10)) or ((div_frac == 1) and (div_int == 2))) # CRS610807... or something
        expected_f_wf_r = int((odd_and_frac > 0) and (odd_and_frac < 9))
        if f_wf_f != expected_f_wf_f or f_wf_r != expected_f_wf_r:
            print('  Note: clock %s fractional circuitry is misconfigured; f_wd_* mismatch' % name)
            return None
        
        # Check f_mux_f. At least I think this is right.
        if ((f_mux_f + 8 - phase) % 8) != (odd_and_frac // 2):
            print('  Note: clock %s high/low VCO taps are configured weirdly' % name)
            return None
        
        # Work out the phase.
        phase = (delay + phase / 8) / div * 360.0
        
        # Duty cycle is not configurable with the fractional dividers. I'm
        # kind of pulling this duty calculation out of my ass, but the
        # manual states that 50/50 duty is impossible with 1/8 fractions,
        # and judging by the way f_mux_f is rounded it seems to favor low
        # time.
        duty = ((div_x8 // 2) / div_x8) * 100.0
        
    else:
        
        # This is much easier compared to fractional division...
        div = low + high
        phase = (delay + phase / 8) / div * 360.0
        duty = (high + edge * 0.5) / div * 100.0
    
    return div, phase, duty

def read_vco_freq(regs):
    """Reads the MMCM VCO frequency."""
    div = read_clock_div(regs, 'DIV')
    fb = read_clock_div(regs, 'FB')
    if fb is None:
        return None
    return 100.0 / div[0] * fb[0]

def read_clock_freq(regs, name, vco=None):
    """Reads the frequency of the specified clock ('REF', 'HSI1', 'HSI2',
    'HSI3', or 'CORE'). If vco is specified, it is used as the current VCO
    frequency, otherwise it is measured by this function."""
    if vco is None:
        vco = read_vco_freq(regs)
    if vco is None:
        return None
    div, _, _ = read_clock_div(regs, name)
    return vco / div

def clock_status(regs):
    """Prints the current status of the MMCM."""

    if regs.STAT.MMCMR.read() == 1:
        print('MMCM status: \033[31;1mreset asserted\033[0m')
    elif regs.STAT.MMCML.read() == 1:
        print('MMCM status: \033[32;1mlocked\033[0m')
    else:
        print('MMCM status: \033[31;1mfailed to lock\033[0m')
    
    vco = read_vco_freq(regs)
    if vco is None:
        print('  \033[31;1mFailed to determine VCO fractional divider.\033[0m')
        return
    
    ref = read_clock_div(regs, 'REF')
    hsi_int = read_clock_div(regs, 'HSI1')
    hsi_out = read_clock_div(regs, 'HSI2')
    hsi_in = read_clock_div(regs, 'HSI3')
    core = read_clock_div(regs, 'CORE')
    
    vco_col = '\033[31;1m' if vco < 600 or vco > 1200 else '\033[32;1m'
    print('  VCO:     %s%7.2f MHz\033[0m' % (vco_col, vco))
    
    def report_clk(mnem, data):
        div, phase, duty = data
        clk = vco / div
        
        # Clock frequency verification.
        if mnem == 'Ref:    ':
            if clk < 190 or clk > 210:
                clk_col = '\033[31;1m' # Reference should be 200 MHz for IDELAYCTRL
            else:
                clk_col = '\033[32;1m' # OK
        elif mnem == 'Core:   ':
            if (div != hsi_int[0]*2) and (div != hsi_int[0]*4):
                clk_col = '\033[31;1m' # Ratio incorrect
            elif clk > 100:
                clk_col = '\033[33;1m' # Overclocked
            else:
                clk_col = '\033[32;1m' # OK
        elif mnem == 'HSI int:':
            if clk > 200:
                clk_col = '\033[33;1m' # Overclocked on FPGA side
            else:
                clk_col = '\033[32;1m' # OK
        else:
            if div != hsi_int[0]:
                clk_col = '\033[31;1m' # Ratio incorrect
            elif clk > 200:
                clk_col = '\033[33;1m' # Overclocked on FPGA side
            else:
                clk_col = '\033[32;1m' # OK
        
        # Clock phase verification.
        if mnem == 'HSI int:' or mnem == 'Core:   ':
            if int(round(phase)) != 0:
                phase_col = '\033[31;1m' # Internal HSI and core clock should not have nonzero phase
            else:
                phase_col = '\033[32;1m' # OK
        else:
            phase_col = '\033[32;1m' # Don't care
        
        # Clock duty cycle verification.
        if duty < 45 or duty > 55:
            if mnem == 'HSI out:':
                duty_col = '\033[33;1m' # User may be trying to compensate for difference in rise/fall time or something
            else:
                duty_col = '\033[31;1m' # Duty cycle should be 50/50
        else:
            duty_col = '\033[32;1m' # OK
        
        print('  %s %s%7.2f MHz\033[0m, %s%4.0f deg\033[0m, %s%2.0f%%\033[0m' % (mnem, clk_col, vco / div, phase_col, phase, duty_col, duty))
        return vco / div
    
    if ref is None:
        print('  Ref:    \033[31;1mFailed to determine fractional divider\033[0m')
    else:
        report_clk('Ref:    ', ref)
    report_clk('HSI int:', hsi_int)
    f_hsi = report_clk('HSI out:', hsi_out)
    report_clk('HSI in: ', hsi_in)
    f_core = report_clk('Core:   ', core)
    
    # Ugly hack: return the HSI and core clock frequencies. This is used by the
    # link status command.
    return f_hsi, f_core

def register_cmds(rootcmd, regs):
    """Registers the clock commands."""
    
    def clock_status_fn(*args):
        """Implementation of command `clock [status]`"""
        clock_status(regs)

    def clock_measure_fn(*args):
        """Implementation of command `clock measure`"""
        print('Measuring...')
        
        # Take the necessary measurements.
        ta1 = time.time()
        a = (regs.CLOCK.MON_REF.read(), regs.CLOCK.MON_HSI.read(), regs.CLOCK.MON_CORE.read())
        ta2 = time.time()
        time.sleep(1)
        tb1 = time.time()
        b = (regs.CLOCK.MON_REF.read(), regs.CLOCK.MON_HSI.read(), regs.CLOCK.MON_CORE.read())
        tb2 = time.time()
        
        # Figure out the time between the samples.
        ta = (ta1 + ta2) / 2
        tb = (tb1 + tb2) / 2
        t = tb - ta
        
        print('Time interval: %.4f' % t)
        
        # Gray to binary convertor.
        def gray2bin(val):
            mask = val
            while mask:
                mask = mask >> 1
                val = val ^ mask
            return val
        
        # Derive the AXI clock.
        axi = list(map(lambda x: gray2bin(x >> 16), a)) + list(map(lambda x: gray2bin(x >> 16), b))
        ref = axi[0]
        add = 0
        for i in range(1, len(axi)):
            axi[i] += add
            if axi[i] < ref:
                axi[i] += 0x10000
                add += 0x10000
            ref = axi[i]
        axi_a = (axi[0] + axi[1] + axi[2]) / 3
        axi_b = (axi[3] + axi[4] + axi[5]) / 3
        f_axi = ((axi_b - axi_a) * 16384) / (1000000 * t)
        
        print('AXI:  ~%7.2f MHz' % f_axi)
        
        for i, name in enumerate(['Ref.:', 'HSI: ', 'Core:']):
            axi_a = gray2bin(a[i] >> 16)
            axi_b = gray2bin(b[i] >> 16)
            if axi_b < axi_a:
                axi_b += 0x10000
            axi = axi_b - axi_a
            
            clk_a = gray2bin(a[i] & 0xFFFF)
            clk_b = gray2bin(b[i] & 0xFFFF)
            if clk_b < clk_a:
                clk_b += 0x10000
            clk = clk_b - clk_a
            
            if axi >= 1:
                ratio = clk / axi
                f_clk = f_axi * ratio
                s = 'x' if ratio >= 1 else '/'
                r = ratio if ratio >= 1 else 1. / ratio
                print('%s ~%7.2f MHz  (AXI %s %.3f)' % (name, f_clk, s, r))
            else:
                f_clk = (clk * 16384) / (1000000 * t)
                print('%s ~%7.2f MHz' % (name, f_clk))

    def clock_config_fn(*args):
        """Implementation of command `clock config [freq] [ratio] [in_phase]
        [out_phase] [out_duty]`"""
        try:
            clk_core  = float(args[0]) if len(args) > 0 else 100.0
            ratio     =   int(args[1]) if len(args) > 1 else 2
            in_phase  = float(args[2]) if len(args) > 2 else 0.0
            out_phase = float(args[3]) if len(args) > 3 else 90.0
            out_duty  = float(args[4]) if len(args) > 4 else 50.0
        except ValueError:
            print('Parse error on command line.')
            return
        
        clk_hsi = clk_core * ratio
        
        # Check stuff. Please excuse my bad humor, it's been a long day.
        if ratio not in [2, 4]:
            print('Ratio must be 2 or 4.')
            return
        if clk_core > 200 or clk_hsi > 400:
            import random
            print(random.choice(['Haha, no.', 'Don\'t be ridiculous, man.', 'That\'s a bit high, don\'t you think?', 'This kills the Vivado.']))
            return
        if clk_core < 10:
            print('Wow, that bad, huh? Sorry, the MMCM can\'t really go below 10 MHz.\nYou\'ll have to change the HDL design.')
            return
        if out_duty < 10 or out_duty > 90:
            print('That\'s a bit of a weird duty cycle you have there.')
            return
        
        # Work out the highest integer HSI divide ratio that works out to a VCO
        # frequency in the range of 600 to 1200 MHz.
        hsi_div = int(1200 / clk_hsi)
        target_vco = hsi_div * clk_hsi
        core_div = hsi_div * ratio
        
        # Find a pre-divider/multiplier pair that gives us a VCO close to that.
        error = target_vco
        for this_pre_div in range(1, 6):
            for this_fb_div in range(1, 64*8):
                this_vco = 100.0 * (this_fb_div / 8) / this_pre_div
                this_error = abs(target_vco - this_vco)
                if this_error < error:
                    pre_div = this_pre_div
                    fb_div = this_fb_div / 8
                    vco = this_vco
                    error = this_error
        
        # Work out the reference clock divider.
        ref_div = round((vco / 200.) * 8) / 8
        
        # Print configuration.
        print('Using VCO = 100 MHz / %d * %.3f = %.1f MHz' % (pre_div, fb_div, vco))
        print('Actual ref clock  = %.1f MHz (should be 200 +/- 10 MHz for IDELAYCTRL)' % (vco / ref_div))
        print('Actual HSI clock  = %.1f MHz' % (vco / hsi_div))
        print('Actual core clock = %.1f MHz' % (vco / core_div))
        
        # Configuration functions.
        def cfg_frac(name, div):
            
            #print(' - Configuring fractional clock %s' % name)
            #print('   req. div     == %.3f' % div)
            
            div = int(round(div * 8))
            div_frac = div % 8
            div_int  = div // 8
            
            even_part = div_int // 2
            odd = div_int % 2
            
            odd_and_frac = odd * 8 + div_frac
            
            #print('   div          == %d' % div)
            #print('   div_frac     == %d' % div_frac)
            #print('   div_int      == %d' % div_int)
            #print('   even_part    == %d' % even_part)
            #print('   odd          == %d' % odd)
            #print('   odd_and_frac == %d' % odd_and_frac)
            def wr(reg, val):
                #print('   %-12s := %d' % (reg, val))
                regs.CLOCK.get(reg).write(val)
            
            wr(name + '_PHASE'  , 0)
            wr(name + '_LOW'    , even_part - (odd_and_frac < 10))
            wr(name + '_HIGH'   , even_part - (odd_and_frac < 9))
            wr(name + '_F_FRAC' , div_frac)
            wr(name + '_F_EN'   , 1)
            wr(name + '_F_WF_F' , int(((odd_and_frac > 1) and (odd_and_frac < 10)) or ((div_frac == 1) and (div_int == 2)))) # CRS610807... or something
            wr(name + '_F_WF_R' , int((odd_and_frac > 0) and (odd_and_frac < 9)))
            wr(name + '_F_MUX_F', odd_and_frac // 2)
            wr(name + '_MX'     , 0)
            wr(name + '_EDGE'   , odd)
            wr(name + '_BYPASS' , 0)
            wr(name + '_DELAY'  , 0)
        
        def cfg_normal(name, div, phase, duty):
            
            #print(' - Configuring normal clock %s' % name)
            #print('   req. div     == %d' % div)
            #print('   req. phase   == %.0f' % phase)
            #print('   req. duty    == %.0d' % duty)
            
            # Phase normalization/calculation.
            while phase > 360.:
                phase -= 360.
            while phase < 0.:
                phase += 360.
            # ^ I was lazy. So sue me.
            phase_vco_x8 = int(round(8 * div * phase / 360.))
            
            # Duty cycle normalization/calculation.
            high_x2 = int(round(div * duty / 50.))
            if high_x2 < 2:
                high_x2 = 2
            elif high_x2 > (div*2 - 2):
                high_x2 = div*2 - 2
            
            #print('   phase        == %d' % phase)
            #print('   phase_vco_x8 == %d' % phase_vco_x8)
            #print('   high_x2      == %d' % high_x2)
            def wr(reg, val):
                #print('   %-12s := %d' % (reg, val))
                regs.CLOCK.get(reg).write(val)
            
            wr(name + '_LOW'   , div - high_x2 // 2)
            wr(name + '_HIGH'  , high_x2 // 2)
            wr(name + '_EDGE'  , high_x2 % 2)
            wr(name + '_BYPASS', int(div == 1))
            if name != 'DIV':
                wr(name + '_PHASE', phase_vco_x8 % 8)
                wr(name + '_MX'   , 0)
                wr(name + '_DELAY', phase_vco_x8 // 8)
        
        # Set power register to enable configuration.
        regs.CLOCK.POWER.write(0xFFFF)
        
        # Configure all the dividers.
        cfg_normal('DIV',  pre_div,  0.,        50.)
        cfg_frac  ('FB',   fb_div)
        cfg_frac  ('REF',  ref_div)
        cfg_normal('HSI1', hsi_div,  0.,        50.)
        cfg_normal('HSI2', hsi_div,  out_phase, out_duty)
        cfg_normal('HSI3', hsi_div,  in_phase,  50.)
        cfg_normal('CORE', core_div, 0.,        50.)
        
        # Figure out the filter and lock parameters in the same way that the
        # DRP reference design does it. "Some people say" that these values
        # actually mean jack shit in practice, but whatever.
        filt_1a, filt_1b, filt_1c, filt_2a, filt_2b, filt_2c, filt_2d, lock_ref_dly, lock_fb_dly, lock_cnt, lock_sat_hi, unlock_cnt = [
            (0b0, 0b01, 0b0, 0b1, 0b11, 0b10, 0b0, 0b00110, 0b00110, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b0, 0b1, 0b11, 0b10, 0b0, 0b00110, 0b00110, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b1, 0b01, 0b10, 0b0, 0b01000, 0b01000, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b0, 0b11, 0b1, 0b0, 0b11, 0b10, 0b0, 0b01011, 0b01011, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b10, 0b1, 0b0, 0b11, 0b10, 0b0, 0b01110, 0b01110, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b0, 0b1, 0b01, 0b10, 0b0, 0b10001, 0b10001, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b0, 0b1, 0b10, 0b10, 0b0, 0b10011, 0b10011, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b0, 0b01, 0b10, 0b0, 0b10110, 0b10110, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b0, 0b0, 0b10, 0b10, 0b0, 0b11001, 0b11001, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b0, 0b10, 0b10, 0b0, 0b11100, 0b11100, 0b1111101000, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b1, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1110000100, 0b1111101001, 0b0000000001),
            (0b1, 0b10, 0b1, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1100111001, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b1, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1011101110, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b1, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1010111100, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b1, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1010001010, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b1, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1001110001, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b0, 0b10, 0b10, 0b0, 0b11111, 0b11111, 0b1000111111, 0b1111101001, 0b0000000001),
            (0b1, 0b11, 0b1, 0b0, 0b10, 0b10, 0b0, 0b11111, 0b11111, 0b1000100110, 0b1111101001, 0b0000000001),
            (0b1, 0b10, 0b0, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b1000001101, 0b1111101001, 0b0000000001),
            (0b1, 0b10, 0b0, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b0111110100, 0b1111101001, 0b0000000001),
            (0b1, 0b10, 0b0, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b0111011011, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0111000010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0110101001, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0110010000, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0110010000, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0101110111, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0101011110, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0101011110, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0101000101, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0101000101, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0100101100, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0100101100, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0100101100, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0100010011, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0100010011, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0100010011, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b1, 0b00, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b1, 0b00, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b1, 0b00, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b1, 0b00, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b1, 0b00, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b11, 0b1, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b11, 0b1, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b0, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b0, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b0, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b0, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b11, 0b0, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b11, 0b0, 0b0, 0b00, 0b10, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b0, 0b11, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b0, 0b11, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b1, 0b0, 0b11, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b0, 0b0, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b10, 0b0, 0b1, 0b01, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001),
            (0b0, 0b01, 0b1, 0b1, 0b10, 0b00, 0b0, 0b11111, 0b11111, 0b0011111010, 0b1111101001, 0b0000000001)
        ][int(fb_div)]
        
        # Write the lock configuration.
        regs.CLOCK.LOCK_CNT.write(lock_cnt)
        regs.CLOCK.LOCK_FB_DLY.write(lock_fb_dly)
        regs.CLOCK.UNLOCK_CNT.write(unlock_cnt)
        regs.CLOCK.LOCK_REF_DLY.write(lock_ref_dly)
        regs.CLOCK.LOCK_SAT_HI.write(lock_sat_hi)
        
        # Write the filter configuration.
        regs.CLOCK.FILT_1A.write(filt_1a)
        regs.CLOCK.FILT_1B.write(filt_1b)
        regs.CLOCK.FILT_1C.write(filt_1c)
        regs.CLOCK.FILT_2A.write(filt_2a)
        regs.CLOCK.FILT_2B.write(filt_2b)
        regs.CLOCK.FILT_2C.write(filt_2c)
        regs.CLOCK.FILT_2D.write(filt_2d)
        
        # Clear MMCM reset by writing to the status register.
        regs.STAT.RAW.write(0)
        
        # Wait for a short amount of time.
        time.sleep(0.1)
        
        # Print status.
        clock_status_fn()

    clockcmd = CommandList('Clock management commands.', 'status')
    rootcmd.register('clock', clockcmd)
    
    clockcmd.register('status', FunctionCommand(
                     'Prints clock (MMCM) status.',
                     'Prints the current MMCM clocks, lock status, etc.',
                     clock_status_fn))
    
    clockcmd.register('measure', FunctionCommand(
                     'Measures the current clock frequencies.',
                     'Measures the frequencies of the AXI, reference, FPGA core, and HSI clocks.',
                     clock_measure_fn))
    
    clockcmd.register('config', FunctionCommand(
                     'Configures the ASIC clocks.',
                     'This command lets you configure the clocks for the ASIC.\n'
                     'Usage: clock config [core=100] [hsi-ratio=2] [in-phase=0] [out-phase=90] [out-duty=50]\n'
                     '[core]      = Core clock in MHz.\n'
                     '[hsi-ratio] = HSI/core clock ratio, must be 2 or 4.\n'
                     '[in-phase]  = Phase of the HSI A2F sampling clock for trace delay compensation.\n'
                     '              This might involve trial and error to get perfect. In theory, it\n'
                     '              should be [out-phase] +/- 90 deg + 2x trace delay + ASIC delay.\n'
                     '              0 is probably still fine for 200 MHz, but the effect will be more\n'
                     '              significant the higher the clock gets.\n'
                     '[out-phase] = Phase of the HSI clock to the ASIC. 90 puts the clock edge in the\n'
                     '              center of the eye in theory, neglecting ASIC clock network delay.\n'
                     '              Of course, the ASIC should be able to do delaycal on its own as\n'
                     '              well, but the FPGA clock is just below its specified limits, so,\n'
                     '              yeah...\n'
                     '[out-duty]  = Duty cycle of the HSI clock to the ASIC. *Maybe* tweaking this can\n'
                     '              make a dysfunctional HSI work, or make it work at slightly better\n'
                     '              frequencies.',
                     clock_config_fn))
    
