
__all__ = ['CommandLineError', 'Command', 'CommandList', 'CommandList', 'FunctionCommand', 'HelpCommand', 'RegAccessCommand', 'get_cmd', 'run_cmd', 'run_cli', 'source']

from .registers import RegisterList, RegisterError
import prompt_toolkit as pt
import os
import textwrap
import traceback

class CommandLineError(Exception):
    pass

class Command(object):
    """Represents an executable command, or possibly a list of subcommands."""
    
    def __init__(self, desc='Not implemented', man='Not implemented'):
        super().__init__()
        self.desc = desc
        self.man = man
    
    def parse_next(self, s):
        """Parse the next command line token and return a new Command() object.
        If the given token is invalid, raise a CommandLineError."""
        raise CommandLineError('Unexpected token: %s' % s)
    
    def suggestions(self):
        """Returns an iterable of suggestions for the next command line
        token."""
        return []
    
    def run(self):
        """Runs this command."""
        raise CommandLineError('Not implemented.')
    
    def describe(self):
        """Returns a one-liner string describing the command."""
        return self.desc
    
    __str__ = describe
    
    def help(self):
        """Prints help for this command."""
        print(self.man)

class CommandList(Command):
    """Represents a list of commands."""
    
    def __init__(self, desc, default=None, isroot=False):
        super().__init__(desc)
        self.cmds = {}
        self.default = default
        self.isroot = isroot
    
    def register(self, name, cmd):
        self.cmds[name] = cmd
    
    def parse_next(self, s):
        cmd = self.cmds.get(s.lower(), None)
        if cmd is None:
            raise CommandLineError('Command not found: %s' % s)
        return cmd
    
    def suggestions(self):
        return self.cmds.items()
    
    def run(self):
        if self.default:
            self.cmds[self.default].run()
        else:
            raise CommandLineError('Subcommand expected.')
    
    def help(self):
        """Prints help for this command."""
        print('\n'.join(textwrap.wrap((self.describe() + ' Available commands:').strip(), 80)))
        for name, cmd in sorted(self.cmds.items()):
            desc = ('\n'+' '*23).join(textwrap.wrap(cmd.describe(), 80 - 23))
            desc = '  %-16s - %s' % (name, desc)
            print(desc)

class FunctionCommand(Command):
    """Represents an actual completed command that just gets passed to a python
    function. The command line arguments get passed as *args."""
    
    def __init__(self, desc, man, fn, args=()):
        super().__init__(desc, man)
        self.fn = fn
        self.args = args
    
    def parse_next(self, s):
        return self.__class__(self.desc, self.man, self.fn, self.args + (s,))
    
    def run(self):
        self.fn(*self.args)

class HelpCommand(Command):
    """Help command."""
    
    def __init__(self, desc, man, cmd):
        super().__init__(desc, man)
        self.cmd = cmd
    
    def parse_next(self, s):
        if isinstance(self.cmd, CommandList) and self.cmd.isroot and s == 'me':
            return self.__class__(self.desc, self.man, 'me')
        if self.cmd != 'me':
            return self.__class__(self.desc, self.man, self.cmd.parse_next(s))
        return self
    
    def suggestions(self):
        """Returns an iterable of suggestions for the next command line
        token."""
        try:
            if self.cmd == 'me':
                return []
            if isinstance(self.cmd, HelpCommand):
                return []
            if isinstance(self.cmd, CommandList) and self.cmd.isroot:
                yield ('me', 'Everything is broken.')
            for x in self.cmd.suggestions():
                yield x
        except CommandLineError as e:
            return []
    
    def run(self):
        if self.cmd == 'me':
            print('Maybe. ' + '.'.join(['jeroen', 'van', 'straten@gmail', 'com']))
        else:
            self.cmd.help()

class RegAccessCommand(Command):
    """Register access command (with register suggestions)."""
    
    def __init__(self, desc, man, fn, reglist):
        super().__init__(desc, man)
        self.fn = fn
        self.reglist = reglist
    
    def parse_next(self, s):
        reg = self.reglist.get(s.upper())
        if reg is None:
            raise CommandLineError('Unknown register: %s' % s)
        if isinstance(reg, RegisterList):
            return self.__class__(self.desc, self.man, self.fn, reg)
        else:
            return FunctionCommand(self.desc, self.man, self.fn, (reg,))
    
    def suggestions(self):
        for reg in self.reglist:
            yield (reg.mnem, reg.desc)
    
    def run(self):
        raise CommandLineError('Register missing or incomplete.')

def get_cmd(rootcmd, args):
    """Returns the Command object for the given argument list."""
    cmd = rootcmd
    for arg in args:
        cmd = cmd.parse_next(arg)
    return cmd

def run_cmd(rootcmd, args):
    """Runs the specified command."""
    get_cmd(rootcmd, args).run()

def run_cli(rootcmd):
    
    # Build command completer.
    class Completer(pt.completion.Completer):
        """Provides autocompletions for commands."""
        def get_completions(self, document, complete_event):
            text = document.text[:document.cursor_position]
            last_wspace = text.rfind(' ')
            current_token = text[last_wspace+1:]
            #last_wspace = max(0, last_wspace)
            leading_tokens = text[:max(0, last_wspace)].split()
            #yield pt.completion.Completion('text:%s, start=%d, lead=%r, cur=%s' % (text, last_wspace, leading_tokens, current_token))
            try:
                leading_cmd = get_cmd(rootcmd, leading_tokens)
            except CommandLineError:
                return
            suggestions = leading_cmd.suggestions()
            for suggestion, description in sorted(suggestions):
                if current_token.lower() in suggestion.lower():
                    description = str(description)
                    if len(description) > 40:
                        description = description[:37] + '...'
                    description = '%-10s - %s' % (suggestion, description)
                    yield pt.completion.Completion(
                        suggestion,
                        display = description,
                        start_position=last_wspace-len(text)+1
                    )

    completer = Completer()

    # Work out where to store command history.
    try:
        hdir = os.environ['HOME']
        if not os.path.isdir(hdir+'/.actrl'):
            os.mkdir(hdir+'/.actrl')
        history = pt.history.FileHistory(hdir+'/.actrl/history')
    except KeyboardInterrupt:
        raise
    except:
        history = pt.history.InMemoryHistory()

    # Command loop.
    while 1:
        try:
            print(end='\033[34;1mactrl \033[0;32m>\033[0m ')
            args = pt.prompt('        ', history=history, completer=completer).split()
            print('\033[1A\033[34;1mactrl \033[0;32m>\033[0m ')
        except KeyboardInterrupt:
            exit(2)
        try:
            run_cmd(rootcmd, args)
        except KeyboardInterrupt:
            print('Interrupted.')
        except SystemExit:
            raise
        except (RuntimeError, CommandLineError, RegisterError) as e:
            print(e)
        except:
            traceback.print_exc()

def source(rootcmd, f):
    """Sources a file."""
    with open(f, 'r') as f:
        cmds = f.read().split('\n')
    for cmd in cmds:
        print('# %s' % cmd)
        cmd = cmd.split('#')[0].strip()
        if not cmd:
            continue
        run_cmd(rootcmd, cmd.split())

