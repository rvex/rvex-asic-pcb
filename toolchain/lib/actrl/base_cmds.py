
import sys
from actrl.commands import *
from plumbum import local, FG, ProcessExecutionError, CommandNotFound

def register_cmds(rootcmd, regs, cmdline_args):
    
    rootcmd.register('exit', FunctionCommand(
                    'Exits.',
                    'Calls Python sys.exit(...).',
                    sys.exit))

    rootcmd.register('help', HelpCommand(
                    'Provides help.',
                    'Provides documentation for the command passed to it.',
                    rootcmd))

    rootcmd.register('read', RegAccessCommand(
                    'Reads a control register.',
                    'Reads a control register.\nUsage: read <reg...>',
                    lambda reg: print(reg.read_fmt()), regs))

    rootcmd.register('write', RegAccessCommand(
                    'Writes a control register.',
                    'Writes a control register.\nUsage: write <reg...> <value>',
                    lambda reg, val: reg.write_fmt(val), regs))

    def rvd_fn(*args):
        x = local['rvd']['-p'][str(cmdline_args.port)]['-h'][cmdline_args.host]
        for arg in args:
            x = x[arg]
        try:
            x & FG
        except ProcessExecutionError:
            pass

    rootcmd.register('rvd', FunctionCommand(
                    'Runs an rvd command.',
                    'Runs an rvd command. For more information run "rvd help" instead.',
                    rvd_fn))

    def sys_fn(*args):
        if len(args) == 0:
            print('Command expected.')
            return
        try:
            x = local
            for arg in args:
                x = x[arg]
            x & FG
        except CommandNotFound:
            print('Command not found.')
        except ProcessExecutionError:
            pass

    rootcmd.register('sys', FunctionCommand(
                    'Runs an external program.',
                    'Runs an external program.\n'
                    'Usage: sys <program> [args...]\n'
                    'Note that the actrl shell currently doesn\'t support quotes or other fancy\n'
                    'escape or shell stuff. So yes, this is pretty minimal.',
                    sys_fn))

