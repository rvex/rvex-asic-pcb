
from .commands import *
from .power import *
from .clock import *
import time

__all__ = ['register_cmds']

def register_cmds(rootcmd, regs):
    
    asiccmd = CommandList('ASIC/HSI link management commands.', 'status')
    rootcmd.register('asic', asiccmd)

    def asic_status_fn(*args):
        ''
        
        def report_reset(reg):
            reset = reg.read()
            print('%s: %s' % (reg.desc, ('\033[31;1mAsserted\033[0m' if reset else '\033[32;1mReleased\033[0m')))
            return reset
        
        # Report power and clock status.
        power_status(regs)
        f_hsi, f_core = clock_status(regs)
        
        # Check whether we can access the control register.
        if not report_reset(regs.STAT.CTRLR):
            raw = regs.CTRL.RAW.read()
            if raw == 0xDEADC0DE or raw == 0xDEADBEEF:
                print('Control register access: \033[31;1mDenied\033[0m')
                return
            else:
                print('Control register access: \033[32;1mOK\033[0m')
        
        # Report the interface state.
        if not report_reset(regs.STAT.HSIR):
            mode = regs.CTRL.MODE.read()
            if mode < 2:
                if mode == 0:
                    print('Interface mode: \033[32;1mReliable UART\033[0m')
                else:
                    print('Interface mode: \033[32;1mFast UART\033[0m')
                print('  ctrl_a2f sample edge: %s' %   ('falling' if regs.CTRL.USMPL.read() else 'rising'))
                print('  ctrl_f2a setup edge: %s' %    ('falling' if regs.CTRL.USET1.read() else 'rising'))
                print('  aresetn_phy setup edge: %s' % ('falling' if regs.CTRL.USET2.read() else 'rising'))
            else:
                if mode == 2:
                    print('Interface mode: \033[32;1mLVDS 1:2\033[0m')
                    f_core = f_hsi / 2
                else:
                    print('Interface mode: \033[32;1mLVDS 1:4\033[0m')
                    f_core = f_hsi / 4
        
        # Report the ASIC state.
        if not report_reset(regs.STAT.ASICR):
            if f_core > 150:
                f_core_col = '\033[31;1m'
            elif f_core > 100:
                f_core_col = '\033[33;1m'
            else:
                f_core_col = '\033[32;1m'
            if regs.CTRL.CLKEN.read():
                clk_state = '\033[32;1mRunning\033[0m'
            else:
                clk_state = '\033[31;1mDisabled\033[0m'
            print('ASIC clock: %s, %s%.2f MHz\033[0m' % (clk_state, f_core_col, f_core))
            
            # Check whether we can access the ASIC debug bus.
            pver1 = regs.RVEX.PVER1.read()
            pver1_expected = 0x00525641
            pver0 = regs.RVEX.PVER0.read()
            pver0_expected = 0x53494331
            if pver1 == pver1_expected and pver0 == pver0_expected:
                print('ASIC access: \033[32;1mOK\033[0m')
            elif pver1 == 0xDEADC0DE or pver0 == 0xDEADC0DE:
                print('ASIC access: \033[31;1mDenied\033[0m')
            elif pver1 == 0xDEADBEEF and pver0 == 0xDEADBEEF:
                print('ASIC access: \033[31;1mNo response\033[0m')
            elif pver1 == 0xDEADBEEF or pver0 == 0xDEADBEEF:
                print('Bridge access: \033[31;1mSporadic response\033[0m')
            else:
                print('Bridge access: \033[31;1mUnexpected response:\033[0m')
                if pver1 != pver1_expected:
                    print('  PVER1 returned 0x%08X but should be 0x%08X' % (pver1, pver1_expected))
                if pver0 != pver0_expected:
                    print('  PVER0 returned 0x%08X but should be 0x%08X' % (pver0, pver0_expected))
        

    asiccmd.register('status', FunctionCommand(
                    'Prints the HSI link status.',
                    'Prints the status of the entire HSI link and tests ASIC health, including power\n'
                    'and clock configuration.',
                    asic_status_fn))

    def asic_init_fn(*args):
        mode = int(args[0]) if len(args) > 0 else 0
        
        if regs.STAT.CTRLR.read() or (regs.CTRL.RAW.read() in {0xDEADC0DE, 0xDEADBEEF}):
            print('Cannot access control register interface. Check the clock configuration!')
            return
        
        # Power cycle.
        print('Power-cycling...')
        regs.POWER.EN.write(0)
        time.sleep(0.8)
        regs.POWER.EN.write(1)
        time.sleep(0.2)
        if regs.POWER.OCP.read() or not regs.POWER.ENS.read():
            print('Failed to power up the ASIC!')
            return
        
        # We need to pass the clock period to the HSI for it to calibrate
        # correctly. So we need to convert the HSI clock period to its delay line
        # resolution, which is ~115ps (80-150ps over process).
        f_hsi = read_clock_freq(regs, 'HSI2')
        period = 1000000./f_hsi # ps
        period = int(round(period / 115))
        
        # Wait, actually, it seems that the period register isn't even connected in
        # the ASIC. What the fuck? Screw it, send the period anyway. Suck it up,
        # ASIC.
        
        # Give the reset command.
        print('Resetting interface...')
        regs.CTRL.RAW.write(0x20000000 | ((mode & 0x1F) << 24) | period)
        for i in range(100):
            if not regs.CTRL.CMD.read():
                break
            time.sleep(0.01)
        else:
            print('Interface reset failed? CMD didn\'t go to zero...')
        
        # If this is a UART mode, we need to explicitly start the clock.
        if (mode & 2) == 0:
            print('Starting clock...')
            regs.CTRL.RAW.write(0x60000000)
            try:
                for i in range(100):
                    if not regs.CTRL.CMD.read():
                        break
                    time.sleep(0.01)
                else:
                    print('Clock start failed? CMD didn\'t go to zero...')
            except KeyboardInterrupt:
                print('Interrupted.')
        
        # Interface should be up, print status.
        asic_status_fn()

    asiccmd.register('init', FunctionCommand(
                    '(Re)initializes the ASIC/HSI link.',
                    '(Re)initializes the ASIC/HSI link.\n'
                    'Usage: link init [mode=0]\n'
                    '<mode> must be 0 for reliable UART, 1 for fast UART, 2 for 1:2 HSI, and 4 for\n'
                    '1:4 HSI. Clock and power configuration are not affected, but a power cycle is\n'
                    'performed before the link is initialized. This is mostly just a convenience\n'
                    'command; there are so many controls involved with starting the HSI that I can\'t\n'
                    'really be bothered to implement them all. Access the HSI control register\n'
                    'manually if you need to do complicated stuff. When you find something that works,\n'
                    'just make it the default here.',
                    asic_init_fn))

    def gen_asic_cmd_fn(cmd, wait_for_cnt=False, arg_max=None, clk=None, uart=None):
        def fn(*args):
            
            # Parse argument.
            if arg_max is None:
                arg = 0
            elif len(args) < 1:
                print('Argument expected.')
                return
            else:
                try:
                    arg = int(args[0], 0)
                except ValueError:
                    print('Failed to parse argument.')
                    return
                if arg < 0 or arg > arg_max:
                    print('Argument out of range.')
                    return
            
            # Ensure that the interface is ready.
            if regs.STAT.CTRLR.read() or (regs.CTRL.RAW.read() in {0xDEADC0DE, 0xDEADBEEF}):
                print('Cannot access control register interface. Check the clock configuration!')
                return
            
            # Check clock state.
            if clk is True and not regs.CTRL.CLKEN.read():
                print('Error: clock is not running.')
                return
            if clk is False and regs.CTRL.CLKEN.read():
                print('Error: clock is running.')
                return
            
            # Check IFSEL state.
            if uart is True and regs.CTRL.IFSEL.read():
                print('Error: must be running un UART mode.')
                return
            if uart is False and not regs.CTRL.IFSEL.read():
                print('Error: must be running in LVDS mode.')
                return
            
            # Give the command.
            regs.CTRL.RAW.write(cmd << 29 | arg)
            
            # Wait for it to finish.
            try:
                def wait():
                    for i in range(100):
                        if not regs.CTRL.CMD.read():
                            return False
                        time.sleep(0.01)
                    return True
                if wait_for_cnt:
                    while wait():
                        print('%d steps remaining...' % regs.CTRL.COUNT.read())
                else:
                    if wait():
                        print('Timeout, CMD didn\'t go to zero yet!')
            except KeyboardInterrupt:
                print('Interrupted.')
            
        return fn

    asiccmd.register('stop', FunctionCommand(
                    'Disables the ASIC clock.',
                    'Disables the ASIC clock.',
                    gen_asic_cmd_fn(2, clk=True)))

    asiccmd.register('start', FunctionCommand(
                    'Enables the ASIC clock.',
                    'Enables the ASIC clock.',
                    gen_asic_cmd_fn(3, clk=False)))

    asiccmd.register('step', FunctionCommand(
                    'Enables the ASIC clock for N cycles.',
                    'Enables the ASIC clock for exactly N cycles. Supply the amount as argument,\n'
                    'the maximum is 4000000.',
                    gen_asic_cmd_fn(4, arg_max=4000000, clk=False)))

    asiccmd.register('scan-read', FunctionCommand(
                    'Reads a scan chain to the buffer.',
                    'Reads the specified scan chain, between 0 and 13. This only works in UART\n'
                    'mode, and the clock must be stopped prior to giving the command.\n',
                    gen_asic_cmd_fn(6, arg_max=13, clk=False, uart=True)))

    asiccmd.register('scan-write', FunctionCommand(
                    'Writes a scan chain from the buffer.',
                    'Writes the specified scan chain, between 0 and 13. This only works in UART\n'
                    'mode, and the clock must be stopped prior to giving the command.\n',
                    gen_asic_cmd_fn(7, arg_max=13, clk=False, uart=True)))
