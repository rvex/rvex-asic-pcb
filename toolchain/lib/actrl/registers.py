
class RegisterError(Exception):
    pass

class Register(object):
    
    def __init__(self,
                 rvd,           # rvd instance
                 mnem,          # mnemonic
                 desc,          # description
                 addr,          # byte address within HSI
                 offs=0,        # bit offset
                 size=32,       # bit length
                 dead=False,    # if entire register is 0xDEADBEEF or 0xDEADC0DE, raise exception
                 signed=False,  # signedness
                 rd=True,       # readable
                 wr=True,       # writable
                 unit=None,     # None: integer, float value: multiplier to normal unit
                 wrmin=None,    # minimum write value (in natural unit)
                 wrmax=None,    # maximum write value (in natural unit)
                 fmt=None):     # print format, defaults to hex with length based on size
        
        super().__init__()
        self.rvd = rvd
        self.mnem = mnem
        self.desc = desc
        self.addr = addr
        self.offs = offs
        self.size = size
        self.dead = dead
        self.signed = signed
        self.rd = rd
        self.wr = wr
        self.unit = unit
        if wrmin is not None:
            if self.unit is None:
                self.wrmin = wrmin
            else:
                self.wrmin = int(round(wrmin / self.unit))
        elif self.signed:
            self.wrmin = -(2**(size-1))
        else:
            self.wrmin = 0
        if wrmax is not None:
            if self.unit is None:
                self.wrmax = wrmax
            else:
                self.wrmax = int(round(wrmax / self.unit))
        elif self.signed:
            self.wrmax = (2**(size-1))-1
        else:
            self.wrmax = (2**size)-1
        if fmt is not None:
            self.fmt = fmt
        elif self.unit is None:
            self.fmt = '0x{:0%dX}' % ((self.size + 3) // 4)
        else:
            x = self.unit
            d = 0
            while abs(self.unit) < 1 and d < 5:
                self.unit *= 10
                d += 1
            self.fmt = '{:.%df}' % d
    
    def read_word(self):
        if not self.rd:
            raise RegisterError('Register %s is not readable' % self.mnem)
        val = self.rvd.readInt(self.addr, 4)
        if self.dead:
            if val == 0xDEADC0DE or val == 0xDEADBEEF:
                raise RegisterError('Register %s seems to not be accessible (0x%08X)' % (self.mnem, val))
        return val
    
    def read_bits(self):
        val = self.read_word()
        val >>= self.offs
        val &= 2**self.size-1
        if self.signed and (val & 2**(self.size-1) != 0):
            val -= 2**self.size
        return val
    
    def read(self):
        val = self.read_bits()
        if self.unit is not None:
            val *= self.unit
        return val
    
    def fmt_val(self, val):
        return self.fmt.format(val)
    
    def read_fmt(self):
        return self.fmt_val(self.read())
    
    def write_word(self, val):
        if not self.wr:
            raise RegisterError('Register %s is not writable' % self.mnem)
        self.rvd.writeInt(self.addr, 4, val)

    def write_bits(self, val):
        if not self.wr:
            raise RegisterError('Register %s is not writable' % self.mnem)
        mask = (2**self.size-1) << self.offs
        if self.rd:
            wval = self.read_word()
            wval &= ~mask
        else:
            wval = 0
        wval |= (val << self.offs) & mask
        self.write_word(wval)
    
    def write(self, val):
        if val < self.wrmin:
            raise RegisterError('Value %s is out of range, min = %s' % (self.fmt_val(val), self.fmt_val(self.wrmin)))
        if val > self.wrmax:
            raise RegisterError('Value %s is out of range, max = %s' % (self.fmt_val(val), self.fmt_val(self.wrmax)))
        if self.unit is not None:
            val = int(round(val / self.unit))
        self.write_bits(val)
    
    def parse_val(self, s):
        if self.unit is not None:
            val = float(s)
        else:
            val = int(s, 0)
        if val < self.wrmin:
            raise RegisterError('Value %s is out of range, min = %s' % (self.fmt_val(val), self.fmt_val(self.wrmin)))
        if val > self.wrmax:
            raise RegisterError('Value %s is out of range, max = %s' % (self.fmt_val(val), self.fmt_val(self.wrmax)))
        return val
    
    def write_fmt(self, s):
        self.write(self.parse_val(s))
    
    def __str__(self):
        return '%s - %s' % (self.mnem, self.desc)

    def __repr__(self):
        return 'Register(%s)' % self


class RegisterList(object):
    
    def __init__(self, rvd, addr, mnem=None, desc=None):
        super().__init__()
        self.rvd = rvd
        self.addr = addr
        self.mnem = mnem
        self.desc = desc
        self.regs = {}
    
    def add(self, mnem, desc, addr, *args, **kwargs):
        assert mnem not in self.regs
        r = Register(self.rvd, mnem, desc, addr+self.addr, *args, **kwargs)
        self.regs[mnem] = r
        return r
    
    def add_list(self, mnem, desc, addr):
        assert mnem not in self.regs
        r = RegisterList(self.rvd, addr+self.addr, mnem, desc)
        self.regs[mnem] = r
        return r
    
    def __getitem__(self, key):
        return self.regs[key]
    
    def __contains__(self, key):
        return key in self.regs
    
    def __getattr__(self, key):
        reg = self.regs.get(key, None)
        if reg is None:
            raise AttributeError(key)
        return reg
    
    def __iter__(self):
        return self.regs.values().__iter__()

    def get(self, key):
        return self.regs.get(key, None)
    
    def __str__(self):
        return '%s - %s' % (self.mnem, self.desc)

    def __repr__(self):
        return 'RegisterList(%s)' % self

def generate(rvd, base):
    hsi = RegisterList(rvd, base, 'HSI', 'HSI control registers')
    
    pwr = hsi.add_list("POWER", "HSI power management registers", 0x0000)
    
    def x(mnem, desc, addr):
        pwr.add(mnem, desc, addr,
                size = 16,
                dead = True,
                signed = True,
                wr = False,
                unit = 0.01,
                fmt = '{:.2f} mA')
    x('VIO_I',  'VddIO current measurement',       0x1000)
    x('VCO_I',  'Core current measurement',        0x1004)
    x('VA2_I',  'Analog 2.5V current measurement', 0x1008)
    x('VA1_I',  'Analog 1.2V current measurement', 0x100C)
    
    def x(mnem, desc, addr):
        pwr.add(mnem, desc, addr,
                size = 16,
                dead = True,
                signed = True,
                wr = False,
                unit = 0.0005,
                fmt = '{:.3f} V')
    x('VIO_V',  'VddIO voltage measurement',       0x1010)
    x('VCO_V',  'Core voltage measurement',        0x1014)
    x('VA2_V',  'Analog 2.5V voltage measurement', 0x1018)
    x('VA1_V',  'Analog 1.2V voltage measurement', 0x101C)
    
    def x(mnem, desc, addr):
        pwr.add(mnem, desc, addr,
                size = 16,
                dead = True,
                wr = False,
                fmt = '{:d}')
    x('OCP',    'Overcurrent status register',     0x1020)
    x('ENS',    'Power enabled status register',   0x1024)
    
    def x(mnem, desc, addr):
        pwr.add(mnem, desc, addr,
                size = 16,
                dead = True,
                wrmin = 0,
                wrmax = 1,
                fmt = '{:d}')
    x('EN',     'Power up/down control register',  0x1028)
    
    def x(mnem, desc, addr):
        pwr.add(mnem, desc, addr,
                size = 16,
                dead = True,
                unit = 0.0005,
                wrmin = 0.0,
                wrmax = 3.3,
                fmt = '{:.3f} V')
    x('VIO_SET', 'Requested VddIO voltage',        0x102C)
    x('VCO_SET', 'Requested core voltage',         0x1030)
    x('VA2_SET', 'Requested analog 2.5V voltage',  0x1034)
    x('VA1_SET', 'Requested analog 1.2V voltage',  0x1038)
    
    def x(mnem, desc, addr):
        pwr.add(mnem, desc, addr,
                size = 16,
                dead = True,
                unit = 0.0001,
                fmt = '{:.4f} mA')
    x('IREF_TX0', 'LVDS sink/TX ch. 0 Iref set',    0x103C)
    x('IREF_TX1', 'LVDS sink/TX ch. 1 Iref set',    0x1040)
    x('IREF_TX2', 'LVDS sink/TX ch. 2 Iref set',    0x1044)
    x('IREF_TX3', 'LVDS sink/TX ch. 3 Iref set',    0x1048)
    x('IREF_RX0', 'LVDS source/RX ch. 0 Iref set',  0x104C)
    x('IREF_RX1', 'LVDS source/RX ch. 1 Iref set',  0x1050)
    x('IREF_RX2', 'LVDS source/RX ch. 2 Iref set',  0x1054)
    x('IREF_RX3', 'LVDS source/RX ch. 3 Iref set',  0x1058)
    
    
    clk = hsi.add_list("CLOCK", "HSI clock management registers", 0x3000)
    
    def x(mnem, desc, addr, offs, size):
        clk.add(mnem, desc, addr,
                offs = offs,
                size = size,
                fmt = '{:d}')
    
    x('REF_PHASE',  'Reference clk phase',              0x08*4, 13,  3)
    x('REF_HIGH',   'Reference clk high time',          0x08*4,  6,  6)
    x('REF_LOW',    'Reference clk low time',           0x08*4,  0,  6)
    x('REF_F_FRAC', 'Reference clk fraction',           0x09*4, 12,  3)
    x('REF_F_EN',   'Reference clk fraction enable',    0x09*4, 11,  1)
    x('REF_F_MUX_F','Reference clk fraction mux',       0x07*4, 11,  3)
    x('REF_F_WF_F', 'Reference clk fraction duty fall', 0x07*4, 10,  1)
    x('REF_F_WF_R', 'Reference clk fraction duty rise', 0x09*4, 10,  1)
    x('REF_MX',     'Reference clk MX',                 0x09*4,  8,  2)
    x('REF_EDGE',   'Reference clk edge',               0x09*4,  7,  1)
    x('REF_BYPASS', 'Reference clk counter bypass',     0x09*4,  6,  1)
    x('REF_DELAY',  'Reference clk delay',              0x09*4,  0,  6)
    
    x('HSI1_PHASE',  'Internal HSI clk phase',          0x0A*4, 13,  3)
    x('HSI1_HIGH',   'Internal HSI clk high time',      0x0A*4,  6,  6)
    x('HSI1_LOW',    'Internal HSI clk low time',       0x0A*4,  0,  6)
    x('HSI1_MX',     'Internal HSI clk MX',             0x0B*4,  8,  2)
    x('HSI1_EDGE',   'Internal HSI clk edge',           0x0B*4,  7,  1)
    x('HSI1_BYPASS', 'Internal HSI clk counter bypass', 0x0B*4,  6,  1)
    x('HSI1_DELAY',  'Internal HSI clk delay',          0x0B*4,  0,  6)
    
    x('HSI2_PHASE',  'Output HSI clk phase',            0x0C*4, 13,  3)
    x('HSI2_HIGH',   'Output HSI clk high time',        0x0C*4,  6,  6)
    x('HSI2_LOW',    'Output HSI clk low time',         0x0C*4,  0,  6)
    x('HSI2_MX',     'Output HSI clk MX',               0x0D*4,  8,  2)
    x('HSI2_EDGE',   'Output HSI clk edge',             0x0D*4,  7,  1)
    x('HSI2_BYPASS', 'Output HSI clk counter bypass',   0x0D*4,  6,  1)
    x('HSI2_DELAY',  'Output HSI clk delay',            0x0D*4,  0,  6)
    
    x('HSI3_PHASE',  'Input HSI clk phase',             0x0E*4, 13,  3)
    x('HSI3_HIGH',   'Input HSI clk high time',         0x0E*4,  6,  6)
    x('HSI3_LOW',    'Input HSI clk low time',          0x0E*4,  0,  6)
    x('HSI3_MX',     'Input HSI clk MX',                0x0F*4,  8,  2)
    x('HSI3_EDGE',   'Input HSI clk edge',              0x0F*4,  7,  1)
    x('HSI3_BYPASS', 'Input HSI clk counter bypass',    0x0F*4,  6,  1)
    x('HSI3_DELAY',  'Input HSI clk delay',             0x0F*4,  0,  6)
    
    x('CORE_PHASE',  'FPGA core clk phase',             0x10*4, 13,  3)
    x('CORE_HIGH',   'FPGA core clk high time',         0x10*4,  6,  6)
    x('CORE_LOW',    'FPGA core clk low time',          0x10*4,  0,  6)
    x('CORE_MX',     'FPGA core clk MX',                0x11*4,  8,  2)
    x('CORE_EDGE',   'FPGA core clk edge',              0x11*4,  7,  1)
    x('CORE_BYPASS', 'FPGA core clk counter bypass',    0x11*4,  6,  1)
    x('CORE_DELAY',  'FPGA core clk delay',             0x11*4,  0,  6)
    
    x('FB_PHASE',    'Feedback clk phase',              0x14*4, 13,  3)
    x('FB_HIGH',     'Feedback clk high time',          0x14*4,  6,  6)
    x('FB_LOW',      'Feedback clk low time',           0x14*4,  0,  6)
    x('FB_F_FRAC',   'Feedback clk fraction',           0x15*4, 12,  3)
    x('FB_F_EN',     'Feedback clk fraction enable',    0x15*4, 11,  1)
    x('FB_F_MUX_F',  'Feedback clk fraction mux',       0x13*4, 11,  3)
    x('FB_F_WF_F',   'Feedback clk fraction duty fall', 0x13*4, 10,  1)
    x('FB_F_WF_R',   'Feedback clk fraction duty rise', 0x15*4, 10,  1)
    x('FB_MX',       'Feedback clk MX',                 0x15*4,  8,  2)
    x('FB_EDGE',     'Feedback clk edge',               0x15*4,  7,  1)
    x('FB_BYPASS',   'Feedback clk counter bypass',     0x15*4,  6,  1)
    x('FB_DELAY',    'Feedback clk delay',              0x15*4,  0,  6)
    
    x('DIV_EDGE',    'Main divider edge',               0x16*4, 13,  1)
    x('DIV_BYPASS',  'Main divider bypass',             0x16*4, 12,  1)
    x('DIV_HIGH',    'Main divider high time',          0x16*4,  6,  6)
    x('DIV_LOW',     'Main divider low time',           0x16*4,  0,  6)
    
    x('LOCK_CNT',    'Lock counter',                    0x18*4,  0, 10)
    x('LOCK_FB_DLY', 'Feedback lock delay',             0x19*4, 10,  5)
    x('UNLOCK_CNT',  'Unlock counter',                  0x19*4,  0, 10)
    x('LOCK_REF_DLY','Reference lock delay',            0x1A*4, 10,  5)
    x('LOCK_SAT_HI', 'Not sure what this is',           0x1A*4,  0, 10)
    
    x('FILT_1A',     'Not sure what this is',           0x4E*4, 15,  1)
    x('FILT_1B',     'Not sure what this is',           0x4E*4, 11,  2)
    x('FILT_1C',     'Not sure what this is',           0x4E*4,  8,  1)
    x('FILT_2A',     'Not sure what this is',           0x4F*4, 15,  1)
    x('FILT_2B',     'Not sure what this is',           0x4F*4, 11,  2)
    x('FILT_2C',     'Not sure what this is',           0x4F*4,  7,  2)
    x('FILT_2D',     'Not sure what this is',           0x4F*4,  4,  1)
    
    x('POWER',       'Power register',                  0x28*4,  0, 16)
    
    x('MON_CORE',    'Core clock monitor register',     0x804,   0, 32)
    x('MON_HSI',     'HSI clock monitor register',      0x808,   0, 32)
    x('MON_REF',     'Reference clock monitor register',0x80C,   0, 32)
    
    
    hsta = hsi.add_list("STAT", "HSI status register", 0x3800)
    
    def x(mnem, desc):
        hsta.add(mnem, desc, 0)
    x('RAW',   'Raw status register')
    
    def x(mnem, desc):
        hsta.add(mnem, desc, 0,
                offs = 0,
                size = 16,
                wr = False)
    x('CFG',   'r-VEX cfg. word (HSI GPIO)')
    
    def x(mnem, desc, offs):
        hsta.add(mnem, desc, 0,
                offs = offs,
                size = 1,
                wr = False,
                fmt = '{:d}')
    x('IDLE0',  'r-VEX ctxt 0 idle (HSI GPIO)',   16)
    x('IDLE1',  'r-VEX ctxt 1 idle (HSI GPIO)',   17)
    x('IDLE2',  'r-VEX ctxt 2 idle (HSI GPIO)',   18)
    x('IDLE3',  'r-VEX ctxt 3 idle (HSI GPIO)',   19)
    x('DONE0',  'r-VEX ctxt 0 done (HSI GPIO)',   20)
    x('DONE1',  'r-VEX ctxt 1 done (HSI GPIO)',   21)
    x('DONE2',  'r-VEX ctxt 2 done (HSI GPIO)',   22)
    x('DONE3',  'r-VEX ctxt 3 done (HSI GPIO)',   23)
    x('MMCMR',  'MMCM reset status',              27)
    x('HSIR',   'Interface reset status',         28)
    x('ASICR',  'ASIC reset status',              29)
    x('CTRLR',  'Control logic reset status',     30)
    x('MMCML',  'MMCM lock status',               31)
    
    
    hctl = hsi.add_list("CTRL", "HSI control register", 0x3C00)
    
    def x(mnem, desc):
        hctl.add(mnem, desc, 0)
    x('RAW',   'Raw control register')
    
    def x(mnem, desc, offs, size):
        hctl.add(mnem, desc, 0,
                offs = offs,
                size = size,
                wr = False,
                fmt = '{:d}')
    x('COUNT',  'Counter value (multi-function)',   0, 22)
    x('EVENT',  'Event flag value',                22,  1)
    x('CLKEN',  'ASIC clock enable',               23,  1)
    x('MODE',   'Interface mode (ifsel:ratio)',    24,  2)
    x('RATIO',  'Clock ratio/UART mode',           24,  1)
    x('IFSEL',  'Interface selection',             25,  1)
    x('USMPL',  'UART sample edge for ctrl_a2f',   26,  1)
    x('USET1',  'UART setup edge for ctrl_f2a',    27,  1)
    x('USET2',  'UART setup edge for aresetn_phy', 28,  1)
    x('CMD',    'Running interface command',       29,  3)
    
    
    rvex = hsi.add_list("RVEX", "ASIC core control registers", 0x4000)
    
    def x(mnem, desc, addr):
        rvex.add(mnem, desc, addr)
    x('PVER1',   'Platform tag 1 and core ID (0x00525641)', 0x0F8)
    x('PVER0',   'Platform tag 2 (0x53494331)',             0x0FC)
    
    
    return hsi
    
