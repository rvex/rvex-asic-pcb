
import time
from actrl.commands import *

__all__ = ['power_status', 'register_cmds']

def power_status(regs):
    if regs.POWER.ENS.read() == 0:
        print('Power status: \033[33;1mPowered down\033[0m')
    elif regs.POWER.OCP.read() == 0:
        print('Power status: \033[32;1mPowered up\033[0m')
    else:
        print('Power status: \033[31;1mOvercurrent triggered\033[0m')
    print('Voltage regulators:')
    
    def check_ratio(v, nom, tol, absmax):
        r = v / nom
        if r > 1 + absmax / 100:
            return '\033[31;1m'
        if r > 1 + tol / 100 or r < 1 - tol / 100:
            return '\033[33;1m'
        return '\033[32;1m'
    
    def vreg(name, friendly, nominal):
        v = regs.POWER['%s_V' % name].read()
        i = regs.POWER['%s_I' % name].read()
        s = regs.POWER['%s_SET' % name].read()
        
        vc = check_ratio(v, nominal, 2, 10)
        sc = check_ratio(s, nominal, 2, 10)
        
        if i > 250 or i < -20:
            ic = '\033[31;1m'
        elif i > 100:
            ic = '\033[33;1m'
        else:
            ic = '\033[32;1m'
        
        print('  %-12s %s%7s\033[0m @ %s%10s\033[0m, trimmed to %s%7s\033[0m' % (
            friendly + ':',
            vc, regs.POWER['%s_V' % name].fmt_val(v),
            ic, regs.POWER['%s_I' % name].fmt_val(i),
            sc, regs.POWER['%s_SET' % name].fmt_val(s)
        ))
    
    vreg('VIO', 'VddIO (2V5)', 2.5)
    vreg('VCO', 'Vcore (1V2)', 1.2)
    vreg('VA2', '2V5A', 2.5)
    vreg('VA1', '1V2A', 1.2)
    
    print('Current references:')
    
    for ch in range(4):
        tx = regs.POWER['IREF_TX%d' % ch].read()
        rx = regs.POWER['IREF_RX%d' % ch].read()
        
        txc = check_ratio(tx, 0.35, 5, 20)
        rxc = check_ratio(rx, 0.5, 5, 20)
        
        print('  HSI%d:  TX %s%7s\033[0m, RX %s%7s\033[0m' % (
            ch,
            txc, regs.POWER.IREF_TX0.fmt_val(tx),
            rxc, regs.POWER.IREF_RX0.fmt_val(rx)
        ))
    
    print('  Note: these are the setpoints only, the board can\'t measure these.')
    print('  TX is specified to 0.35mA, RX is specified to 0.5mA.')


def register_cmds(rootcmd, regs):
    
    powercmd = CommandList('Power management commands.', 'status')
    rootcmd.register('power', powercmd)
    
    def power_status_fn(*args):
        power_status(regs)
    
    powercmd.register('status', FunctionCommand(
                    'Prints ASIC power status.',
                    'Prints the current ASIC power management status, i.e. currents, voltages, etc.',
                    power_status_fn))
    
    powercmd.register('up', FunctionCommand(
                    'Powers up the ASIC.',
                    'Powers up the ASIC.',
                    lambda *args: (regs.POWER.EN.write(1), time.sleep(0.2), power_status_fn())))
    
    powercmd.register('down', FunctionCommand(
                    'Powers down the ASIC.',
                    'Powers down the ASIC.',
                    lambda *args: (regs.POWER.EN.write(0), time.sleep(0.8), power_status_fn())))
    
    powercmd.register('cycle', FunctionCommand(
                    'Power-cycles the ASIC.',
                    'Power-cycles the ASIC.',
                    lambda *args: (regs.POWER.EN.write(0), time.sleep(0.8), power_status_fn(), regs.POWER.EN.write(1), time.sleep(0.2), power_status_fn())))
    
    powerrampcmd = CommandList('Changes a voltage or Iref.', 'ramp')
    powercmd.register('ramp', powerrampcmd)
    
    for (name, reg, rate) in [
        ('VddIO',    regs.POWER.VIO_SET,  0.02),
        ('Vcore',    regs.POWER.VCO_SET,  0.02),
        ('2V5A',     regs.POWER.VA2_SET,  0.02),
        ('1V2A',     regs.POWER.VA1_SET,  0.02),
        ('Iref_TX0', regs.POWER.IREF_TX0, 0.02),
        ('Iref_TX1', regs.POWER.IREF_TX1, 0.02),
        ('Iref_TX2', regs.POWER.IREF_TX2, 0.02),
        ('Iref_TX3', regs.POWER.IREF_TX3, 0.02),
        ('Iref_RX0', regs.POWER.IREF_RX0, 0.02),
        ('Iref_RX1', regs.POWER.IREF_RX1, 0.02),
        ('Iref_RX2', regs.POWER.IREF_RX2, 0.02),
        ('Iref_RX3', regs.POWER.IREF_RX3, 0.02)
    ]:
        def gen_ramp(name, reg, rate):
            def ramp(value):
                target = reg.parse_val(value)
                current = reg.read()
                print('Starting at %s...' % reg.fmt_val(current))
                prev = 0
                i = 0
                while current != prev:
                    if current < target:
                        request = min(current + rate, target)
                    else:
                        request = max(current - rate, target)
                    reg.write(request)
                    time.sleep(0.05)
                    prev = current
                    current = reg.read()
                    i += 1
                    if i == 10:
                        print('Now at %s...' % reg.fmt_val(current))
                        i = 0
                if abs(current - target) > rate:
                    print('Reached device limit: %s.' % reg.fmt_val(current))
                else:
                    print('Now at %s...' % reg.fmt_val(current))
                    print('Done!')
            return ramp
        
        powerrampcmd.register(name.lower(), FunctionCommand(
                        'Ramps %s up or down.' % name,
                        'Ramps %s up or down slowly enough to avoid over/undervoltage protection.\n'
                        'Usage: power ramp %s <target>' % (name, name.lower()),
                        gen_ramp(name, reg, rate)))

