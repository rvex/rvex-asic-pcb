
Teensy firmware
===============

This directory contains the firmware for the on-board Teensy. It manages the
voltage regulators, DACs, and low-speed ADCs (everything except the ADS8885).
It's also connected to the two USB host ports, and has two currently unused UART
channels to the FPGA. These are intended for USB keyboard/mouse support and a
USB-serial bridge for ASIC stdin/stdout, but those are not implemented yet as of
2018-04-11.

To change the firmware, follow the instructions at
https://www.pjrc.com/teensy/teensyduino.html
to install Arduino + Teensy board support. I'm using Arduino 1.8.5 right now.
Teensyduino version support is rather erratic so that particular version will
probably not be supported anymore when you read this, but it shouldn't matter
too much.

Once in the Arduino IDE, select `Tools` -> `Board: ...` -> `Teensy 3.6`. Connect
a USB cable to the micro-B connector on the Teensy (below the mini-B connector
for FPGA JTAG/UART access) and press the right-arrow button (ot `Sketch` ->
`Upload` in the menu) to do the upload.

It's possible for the Teensy to be in a state where it doesn't respond to the
"enter bootloader" command over USB, although I haven't had it happen to me yet
with the current firmware. When that happens, press the "Teensy program" button
to the front-right of the FPGA carrier board. This button is hardwired to the
button on the Teensy itself, which, of course, you can't get to without taking
the FPGA carrier off.
