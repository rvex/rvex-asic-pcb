
#define PIN_RX1       27
#define PIN_TX1       26
#define PIN_RTS1      19
#define PIN_RX2        9
#define PIN_TX2       10
#define PIN_RTS2      22
#define PIN_RX3        7
#define PIN_TX3        8


//#############################################################################
// ASIC power management
//#############################################################################

// Library includes.
#include "Wire.h"

// Pin definitions.
#define PIN_LDAC01     0
#define PIN_LDAC23     1
#define PIN_LDACP      2
#define PIN_SCL        3
#define PIN_SDA        4
#define PIN_PWR_ERR   20
#define PIN_PWR_ENA   21
#define PIN_OCPA      24
#define PIN_OCPB      25
#define PIN_OCPC      28
#define PIN_OCPD      29

// I2C address definitions.
#define ADDR_INA_MIN  0x40
#define ADDR_INA_2V5D 0x40
#define ADDR_INA_1V2D 0x41
#define ADDR_INA_2V5A 0x42
#define ADDR_INA_1V2A 0x43
#define ADDR_INA_MAX  0x43
#define ADDR_DAC_INIT 0x60
#define ADDR_DAC_01   0x61
#define ADDR_DAC_23   0x62
#define ADDR_DAC_P    0x63

// DAC channel definitions.
#define DAC_IREF_TX_0 0x00
#define DAC_IREF_RX_0 0x01
#define DAC_IREF_TX_1 0x02
#define DAC_IREF_RX_1 0x03
#define DAC_IREF_TX_2 0x04
#define DAC_IREF_RX_2 0x05
#define DAC_IREF_TX_3 0x06
#define DAC_IREF_RX_3 0x07
#define DAC_VREG_2V5D 0x08
#define DAC_VREG_1V2D 0x09
#define DAC_VREG_2V5A 0x0A
#define DAC_VREG_1V2A 0x0B

// DAC I2C address configuration bitbang sequence.
enum {
  DAC_ADDR_END = 0,
  DAC_ADDR_LOW,
  DAC_ADDR_HIGH,
  DAC_ADDR_ACK,
  DAC_ADDR_PA2,
  DAC_ADDR_PA1,
  DAC_ADDR_PA0,
  DAC_ADDR_NA2,
  DAC_ADDR_NA1,
  DAC_ADDR_NA0,
  DAC_ADDR_LDAC
};
#define d(x) DAC_ADDR_ ## x
static const char DAC_ADDR_CMD[] = {
  // 7        6        5        4        3        2        1        0       A
  d(HIGH), d(HIGH), d(LOW),  d(LOW),  d(PA2),  d(PA1),  d(PA0),  d(LOW),  d(ACK),
  d(LOW),  d(HIGH), d(HIGH), d(PA2),  d(PA1),  d(PA0),  d(LOW),  d(HIGH), d(LDAC),
  d(LOW),  d(HIGH), d(HIGH), d(NA2),  d(NA1),  d(NA0),  d(HIGH), d(LOW),  d(ACK),
  d(LOW),  d(HIGH), d(HIGH), d(NA2),  d(NA1),  d(NA0),  d(HIGH), d(HIGH), d(ACK),
  d(END)
};
#undef d

// INA220 readout structure.
typedef struct pwr_ina_data_t {
  
  // Current in 10uA increments (for 1R shunt).
  short current;
  
  // Voltage in 0.5mV increments.
  unsigned short voltage;
  
  // Whether or not this record is valid.
  short valid;
  
} pwr_ina_data_t;

// Powerup data. This is just the first 256 INA220 samples, sampled at maximum
// samplerate. That should amount to about 10ms of data.
typedef struct pwr_up_data_t {
  
  // I2C address of the INA220, or 0 if invalid.
  short channel;
  
  // Microseconds since enable asserted.
  unsigned short micros;
  
  // Readout, in the same format as pwr_ina_data_t.
  short current;
  unsigned short voltage;
  
} pwr_up_data_t;
#define POWERUP_DATA_COUNT 256

// Power register file exposed over the UARTs.
typedef struct pwr_api_t {
  union {
    
    struct {
      
      // *** API read-only registers ***
      
      // Powerup data.
      pwr_up_data_t powerup_data[POWERUP_DATA_COUNT]; // registers 0x000..0x3FF
      
      // Current voltage and current readings of the power rails. Format as described
      // in pwr_ina_data_t comments.
      short I2V5D; // register 0x400
      short I1V2D; // register 0x401
      short I2V5A; // register 0x402
      short I1V2A; // register 0x403
      unsigned short U2V5D; // register 0x404
      unsigned short U1V2D; // register 0x405
      unsigned short U2V5A; // register 0x406
      unsigned short U1V2A; // register 0x407
      
      // Set when overcurrent is detected in pwr_ina_read(). Cleared at the start of
      // the powerup sequence.
      short ocp; // register 0x408
      
      // Whether the ASIC is currently powered up, ignoring the overcurrent
      // protection condition. i.e., pwr.ocp set does not imply that pwr.powered
      // is cleared, even though the system will not be powered at that point. In
      // that case the power error LED is on.
      short powered; // register 0x409
      
      // *** API read/write registers ***
      
      // Request flag for ASIC powered up. This is written by the user interface.
      short enable; // register 0x40A
      
      // Requested supply voltages, set using the DACs for as far as possible.
      // Format is 0.5mV/LSB.
      unsigned short trim_2V5D; // register 0x40B
      unsigned short trim_1V2D; // register 0x40C
      unsigned short trim_2V5A; // register 0x40D
      unsigned short trim_1V2A; // register 0x40E
      
      // Transmit and receive current references per pair. Format is 0.1uA/LSB.
      unsigned short iref_tx[4]; // registers 0x40F..0x412
      unsigned short iref_rx[4]; // registers 0x413..0x416
      
    };
    
    // Exposed as 16bit registers.
    unsigned short regs[POWERUP_DATA_COUNT*4+23];
    
  };
} pwr_api_t;

#define PWR_REG_TOTAL  (POWERUP_DATA_COUNT*4+23)
#define PWR_REG_RDONLY (POWERUP_DATA_COUNT*4+9)

static pwr_api_t pwr;

static unsigned char pwr_serial_state;
static unsigned short pwr_serial_addr;
static unsigned short pwr_serial_data;

// Initializes the DAC I2C addresses.
int pwr_dac_addr(int ldacPin, int oldAddr, int newAddr) {
  
  // Send start.
  digitalWriteFast(PIN_SDA, LOW);
  delayMicroseconds(10);
  digitalWriteFast(PIN_SCL, LOW);
  delayMicroseconds(2);
  
  // Bit loop.
  const char *cmd = DAC_ADDR_CMD;
  int ack = 1;
  while (*cmd) {
    char sda = 0;
    switch (*cmd) {
      case DAC_ADDR_LOW:  sda = 0;           break;
      case DAC_ADDR_HIGH: sda = 1;           break;
      case DAC_ADDR_ACK:  sda = 1;           break;
      case DAC_ADDR_PA2:  sda = oldAddr & 4; break;
      case DAC_ADDR_PA1:  sda = oldAddr & 2; break;
      case DAC_ADDR_PA0:  sda = oldAddr & 1; break;
      case DAC_ADDR_NA2:  sda = newAddr & 4; break;
      case DAC_ADDR_NA1:  sda = newAddr & 2; break;
      case DAC_ADDR_NA0:  sda = newAddr & 1; break;
      case DAC_ADDR_LDAC: sda = 1;           break;
    }
    digitalWriteFast(PIN_SDA, sda ? HIGH : LOW);
    if (*cmd == DAC_ADDR_LDAC) digitalWrite(ldacPin, LOW);
    delayMicroseconds(3);
    digitalWriteFast(PIN_SCL, HIGH);
    delayMicroseconds(2);
    if (*cmd == DAC_ADDR_ACK || *cmd == DAC_ADDR_LDAC) {
      if (digitalReadFast(PIN_SDA)) {
        ack = 0;
      }
      delayMicroseconds(10);
    }
    delayMicroseconds(3);
    digitalWriteFast(PIN_SCL, LOW);
    delayMicroseconds(2);
    cmd++;
  }
  
  // Send stop.
  digitalWriteFast(PIN_SDA, LOW);
  delayMicroseconds(5);
  digitalWriteFast(PIN_SCL, HIGH);
  delayMicroseconds(10);
  digitalWriteFast(PIN_SDA, HIGH);
  delayMicroseconds(10);
  
  return ack;
}

// Sets a DAC voltage.
int pwr_dac_set(int ch, float v) {
  
  // Calculate DAC code.
  int code = (int)(v * (4096.0f/3.3f));
  if (code > 4095) code = 4095;
  if (code < 0) code = 0;
  
  // Send the command.
  Wire2.beginTransmission(ADDR_DAC_01 + (ch >> 2));
  Wire2.write(0x50 | ((ch & 3) << 1));
  Wire2.write(code >> 8);
  Wire2.write(code & 0xFF);
  return Wire2.endTransmission(true);
}

// Forces the regulators off after detection of an overcurrent condition or
// other error.
void pwr_error(void) {
  digitalWriteFast(PIN_OCPA, LOW);
  digitalWriteFast(PIN_OCPB, LOW);
  digitalWriteFast(PIN_OCPC, LOW);
  digitalWriteFast(PIN_OCPD, LOW);
  pwr.ocp = 1;
}

// Configures an INA220. If fast is set, the maximum conversion rate is
// selected (9-bit, 168us). If cleared, the highest resolution is selected
// (12-bit, 34.04ms).
int pwr_ina_cfg(int fast) {
  int value;
  int error = 0;
  if (fast) {
    value = 0b0001100000000111;
  } else {
    value = 0b0001111111111111;
  }
  for (int addr = ADDR_INA_MIN; addr <= ADDR_INA_MAX; addr++) {
    Wire2.beginTransmission(addr);
    Wire2.write(0);
    Wire2.write(value >> 8);
    Wire2.write(value & 0xFF);
    error <<= 4;
    error += Wire2.endTransmission(true);
  }
  return error;
}

// Reads the INA220 shunt voltage register. LSB is 10uV.
pwr_ina_data_t pwr_ina_read(int addr) {
  pwr_ina_data_t ret = {0, 0, 0};
  int error = 1;
  
  // Read current.
  Wire2.beginTransmission(addr);
  Wire2.write(1);
  if (Wire2.endTransmission(false)) goto done;
  if (Wire2.requestFrom(addr, 2, false) != 2) goto done;
  while (Wire2.available() < 2);
  ret.current = Wire2.read() & 0xFF;
  ret.current <<= 8;
  ret.current |= Wire2.read() & 0xFF;
  
  // Read voltage.
  Wire2.beginTransmission(addr);
  Wire2.write(2);
  if (Wire2.endTransmission(false)) goto done;
  if (Wire2.requestFrom(addr, 2, true) != 2) goto done;
  while (Wire2.available() < 2);
  ret.voltage = Wire2.read() & 0xFF;
  ret.voltage <<= 8;
  ret.voltage |= Wire2.read() & 0xFF;
  
  // Set valid if bit 1 of the voltage register is set.
  if (ret.voltage & 2) ret.valid = 1;
  
  // Clear the status flags from the voltage register.
  ret.voltage &= 0xFFF8;
  
  error = 0;
  
done:
  
  // If the shunt voltage is more than 250mV in either direction or we couldn't
  // read the INA220, trigger overcurrent protection.
  if (error || (ret.current > 25000) || (ret.current < -25000)) {
    pwr_error();
  }
  
  return ret;
}

// ASIC powerup sequence.
int pwr_up(void) {
  
  // Make sure the system is actually powered down.
  if (pwr.powered) return -1;

  // Clear the OCP flag.
  pwr.ocp = 0;
  
  // Configure the INA220s in fast mode. Refuse to power up if one or more INAs
  // are not responding.
  pwr_ina_cfg(1);
  
  // Wait until the INA220s have finished their current (slow) sample in the
  // worst case.
  delay(35);
  
  // Invalidate powerup data.
  for (int i = 0; i < POWERUP_DATA_COUNT; i++) {
    pwr.powerup_data[i].channel = 0;
  }
  
  // Enable power.
  pwr.powered = 1;
  digitalWriteFast(PIN_OCPA, HIGH);
  digitalWriteFast(PIN_OCPB, HIGH);
  digitalWriteFast(PIN_OCPC, HIGH);
  digitalWriteFast(PIN_OCPD, HIGH);
  digitalWriteFast(PIN_PWR_ENA, HIGH);
  uint32_t start = micros();
  
  // Read the INAs at high speed for 50ms. The first N samples are stored in a
  // buffer. We continue reading even after the buffer is full because the read
  // function itself checks for power errors.
  int i = 0;
  while ((micros() - start) < 50000) {
    for (int addr = ADDR_INA_MIN; addr <= ADDR_INA_MAX; addr++) {
      pwr_ina_data_t data = pwr_ina_read(addr);
      if (data.valid && (i < POWERUP_DATA_COUNT)) {
        pwr.powerup_data[i].channel = addr;
        pwr.powerup_data[i].micros = (unsigned short)(micros() - start);
        pwr.powerup_data[i].current = data.current;
        pwr.powerup_data[i].voltage = data.voltage;
        i++;
      }
    }
  }
  
  // If we still don't have PGOOD after 50ms, give up.
  if (digitalRead(PIN_PWR_ERR)) {
    pwr_error();
  }
  
  // Go back to slow but more accurate INA220 readouts.
  pwr_ina_cfg(0);
  
  // Return the overcurrent protection flag.
  return pwr.ocp;
}

// ASIC powerdown sequence.
int pwr_down(void) {
  
  // Make sure the system is actually powered up.
  if (!pwr.powered) return -1;
  
  // Disable the regulators.
  pwr.powered = 0;
  digitalWriteFast(PIN_PWR_ENA, LOW);
  
  return 0;
}

// Initializes power stuff.
void pwr_setup(void) {
  
  // Initialize nonzero parts of the API structure, because apparently
  // designated initializers are hard to implement in a compiler.
  pwr.trim_2V5D = 5000;
  pwr.trim_1V2D = 2400;
  pwr.trim_2V5A = 5000;
  pwr.trim_1V2A = 2400;
  pwr.iref_tx[0] = 3500;
  pwr.iref_tx[1] = 3500;
  pwr.iref_tx[2] = 3500;
  pwr.iref_tx[3] = 3500;
  pwr.iref_rx[0] = 5000;
  pwr.iref_rx[1] = 5000;
  pwr.iref_rx[2] = 5000;
  pwr.iref_rx[3] = 5000;
  //pwr.enable = 1;
  
  // Configure pins.
  pinMode     (PIN_LDAC01,  OUTPUT);
  digitalWrite(PIN_LDAC01,  HIGH);
  pinMode     (PIN_LDAC23,  OUTPUT);
  digitalWrite(PIN_LDAC23,  HIGH);
  pinMode     (PIN_LDACP,   OUTPUT);
  digitalWrite(PIN_LDACP,   HIGH);
  pinMode     (PIN_SCL,     OUTPUT_OPENDRAIN);
  digitalWrite(PIN_SCL,     HIGH);
  pinMode     (PIN_SDA,     OUTPUT_OPENDRAIN);
  digitalWrite(PIN_SDA,     HIGH);
  pinMode     (PIN_PWR_ERR, INPUT_PULLUP);
  digitalWrite(PIN_PWR_ERR, HIGH);
  pinMode     (PIN_PWR_ENA, OUTPUT);
  digitalWrite(PIN_PWR_ENA, LOW);
  pinMode     (PIN_OCPA,    OUTPUT_OPENDRAIN);
  digitalWrite(PIN_OCPA,    HIGH);
  pinMode     (PIN_OCPB,    OUTPUT_OPENDRAIN);
  digitalWrite(PIN_OCPB,    HIGH);
  pinMode     (PIN_OCPC,    OUTPUT_OPENDRAIN);
  digitalWrite(PIN_OCPC,    HIGH);
  pinMode     (PIN_OCPD,    OUTPUT_OPENDRAIN);
  digitalWrite(PIN_OCPD,    HIGH);
  
  // Before we use the wire library, we need to bit-bang the set-address
  // command to the MCP4728s. This command requires a timing-sensitive falling
  // edge on the LDAC pin that we can't do with the I2C peripheral.
  pwr_dac_addr(PIN_LDAC01, ADDR_DAC_INIT, ADDR_DAC_01);
  pwr_dac_addr(PIN_LDAC23, ADDR_DAC_INIT, ADDR_DAC_23);
  pwr_dac_addr(PIN_LDACP,  ADDR_DAC_INIT, ADDR_DAC_P);
  
  // Initialize I2C driver.
  Wire2.setSCL(PIN_SCL);
  Wire2.setSDA(PIN_SDA);
  Wire2.begin();
  Wire2.setClock(5000000);
  // bitch, let me pick my own damn clock
  I2C2_F = 0; // MAXIMUM SPEEEEEEEEEEEED
  
  // Start the INA220s.
  pwr_ina_cfg(1);

  // Initialize the serial port.
  Serial3.begin(115200);
  pwr_serial_state = 0;
  
}

// Peeks a power register.
unsigned short pwr_peek(unsigned short reg) {
  if (reg >= PWR_REG_TOTAL) return 0;
  return pwr.regs[reg];
}

// Pokes a power register.
void pwr_poke(unsigned short reg, unsigned short value) {
  if (reg < PWR_REG_RDONLY) return;
  if (reg >= PWR_REG_TOTAL) return;
  pwr.regs[reg] = value;
}

// Updates power stuff.
void pwr_loop(void) {
  
  // Do only a single readout/update at a time in order to not hog update loop
  // time too much.
  static int state = 0;
  if (state < 4) {
    
    // Read an INA220.
    pwr_ina_data_t data = pwr_ina_read(ADDR_INA_MIN + state);
    if (data.valid) {
      switch (ADDR_INA_MIN + state) {
        case ADDR_INA_2V5D:
          pwr.I2V5D = data.current;
          pwr.U2V5D = data.voltage;
          break;
        case ADDR_INA_1V2D:
          pwr.I1V2D = data.current;
          pwr.U1V2D = data.voltage;
          break;
        case ADDR_INA_2V5A:
          pwr.I2V5A = data.current;
          pwr.U2V5A = data.voltage;
          break;
        case ADDR_INA_1V2A:
          pwr.I1V2A = data.current;
          pwr.U1V2A = data.voltage;
          break;
      }
    }
    
  } else if (state < 8) {
    
    // Update a vreg trim DAC.
    int ch;
    unsigned short *val;
    float nom;
    float rat;
    if (state < 6) {
      ch = (state == 4) ? DAC_VREG_2V5D : DAC_VREG_2V5A;
      val = (state == 4) ? &pwr.trim_2V5D : &pwr.trim_2V5A;
      nom = 2.48595f;
      rat = -0.32455f;
    } else {
      ch = (state == 6) ? DAC_VREG_1V2D : DAC_VREG_1V2A;
      val = (state == 6) ? &pwr.trim_1V2D : &pwr.trim_1V2A;
      nom = 1.20000f;
      rat = -0.22636f;
    }
    
    // Compute DAC voltage required to get the requested voltage.
    float dac = ((*val * 0.0005f) - nom) / rat + 0.8f;
    
    // Clamp to what the DAC can actually do.
    if (dac < 0.0) dac = 0.0f;
    if (dac > 3.2) dac = 3.2f;
    
    // Compute the actual trimmed voltage.
    *val = (unsigned short)((nom + (dac - 0.8f) * rat) * 2000.0f);
    
    // Update the DAC.
    pwr_dac_set(ch, dac);
    
  } else if (state < 16) {
    
    // Update an Iref trim DAC.
    int ch;
    unsigned short *val;
    if (state < 12) {
      ch = ((state - 8) << 1) + DAC_IREF_TX_0;
      val = &(pwr.iref_tx[state - 8]);
    } else {
      ch = ((state - 12) << 1) + DAC_IREF_RX_0;
      val = &(pwr.iref_rx[state - 12]);
    }
    
    // Compute DAC voltage required to get the requested current.
    float dac = *val * 0.0002f;
    
    // Clamp to what the DAC can actually do.
    if (dac < 0.0) dac = 0.0f;
    if (dac > 3.2) dac = 3.2f;
    
    // Compute the actual trimmed voltage.
    *val = (unsigned short)(dac * 5000.0f);
    
    // Update the DAC.
    pwr_dac_set(ch, dac);
    
  } else {
    
    // Power up/down when requested.
    if (pwr.enable && !pwr.powered) pwr_up();
    if (!pwr.enable && pwr.powered) pwr_down();
    
    state = -1;
  }
  state++;
  
  // Handle incoming UART commands. The protocol for reads is:
  //   F->T 0b00AAAAAA (address bit 10 downto 5).
  //   F->T 0b011AAAAA (address bit 4 downto 0).
  //   T->F 0b100DDDDD (read data bit 15 downto 11).
  //   T->F 0b101DDDDD (read data bit 10 downto 6).
  //   T->F 0b11DDDDDD (read data bit 5 downto 0).
  //
  // And for writes:
  //   F->T 0b00AAAAAA (address bit 10 downto 5).
  //   F->T 0b010AAAAA (address bit 4 downto 0).
  //   F->T 0b100DDDDD (write data bit 15 downto 11).
  //   F->T 0b101DDDDD (write data bit 10 downto 6).
  //   F->T 0b11DDDDDD (write data bit 5 downto 0).
  //   T->F 0b01010101 (ack)
  //
  // The protocol is designed such that a byte uniquely defines the state it's
  // supposed to be sent/received in. If bytes are received out of sequence,
  // there was a transmission error, so we should reset and wait for the next
  // byte matching 0b00------.
  while (Serial3.available()) {
    int b = Serial3.read();

    // Handle nominal cases.
    if (pwr_serial_state == 0) {
      
      // Idle, waiting for address high.
      if ((b >> 6) == 0) {
        pwr_serial_addr = b << 5;
        pwr_serial_state = 1;
        continue;
      }
      
    } else if (pwr_serial_state == 1) {
      
      // Waiting for address low.
      if ((b >> 6) == 1) {
        pwr_serial_addr |= b & 0x1F;
        if (b & 0x20) {
          
          // Read from this address.
          pwr_serial_data = pwr_peek(pwr_serial_addr);
          if (Serial) {
            Serial.printf("FPGA power reg read: *0x%04X == 0x%04X\n", pwr_serial_addr, pwr_serial_data);
          }
          Serial3.write(0x80 | ((pwr_serial_data >> 11) & 0x1F));
          Serial3.write(0xA0 | ((pwr_serial_data >> 6) & 0x1F));
          Serial3.write(0xC0 | ((pwr_serial_data >> 0) & 0x3F));
          pwr_serial_state = 0;
          
        } else {
          pwr_serial_state = 2;
        }
        continue;
      }
      
    } else if (pwr_serial_state == 2) {
      
      // Waiting for write data high.
      if ((b >> 5) == 4) {
        pwr_serial_data = (b & 0x1F) << 11;
        pwr_serial_state = 3;
        continue;
      }
      
    } else if (pwr_serial_state == 3) {
      
      // Waiting for write data middle.
      if ((b >> 5) == 5) {
        pwr_serial_data |= (b & 0x1F) << 6;
        pwr_serial_state = 4;
        continue;
      }
      
    } else if (pwr_serial_state == 4) {
      
      // Waiting for write data low.
      if ((b >> 6) == 3) {
        pwr_serial_data |= b & 0x3F;
        pwr_poke(pwr_serial_addr, pwr_serial_data);
        Serial3.write(0x55);
        if (Serial) {
          Serial.printf("FPGA power reg write: *0x%04X <- 0x%04X\n", pwr_serial_addr, pwr_serial_data);
        }
        pwr_serial_state = 0;
        continue;
      }
      
    }

    // Handle unexpected bytes.
    if (Serial) {
      Serial.printf("Unexpected power byte from FPGA: 0x%02X, state %d\n", b, pwr_serial_state);
    }
    pwr_serial_state = 0;
    
  }
  
}

//#############################################################################
// USB host
//#############################################################################

// Library includes.
#include "USBHost_t36.h"

// Driver definitions.
USBHost usb;
USBHub usb_hub1(usb);
USBHub usb_hub2(usb);
USBHub usb_hub3(usb);
KeyboardController usb_keybd(usb);
MouseController usb_mouse(usb);

// Pin definitions.
#define PIN_LED_A     16
#define PIN_LED_B     17
#define PIN_HUB_RST   18

// Initializes USB host stuff.
void usb_setup(void) {

  // Initialize USB drivers.
  usb.begin();

  // Enable the on-board USB hub.
  pinMode(PIN_HUB_RST, OUTPUT);
  digitalWrite(PIN_HUB_RST, HIGH);

  // Initialize LEDs.
  pinMode(PIN_LED_A, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
}

// Updates USB host stuff.
void usb_loop(void) {
  usb.Task();
  digitalWrite(PIN_LED_A, (bool)usb_keybd);
  digitalWrite(PIN_LED_B, (bool)usb_mouse);
}


//#############################################################################
// Global
//#############################################################################

void setup(void) {
  pwr_setup();
  usb_setup();
}

void loop(void) {
  pwr_loop();
  usb_loop();
}


