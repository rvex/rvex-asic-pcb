#!/usr/bin/env python3

import pickle

from lib import Instance, Module, Net, Interface, Netlist

print('Loading pickle file...')
with open('interpreted.pickle', 'rb') as f:
    module = pickle.load(f)

netlist = Netlist(module)

print('Writing pickle file...')
with open('netlist.pickle', 'wb') as f:
    pickle.dump(netlist, f)
