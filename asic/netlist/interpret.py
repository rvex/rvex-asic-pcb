#!/usr/bin/env python3

import re
import pickle
import sys

from lib import Instance, Module

# Load the Verilog netlist file into memory.
print('Loading Verilog file...')
with open(sys.argv[1], 'r') as f:
    data = f.read()

# Strip comments.
data = re.sub(r'//[^\n]+\n', '\n', data, flags = re.M | re.S)
data = re.sub(r'/\*[^*]*\*/', '', data, flags = re.M | re.S)

# Strip whitespace.
data = re.sub(r'[ \t\n]+', ' ', data, flags = re.M | re.S)

# Split into statements.
stmts = [stmt.strip() for stmt in data.split(';')]

# Parse the statements.
modules = {}
curmod = None
for stmt in stmts:
    
    # Strip endmodule lines, as they don't have a semicolon at the end.
    if stmt.startswith('endmodule'):
        stmt = stmt[9:].strip()
    if not stmt:
        continue
    
    if stmt.startswith('module '):
        
        # Start of a module.
        _, name, ports = stmt.split(maxsplit=2)
        ports = [port.strip() for port in ports.split('(', maxsplit=1)[1].rsplit(')', maxsplit=1)[0].split(',')]
        
        print('Parsing module %s...' % name)
        
        curmod = Module(name, ports)
        modules[name] = curmod
        
    elif stmt.startswith('input ') or stmt.startswith('output ') or stmt.startswith('wire '):
        
        # Signal declaration.
        _, *net = stmt.split()
        if len(net) == 2:
            rng, net = net
            start, stop = [int(x.strip()) for x in rng.split('[', maxsplit=1)[1].rsplit(']', maxsplit=1)[0].split(':')]
            r = range(start, stop+1) if stop > start else reversed(range(stop, start+1))
            for i in r:
                curmod.add_net('%s[%d]' % (net, i))
                #print('%s[%d]' % (net, i))
        elif len(net) == 1:
            curmod.add_net(net[0])
            #print(net[0])
        
    elif stmt.startswith('assign '):
        
        # Signal assignment (= equivalence for us).
        _, a, _, b, *_ = stmt.split()
        curmod.group_nets(a, b, False)
        
    else:
        
        # Presumably an instantiation.
        mod, name, ports = stmt.split(maxsplit=2)
        ports = [port.strip() for port in ports.split('(', maxsplit=1)[1].rsplit(')', maxsplit=1)[0].split('),')]
        ports = [tuple((x.strip() for x in port.split('.', maxsplit=1)[1].rsplit(')', maxsplit=1)[0].split('(', maxsplit=1))) for port in ports]
        
        # Handle buffers and inverters.
        if len(ports) == 2:
            # We don't care about buffers, we're only interested in
            # function. So group buffered nets together as if they're the
            # same net.
            if mod.startswith('INV') or mod.startswith('CKINV'):
                curmod.group_nets(ports[0][1], ports[1][1], True)
            if mod.startswith('BUF') or mod.startswith('CKBUF') or mod.startswith('DEL'):
                curmod.group_nets(ports[0][1], ports[1][1], False)
                continue
        
        # Handle (flatten) instances of previously defined modules.
        if mod in modules:
            submodule = modules[mod]
            prefix = '%s/' % name
            
            print('Instantiating module %s as %s in %s...' % (mod, name, curmod.name))
            
            # Add the nets of the submodule to us.
            for net in submodule.nets:
                curmod.add_net(prefix + net)
            
            # Add the netgroups of the submodule to us.
            for netgroup in submodule.netgroups:
                curmod.netgroups.append({prefix + net: inv for net, inv in netgroup.items()})
            
            # Add the logic of the submodule to us.
            for instance in submodule.instances:
                curmod.instances.append(instance.prefixed(prefix))
            
            # Connect the ports.
            for port, main_net in ports:
                if port not in submodule.ports:
                    continue
                
                # Look up the subnets that belong to this port.
                subnets = [prefix + subnet for subnet in submodule.get_nets(port)]
                
                # Look up the main nets connected to this port.
                if main_net.strip().startswith('{'):
                    main_nets = [net.strip() for net in main_net.split('{', maxsplit=1)[1].rsplit('}', maxsplit=1)[0].split(',')]
                    nets = []
                    for x in main_nets:
                        y, = curmod.get_nets(x)
                        nets.append(y)
                else:
                    nets = curmod.get_nets(main_net)
                
                # Check sanity.
                if len(subnets) != len(nets):
                    print(subnets)
                    print(nets)
                    raise Exception('Stuff if broken :(')
                
                # Connect the nets.
                for subnet, net in zip(subnets, nets):
                    curmod.group_nets(subnet, net, False)
            
        else:
            
            # Black box instantiation.
            curmod.instances.append(Instance(mod, name, ports))

print('Writing pickle file...')
with open('interpreted.pickle', 'wb') as f:
    pickle.dump(modules['rvsys_asic'], f)
