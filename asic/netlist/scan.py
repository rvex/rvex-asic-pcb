#!/usr/bin/env python3

from lib import Instance, Module, Net, Interface, Netlist, load_netlist

netlist = load_netlist()


# There's 3 possible inversions in one of the get_chain loop iterations:
#
#      A     B   ____  C     D
# net ---o<|---o|Q SD|---o<|--- net
#               |>___|
#
# We're interested in the register value, so we want the value at C.


def get_chain(scan_out, scan_in):
    net = netlist.net_lookup(scan_out)
    inv = net.isinverted(scan_out)
    
    chain = []
    
    while True:
        
        # If this is the scan input net, the trace is complete.
        for name, inv in net.names:
            if name == scan_in:
                return chain, inv
        
        # Look through the connections of this net for a scan flipflop output.
        for instindex, portindex, bitindex, conninv in net.connections:
            inst = netlist.inst_lookup(instindex)
            if inst.mod.startswith('SDF'):
                if inst.portnames[portindex] == 'Q':
                    reginv = False
                    break
                elif inst.portnames[portindex] == 'QB':
                    reginv = True
                    break
        else:
            print(chain[-1])
            print('Failed to trace:')
            net.dump(netlist)
            return
        
        # inv @ A
        if conninv:
            inv = not inv
        # inv @ B
        if reginv:
            inv = not inv
        # inv @ C
        
        # Add the register to the scan chain.
        chain.append((inst.name, inv))
        
        # Figure out which net scan enable is connected to.
        connidx = inst.portnames.index('SE')
        if len(inst.connections[connidx]) != 1:
            raise Exception('????')
        for netidx, netinv in inst.connections[connidx].values():
            break
        senet = netlist.net_lookup(netidx)
        
        # If scan enable is connected to '0', this is a scan chain extension
        # register. In that case we need to trace pin D instead of SD.
        pin = 'SD'
        for name, inv in senet.names:
            if name == '0':
                pin = 'D'
                break
        
        # Find the net connected to the scan data input.
        connidx = inst.portnames.index(pin)
        if len(inst.connections[connidx]) != 1:
            raise Exception('????')
        for netidx, netinv in inst.connections[connidx].values():
            break
        
        # Load the new net.
        net = netlist.net_lookup(netidx)
        
        # inv @ C
        if netinv:
            inv = not inv
        # inv @ D


chains = []
for i in range(14):
    print('Tracing chain %d...' % i)
    chain, inv = get_chain('scan_out_net%d' % i, 'scan_in_net%d' % i)
    if inv:
        raise Exception('a whole chain is inverted?!')
    chains.append(chain)
    with open('scanchain/chain.%d' % i, 'w') as f:
        i = 0
        for reg, inv in chain:
            f.write('%04d %s%s\n' % (i, ('!' if inv else ''), reg))
            i += 1

with open('scanchain/chain.md', 'w') as md:
    md.write("""
Extracted scan chain data
=========================

Register numbers correspond to the scan chain block RAM within the HSI IP
block.
""")
    
    for ci, chain in enumerate(chains):
        md.write("""
Scan chain %d
--------------
""" % ci)
        
        for reg in range(62):
            md.write("""
### Register 0x%04X

""" % (0x2000 + 0x100*ci + 0x4*reg))
            
            for bit in reversed(range(32)):
                s, inv = chain[-1-(reg*32 + bit)]
                if inv:
                    s = 'not ' + s
                md.write(' - Bit %d: %s\n' % (bit, s))

        md.write("""
### Register 0x%04X

Unused.
""" % (0x2000 + 0x100*ci + 0xFC))
