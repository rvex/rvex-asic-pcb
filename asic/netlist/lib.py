
from collections import namedtuple

InstanceTup = namedtuple('Instance', ['mod', 'name', 'ports'])

def prefix_port_expr(prefix, expr):
    if expr.strip().startswith('{'):
        netnames = [prefix + net.strip() for net in expr.split('{', maxsplit=1)[1].rsplit('}', maxsplit=1)[0].split(',')]
        return '{' + ','.join(netnames) + '}'
    else:
        return prefix + expr

class Instance(InstanceTup):
    
    def prefixed(self, prefix):
        return Instance(self.mod, prefix + self.name, [(port, prefix_port_expr(prefix, net)) for port, net in self.ports])


class Module(object):
    
    def __init__(self, name, ports):
        self.name = name
        self.ports = ports
        self.nets = ['0', '1']
        self.net_set = set(['0', '1'])
        self.netgroups = []
        self.instances = []
    
    def add_net(self, net):
        if net in self.net_set:
            raise Exception('Net %s is already in module %s' % (net, self.name))
        self.net_set.add(net)
        self.nets.append(net)
    
    def get_nets(self, x):
        if "'b" in x:
            count, value = [int(y) for y in x.split('/')[-1].split("'b")]
            nets = []
            for i in reversed(range(count)):
                if value & (1 << i):
                    nets.append('1')
                else:
                    nets.append('0')
            return nets
        if x in self.net_set:
            return [x]
        nets = []
        for net in self.nets:
            if net.startswith(x) and net[len(x):].startswith('['):
                nets.append(net)
        if not nets:
            raise Exception('Net %s doesn\'t exist in module %s' % (x, self.name))
        return nets
    
    def group_nets(self, a, b, invert):
        #print('Grouping nets %s and %s' % (a, b))
        ax = self.get_nets(a)
        bx = self.get_nets(b)
        if len(ax) != len(bx):
            raise Exception('Can\'t make nets %s and %s equivalent for module %s; they differ in width' % (a, b, self.name))
        for a, b in zip(ax, bx):
            agroup = None
            bgroup = None
            bgroupi = None
            for i, netgroup in enumerate(self.netgroups):
                inv = netgroup.get(a, None)
                if inv is not None:
                    agroup = netgroup
                    agroupinv = inv
                inv = netgroup.get(b, None)
                if inv is not None:
                    bgroup = netgroup
                    bgroupinv = inv
                    bgroupi = i
            if agroup is None and bgroup is None:
                self.netgroups.append({a: False, b: invert})
            elif agroup is None:
                bgroup[a] = (not bgroupinv) if invert else bgroupinv
            elif bgroup is None:
                agroup[b] = (not agroupinv) if invert else agroupinv
            else:
                for net, binv in bgroup.items():
                    agroup[net] = (not binv) if invert else binv
                del self.netgroups[bgroupi]


class Net(object):
    
    def __init__(self, index, names):
        self.index = index
        self.names = tuple(names.items())
        self.connections = []
    
    def isinverted(self, name):
        for n, i in self.names:
            if n == name:
                return i
        raise KeyError('Net: %s' % name)
    
    def dump(self, netlist):
        print('Net %d:' % self.index)
        for name, inv in self.names:
            print(' - alias %s%s' % (('!' if inv else ''), name))
        for instindex, portindex, bitindex, inv in self.connections:
            inst = netlist.inst_lookup(instindex)
            print(' - connected to %s%d.%d.%d = %s %s%s.%s[%d]' % (
                ('!' if inv else ''), instindex, portindex, bitindex,
                inst.name, ('!' if inv else ''), inst.mod, inst.portnames[portindex], bitindex))


class Interface(object):
    
    def __init__(self, index, mod, name, portnames):
        self.index = index
        self.mod = mod
        self.name = name
        self.portnames = tuple(portnames)
        self.connections = [{} for name in portnames]
    
    def connect(self, portindex, bitindex, net, invert):
        self.connections[portindex][bitindex] = (net.index, invert)
        net.connections.append((self.index, portindex, bitindex, invert))
    

class Netlist(object):
    
    def __init__(self, module):
        
        # Create all the net objects.
        self.nets = []
        self.netlookup = {}
        
        def addnet(names):
            net = Net(len(self.nets), names)
            self.nets.append(net)
            for netname in names:
                self.netlookup[netname] = net.index
        
        print('Parsing netgroups...')
        for netgroup in module.netgroups:
            addnet(netgroup)
        print('Parsing single nets...')
        for netname in module.nets:
            if netname not in self.netlookup:
                addnet({netname: False})
        
        # Create interfaces.
        self.ifs = []
        self.iflookup = {}
        
        def add_iface(mod, name, ports):
            portnames, portnetnames = zip(*ports)
            interface = Interface(len(self.ifs), mod, name, portnames)
            for portindex, netname in enumerate(portnetnames):
                
                if netname.strip().startswith('{'):
                    netnames = [net.strip() for net in netname.split('{', maxsplit=1)[1].rsplit('}', maxsplit=1)[0].split(',')]
                    bitnetnames = []
                    for x in netnames:
                        y, = module.get_nets(x)
                        bitnetnames.append(y)
                else:
                    bitnetnames = module.get_nets(netname)
                for bitnetname in bitnetnames:
                    if bitnetname.endswith(']'):
                        bitindex = int(bitnetname.split('[')[-1][:-1])
                    else:
                        bitindex = 0
                    bitnet = self.nets[self.netlookup[bitnetname]]
                    bitnetinv = bitnet.isinverted(bitnetname)
                    interface.connect(portindex, bitindex, bitnet, bitnetinv)
            self.ifs.append(interface)
            self.iflookup[name] = interface.index
        
        print('Parsing instances...')
        
        # Create the netlist external interface.
        add_iface(module.name, '/', zip(module.ports, module.ports))
        
        # Add all the instances.
        for instance in module.instances:
            add_iface(instance.mod, instance.name, instance.ports)
    
    def net_lookup(self, x):
        if isinstance(x, str):
            x = self.netlookup[x]
        return self.nets[x]
    
    def inst_lookup(self, x):
        if isinstance(x, str):
            x = self.iflookup[x]
        return self.ifs[x]
    

def load_netlist():
    import pickle
    with open('netlist.pickle', 'rb') as f:
        return pickle.load(f)
