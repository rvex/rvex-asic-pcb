
ASIC data archive
=================

This folder contains some archived data from the ASIC. The `hdl` folder
contains the original ASIC HDL code aside from some simulation-only/comment
changes. The `netlist` folder contains the Verilog netlist and some scripts to
extract the scan chains from it (because unfortunately we forgot to generate
them with the proper tools at the time). Those extracted scan chains can be
found in the `scanchain` folder.

The source files for the ASIC and synthesis scripts can be found at
https://bitbucket.org/lvbremen/rvex-asic (`asic-lennart` branch) and
https://bitbucket.org/lvbremen/asic-synthesis . Both repositories have `read`
access granted to the `rvex-developers` group. Stephan Wong has admin access to
the repositories.

The binary files are archived by Antoon Frehe. Ask for the files of user
`lvanbremen`. The VM used to run synthesis was called `icdvm3.ewi.tudelft.nl`.
If I understood Lennart correctly, the correct files are in
`rVEX/toolchain/rVEX_clean`.


Errata
======

Here's the ASIC's errata for as far they have been discovered.

Debug bus access while lane group 0 is running code causes undefined program behavior
-------------------------------------------------------------------------------------

*PROBLEM:* a debug bus access of the core causes the address of general purpose
register file read and write port zero to be destroyed until the next
instruction, therefore making the register file contents undefined. This should
not affect reads from $r0.0, but writes to $r0.0 *are* affected.

This is obviously a pretty big problem, so the workarounds are not great.

*WORKAROUND 1:* don't do debug bus accesses. That includes loopback access from
the cores within the ASIC, so programs cannot write to global control registers
or access the control registers of other contexts. The GPIO channel of the HSI
can fortunately be used to detect program completion.

*WORKAROUND 2:* don't use one of the contexts, let's say context 3. Set the
DCR.B flag of context 3 prior to starting the core. The core will reset into
run mode, so you first have to stop it in the usual (unsafe) way; then you can
write the register, load the program into memory, and reset context 0. When you
want to access the debug bus, use the breakpoint broadcasting functionality of
the interrupt controller to halt the other cores by writing 0xF to 0x6460, then
wait until all the idle flags as reported by the HSI GPIO channel are set (mask
0x000F0000 at address 0x3800). When done, write write 0 to 0x6460 to start the
contexts up again.

*WORKAROUND 3:* don't use lane group 0. This limits the core to 1x2, 2x2, 3x2,
1x4, and 1x2+1x4. The core boots up in 1x8, but you could have the start code
reconfigure to 0x0088 and wait for it to do so by monitoring the configuration
word through the HSI GPIO channel. This is the only known way to safely let
programs running on the ASIC access the debug bus.

*WORKAROUND 4:* start the ASIC in UART mode and use the scan chain to write the
DCR.B register before accessing the debug bus. These bits are in scan chain 7
and can be accessed from registers:

 - Context 0: 0x2758 bit 4
 - Context 1: 0x2734 bit 12
 - Context 2: 0x277C bit 25
 - Context 3: 0x277C bit 10 (inverted)

*WORKAROUND 5:* use the sleep-and-wakeup system and an interrupt to stop the
core. Context 0 can not be used in this case, because if context 0 is active
the sleep-and-wakeup system won't trigger. This is a pretty convoluted process
and should only be done if the other options are not applicable... but it should
be possible to configure the sleep-and-wakeup system and interrupt controller to
reconfigure to 0x8888 when requested through the HSI GPIO channel.

*WORKAROUND 6:* theoretically, one could change the assembler to not allow
instructions that use the first source register or write to the general purpose
register file to be scheduled in lane 0. But this is obviously highly
inefficient, because that includes the vast majority of all instructions. It
gets even worse when the binary must be generic, effectively preventing the use
of even-indexed lanes altogether.

Scan chain event system not implemented
---------------------------------------

*PROBLEM:* the scan chains were not generated while taking the event system
into consideration. That is, the first bits of each scan chain (the one that is
available combinatorially) have been chosen randomly by the tools and are
therefore not very interesting.

The event system, in case the reader is not aware, was an attempt to speed up
scanning the system while it is in operation. The idea was that the first
flipflop of every scan chain (and *only* the first one) can be read without
pumping the entire scan chain along. That makes it possible to relatively
quickly send clock strobes to the core while busy-waiting for one of those
registers to change. If those registers had been chosen intelligently (for
instance, the stall registers/signals), it would for instance have been
possible to rapidly step to the next unstalled cycle of the core. The speedup
of this would have been very significant, especially since the UART doesn't run
while scanning either, which means that waiting for the next unstalled cycle of
the core will take hundreds of scan cycles without it if the core is doing a
memory access.

*WORKAROUND:* there is no event system, so you will need to pump the whole scan
chain along every clock cycle to wait for a register to change state. Grab a
cup of coffee and hope you don't get bit errors.

A change in GPIO will interrupt a multi-word HSI transaction
------------------------------------------------------------

*PROBLEM:* due to a copy-paste mistake, a change in an idle flag, done flag, or
configuration word will reset the TX transport layer state machine. The only
thing this state does is remember which packet is currently being transmitted.

*WORKAROUND:* I don't think this will break anything, since the receiver will
just ignore packets that aren't received completely, and the TX FSM only
acknowledges requests made to it when it sends the last word. This would only
be a problem if the GPIOs change so often that the HSI is saturated with
incomplete packets.
