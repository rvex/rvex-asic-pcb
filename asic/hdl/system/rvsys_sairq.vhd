-- r-VEX processor
-- Copyright (C) 2008-2016 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2016 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

library asic;
use asic.common_pkg.all;
use asic.utils_pkg.all;
-- pragma translate_off
use asic.simUtils_pkg.all;
-- pragma translate_on
use asic.bus_pkg.all;
use asic.core_pkg.all;
use asic.core_ctrlRegs_pkg.all;
use asic.cache_pkg.all;

--=============================================================================
-- This unit wraps a single r-VEX core, L1 cache, and interrupt controller. It
-- is intended for single core systems (for multicore, you'd want to share a
-- single interrupt controller).
--
-------------------------------------------------------------------------------
-- Block diagram:
--
--             .---------------.     .---------.     .---------.
-- Memory <----|<0xFFFFC000    |     |         |<----|         |
--         ,-' |     demux     |<-o--| arbiter |  :  |         |
--        , .--|>=0xFFFFC000   |  |  |         |<----|  cache  |
--       :  |  '---------------'  |  '---------'     |         |
--       :  |      (does not    ` '----------------->|snoop    |     .-------.
--       :  |     use standard   `.         .------->|flush    |<===>|cache  |
--       :  |    bus components*)  `-..     |        '---------'     |       |
--       :  |  .---------.     .---------------.                     |       |
--        ` '->|low-pri  | reg |       bit 13=0|-------------------->|debug  |
--         `.  | arbiter |<=|=>|  demux/flush  |     .---------.     | r-VEX |
--  Debug ---->|hi-pri   |     |       bit 13=1|---->|         |     |       |
--            `'---------'- - -'---------------'     | irqctrl |<===>|rctrl  |
--   IRQs ------------------------------------------>|         |     |       |
--                                                   '---------'     |       |
--  Trace <----------------------------------------------------------|trace  |
--                                                                   '-------'
--
-- * This hopefully makes it optimize better, and also avoids the problems with
-- the addrConv package with toolchains that do not fully support std_logic
-- during elaboration.
--
-------------------------------------------------------------------------------
-- Memory map:
--  - 0xFFFFFC00..0xFFFFFFFF: own control register file (only from the core).
--  - 0xFFFFE000..0xFFFFE7FF: interrupt controller registers.
--  - 0xFFFFC000..0xFFFFDFFF: core control register files through the debug
--                            bus. Writes to CR_AFF (which is read-only in the
--                            core) direct cache flushes as shown:
--
--          |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
-- CR_AFF(w)|           (unused)            |  Data flush   | Instr. flush  |
--          |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
-- 
-------------------------------------------------------------------------------
entity rvsys_sairq is
--=============================================================================
  generic (
    
    -- Core configuration. Must be equal to the configuration presented to the
    -- rvex core connected to the cache.
    RCFG                        : rvex_generic_config_type := rvex_cfg;
    
    -- Cache configuration.
    CCFG                        : cache_generic_config_type := cache_cfg;
    
    -- Number of external interrupts to service. Min 1, max 31. The timer
    -- interrupt is shared with interrupt 1.
    IRQ_NUM_IRQ                 : natural := 9;
    
    -- Number of bits in the timer value. 0 for timer disabled. Min 2, max 32
    -- for an actual timer.
    IRQ_TIMER_BITS              : natural := 32;
    
    -- Enables (1) or disables (0) configurable interrupt priorities. That is,
    -- this controls the existence of the PRIOn registers and a double-width
    -- priority decoder.
    IRQ_CONFIG_PRIO_ENABLE      : natural := 1;
    
    -- Enables (1) or disables (0) interrupt nesting support. That is, this
    -- controls the existence of the LEVELn registers and the interrupt level
    -- comparator.
    IRQ_NESTING_ENABLE          : natural := 1;
    
    -- Enables (1) or disables (0) breakpoint broadcasting (i.e. stopping cores
    -- and/or the timer in response to a breakpoint in another core). That is,
    -- this controls the existence of the BRBROn registers and the broadcasting
    -- logic.
    IRQ_BREAKPOINT_BROADCASTING : natural := 1;
    
    -- Enables (1) or disables (0) configurable reset vectors. That is, this
    -- controls the existence of the RVECTn registers. When disabled, the
    -- reset vector output is always zero for each context.
    IRQ_CONFIG_RVECT_ENABLE     : natural := 1;
    
    -- Enables (1) or disables (0) inserting a register in the interrupt
    -- request path from the controller to the processor. This can be used to
    -- break the critical path if it ends up here or decrease the strain on
    -- the router a bit at the cost of an extra cycle's worth of interrupt
    -- latency.
    IRQ_OUTPUT_REGISTER         : natural := 0;
    
    -- Platform version tag. This is put in the global control registers of the
    -- processor.
    PLATFORM_TAG                : std_logic_vector(55 downto 0) := (others => '0');
    
    -- Register consistency check configuration (see core.vhd).
    RCC_RECORD                  : string := "";
    RCC_CHECK                   : string := "";
    RCC_CTXT                    : natural := 0
    
  );
  port (
    
    -- Active high synchronous reset input.
    reset                       : in  std_logic := '0';
    
    -- Clock input, registers are rising edge triggered.
    clk                         : in  std_logic;
    
    -- Active high global clock enable input.
    clkEn                       : in  std_logic := '1';
    
    -- Memory bus for the cache.
    rv2mem                      : out bus_mst2slv_type;
    mem2rv                      : in  bus_slv2mst_type;
    
    -- Debug bus.
    dbg2rv                      : in  bus_mst2slv_type := BUS_MST2SLV_IDLE;
    rv2dbg                      : out bus_slv2mst_type;
    
    -- Interrupt inputs (active high strobe).
    irq2rv                      : in  std_logic_vector(IRQ_NUM_IRQ downto 1) := (others => '0');
    
    -- Trace interface
    rv2trsink_push              : out std_logic;
    rv2trsink_data              : out rvex_byte_type;
    rv2trsink_end               : out std_logic;
    trsink2rv_busy              : in  std_logic := '0';
    
    -- Status outputs.
    done                        : out std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
    idle                        : out std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
    configWord                  : out rvex_data_type
    
  );
end rvsys_sairq;

--=============================================================================
architecture Behavioral of rvsys_sairq is
--=============================================================================
  
component core is
-- =============================================================================
  generic (
    
    -- Configuration.
    CFG                         : rvex_generic_config_type := RVEX_CURRENT_CONFIG;
    
    -- This is used as the core index register in the global control registers.
    CORE_ID                     : natural := 0;
    
    -- Does the same thing as CORE_ID above (or well, they're added to each
    -- other so both are supported). Exists for compatibility only; new designs
    -- should use CORE_ID as it adheres to the naming conventions.
    CoreID                      : natural := 0;
    
    -- Platform version tag. This is put in the global control registers.
    PLATFORM_TAG                : std_logic_vector(55 downto 0) := (others => '0');
    
    -- Register consistency check output filename.
    RCC_RECORD                  : string := "";
    
    -- Register consistency check input filename.
    RCC_CHECK                   : string := "";
    
    -- Context to use for the consistency check.
    RCC_CTXT                    : natural := 0
    
  );
  port (
    
    ---------------------------------------------------------------------------
    -- System control
    ---------------------------------------------------------------------------
    -- Active high synchronous reset input.
    reset                       : in  std_logic;
    
    -- This signal is asserted high when the debug bus writes a one to the
    -- reset flag in the control registers. In this case, reset is already
    -- asserted internally, so this signal may be ignored. For more complex
    -- systems, the signal may be used to reset support systems as well.
    resetOut                    : out std_logic;
    
    -- Clock input, registers are rising edge triggered.
    clk                         : in  std_logic;
    fclk                        : in  std_logic := '0';
    
    -- Active high global clock enable input.
    clkEn                       : in  std_logic := '1';
    
    ---------------------------------------------------------------------------
    -- VHDL simulation debug information
    ---------------------------------------------------------------------------
    -- pragma translate_off
    
    -- Describes the current state of the processor, aligned with the last
    -- pipeline stage. Only generated when GEN_VHDL_SIM_INFO in
    -- rvex_intIface_pkg is true. You don't need to connect anything to this
    -- (and with such a complicated config-dependent array size you don't want
    -- to either); just leave it open but add it to the simulation trace if
    -- you want to see what the processor is doing.
    rv2sim                      : out rvex_string_array(1 to 2*2**CFG.numLanesLog2+2**CFG.numLaneGroupsLog2+2**CFG.numContextsLog2);
    
    -- pragma translate_on
    
    ---------------------------------------------------------------------------
    -- Run control interface
    ---------------------------------------------------------------------------
    -- External interrupt request signal, active high.
    rctrl2rv_irq                : in  std_logic_vector(2**CFG.numContextsLog2-1 downto 0) := (others => '0');
    
    -- External interrupt identification. Guaranteed to be loaded in the trap
    -- argument register in the same clkEn'd cycle where irqAck is high.
    rctrl2rv_irqID              : in  rvex_address_array(2**CFG.numContextsLog2-1 downto 0) := (others => (others => '0'));
    
    -- External interrupt acknowledge signal, active high. Goes high for one
    -- clkEn'abled cycle.
    rv2rctrl_irqAck             : out std_logic_vector(2**CFG.numContextsLog2-1 downto 0);
    
    -- Active high run signal. When released, the context will stop running as
    -- soon as possible.
    rctrl2rv_run                : in  std_logic_vector(2**CFG.numContextsLog2-1 downto 0) := (others => '1');
    
    -- Active high idle output. This is asserted when the core is no longer
    -- doing anything.
    rv2rctrl_idle               : out std_logic_vector(2**CFG.numContextsLog2-1 downto 0);
    
    -- Active high break output. This is asserted when the core is waiting for
    -- an externally handled breakpoint, or the B flag in DCR is otherwise set.
    rv2rctrl_break              : out std_logic_vector(2**CFG.numContextsLog2-1 downto 0);
    
    -- Active high trace stall output. This can be used to stall other cores
    -- and timers simultaneously in order to be able to trace more accurately.
    rv2rctrl_traceStall         : out std_logic;
    
    -- Trace stall input. This just stalls all lane groups when asserted.
    rctrl2rv_traceStall         : in  std_logic := '0';
    
    -- Active high context reset input. When high, the context control
    -- registers (including PC, done and break flag) will be reset.
    rctrl2rv_reset              : in  std_logic_vector(2**CFG.numContextsLog2-1 downto 0) := (others => '0');
    
    -- Reset vector. When the context or the entire core is reset, the PC
    -- register will be set to this value.
    rctrl2rv_resetVect          : in  rvex_address_array(2**CFG.numContextsLog2-1 downto 0) := CFG.resetVectors(2**CFG.numContextsLog2-1 downto 0);
    
    -- Active high done output. This is asserted when the context encounters
    -- a stop syllable. Processing a stop signal also sets the BRK control
    -- register, which stops the core. This bit can be reset by issuing a core
    -- reset or by means of the debug interface.
    rv2rctrl_done               : out std_logic_vector(2**CFG.numContextsLog2-1 downto 0);
    
    -- Current configuration.
    rv2rctrl_configWord         : out rvex_data_type;
    
    ---------------------------------------------------------------------------
    -- Common memory interface
    ---------------------------------------------------------------------------
    -- Decouple vector to the instruction and data memory/caches. This vector
    -- works as follows. Each pipelane group has a bit in the vector. When this
    -- bit is low, the pipelane group is a slave to the first higher-indexed
    -- group which has a high decouple bit. In such a case, the following
    -- interfacing rules apply:
    --  - All groups will issue instruction memory read commands regardless of
    --    decouple state. However, coupled groups will always make aligned
    --    accesses. In other words, you could for example only use the PC from
    --    the lowest indexed pipelane group just make wider memory accesses to
    --    deliver all the syllables.
    --  - The memories must provide equal stall and blockReconfig signals to
    --    coupled pipelane groups or behavior will be undefined.
    --  - The memories must provide equal stall signals to coupled pipelane
    --    groups or behavior will be undefined.
    -- The rvex core will follow the following rules:
    --  - Pipelane groups working together are properly aligned (see also the
    --    config control signal documentation) and the highest indexed debouple
    --    bit is always high. For example, for an rvex with 8 lanes and 4
    --    pipelane groups, the only decouple outputs generated under normal
    --    conditions are "1111", "1110", "1011", "1010" and "1000".
    --  - The decouple outputs will not split or merge two groups when either
    --    group is asserting the blockReconfig signal.
    rv2mem_decouple             : out std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Active high reconfiguration block input from the instruction and data
    -- memories. When this is low, associated lanes may not be reconfigured.
    -- The processor assumes that this signal will go low eventually when no
    -- fetch/read/write requests are made by associated lanes.
    mem2rv_blockReconfig        : in  std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    
    -- Stall inputs from the instruction and data memories. When a bit in this
    -- vector is high, the associated pipelane group will stall. Equal stall
    -- signals must be provided to coupled pipelane groups (see also the
    -- mem_decouple signal documentation).
    mem2rv_stallIn              : in  std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    
    -- Stall outputs to the instruction and data memories. When a bit in this
    -- vector is high, the associated pipelane group will not register data
    -- from the memories on the next rising clock edge, and the memories should
    -- ignore any commands given. The stall output is guaranteed to be high if
    -- the stall input is high, but the rvex may pull the stall output high for
    -- reasons other than memory stalls as well.
    rv2mem_stallOut             : out std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Cache performance information signals. Optional. Refer to core_pkg.vhd
    -- for more information about this signal (look for rvex_cacheStatus_type).
    mem2rv_cacheStatus          : in  rvex_cacheStatus_array(2**CFG.numLaneGroupsLog2-1 downto 0) := (others => RVEX_CACHE_STATUS_IDLE);
    
    ---------------------------------------------------------------------------
    -- Instruction memory interface
    ---------------------------------------------------------------------------
    -- Program counters from each pipelane group.
    rv2imem_PCs                 : out rvex_address_array(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Active high instruction fetch enable signal. When a bit in this vector
    -- is high, the bit in mem_stallOut is low and the bit in mem_decouple is
    -- high, the instruction memory must fetch the instruction pointed to by
    -- the associated vector in imem_pcs.
    rv2imem_fetch               : out std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Combinatorial cancel signal, valid one cycle after rv2imem_PCs and
    -- rv2imem_fetch, regardless of memory stalls. This will go high when a
    -- branch is detected by the next pipeline stage and the previously
    -- requested instruction is not going to be executed. In this case, the
    -- instruction memory may choose not to complete the request if that is
    -- faster somehow (a cache may choose to cancel line validation if a miss
    -- occured to allow the core to continue earlier). Note that this signal
    -- can be safely ignored for proper operation, it's just a hint which may
    -- be used to speed things up.
    rv2imem_cancel              : out std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- (L_IF_MEM clock cycles delay with clkEn high and stallOut low; L_IF_MEM
    -- is set in core_pipeline_pkg.vhd)
    
    -- Fetched instruction, from instruction memory to the rvex.
    imem2rv_instr               : in  rvex_syllable_array(2**CFG.numLanesLog2-1 downto 0);
    
    -- Cache block affinity data from cache. This should be set to cache block
    -- index which serviced the request. This is just a hint for the processor
    -- (when a core splits, the affinity values are used to determine which
    -- lane the context which was running should be run on for maximum cache
    -- locality).
    imem2rv_affinity            : in  std_logic_vector(2**CFG.numLaneGroupsLog2*CFG.numLaneGroupsLog2-1 downto 0) := (others => '1');
    
    -- Active high fault signals from the instruction memory. When high,
    -- imem2rv_instr is assumed to be invalid and an exception will be thrown.
    imem2rv_busFault            : in  std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    
    ---------------------------------------------------------------------------
    -- Data memory interface
    ---------------------------------------------------------------------------
    -- Data memory addresses from each pipelane group. Note that a section
    -- of the address space 1kiB in size must be mapped to the core control
    -- registers, making that section of the data memory inaccessible.
    -- The start address of this section is configurable with CFG.
    rv2dmem_addr                : out rvex_address_array(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Active high read enable from each pipelane group. When a bit in this
    -- vector is high, the bit in mem_stallOut is low and the bit in
    -- mem_decouple is high, the data memory must fetch the data at the address
    -- specified by the associated vector in dmem_addr.
    rv2dmem_readEnable          : out std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Write data from the rvex to the data memory.
    rv2dmem_writeData           : out rvex_data_array(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Write byte mask from the rvex to the data memory, active high.
    rv2dmem_writeMask           : out rvex_mask_array(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Active write enable from each pipelane group. When a bit in this
    -- vector is high, the bit in mem_stallOut is low and the bit in
    -- mem_decouple is high, the data memory must write the data in
    -- dmem_writeData to the address specified by dmem_addr, respecting the
    -- byte mask specified by dmem_writeMask.
    rv2dmem_writeEnable         : out std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- (L_MEM clock cycles delay with clkEn high and stallOut low; L_MEM is set
    -- in core_pipeline_pkg.vhd)
    
    -- Data output from data memory to rvex.
    dmem2rv_readData            : in  rvex_data_array(2**CFG.numLaneGroupsLog2-1 downto 0);
    
    -- Active high fault signals from the data memory. When high,
    -- dmem2rv_readData is assumed to be invalid and an exception will be
    -- thrown.
    dmem2rv_ifaceFault          : in  std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    dmem2rv_busFault            : in  std_logic_vector(2**CFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    
    ---------------------------------------------------------------------------
    -- Control/debug bus interface
    ---------------------------------------------------------------------------
    -- The control/debug bus interface may be used to access the core control
    -- registers for debugging. All cores are forcibly stalled when a read or
    -- write is requested here, such that addressing logic may be reused. More
    -- information about the memory map is available in core_ctrlRegs_pkg.vhd.
    
    -- Address for the request. Only the 13 LSB are currently used in the
    -- largest configuration.
    dbg2rv_addr                 : in  rvex_address_type := (others => '0');
    
    -- Active high read enable signal.
    dbg2rv_readEnable           : in  std_logic := '0';
    
    -- Active high write enable signal.
    dbg2rv_writeEnable          : in  std_logic := '0';
    
    -- Active high byte write mask signal.
    dbg2rv_writeMask            : in  rvex_mask_type := (others => '1');
    
    -- Write data.
    dbg2rv_writeData            : in  rvex_data_type := (others => '0');
    
    -- (one clock cycle delay with clkEn high)
    
    -- Read data.
    rv2dbg_readData             : out rvex_data_type;
    
    ---------------------------------------------------------------------------
    -- Trace interface
    ---------------------------------------------------------------------------
    -- These signals connect to the optional trace unit. When the trace unit is
    -- disabled in CFG, these signals are unused.
    
    -- When high, data is valid and should be registered in the next clkEn'd
    -- cycle.
    rv2trsink_push              : out std_logic;
    
    -- Trace data signal. Valid when push is high.
    rv2trsink_data              : out rvex_byte_type;
    
    -- When high, this is the last byte of this trace packet. This has the same
    -- timing as the data signal.
    rv2trsink_end               : out std_logic;
    
    -- When high while push is high, the trace unit is stalled. While stalled,
    -- push will stay high and data and end will remain stable.
    trsink2rv_busy              : in  std_logic := '0'
    
  );
end component;

component cache is
--=============================================================================
  generic (
    
    -- Core configuration. Must be equal to the configuration presented to the
    -- rvex core connected to the cache.
    RCFG                        : rvex_generic_config_type := RVEX_CURRENT_CONFIG;
    
    -- Cache configuration.
    CCFG                        : cache_generic_config_type := CACHE_CURRENT_CONFIG
    
  );
  port (
    
    ---------------------------------------------------------------------------
    -- System control
    ---------------------------------------------------------------------------
    -- Active high synchronous reset input.
    reset                       : in  std_logic;
    
    -- Clock input, registers are rising edge triggered.
    clk                         : in  std_logic;
    
    -- Active high CPU interface clock enable input.
    clkEnCPU                    : in  std_logic;
    
    -- Active high bus interface clock enable input.
    clkEnBus                    : in  std_logic;
    
    ---------------------------------------------------------------------------
    -- Core interface
    ---------------------------------------------------------------------------
    -- The data cache bypass signal may be used to access volatile memory
    -- regions (i.e. peripherals): when high, the cache is bypassed and the bus
    -- is accessed transparently. Refer to the entity description in core.vhd
    -- for documentation on the rest of the signals. The timing of these
    -- signals is governed by clkEnCPU.
    
    -- Common memory interface.
    rv2cache_decouple           : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    cache2rv_blockReconfig      : out std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    cache2rv_stallIn            : out std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2cache_stallOut           : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    cache2rv_status             : out rvex_cacheStatus_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    
    -- Instruction memory interface.
    rv2icache_PCs               : in  rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2icache_fetch             : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2icache_cancel            : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    icache2rv_instr             : out rvex_syllable_array(2**RCFG.numLanesLog2-1 downto 0);
    icache2rv_busFault          : out std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    icache2rv_affinity          : out std_logic_vector(2**RCFG.numLaneGroupsLog2*RCFG.numLaneGroupsLog2-1 downto 0);
    
    -- Data memory interface.
    rv2dcache_addr              : in  rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2dcache_readEnable        : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2dcache_writeData         : in  rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2dcache_writeMask         : in  rvex_mask_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2dcache_writeEnable       : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    rv2dcache_bypass            : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    dcache2rv_readData          : out rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    dcache2rv_busFault          : out std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    dcache2rv_ifaceFault        : out std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    
    ---------------------------------------------------------------------------
    -- Bus master interface
    ---------------------------------------------------------------------------
    -- Bus interface for the caches. The timing of these signals is governed by
    -- clkEnBus. 
    cache2bus_bus               : out bus_mst2slv_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    bus2cache_bus               : in  bus_slv2mst_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    
    ---------------------------------------------------------------------------
    -- Bus snooping interface
    ---------------------------------------------------------------------------
    -- These signals are optional. They are needed for cache coherency on
    -- multi-processor systems and/or for dynamic cores. The timing of these
    -- signals is governed by clkEnBus.
    
    -- Bus address which is to be invalidated when invalEnable is high.
    bus2cache_invalAddr         : in  rvex_address_type := (others => '0');
    
    -- If one of the data caches is causing the invalidation due to a write,
    -- the signal in this vector indexed by that data cache must be high. In
    -- all other cases, these signals should be low.
    bus2cache_invalSource       : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    
    -- Active high enable signal for line invalidation.
    bus2cache_invalEnable       : in  std_logic := '0';
    
    ---------------------------------------------------------------------------
    -- Status and control signals
    ---------------------------------------------------------------------------
    -- The timing of these signals is governed by clkEnBus.
    
    -- Cache flush request signals for each instruction and data cache.
    sc2icache_flush             : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0) := (others => '0');
    sc2dcache_flush             : in  std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0) := (others => '0')
    
  );
end component;

component periph_irq is
--=============================================================================
  generic (
    
    -- Base address of the interrupt controller registers as seen from the
    -- processors. This is part of the interrupt request ID (i.e. the trap
    -- argument), allowing a generic interrupt handler to be written in
    -- assembly. This must be aligned to a 2 kiB boundary.
    BASE_ADDRESS                : rvex_address_type := (others => '0');
    
    -- Number of contexts to service. Min 1, max 31.
    NUM_CONTEXTS                : natural := 1;
    
    -- Number of external interrupts to service. Min 1, max 31. The timer
    -- interrupt is shared with interrupt 1.
    NUM_IRQ                     : natural := 1;
    
    -- Number of bits in the timer value. 0 for timer disabled. Min 2, max 32
    -- for an actual timer.
    TIMER_BITS                  : natural := 32;
    
    -- Enables (1) or disables (0) configurable interrupt priorities. That is,
    -- this controls the existence of the PRIOn registers and a double-width
    -- priority decoder.
    CONFIG_PRIO_ENABLE          : natural := 1;
    
    -- Enables (1) or disables (0) interrupt nesting support. That is, this
    -- controls the existence of the LEVELn registers and the interrupt level
    -- comparator.
    NESTING_ENABLE              : natural := 1;
    
    -- Enables (1) or disables (0) breakpoint broadcasting (i.e. stopping cores
    -- and/or the timer in response to a breakpoint in another core). That is,
    -- this controls the existence of the BRBROn registers and the broadcasting
    -- logic.
    BREAKPOINT_BROADCASTING     : natural := 1;
    
    -- Enables (1) or disables (0) configurable reset vectors. That is, this
    -- controls the existence of the RVECTn registers. When disabled, the
    -- reset vector output is always zero for each context.
    CONFIG_RVECT_ENABLE         : natural := 1;
    
    -- Enables (1) or disables (0) inserting a register in the interrupt
    -- request path from the controller to the processor. This can be used to
    -- break the critical path if it ends up here or decrease the strain on
    -- the router a bit at the cost of an extra cycle's worth of interrupt
    -- latency.
    OUTPUT_REGISTER             : natural := 0
    
  );
  port (
    
    ---------------------------------------------------------------------------
    -- System control
    ---------------------------------------------------------------------------
    -- Active high synchronous reset input.
    reset                       : in  std_logic;
    
    -- Clock input, registers are rising edge triggered.
    clk                         : in  std_logic;
    
    -- Active high global clock enable input.
    clkEn                       : in  std_logic;
    
    ---------------------------------------------------------------------------
    -- r-VEX interface
    ---------------------------------------------------------------------------
    -- Interrupt request signals to the r-VEX.
    irq2rv_irq                  : out std_logic_vector(NUM_CONTEXTS-1 downto 0);
    irq2rv_irqID                : out rvex_address_array(NUM_CONTEXTS-1 downto 0);
    
    -- Interrupt acknowledge signal back from the r-VEX. Must be high for one
    -- clkEn'd cycle only.
    rv2irq_irqAck               : in  std_logic_vector(NUM_CONTEXTS-1 downto 0) := (others => '0');
    
    -- Active high run output. This is pulled low when another context is
    -- waiting for an externally managed breakpoint to be serviced and the
    -- respective BRBRO bit is set.
    irq2rv_run                  : out std_logic_vector(NUM_CONTEXTS-1 downto 0);
    
    -- Active high idle signal from the r-VEX. This is asserted when the core
    -- is not doing anything.
    rv2irq_idle                 : in  std_logic_vector(NUM_CONTEXTS-1 downto 0) := (others => '0');
    
    -- Active high break signal from the r-VEX. This is asserted when the core
    -- is waiting for an externally handled breakpoint.
    rv2irq_break                : in  std_logic_vector(NUM_CONTEXTS-1 downto 0) := (others => '0');
    
    -- Trace stall input. When this is asserted, the timer is stopped.
    rv2irq_traceStall           : in  std_logic := '0';
    
    -- Active high context reset output. When high, the context control
    -- registers (including PC, done and break flag) should be reset.
    irq2rv_reset                : out std_logic_vector(NUM_CONTEXTS-1 downto 0);
    
    -- Reset vector. When the context or the entire core is reset, the PC
    -- register should be set to this value.
    irq2rv_resetVect            : out rvex_address_array(NUM_CONTEXTS-1 downto 0);
    
    -- Active high done signal from the r-VEX. This is asserted when the
    -- context encounters a stop syllable.
    rv2irq_done                 : in  std_logic_vector(NUM_CONTEXTS-1 downto 0) := (others => '0');
   
    ---------------------------------------------------------------------------
    -- Interrupt inputs
    ---------------------------------------------------------------------------
    -- External interrupt IDs. Interrupt 1 is shared with the timer. To pend an
    -- interrupt, the respective periph2irq bit must be asserted high for one
    -- clkEn'd cycle.
    periph2irq                  : in  std_logic_vector(NUM_IRQ downto 1) := (others => '0');
    
    ---------------------------------------------------------------------------
    -- Bus interface
    ---------------------------------------------------------------------------
    -- This bus master is controlled by the debug packets, to allow the
    -- debugging software running on the computer to access mapped memory
    -- regions.
    bus2irq                     : in  bus_mst2slv_type;
    irq2bus                     : out bus_slv2mst_type
    
  );
end component;


  
  -- Core common memory interface <-> cache.
  signal rv2cache_decouple      : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_blockReconfig : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_stallIn       : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2cache_stallOut      : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_status        : rvex_cacheStatus_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  
  -- Core instruction memory interface <-> cache.
  signal rv2icache_PCs          : rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2icache_fetch        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2icache_cancel       : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal icache2rv_instr        : rvex_syllable_array(2**RCFG.numLanesLog2-1 downto 0);
  signal icache2rv_affinity     : std_logic_vector(2**RCFG.numLaneGroupsLog2*RCFG.numLaneGroupsLog2-1 downto 0);
  signal icache2rv_busFault     : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  
  -- Core data memory interface <-> cache.
  signal rv2dcache_addr         : rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_readEnable   : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_writeData    : rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_writeMask    : rvex_mask_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_writeEnable  : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_bypass       : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal dcache2rv_readData     : rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal dcache2rv_ifaceFault   : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal dcache2rv_busFault     : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  
  -- Cache to arbiter busses.
  signal cache2arb              : bus_mst2slv_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal arb2cache              : bus_slv2mst_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  
  -- Index of the cache block making the current request on cache2bus_arb.
  signal arb_source             : rvex_data_type;
  
  -- Bus snooping interface.
  signal bus2cache_invalAddr    : rvex_address_type;
  signal bus2cache_invalSource  : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal bus2cache_invalEnable  : std_logic;
  
  -- Cache flush control signal.
  signal sc2dcache_flush        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal sc2icache_flush        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  
  -- Arbiter to interconnect.
  signal arb2icon               : bus_mst2slv_type;
  signal icon2arb               : bus_slv2mst_type;
  
  -- r-VEX control/debug bus interface.
  signal icon2rv_addr           : rvex_address_type;
  signal icon2rv_readEnable     : std_logic;
  signal icon2rv_writeEnable    : std_logic;
  signal icon2rv_writeMask      : rvex_mask_type;
  signal icon2rv_writeData      : rvex_data_type;
  signal rv2icon_readData       : rvex_data_type;
  
  -- Interrupt controller interface.
  signal icon2irq               : bus_mst2slv_type;
  signal irq2icon               : bus_slv2mst_type;
  
  -- Core to interrupt controller interface.
  signal irq2rv_irq             : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal irq2rv_irqID           : rvex_address_array(2**RCFG.numContextsLog2-1 downto 0);
  signal rv2irq_irqAck          : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal irq2rv_run             : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal rv2irq_idle            : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal rv2irq_break           : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal rv2irq_traceStall      : std_logic;
  signal irq2rv_reset           : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  signal irq2rv_resetVect       : rvex_address_array(2**RCFG.numContextsLog2-1 downto 0);
  signal rv2irq_done            : std_logic_vector(2**RCFG.numContextsLog2-1 downto 0);
  
--=============================================================================
begin -- architecture
--=============================================================================
  
  -----------------------------------------------------------------------------
  -- Instantiate the rvex core
  -----------------------------------------------------------------------------
  core_inst: core
    generic map (
      CFG                       => RCFG,
      CORE_ID                   => 0,
      PLATFORM_TAG              => PLATFORM_TAG,
      RCC_RECORD                => RCC_RECORD,
      RCC_CHECK                 => RCC_CHECK,
      RCC_CTXT                  => RCC_CTXT
    )
    port map (
      
      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,
      
      -- Run control interface.
      rctrl2rv_irq              => irq2rv_irq,
      rctrl2rv_irqID            => irq2rv_irqID,
      rv2rctrl_irqAck           => rv2irq_irqAck,
      rctrl2rv_run              => irq2rv_run,
      rv2rctrl_idle             => rv2irq_idle,
      rv2rctrl_break            => rv2irq_break,
      rv2rctrl_traceStall       => rv2irq_traceStall,
      rctrl2rv_reset            => irq2rv_reset,
      rctrl2rv_resetVect        => irq2rv_resetVect,
      rv2rctrl_done             => rv2irq_done,
      rv2rctrl_configWord       => configWord,
      
      -- Common memory interface.
      rv2mem_decouple           => rv2cache_decouple,
      mem2rv_blockReconfig      => cache2rv_blockReconfig,
      mem2rv_stallIn            => cache2rv_stallIn,
      rv2mem_stallOut           => rv2cache_stallOut,
      mem2rv_cacheStatus        => cache2rv_status,
      
      -- Instruction memory interface.
      rv2imem_PCs               => rv2icache_PCs,
      rv2imem_fetch             => rv2icache_fetch,
      rv2imem_cancel            => rv2icache_cancel,
      imem2rv_instr             => icache2rv_instr,
      imem2rv_affinity          => icache2rv_affinity,
      imem2rv_busFault          => icache2rv_busFault,
      
      -- Data memory interface.
      rv2dmem_addr              => rv2dcache_addr,
      rv2dmem_readEnable        => rv2dcache_readEnable,
      rv2dmem_writeData         => rv2dcache_writeData,
      rv2dmem_writeMask         => rv2dcache_writeMask,
      rv2dmem_writeEnable       => rv2dcache_writeEnable,
      dmem2rv_readData          => dcache2rv_readData,
      dmem2rv_busFault          => dcache2rv_busFault,
      dmem2rv_ifaceFault        => dcache2rv_ifaceFault,
      
      -- Control/debug bus interface.
      dbg2rv_addr               => icon2rv_addr,
      dbg2rv_readEnable         => icon2rv_readEnable,
      dbg2rv_writeEnable        => icon2rv_writeEnable,
      dbg2rv_writeMask          => icon2rv_writeMask,
      dbg2rv_writeData          => icon2rv_writeData,
      rv2dbg_readData           => rv2icon_readData,
      
      -- Trace interface.
      rv2trsink_push            => rv2trsink_push,
      rv2trsink_data            => rv2trsink_data,
      rv2trsink_end             => rv2trsink_end,
      trsink2rv_busy            => trsink2rv_busy
      
    );
  
  -- Generate the bypass signal.
  bypass_gen: for laneGroup in 0 to 2**RCFG.numLaneGroupsLog2-1 generate
    rv2dcache_bypass(laneGroup) <= rv2dcache_addr(laneGroup)(31);
  end generate;
  
  -----------------------------------------------------------------------------
  -- Instantiate the cache
  -----------------------------------------------------------------------------
  cache_inst: cache
    generic map (
      RCFG                      => RCFG,
      CCFG                      => CCFG
    )
    port map (
      
      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEnCPU                  => clkEn,
      clkEnBus                  => clkEn,
      
      -- Common memory interface.
      rv2cache_decouple         => rv2cache_decouple,
      cache2rv_blockReconfig    => cache2rv_blockReconfig,
      cache2rv_stallIn          => cache2rv_stallIn,
      rv2cache_stallOut         => rv2cache_stallOut,
      cache2rv_status           => cache2rv_status,
      
      -- Instruction memory interface.
      rv2icache_PCs             => rv2icache_PCs,
      rv2icache_fetch           => rv2icache_fetch,
      rv2icache_cancel          => rv2icache_cancel,
      icache2rv_instr           => icache2rv_instr,
      icache2rv_affinity        => icache2rv_affinity,
      icache2rv_busFault        => icache2rv_busFault,
      
      -- Data memory interface.
      rv2dcache_addr            => rv2dcache_addr,
      rv2dcache_readEnable      => rv2dcache_readEnable,
      rv2dcache_writeData       => rv2dcache_writeData,
      rv2dcache_writeMask       => rv2dcache_writeMask,
      rv2dcache_writeEnable     => rv2dcache_writeEnable,
      rv2dcache_bypass          => rv2dcache_bypass,
      dcache2rv_readData        => dcache2rv_readData,
      dcache2rv_ifaceFault      => dcache2rv_ifaceFault,
      dcache2rv_busFault        => dcache2rv_busFault,
      
      -- Bus master interface.
      cache2bus_bus             => cache2arb,
      bus2cache_bus             => arb2cache,
      
      -- Bus snooping interface.
      bus2cache_invalAddr       => bus2cache_invalAddr,
      bus2cache_invalSource     => bus2cache_invalSource,
      bus2cache_invalEnable     => bus2cache_invalEnable,
      
      -- Status and control signals.
      sc2icache_flush           => sc2icache_flush,
      sc2dcache_flush           => sc2dcache_flush
      
    );
  
  -- Snoop the debug bus to generate the cache flush signals. Delay doesn't
  -- matter that much here, so we can register this here (they are high-fanout
  -- nets). Behavior one cycle after reset is don't care.
  flush_regs: process (clk) is
  begin
    if rising_edge(clk) then
      if clkEn = '1' then
        sc2icache_flush <= (others => '0');
        sc2dcache_flush <= (others => '0');
        if icon2rv_addr(9 downto 2) = uint2vect(CR_AFF, 8) then
          if icon2rv_writeEnable = '1' then
            if icon2rv_writeMask(0) = '1' then
              sc2icache_flush <= icon2rv_writeData(2**RCFG.numLaneGroupsLog2-1 downto 0);
            end if;
            if icon2rv_writeMask(1) = '1' then
              sc2dcache_flush <= icon2rv_writeData(2**RCFG.numLaneGroupsLog2+7 downto 8);
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;
  
  -----------------------------------------------------------------------------
  -- Bus access arbiter
  -----------------------------------------------------------------------------
  cache_arbiter: entity asic.bus_arbiter
    generic map (
      NUM_MASTERS               => 2**RCFG.numLaneGroupsLog2
    )
    port map (
      
      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,
      
      -- Master busses.
      mst2arb                   => cache2arb,
      arb2mst                   => arb2cache,
      
      -- Slave bus.
      arb2slv                   => arb2icon,
      slv2arb                   => icon2arb,
      
      -- Index of the master which is making the current bus request.
      arb2slv_source            => arb_source
      
    );
  
  -----------------------------------------------------------------------------
  -- Cache line invalidation (snooping)
  -----------------------------------------------------------------------------
  -- Connect address end enable. Only enable invalidation when the access is in
  -- the lower half of the address space, as the upper half is uncached.
  bus2cache_invalAddr   <= arb2icon.address;
  bus2cache_invalEnable <= arb2icon.writeEnable
                           and not arb2icon.address(31);
  
  -- Decode arb2slv_source to get the bus2cache_invalSource signal.
  inval_source_proc: process (arb_source) is
  begin
    bus2cache_invalSource <= (others => '0');
    for laneGroup in 0 to 2**RCFG.numLaneGroupsLog2-1 loop
      if vect2uint(arb_source) = laneGroup then
        bus2cache_invalSource(laneGroup) <= '1';
      end if;
    end loop;
  end process;
  
  -----------------------------------------------------------------------------
  -- Interrupt controller
  -----------------------------------------------------------------------------
  irq_ctrl: periph_irq
    generic map (
      BASE_ADDRESS              => X"FFFFE000",
      NUM_CONTEXTS              => 2**RCFG.numContextsLog2,
      NUM_IRQ                   => IRQ_NUM_IRQ,
      TIMER_BITS                => IRQ_TIMER_BITS,
      CONFIG_PRIO_ENABLE        => IRQ_CONFIG_PRIO_ENABLE,
      NESTING_ENABLE            => IRQ_NESTING_ENABLE,
      BREAKPOINT_BROADCASTING   => IRQ_BREAKPOINT_BROADCASTING,
      CONFIG_RVECT_ENABLE       => IRQ_CONFIG_RVECT_ENABLE,
      OUTPUT_REGISTER           => IRQ_OUTPUT_REGISTER
    )
    port map (
      
      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,
      
      -- r-VEX interface.
      irq2rv_irq                => irq2rv_irq,
      irq2rv_irqID              => irq2rv_irqID,
      rv2irq_irqAck             => rv2irq_irqAck,
      irq2rv_run                => irq2rv_run,
      rv2irq_idle               => rv2irq_idle,
      rv2irq_break              => rv2irq_break,
      rv2irq_traceStall         => rv2irq_traceStall,
      irq2rv_reset              => irq2rv_reset,
      irq2rv_resetVect          => irq2rv_resetVect,
      rv2irq_done               => rv2irq_done,
     
      -- Interrupt inputs.
      periph2irq                => irq2rv,
      
      -- Bus interface.
      bus2irq                   => icon2irq,--NOTE: THIS UNIT ASSUMES THAT THIS
      irq2bus                   => irq2icon --IS ALWAYS A SINGLE-CYCLE BUS.
      
    );
  
  idle <= rv2irq_idle;
  done <= rv2irq_done;
  
  -----------------------------------------------------------------------------
  -- Bus interconnect
  -----------------------------------------------------------------------------
  -- The block below implements the following:
  --
  --             .-------------.
  --             |  <0xFFFFC000|------------------------------> rv2mem
  -- arb2icon -->|   demux A   |   .---------.   .---------.
  --             | >=0xFFFFC000|-->|low-pri  |   |   b13=0 |--> icon2rv
  --             '-------------'   | arbiter |-->| demux B |
  --   dbg2rv ---------------------|hi-pri   |   |   b13=1 |--> icon2irq
  --                               '---------'   '---------'
  --
  icon_block: block is
    
    -- Demux A control signals.
    signal dema_request_mux     : std_logic;
    signal dema_result_mux      : std_logic;
    
    -- Demux A to arbiter bus.
    signal dema2arb_address     : std_logic_vector(13 downto 2);
    signal dema2arb_readEnable  : std_logic;
    signal dema2arb_writeEnable : std_logic;
    signal dema2arb_writeMask   : rvex_mask_type;
    signal dema2arb_writeData   : rvex_data_type;
    signal arb2dema_readData    : rvex_data_type;
    signal arb2dema_fault       : std_logic;
    signal arb2dema_busy        : std_logic;
    signal arb2dema_ack         : std_logic;
    
    -- Debug to arbiter bus.
    signal dbg2arb_address      : std_logic_vector(13 downto 2);
    signal dbg2arb_readEnable   : std_logic;
    signal dbg2arb_writeEnable  : std_logic;
    signal dbg2arb_writeMask    : rvex_mask_type;
    signal dbg2arb_writeData    : rvex_data_type;
    signal arb2dbg_readData     : rvex_data_type;
    signal arb2dbg_fault        : std_logic;
    signal arb2dbg_busy         : std_logic;
    signal arb2dbg_ack          : std_logic;
    
    -- Delayed request signals for the arbiter (to break long bus request
    -- paths).
    signal dema2arb_address_r   : std_logic_vector(13 downto 2);
    signal dema2arb_readEnable_r: std_logic;
    signal dema2arb_writeEnable_r:std_logic;
    signal dema2arb_writeMask_r : rvex_mask_type;
    signal dema2arb_writeData_r : rvex_data_type;
    signal dbg2arb_address_r    : std_logic_vector(13 downto 2);
    signal dbg2arb_readEnable_r : std_logic;
    signal dbg2arb_writeEnable_r: std_logic;
    signal dbg2arb_writeMask_r  : rvex_mask_type;
    signal dbg2arb_writeData_r  : rvex_data_type;
    
    -- Arbiter bus state signals. The timing is as shown in the diagram. The
    -- request to the r-VEX debug bus/interrupt controller access bus is active
    -- while phase(0) is asserted and stall is not asserted (in this case the
    -- other bus has requested something as well and took precedence). The
    -- result is valid while phase(1) is asserted; this is subsequently delayed
    -- by one cycle before being returned to the master to break the return
    -- path in two as well.
    --
    --         |__    __    __    __    __    __    __    __    __    __    |
    --     clk |  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/|
    --         |............................................................|
    --         |_____  ________________  ________________  _________________|
    -- request |nop__><req1____________><req2____________><nop______________|
    --         |                         ____              ____             |
    --  result |------------------------<res1>------------<res2>------------|
    --         |             ___________       _________________            |
    --    busy |____________/           \_____/                 \___________|
    --         |                         _____                   _____      |
    --     ack |________________________/     \_________________/     \_____|
    --         |............................................................|
    --         |             _____             ___________                  |
    -- phase(0)|____________/     \___________/           \_________________|
    --         |                   _____                   _____            |
    -- phase(1)|__________________/     \_________________/     \___________|
    --         |                                 ___                        |
    --   stall |______________________________///   \\\_____________________|
    --         |                                                            |
    signal dema2arb_phase       : std_logic_vector(1 downto 0);
    signal dbg2arb_phase        : std_logic_vector(1 downto 0);
    signal dema2arb_stall       : std_logic;
    
    -- Mux select signals for the r-VEX debug bus and the interrupt controller
    -- access bus requests. '0' indicates dema2arb, '1' indicates dbg2arb.
    signal arb2rv_mux           : std_logic;
    signal arb2irq_mux          : std_logic;
    
  begin
    
    -------------
    -- Demux A --
    -------------
    
    -- Determine which bus the request is intended for.
    dema_request_mux <= '1' when
      arb2icon.address(31 downto 14) = "11111111"&"11111111"&"11" else '0';
    
    -- Construct the rv2mem request signal.
    rv2mem <= bus_gate(arb2icon, not dema_request_mux);
    
    -- Construct the dema2arb request signal.
    dema2arb_address <= arb2icon.address(13 downto 2);
    dema2arb_readEnable <= arb2icon.readEnable and dema_request_mux;
    dema2arb_writeEnable <= arb2icon.writeEnable and dema_request_mux;
    dema2arb_writeMask <= arb2icon.writeMask;
    dema2arb_writeData <= arb2icon.writeData;
    
    -- Delay the mux signal by one cycle to align it with the busy/ack response
    -- signals.
    demux_a_reg: process (clk) is
    begin
      if rising_edge(clk) then
        if clkEn = '1' then
          dema_result_mux <= dema_request_mux;
        end if;
      end if;
    end process;
    
    -- Select the result. The ack/busy signals can just be wired-or assuming
    -- everything else is implemented correctly.
    demux_a_result: process (
      dema_result_mux, mem2rv, arb2dema_readData, arb2dema_fault,
      arb2dema_busy, arb2dema_ack
    ) is
    begin
      icon2arb <= BUS_SLV2MST_IDLE;
      if dema_result_mux = '1' then
        icon2arb.readData <= arb2dema_readData;
        icon2arb.fault <= arb2dema_fault;
      else
        icon2arb.readData <= mem2rv.readData;
        icon2arb.fault <= mem2rv.fault;
      end if;
      icon2arb.busy <= arb2dema_busy or mem2rv.busy;
      icon2arb.ack <= arb2dema_ack or mem2rv.ack;
    end process;
    
    -------------------
    -- Debug arbiter --
    -------------------
    
    -- Unpack dbg2rv and pack rv2dbg.
    dbg2arb_address <= dbg2rv.address(13 downto 2);
    dbg2arb_readEnable <= dbg2rv.readEnable;
    dbg2arb_writeEnable <= dbg2rv.writeEnable;
    dbg2arb_writeMask <= dbg2rv.writeMask;
    dbg2arb_writeData <= dbg2rv.writeData;
    rv2dbg_pack_proc: process (
      arb2dbg_readData, arb2dbg_fault, arb2dbg_busy, arb2dbg_ack
    ) is
    begin
      rv2dbg <= BUS_SLV2MST_IDLE;
      rv2dbg.readData <= arb2dbg_readData;
      rv2dbg.fault <= arb2dbg_fault;
      rv2dbg.busy <= arb2dbg_busy;
      rv2dbg.ack <= arb2dbg_ack;
    end process;
    
    -- Instantiate the arbiter registers.
    enable_regs: process (clk) is
    begin
      if rising_edge(clk) then
        if reset = '1' then
          
          -- Reset the dema2arb and arb2dema signals.
          dema2arb_readEnable_r  <= '0';
          dema2arb_writeEnable_r <= '0';
          dema2arb_phase         <= "00";
          arb2dema_busy          <= '0';
          arb2dema_ack           <= '0';
          
          -- Reset the dbg2arb and arb2dbg signals.
          dbg2arb_readEnable_r   <= '0';
          dbg2arb_writeEnable_r  <= '0';
          dbg2arb_phase          <= "00";
          arb2dbg_busy           <= '0';
          arb2dbg_ack            <= '0';
          
          -- Extra reset signals, needed for clock gating
          arb2rv_mux             <= '0';
          arb2irq_mux            <= '0';
          
        elsif clkEn = '1' then
          
          -- Register the dema2arb bus requests to break combinatorial paths.
          dema2arb_readEnable_r  <= dema2arb_readEnable;
          dema2arb_writeEnable_r <= dema2arb_writeEnable;
          if dema2arb_writeEnable = '1' or dema2arb_readEnable = '1' then
            dema2arb_address_r   <= dema2arb_address;
          end if;
          if dema2arb_writeEnable = '1' then
            dema2arb_writeMask_r <= dema2arb_writeMask;
            dema2arb_writeData_r <= dema2arb_writeData;
          end if;
          
          -- Generate the dema2arb phase signals. Refer to the timing diagram
          -- at the signal declaration for an explanation.
          dema2arb_phase(0)
            <= (dema2arb_writeEnable or dema2arb_readEnable) -- Only when request is active.
            and (
              (not (dema2arb_phase(0) or dema2arb_phase(1))) -- Don't start while busy.
              or (dema2arb_phase(0) and dema2arb_stall) -- Stay in state if stalled.
            );
          dema2arb_phase(1) <= dema2arb_phase(0) and not dema2arb_stall;
          
          -- Drive the result signals.
          if dema2arb_phase(1) = '0' then
            arb2dema_busy <= dema2arb_readEnable or dema2arb_writeEnable;
            arb2dema_ack <= '0';
          else
            arb2dema_busy <= '0';
            arb2dema_ack <= '1';
            if dema2arb_address_r(13) = '0' then
              -- Connect to r-VEX debug bus result.
              arb2dema_readData <= rv2icon_readData;
              arb2dema_fault <= '0';
            else
              -- Connect to interrupt controller access bus result.
              arb2dema_readData <= irq2icon.readData;
              arb2dema_fault <= irq2icon.fault;
            end if;
          end if;
          
          
          -- Register the dbg2arb bus requests to break combinatorial paths.
          dbg2arb_readEnable_r   <= dbg2arb_readEnable;
          dbg2arb_writeEnable_r  <= dbg2arb_writeEnable;
          if dbg2arb_writeEnable = '1' or dbg2arb_readEnable = '1' then
            dbg2arb_address_r    <= dbg2arb_address;
          end if;
          if dbg2arb_writeEnable = '1' then
            dbg2arb_writeMask_r  <= dbg2arb_writeMask;
            dbg2arb_writeData_r  <= dbg2arb_writeData;
          end if;
          
          -- Generate the dema2arb phase signals. Refer to the timing diagram
          -- at the signal declaration for an explanation.
          dbg2arb_phase(0)
            <= (dbg2arb_writeEnable or dbg2arb_readEnable) -- Only when request is active.
            and not (dbg2arb_phase(0) or dbg2arb_phase(1)); -- Don't start while busy.
          dbg2arb_phase(1) <= dbg2arb_phase(0);
          
          -- Drive the result signals.
          if dbg2arb_phase(1) = '0' then
            arb2dbg_busy <= dbg2arb_readEnable or dbg2arb_writeEnable;
            arb2dbg_ack <= '0';
          else
            arb2dbg_busy <= '0';
            arb2dbg_ack <= '1';
            if dbg2arb_address_r(13) = '0' then
              -- Connect to r-VEX debug bus result.
              arb2dbg_readData <= rv2icon_readData;
              arb2dbg_fault <= '0';
            else
              -- Connect to interrupt controller access bus result.
              arb2dbg_readData <= irq2icon.readData;
              arb2dbg_fault <= irq2icon.fault;
            end if;
          end if;
          
          -- Drive the request mux signals.
          arb2rv_mux <= '0';
          arb2irq_mux <= '0';
          if dbg2arb_readEnable = '1' or dbg2arb_writeEnable = '1' then
            arb2rv_mux <= not dbg2arb_address(13);
            arb2irq_mux <= dbg2arb_address(13);
          end if;
          
        end if;
      end if;
    end process;
    
    -- The dbg2arb bus overrides the dema2arb bus when two accesses conflict.
    -- In this case, the dema2arb bus must be stalled.
    dema2arb_stall
       <= (dema2arb_phase(0) and dbg2arb_phase(0)) -- Both active.
      and (dema2arb_address_r(13) xnor dbg2arb_address_r(13)); -- Same bus.
    
    -- Drive the bus request signals.
    bus_req_proc: process (
      dema2arb_address_r, dema2arb_readEnable_r, dema2arb_writeEnable_r,
      dema2arb_writeMask_r, dema2arb_writeData_r, dbg2arb_address_r,
      dbg2arb_readEnable_r, dbg2arb_writeEnable_r, dbg2arb_writeMask_r,
      dbg2arb_writeData_r, arb2rv_mux, arb2irq_mux
    ) is
    begin
      
      -- Drive the r-VEx debug bus.
      if arb2rv_mux = '0' then
        icon2rv_addr        <= X"0000"&"000" & dema2arb_address_r(12 downto 2) & "00";
        icon2rv_readEnable  <= dema2arb_readEnable_r;
        icon2rv_writeEnable <= dema2arb_writeEnable_r;
        icon2rv_writeMask   <= dema2arb_writeMask_r;
        icon2rv_writeData   <= dema2arb_writeData_r;
      else
        icon2rv_addr        <= X"0000"&"000" & dbg2arb_address_r(12 downto 2) & "00";
        icon2rv_readEnable  <= dbg2arb_readEnable_r;
        icon2rv_writeEnable <= dbg2arb_writeEnable_r;
        icon2rv_writeMask   <= dbg2arb_writeMask_r;
        icon2rv_writeData   <= dbg2arb_writeData_r;
      end if;
      
      -- Drive the interrupt controller access bus.
      icon2irq <= BUS_MST2SLV_IDLE;
      if arb2irq_mux = '0' then
        icon2irq.address    <= X"0000"&"000" & dema2arb_address_r(12 downto 2) & "00";
        icon2irq.readEnable <= dema2arb_readEnable_r;
        icon2irq.writeEnable<= dema2arb_writeEnable_r;
        icon2irq.writeMask  <= dema2arb_writeMask_r;
        icon2irq.writeData  <= dema2arb_writeData_r;
      else
        icon2irq.address    <= X"0000"&"000" & dbg2arb_address_r(12 downto 2) & "00";
        icon2irq.readEnable <= dbg2arb_readEnable_r;
        icon2irq.writeEnable<= dbg2arb_writeEnable_r;
        icon2irq.writeMask  <= dbg2arb_writeMask_r;
        icon2irq.writeData  <= dbg2arb_writeData_r;
      end if;
      
    end process;
    
  end block;
  
end Behavioral;

