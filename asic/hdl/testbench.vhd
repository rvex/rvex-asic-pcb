package testbench_pkg is
  type designStage_type is (stage_rvex, stage_asic, stage_syn, stage_par);
  type operationalMode_type is (UART_reliable, UART_fast, HSI); -- TODO: Scan test mode
end testbench_pkg;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.testbench_pkg.all;

library asic;
use asic.common_pkg.all;
use asic.utils_pkg.all;
use asic.simUtils_pkg.all;
use asic.simUtils_mem_pkg.all;
use asic.bus_pkg.all;
use asic.core_pkg.all;
use asic.cache_pkg.all;

-- library syn;
-- library par;

entity testbench is
  generic (
    -- One of:
    -- * stage_rvex     Original rVEX code
    -- * stage_asic     rVEX code replaced with synthesis ready components
    -- * stage_syn      post Synthesis
    -- * stage_par      post Place And Route
--     DESIGN_STAGE                : designStage_type := stage_rvex;
    -- One of:
    -- * UART_reliable  10 MHz reliable UART mode
    -- * UART_fast      100 MHz fast UART mode
    -- * HSI            100 MHz core clock with HSI clock multiplied by HSI_DIV
--     OPERATIONAL_MODE            : operationalMode_type := UART_fast;
    -- TODO this should be dependent on operational mode only...
--     HSI_DIV                     : natural := 4;
    
    -- The file that we are going to load for simulation.
    SIMULATION_FILE             : string := "../test-progs/mem.srec";
    
    -- Clock speed, should usually be 5 ns (100 MHz)
    T_CLK                       : time := 5 ns
  );
end testbench;

architecture Behavioral of testbench is

  constant N_RX                 : natural := 4;
  constant N_TX                 : natural := 4;
  constant N_CK                 : natural := 1;
  
  -- 100 MHz "AHB" clock and reset.
  signal clk                    : std_logic := '1';
  signal clk_en                 : std_logic := '1';
  signal core_busy              : std_logic := '1';
  signal reset                  : std_logic := '1';
  
  -- Bus connected to the memory model.
  signal core2mem               : bus_mst2slv_type;
  signal mem2core               : bus_slv2mst_type;
  
  -- Stimulus control signals.
  signal dbg2core               : bus_mst2slv_type;
  signal core2dbg               : bus_slv2mst_type;
  signal irq                    : std_logic_vector(15 downto 0);
  signal done                   : std_logic_vector(3 downto 0);
  signal idle                   : std_logic_vector(3 downto 0);
  
  -- FPGA side of the interconnect signals.
  signal fpga_aresetn_core      : std_logic;
  signal fpga_aresetn_phy       : std_logic;
  signal fpga_backup_clk        : std_logic;
  signal fpga_hsi_clk_p         : std_logic;
  signal fpga_hsi_clk_n         : std_logic;
  signal fpga_f2a_p             : std_logic_vector(N_RX-1 downto 0);
  signal fpga_f2a_n             : std_logic_vector(N_RX-1 downto 0);
  signal fpga_a2f_p             : std_logic_vector(N_TX-1 downto 0);
  signal fpga_a2f_n             : std_logic_vector(N_TX-1 downto 0);
  signal fpga_mode              : std_logic;
  signal fpga_ctrl_f2a          : std_logic;
  signal fpga_ctrl_a2f          : std_logic;
  
  -- Interconnect delay.
  constant INTERCON_DELAY       : time := 1 ns;
  
  -- ASIC side of the interconnect signals.
  signal asic_aresetn_core      : std_logic;
  signal asic_aresetn_phy       : std_logic;
  signal asic_backup_clk        : std_logic;
  signal asic_hsi_clk_p         : std_logic_vector(N_CK-1 downto 0);
  signal asic_hsi_clk_n         : std_logic_vector(N_CK-1 downto 0);
  signal asic_f2a_p             : std_logic_vector(N_RX-1 downto 0);
  signal asic_f2a_n             : std_logic_vector(N_RX-1 downto 0);
  signal asic_a2f_p             : std_logic_vector(N_TX-1 downto 0);
  signal asic_a2f_n             : std_logic_vector(N_TX-1 downto 0);
  signal asic_mode              : std_logic;
  signal asic_ctrl_f2a          : std_logic;
  signal asic_ctrl_a2f          : std_logic;
  
  
--=============================================================================
begin -- architecture
--=============================================================================
  
  -----------------------------------------------------------------------------
  -- Generate clock, reset, and debug bus requests to start up the ASIC
  -----------------------------------------------------------------------------
  process (idle, clk, fpga_aresetn_core)
    variable idle_count         : integer;
  begin
    if (unsigned(idle) /= (2**idle'length - 1)) or fpga_aresetn_core = '0' then
      core_busy <= '1';
      idle_count := 0;
    elsif core_busy = '1' then
      -- during reconfiguration core is marked as idle as well (for about 4
      -- cycles). Check for some idle cycles before ending testbench.
      if rising_edge(clk) then
        idle_count := idle_count + 1;
      end if;
      if idle_count > 10 then
        core_busy <= '0';
      end if;
    end if;
  end process;
  clk <= not clk after T_CLK
         when clk_en = '1'
         else '0';
  
  boot_proc: process is
    
    procedure w(
      addr  : in  rvex_address_type;
      data  : in  rvex_data_type
    ) is
    begin
      wait until rising_edge(clk);
      dbg2core.address <= addr;
      dbg2core.writeData <= data;
      dbg2core.writeMask <= "1111";
      dbg2core.writeEnable <= '1';
      wait until rising_edge(clk);
      if core2dbg.ack /= '1' then
        wait until core2dbg.ack = '1';
      end if;
      dbg2core.writeEnable <= '0';
      wait until rising_edge(clk);
      dbg2core <= BUS_MST2SLV_IDLE;
    end w;
    
    procedure r(
      addr  : in  rvex_address_type;
      data  : out rvex_data_type
    ) is
    begin
      wait until rising_edge(clk);
      dbg2core.address <= addr;
      dbg2core.readEnable <= '1';
      wait until rising_edge(clk);
      if core2dbg.ack /= '1' then
        wait until core2dbg.ack = '1';
      end if;
      dbg2core.readEnable <= '0';
      data := core2dbg.readData;
      wait until rising_edge(clk);
      dbg2core <= BUS_MST2SLV_IDLE;
    end r;
    
    variable d  : rvex_data_type;
    
  begin
    dbg2core <= BUS_MST2SLV_IDLE;
    irq <= (others => '0');
    clk_en <= '1';
    reset <= '1';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    reset <= '0';
    
    -- HSI 2:1 clock.
    report "Testing 2:1 HSI mode..." severity NOTE;
    wait for 1 us;
    w(X"01000000", "00100010" & X"000000"); -- Reset in 2:1 HSI mode
    
    wait until core_busy = '0';
    wait for 10 us;
    
    -- HSI 4:1 clock.
    report "Testing 4:1 HSI mode..." severity NOTE;
    wait for 1 us;
    w(X"01000000", "00100011" & X"000000"); -- Reset in 4:1 HSI mode
    
    wait until core_busy = '0';
    wait for 10 us;
    
    -- Fast UART mode.
    report "Testing fast UART mode..." severity NOTE;
    wait for 1 us;
    w(X"01000000", "00100001" & X"000000"); -- Reset in fast UART mode
    wait for 1 us;
    w(X"01000000", "011" & "00000" & X"000000"); -- Start the core (only needed in UART mode)
    
    wait until core_busy = '0';
    wait for 10 us;
    
    -- Reliable UART mode.
    report "Testing reliable UART mode..." severity NOTE;
    wait for 1 us;
    w(X"01000000", "00100000" & X"000000"); -- Reset in reliable UART mode
    wait for 1 us;
    w(X"01000000", "011" & "00000" & X"000000"); -- Start the core (only needed in UART mode)
    
    wait until core_busy = '0';
    wait for 10 us;
    
    clk_en <= '0';
    wait;
  end process;
  
  -----------------------------------------------------------------------------
  -- Model the memory accessed by the cache
  -----------------------------------------------------------------------------
  mem_model: process is
    variable mem      : rvmem_memoryState_type;
    variable readData : rvex_data_type;
    variable l        : std.textio.line;
    variable c        : character;
  begin
    
    -- Load the srec file into the memory.
    rvmem_clear(mem, '0');
    rvmem_loadSRec(mem, SIMULATION_FILE);
    
    -- Initialize the bus output.
    mem2core <= BUS_SLV2MST_IDLE;
    
    -- Handle memory requests.
    loop
      
      -- Wait for the next clock.
      wait until rising_edge(clk);
      
      -- If we have a request, delay for a random amount of cycles.
      if core2mem.readEnable = '1' or core2mem.writeEnable = '1' then
        mem2core <= BUS_SLV2MST_IDLE;
        mem2core.busy <= '1';
        
        -- (TODO: this is not random)
        wait until rising_edge(clk);
        
      end if;
      
      -- Handle the bus request.
      mem2core <= BUS_SLV2MST_IDLE;
      if core2mem.readEnable = '1' then
        rvmem_read(mem, core2mem.address, readData);
        mem2core.readData <= readData;
        mem2core.ack <= '1';
      elsif core2mem.writeEnable = '1' then
        rvmem_write(mem, core2mem.address, core2mem.writeData, core2mem.writeMask);
        mem2core.ack <= '1';
        
        -- Handle application debug output.
        if core2mem.address = X"DEB00000" then
          c := character'val(to_integer(unsigned(core2mem.writeData(31 downto 24))));
          if c = LF then
            writeline(std.textio.output, l);
          else
            write(l, c);
          end if;
        end if;
        
      end if;
      
      -- Force a bus fault for addresses starting with 0xFF in order to test
      -- cache behavior in such a case.
      if core2mem.address(31 downto 24) = X"FF" then
        mem2core.readData <= X"01234567";
        mem2core.fault <= '1';
      else
        mem2core.fault <= '0';
      end if;
      
    end loop;
    
  end process;
  
  -----------------------------------------------------------------------------
  -- Instantiate the FPGA side of the HSI
  -----------------------------------------------------------------------------
  hsi_fpga_inst: entity work.hsi_fpga
    generic map (
      SCAN_LENGTH               => 256,
      N_RX                      => N_RX,
      N_TX                      => N_TX,
      HSI_DIV                   => 2 -- FIXME: ?????
    )
    port map (
      reset                     => reset,
      clk_100MHz                => clk,
      clk_100MHz_en             => clk_en,
      core2mem                  => core2mem,
      mem2core                  => mem2core,
      dbg2core                  => dbg2core,
      core2dbg                  => core2dbg,
      irq                       => irq,
      done                      => done,
      idle                      => idle,
      pad_aresetn_core          => fpga_aresetn_core,
      pad_aresetn_phy           => fpga_aresetn_phy,
      pad_backup_clk            => fpga_backup_clk,
      pad_hsi_clk_p             => fpga_hsi_clk_p,
      pad_hsi_clk_n             => fpga_hsi_clk_n,
      pad_f2a_p                 => fpga_f2a_p,
      pad_f2a_n                 => fpga_f2a_n,
      pad_a2f_p                 => fpga_a2f_p,
      pad_a2f_n                 => fpga_a2f_n,
      pad_mode                  => fpga_mode,
      pad_ctrl_f2a              => fpga_ctrl_f2a,
      pad_ctrl_a2f              => fpga_ctrl_a2f
    );
  
  -----------------------------------------------------------------------------
  -- Model the interconnections
  -----------------------------------------------------------------------------
  asic_aresetn_core <= transport fpga_aresetn_core  after INTERCON_DELAY;
  asic_aresetn_phy  <= transport fpga_aresetn_phy   after INTERCON_DELAY;
  asic_backup_clk   <= transport fpga_backup_clk    after INTERCON_DELAY;
  gen_clk: for i in 0 to N_CK-1 generate
    asic_hsi_clk_p(i)   <= transport fpga_hsi_clk_p after INTERCON_DELAY;
    asic_hsi_clk_n(i)   <= transport fpga_hsi_clk_n after INTERCON_DELAY;
  end generate;
  asic_f2a_p        <= transport fpga_f2a_p         after INTERCON_DELAY;
  asic_f2a_n        <= transport fpga_f2a_n         after INTERCON_DELAY;
  fpga_a2f_p        <= transport asic_a2f_p         after INTERCON_DELAY;
  fpga_a2f_n        <= transport asic_a2f_n         after INTERCON_DELAY;
  asic_mode         <= transport fpga_mode          after INTERCON_DELAY;
  asic_ctrl_f2a     <= transport fpga_ctrl_f2a      after INTERCON_DELAY;
  fpga_ctrl_a2f     <= transport asic_ctrl_a2f      after INTERCON_DELAY;
  
  -----------------------------------------------------------------------------
  -- Instantiate the ASIC toplevel
  -----------------------------------------------------------------------------
--   gen_rvex: if DESIGN_STAGE = stage_rvex generate
    asic_inst: entity asic.rvsys_asic
      generic map (
        CACHE_IMPL                => CACHE_IMPL_MEM,
        N_RX                      => N_RX,
        N_TX                      => N_TX,
        N_CK                      => N_CK
      )
      port map (
        pad_aresetn_core          => asic_aresetn_core,
        pad_aresetn_phy           => asic_aresetn_phy,
        pad_backup_clk            => asic_backup_clk,
        pad_Iref_tx               => (others => 'Z'),
        pad_Iref_rx               => open,
        pad_hsi_clk_p             => asic_hsi_clk_p,
        pad_hsi_clk_n             => asic_hsi_clk_n,
        pad_f2a_p                 => asic_f2a_p,
        pad_f2a_n                 => asic_f2a_n,
        pad_a2f_p                 => asic_a2f_p,
        pad_a2f_n                 => asic_a2f_n,
        pad_mode                  => asic_mode,
        pad_ctrl_f2a              => asic_ctrl_f2a,
        pad_ctrl_a2f              => asic_ctrl_a2f
      );
--   end generate;
  
--   gen_asic: if DESIGN_stage = stage_asic generate
--     asic_inst: entity asic.rvsys_asic
--       generic map (
--         CACHE_IMPL                => CACHE_IMPL_ASIC,
--         N_RX                      => N_RX,
--         N_TX                      => N_TX,
--         N_CK                      => N_CK
--       )
--       port map (
--         pad_aresetn_core          => asic_aresetn_core,
--         pad_aresetn_phy           => asic_aresetn_phy,
--         pad_backup_clk            => asic_backup_clk,
--         pad_Iref_tx               => (others => 'Z'),
--         pad_Iref_rx               => open,
--         pad_hsi_clk_p             => asic_hsi_clk_p,
--         pad_hsi_clk_n             => asic_hsi_clk_n,
--         pad_f2a_p                 => asic_f2a_p,
--         pad_f2a_n                 => asic_f2a_n,
--         pad_a2f_p                 => asic_a2f_p,
--         pad_a2f_n                 => asic_a2f_n,
--         pad_mode                  => asic_mode,
--         pad_ctrl_f2a              => asic_ctrl_f2a,
--         pad_ctrl_a2f              => asic_ctrl_a2f
--       );
--   end generate;
  
--   gen_syn: if DESIGN_STAGE = stage_syn generate
--     asic_inst: entity syn.rvsys_asic
--       port map (
--         pad_aresetn_core          => asic_aresetn_core,
--         pad_aresetn_phy           => asic_aresetn_phy,
--         pad_backup_clk            => asic_backup_clk,
--         pad_Iref_tx               => (others => 'Z'),
--         pad_Iref_rx               => open,
--         pad_hsi_clk_p             => asic_hsi_clk_p,
--         pad_hsi_clk_n             => asic_hsi_clk_n,
--         pad_f2a_p                 => asic_f2a_p,
--         pad_f2a_n                 => asic_f2a_n,
--         pad_a2f_p                 => asic_a2f_p,
--         pad_a2f_n                 => asic_a2f_n,
--         pad_mode                  => asic_mode,
--         pad_ctrl_f2a              => asic_ctrl_f2a,
--         pad_ctrl_a2f              => asic_ctrl_a2f
--       );
--   end generate;
  
--   gen_par: if DESIGN_STAGE = stage_par generate
--     asic_inst: entity par.rvsys_asic
--       port map (
--         pad_aresetn_core          => asic_aresetn_core,
--         pad_aresetn_phy           => asic_aresetn_phy,
--         pad_backup_clk            => asic_backup_clk,
--         pad_Iref_tx               => (others => 'Z'),
--         pad_Iref_rx               => open,
--         pad_hsi_clk_p             => asic_hsi_clk_p,
--         pad_hsi_clk_n             => asic_hsi_clk_n,
--         pad_f2a_p                 => asic_f2a_p,
--         pad_f2a_n                 => asic_f2a_n,
--         pad_a2f_p                 => asic_a2f_p,
--         pad_a2f_n                 => asic_a2f_n,
--         pad_mode                  => asic_mode,
--         pad_ctrl_f2a              => asic_ctrl_f2a,
--         pad_ctrl_a2f              => asic_ctrl_a2f
--       );
--   end generate;
  
end Behavioral;

