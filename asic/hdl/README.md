
These files are a snapshot of the r-VEX files used for the ASIC. The only
modification are those that are needed to get it working in this modelsim
project, i.e. library names. The initial commit of these files are copied
straight from Lennart's final rvex-rewrite commit without modifications, so go
back to whatever commit that is if you need the originals.
