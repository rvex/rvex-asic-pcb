-- r-VEX processor
-- Copyright (C) 2008-2016 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2016 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library asic;
use asic.common_pkg.all;
use asic.core_pkg.all;
use asic.cache_pkg.all;

--=============================================================================
-- This entity infers the block RAMs which store the cache tags for an
-- instruction cache block and checks whether the tag matches the incoming
-- address.
-------------------------------------------------------------------------------
entity cache_instr_blockTag_asic is
--=============================================================================
  generic (
    
    -- Core configuration. Must be equal to the configuration presented to the
    -- rvex core connected to the cache.
    RCFG                        : rvex_generic_config_type := RVEX_CURRENT_CONFIG;
    
    -- Cache configuration.
    CCFG                        : cache_generic_config_type := CACHE_CURRENT_CONFIG
    
  );
  port (
    
    -- Clock input.
    clk                         : in  std_logic;
    
    -- Active high enable input for CPU signals.
    enableCPU                   : in  std_logic;
    
    -- Active high enable input for bus signals.
    enableBus                   : in  std_logic;
    
    -- CPU address/PC input.
    cpuAddr                     : in  rvex_address_type;
    
    -- Hit output for the CPU, delayed by one cycle with enable high due to
    -- the memory.
    cpuHit                      : out std_logic;
    
    -- Write enable signal to write the CPU tag to the memory.
    writeCpuTag                 : in  std_logic;
    
    -- Invalidate address input.
    invalAddr                   : in  rvex_address_type;
    
    -- Hit output for the invalidation logic, delayed by one cycle with
    -- enable high due to the memory.
    invalHit                    : out std_logic
    
  );
end cache_instr_blockTag_asic;

--=============================================================================
architecture Behavioral of cache_instr_blockTag_asic is
--=============================================================================
  
  -- Declare XST RAM extraction hints.
  attribute ram_extract         : string;
  attribute ram_style           : string;
  
  -- Load shorthand notations for the address vector metrics.
  constant OFFSET_LSB           : natural := icacheOffsetLSB(RCFG, CCFG);
  constant OFFSET_SIZE          : natural := icacheOffsetSize(RCFG, CCFG);
  constant TAG_LSB              : natural := icacheTagLSB(RCFG, CCFG);
  constant TAG_SIZE             : natural := icacheTagSize(RCFG, CCFG);
  
  -- Cache tag memory.
  type ram_tag_type
    is array(0 to 2**CCFG.instrCacheLinesLog2-1)
    of std_logic_vector(TAG_SIZE-1 downto 0);
  signal ram_tag                : ram_tag_type := (others => (others => 'X'));
  
  -- Hints for XST to implement the tag memory in block RAMs.
  attribute ram_extract of ram_tag  : signal is "yes";
  attribute ram_style   of ram_tag  : signal is "block";
  
  -- CPU address/PC signals.
  signal cpuOffset              : std_logic_vector(OFFSET_SIZE-1 downto 0);
  signal cpuTag                 : std_logic_vector(TAG_SIZE-1 downto 0);
  signal cpuTag_r               : std_logic_vector(TAG_SIZE-1 downto 0);
  signal cpuTag_mem             : std_logic_vector(TAG_SIZE-1 downto 0);
  
  -- Invalidate address/PC signals.
  signal invalOffset            : std_logic_vector(OFFSET_SIZE-1 downto 0);
  signal invalTag               : std_logic_vector(TAG_SIZE-1 downto 0);
  signal invalTag_r             : std_logic_vector(TAG_SIZE-1 downto 0);
  signal invalTag_mem           : std_logic_vector(TAG_SIZE-1 downto 0);
  
  -- Active low enable signals for Faraday memory
  signal WEAN                   : std_logic;
  signal CSAN                   : std_logic;
  signal CSBN                   : std_logic;
  
  component MEM_INVALID end component;
  
  component SJKA65_256X19X1CM4
    port(
         DOA                     :   OUT  std_logic_vector (18 downto 0); 
         DOB                     :   OUT  std_logic_vector (18 downto 0); 
         A                       :   IN   std_logic_vector (7 downto 0);
         B                       :   IN   std_logic_vector (7 downto 0);  
         DIA                     :   IN   std_logic_vector (18 downto 0);
         DIB                     :   IN   std_logic_vector (18 downto 0);
         WEAN                    :   IN   std_logic;
         WEBN                    :   IN   std_logic;
         DVSE                    :   IN   std_logic;
         DVS                     :   IN   std_logic_vector (3 downto 0);
         CKA                     :   IN   std_logic;
         CKB                     :   IN   std_logic;
         CSAN                    :   IN   std_logic;
         CSBN                    :   IN   std_logic
       );
  end component SJKA65_256X19X1CM4;
  
  component SJKA65_256X20X1CM4
    port(
         DOA                     :   OUT  std_logic_vector (19 downto 0); 
         DOB                     :   OUT  std_logic_vector (19 downto 0); 
         A                       :   IN   std_logic_vector (7 downto 0);
         B                       :   IN   std_logic_vector (7 downto 0);  
         DIA                     :   IN   std_logic_vector (19 downto 0);
         DIB                     :   IN   std_logic_vector (19 downto 0);
         WEAN                    :   IN   std_logic;
         WEBN                    :   IN   std_logic;
         DVSE                    :   IN   std_logic;
         DVS                     :   IN   std_logic_vector (3 downto 0);
         CKA                     :   IN   std_logic;
         CKB                     :   IN   std_logic;
         CSAN                    :   IN   std_logic;
         CSBN                    :   IN   std_logic
       );
  end component SJKA65_256X20X1CM4;
  
  component SJKA65_256X21X1CM4
    port(
         DOA                     :   OUT  std_logic_vector (20 downto 0); 
         DOB                     :   OUT  std_logic_vector (20 downto 0); 
         A                       :   IN   std_logic_vector (7 downto 0);
         B                       :   IN   std_logic_vector (7 downto 0);  
         DIA                     :   IN   std_logic_vector (20 downto 0);
         DIB                     :   IN   std_logic_vector (20 downto 0);
         WEAN                    :   IN   std_logic;
         WEBN                    :   IN   std_logic;
         DVSE                    :   IN   std_logic;
         DVS                     :   IN   std_logic_vector (3 downto 0);
         CKA                     :   IN   std_logic;
         CKB                     :   IN   std_logic;
         CSAN                    :   IN   std_logic;
         CSBN                    :   IN   std_logic
       );
  end component SJKA65_256X21X1CM4;
	
--=============================================================================
begin -- architecture
--=============================================================================
  
  -- Extract the offsets and tags from the CPU and invalidate addresses.
  cpuOffset   <= cpuAddr  (OFFSET_LSB + OFFSET_SIZE-1 downto OFFSET_LSB);
  cpuTag      <= cpuAddr  (TAG_LSB    + TAG_SIZE-1    downto TAG_LSB);
  invalOffset <= invalAddr(OFFSET_LSB + OFFSET_SIZE-1 downto OFFSET_LSB);
  invalTag    <= invalAddr(TAG_LSB    + TAG_SIZE-1    downto TAG_LSB);
  
  -- Register the CPU and invalidate tags for the tag comparator, to account
  -- for the delay in memory access.
  tag_registers: process (clk) is
  begin
    if rising_edge(clk) then
      if enableCPU = '1' then
        cpuTag_r <= cpuTag;
      end if;
      if enableBus = '1' then
        invalTag_r <= invalTag;
      end if;
    end if;
  end process;
  
  -- Faraday memories have active low enable
  WEAN <= not writeCpuTag;
  CSAN <= not enableCPU;
  CSBN <= (not enableBus) or writeCpuTag;
  
  
  -- Only a few implementations exist for now
  -- assert an error when required component is not available
  -- TODO: better compile time assert?
  compile_error: if (TAG_SIZE < 19 or TAG_SIZE > 20) or
                    (OFFSET_SIZE /= 8) generate
    mem_faraday: MEM_INVALID;
    assert true
    report "Invalid memory IP component requested" severity failure;
  end generate;
  
  
  -- Actually only valid for:
  --  OFFSET_SIZE = LOG2(256)
  --  TAG_SIZE    = 19
  mem_tag_19: if TAG_SIZE = 19 generate
    mem_faraday: SJKA65_256X19X1CM4
      port map (
        DOA  => cpuTag_mem,
        DOB  => invalTag_mem,
        A    => cpuOffset,
        B    => invalOffset,
        DIA  => cpuTag_r,
        DIB  => (others => '-'),
        WEAN => WEAN,
        WEBN => '1',
        DVSE => '0',
        DVS  => (others => '-'),
        CKA  => clk,
        CKB  => clk,
        CSAN => CSAN,
        CSBN => CSBN
      );
  end generate;
  
  -- Actually only valid for:
  --  OFFSET_SIZE = LOG2(256)
  --  TAG_SIZE    = 20
  mem_tag_20: if TAG_SIZE = 20 generate
    mem_faraday: SJKA65_256X20X1CM4
      port map (
        DOA  => cpuTag_mem,
        DOB  => invalTag_mem,
        A    => cpuOffset,
        B    => invalOffset,
        DIA  => cpuTag_r,
        DIB  => (others => '-'),
        WEAN => WEAN,
        WEBN => '1',
        DVSE => '0',
        DVS  => (others => '-'),
        CKA  => clk,
        CKB  => clk,
        CSAN => CSAN,
        CSBN => CSBN
      );
  end generate;
  
  -- Actually only valid for:
  --  OFFSET_SIZE = LOG2(256)
  --  TAG_SIZE    = 21
  mem_tag_21: if TAG_SIZE = 21 generate
    mem_faraday: SJKA65_256X21X1CM4
      port map (
        DOA  => cpuTag_mem,
        DOB  => invalTag_mem,
        A    => cpuOffset,
        B    => invalOffset,
        DIA  => cpuTag_r,
        DIB  => (others => '-'),
        WEAN => WEAN,
        WEBN => '1',
        DVSE => '0',
        DVS  => (others => '-'),
        CKA  => clk,
        CKB  => clk,
        CSAN => CSAN,
        CSBN => CSBN
      );
  end generate;
  
  -- Determine the hit outputs.
  cpuHit <= '1' when cpuTag_mem = cpuTag_r else '0';
  invalHit <= '1' when invalTag_mem = invalTag_r else '0';
  
end Behavioral;

