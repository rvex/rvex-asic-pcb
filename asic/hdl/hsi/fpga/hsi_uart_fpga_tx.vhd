library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hsi_uart_fpga_tx is
  port (
    
    -- System control.
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    clkEn_core                  : in  std_logic;
    
    -- Data outputs.
    uart_tx                     : out std_logic_vector(1 downto 0);
    
    -- Mode input: 0=reliable, 1=fast.
    uart_mode                   : in  std_logic;
    
    -- Setup edge selection. 0=rising, 1=falling.
    uart_setup_edge             : in  std_logic_vector(1 downto 0);
    
    -- Word stream input.
    tl2dll_data                 : in  std_logic_vector(31 downto 0);
    tl2dll_first                : in  std_logic;
    tl2dll_valid                : in  std_logic;
    dll2tl_ack                  : out std_logic
    
  );
end hsi_uart_fpga_tx;

architecture Behavioral of hsi_uart_fpga_tx is
  
  -- Counter for divide-by-7 clock.
  signal bit_clk_div            : unsigned(2 downto 0);
  
  -- Clock enable signal for the UART shift register.
  signal bit_clk_en             : std_logic;
  
  -- Data shift registers.
  signal shift_reg_a            : std_logic_vector(16 downto 0);
  signal shift_reg_b            : std_logic_vector(16 downto 0);
  
  -- Falling-edge output registers.
  signal falling_a              : std_logic;
  signal falling_b              : std_logic;
  
  -- Number of bits remaining before we can start the next packet. If this is
  -- at 1 we can start the next frame if it belongs to the same packet.
  signal bit_counter            : unsigned(4 downto 0);
  
  -- Strobe signal which initiates a new transfer.
  signal start                  : std_logic;
  
begin
  
  -- Generate the clock enable signal for the bit clock. This is always high
  -- when the UART is in fast mode, and high every 7th cycle when the UART is
  -- in reliable mode.
  bit_clock_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' then
        bit_clk_div <= "000";
      elsif clkEn_core = '1' then
        if bit_clk_div = "000" then
          if uart_mode = '0' then
            bit_clk_div <= "110";
          else
            bit_clk_div <= "000";
          end if;
        else
          bit_clk_div <= bit_clk_div - 1;
        end if;
      end if;
    end if;
  end process;
  bit_clk_en <= '1' when bit_clk_div = "000" else '0';
  
  -- Generate the data shift registers and bit counter.
  shift_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' then
        bit_counter <= "00000";
        shift_reg_a <= (others => '1');
        shift_reg_b <= (others => '1');
      elsif clkEn_core = '1' then
        if bit_clk_en = '1' then
          if start = '1' then
            
            -- Load the shift registers with the new data word.
            shift_reg_a(16) <= '0';
            shift_reg_a(15 downto 0) <= tl2dll_data(31 downto 16);
            shift_reg_b(16) <= '1';
            shift_reg_b(15 downto 0) <= tl2dll_data(15 downto 0);
            
            -- Load the bit counter.
            bit_counter <= "10010";
            
          else
            
            -- Shift the shift registers.
            shift_reg_a <= shift_reg_a(15 downto 0) & "1";
            shift_reg_b <= shift_reg_b(15 downto 0) & "1";
            
            -- Decrement the bit counter if it is nonzero.
            if bit_counter /= "00000" then
              bit_counter <= bit_counter - 1;
            end if;
            
          end if;
        end if;
      end if;
    end if;
  end process;
  
  -- Generate the falling-edge holding registers.
  falling_proc: process (clk_core) is
  begin
    if falling_edge(clk_core) then
      falling_a <= shift_reg_a(16);
      falling_b <= shift_reg_b(16);
    end if;
  end process;
  
  -- Connect the UART output to the shift register.
  uart_tx(0) <= falling_a when uart_setup_edge(0) = '1' else shift_reg_a(16);
  uart_tx(1) <= falling_b when uart_setup_edge(1) = '1' else shift_reg_b(16);
  
  -- Determine if we're ready to start a new transmission and acknowledge an
  -- incoming transfer request.
  start_proc: process (
    tl2dll_first, tl2dll_valid, bit_counter, bit_clk_en
  ) is
  begin
    start <= '0';
    dll2tl_ack <= '0';
    
    -- If we have data pending...
    if tl2dll_valid = '1' then
      
      -- And we're not sending anything anymore...
      if bit_counter(4 downto 1) = "0000" then
        
        -- And the next word is still part of the same packet or we've had at
        -- least one idle cycle since the previous word...
        if tl2dll_first = '0' or bit_counter(0) = '0' then
          
          -- Then get ready to load the new data word into the shift
          -- registers...
          start <= '1';
          
          -- And acknowledge the new word if bit_clk_en is set.
          dll2tl_ack <= bit_clk_en;
          
        end if;
      end if;
    end if;
  end process;
  
end Behavioral;

