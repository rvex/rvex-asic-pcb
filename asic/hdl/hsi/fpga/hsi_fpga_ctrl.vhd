library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library asic;
use asic.bus_pkg.all;

entity hsi_fpga_ctrl is
  generic (
    
    -- Scan chain length.
    SCAN_LENGTH                 : natural := 256;
    
    -- Scan clock half-period (for mode and backup_clk) in clk cycles minus
    -- one.
    SCAN_PRESCALE               : natural := 2
    
  );
  port (
    
    -- Our clock and reset. The clock is connected to the core_clk output of
    -- the MMCM, reset is controlled by the FPGA master reset and MMCM lock
    -- signals. When we are under reset, the ASIC will also be reset.
    clk                         : in  std_logic;
    reset                       : in  std_logic;
    
    -- Command interface.
    --
    -- Write to 0x0000:
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |0|0|1|C|B|A|I|R| | | | | | | | || | | | | | | | | | | | | | | | | Reset
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is stopped. Resets ASIC using ifsel=I and
    --   ratio=R. Sample UART on edge A, setup UART on edge B (ctrl f2a) and
    --   C (aresetn_phy). Returns the ASIC with the clock stopped.
    --   
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |0|1|0| | | | | | | | | | | | | || | | | | | | | | | | | | | | | | Pause
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is running. Stops the ASIC clock.
    --   
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |0|1|1| | | | | | | | | | | | | || | | | | | | | | | | | | | | | | Continue
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is stopped. Starts the ASIC clock.
    --   
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |1|0|0| | | | | | | |                 Step count                 | Step
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is stopped. Sends <step count> clock cycles.
    --   
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |1|0|1| | | | | | |E|0|0|0|0|0|0||0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0| Wait
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is stopped, and should only be done after
    --   writing to chain 0x80 to select event. Sends clock cycles until the
    --   event flag equals E. Can be aborted by writing this command again
    --   with E in the other state so it matches what the ASIC is outputting.
    --   
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |1|1|0| | | | | | | | | | | | | || | | | | | | | |     Chain     | Read
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is stopped. Reads the given scan chain.
    --   
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   |1|1|1| | | | | | | | | | | | | || | | | | | | | |     Chain     | Read/write
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Only allowed when clock is stopped. Read/modify/writes the given scan
    --   chain.
    -- 
    -- Read from 0x0000:
    --   |-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-||-.-.-.-|-.-.-.-|-.-.-.-|-.-.-.-|
    --   | Cmd |C|B|A|I|R|K|E|                 Step count                 | Status
    --   '-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-''-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'-'
    --   Cmd: the code for the currently running command, or 000 if idle.
    --   C: falling edge setup for aresetn_phy in UART mode
    --   B: falling edge setup for ctrl_f2a in UART mode
    --   A: falling edge sample for ctrl_a2f in UART mode
    --   I: ifsel; 1=HSI, 0=UART
    --   R: ratio; 1=1:2/fast UART, 0=1:1/reliable UART
    --   K: whether the ASIC clock is currently running
    --   E: event flag state to wait for
    --   Step count: steps remaining if stepping, steps done while waiting for
    --     event during or after wait command, undefined otherwise
    bus2fsm                     : in  bus_mst2slv_type;
    fsm2bus                     : out bus_slv2mst_type;
    
    -- ASIC interface pins.
    aresetn_phy                 : out std_logic;
    aresetn_core                : out std_logic;
    mode                        : out std_logic;
    ctrl_a2f                    : in  std_logic;
    ctrl_f2a                    : out std_logic;
    
    -- Clock enable signals.
    diff_clk_ena                : out std_logic;
    backup_clk_ena              : out std_logic;
    backup_clk_high             : out std_logic;
    
    -- Interface modes.
    ifsel                       : out std_logic;
    ratio                       : out std_logic;
    uart_sample_edge            : out std_logic;
    uart_setup_edge             : out std_logic_vector(1 downto 0);
    
    -- HSI calibration signals for the FPGA side.
    calib                       : out std_logic;
    lock                        : in  std_logic;
    
    -- Signals to UART receiver and transmiter.
    uart_rx                     : out std_logic;
    uart_tx                     : in  std_logic_vector(1 downto 0)
    
  );
end hsi_fpga_ctrl;

architecture Behavioral of hsi_fpga_ctrl is
  
  -- State type.
  type state_t is (
    S_RESET,       -- reset_core asserted, waiting for command
    S_RESET_1,     -- mode set to ifsel
    S_RESET_2,     -- reset phy asserted
    S_RESET_3,     -- mode set to ratio
    S_RESET_4,     -- reset phy released
    S_RESET_5,     -- mode set low
    S_CALIB_1,     -- clk enabled
    S_CALIB_2,     -- calib asserted
    S_CALIB_3,     -- lock asserted
    S_CALIB_4,     -- calibrated, core still being reset
    S_PAUSED,      -- clock stopped, waiting for command
    s_RUN,         -- clock enabled, waiting for command
    S_STEP,        -- clock enabled, counting down cycles
    S_SCAN_SEL_1,  -- ctrl_f2a = bit 7, mode low
    S_SCAN_SEL_2,  -- ctrl_f2a = bit 7, mode high
    S_SCAN_SEL_3,  -- ctrl_f2a = bit 6, mode low
    S_SCAN_SEL_4,  -- ctrl_f2a = bit 6, mode high
    S_SCAN_SEL_5,  -- ctrl_f2a = bit 5, mode low
    S_SCAN_SEL_6,  -- ctrl_f2a = bit 5, mode high
    S_SCAN_SEL_7,  -- ctrl_f2a = bit 4, mode low
    S_SCAN_SEL_8,  -- ctrl_f2a = bit 4, mode high
    S_SCAN_SEL_9,  -- ctrl_f2a = bit 3, mode low
    S_SCAN_SEL_10, -- ctrl_f2a = bit 3, mode high
    S_SCAN_SEL_11, -- ctrl_f2a = bit 2, mode low
    S_SCAN_SEL_12, -- ctrl_f2a = bit 2, mode high
    S_SCAN_SEL_13, -- ctrl_f2a = bit 1, mode low
    S_SCAN_SEL_14, -- ctrl_f2a = bit 1, mode high
    S_SCAN_SEL_15, -- ctrl_f2a = bit 0, mode low
    S_SCAN_SEL_16, -- ctrl_f2a = bit 0, mode high, load scan length
    S_SCAN_LOW,    -- mode high, backup_clk low
    S_SCAN_HIGH,   -- mode high, backup_clk high, decrement
    S_WAIT_1,      -- mode low, backup_clk low, mode low, increment
    S_WAIT_2,      -- mode low, backup_clk high, mode low
    S_WAIT_3,      -- mode low, backup_clk low, mode low
    S_WAIT_4,      -- mode low, backup_clk low, mode low, override ctrl_f2a low
    S_WAIT_5       -- mode low, backup_clk low, mode high, override ctrl_f2a low
  );
  
  -- Control registers.
  constant COUNTER_WIDTH        : natural := 22;
  signal ctrl_cmd               : std_logic_vector(2 downto 0);
  signal ctrl_c                 : std_logic;
  signal ctrl_b                 : std_logic;
  signal ctrl_a                 : std_logic;
  signal ctrl_i                 : std_logic;
  signal ctrl_r                 : std_logic;
  signal ctrl_e                 : std_logic;
  signal ctrl_count             : std_logic_vector(COUNTER_WIDTH-1 downto 0);
  
  -- FSM register outputs.
  constant TIMER_WIDTH          : natural := 8;
  signal state                  : state_t;
  signal timer                  : unsigned(TIMER_WIDTH-1 downto 0);
  
  -- FSM combinatorial outputs.
  signal clk_ena                : std_logic;
  signal clk_high               : std_logic;
  signal reset_phy              : std_logic;
  signal reset_core             : std_logic;
  signal mode_s                 : std_logic;
  signal calib_s                : std_logic;
  signal ctrl_over_val          : std_logic;
  signal ctrl_over              : std_logic;
  
  -- Scan logic.
  signal scan_out               : std_logic;
  signal scan_in                : std_logic;
  signal scan_wdata             : std_logic;
  signal scan_wen               : std_logic;
  
begin
  
  -- Assign the combinatorial output signals.
  aresetn_phy        <= (not reset_phy) when reset_core = '1'
                   else uart_tx(1);
  aresetn_core       <= not reset_core
                    and not reset;
  mode               <= mode_s;
  ctrl_f2a           <= ctrl_over_val when ctrl_over = '1'
                   else scan_in when mode_s = '1'
                   else calib_s when ctrl_i = '1'
                   else uart_tx(0);
  diff_clk_ena       <= clk_ena 
                    and ctrl_i
                    and not reset;
  backup_clk_ena     <= ((clk_ena and not ctrl_i) or clk_high)
                    and not reset;
  backup_clk_high    <= clk_high;
  ifsel              <= ctrl_i;
  ratio              <= ctrl_r;
  uart_sample_edge   <= ctrl_a;
  uart_setup_edge(0) <= ctrl_b;
  uart_setup_edge(1) <= ctrl_c;
  calib              <= calib_s;
  uart_rx            <= ctrl_a2f;
  
  -- Control register/bus interface.
  reg_proc: process(clk) is
    variable scan_en : boolean;
  begin
    if rising_edge(clk) then
      if reset = '1' then
        
        -- Reset all the control registers.
        ctrl_cmd <= "000";
        ctrl_c <= '0';
        ctrl_b <= '0';
        ctrl_a <= '0';
        ctrl_i <= '0';
        ctrl_r <= '0';
        ctrl_e <= '0';
        ctrl_count <= (others => '0');
        
        -- Reset state machine.
        state <= S_RESET;
        timer <= "00000000";
        
        -- Reset control signal registers.
        clk_ena       <= '0';
        clk_high      <= '0';
        reset_phy     <= '0';
        reset_core    <= '1';
        mode_s        <= '0';
        calib_s       <= '0';
        ctrl_over     <= '0';
        ctrl_over_val <= '0';
        
        -- Scan signals.
        scan_out      <= '0';
        
      else
        
        -----------------------------------------------------------------------
        -- Handle state machine
        -----------------------------------------------------------------------
        scan_en := false;
        if timer /= to_unsigned(0, TIMER_WIDTH) then
          timer <= timer - 1;
        else
          case state is
            when S_RESET => -- reset_core asserted, waiting for command
              case ctrl_cmd is
                when "001" => -- reset
                  mode_s <= ctrl_i;
                  timer <= to_unsigned(10, TIMER_WIDTH);
                  state <= S_RESET_1;
                when others => -- illegal/unknown
                  ctrl_cmd <= "000";
              end case;
            
            when S_RESET_1 => -- mode set to ifsel
              reset_phy <= '1';
              timer <= to_unsigned(10, TIMER_WIDTH);
              state <= S_RESET_2;
              
            when S_RESET_2 => -- reset phy asserted
              mode_s <= ctrl_r;
              timer <= to_unsigned(10, TIMER_WIDTH);
              state <= S_RESET_3;
              
            when S_RESET_3 => -- mode set to ratio
              reset_phy <= '0';
              timer <= to_unsigned(10, TIMER_WIDTH);
              state <= S_RESET_4;
              
            when S_RESET_4 => -- reset phy released
              mode_s <= '0';
              timer <= to_unsigned(10, TIMER_WIDTH);
              state <= S_RESET_5;
              
            when S_RESET_5 => -- mode set low
              if ctrl_i = '1' then
                clk_ena <= '1';
                timer <= to_unsigned(10, TIMER_WIDTH);
                state <= S_CALIB_1;
              else
                reset_core <= '0';
                ctrl_cmd <= "000";
                state <= S_PAUSED;
              end if;
              
            when S_CALIB_1 => -- clk enabled
              calib_s <= '1';
              timer <= to_unsigned(10, TIMER_WIDTH);
              state <= S_CALIB_2;
              
            when S_CALIB_2 => -- calib asserted
              if lock = '1' and ctrl_a2f = '1' then
                timer <= to_unsigned(10, TIMER_WIDTH);
                state <= S_CALIB_3;
              end if;
              
            when S_CALIB_3 => -- lock asserted
              calib_s <= '0';
              timer <= to_unsigned(10, TIMER_WIDTH);
              state <= S_CALIB_4;
            
            when S_CALIB_4 => -- calibrated, core still being reset
              reset_core <= '0';
              ctrl_cmd <= "000";
              state <= S_RUN;
            
            when S_PAUSED => -- clock stopped, waiting for command
              case ctrl_cmd is
                when "001" => -- reset
                  clk_ena       <= '0';
                  clk_high      <= '0';
                  reset_phy     <= '0';
                  reset_core    <= '1';
                  mode_s        <= '0';
                  calib_s       <= '0';
                  ctrl_over     <= '0';
                  ctrl_over_val <= '0';
                  timer         <= to_unsigned(10, TIMER_WIDTH);
                  state         <= S_RESET;
                
                when "011" => -- continue
                  clk_ena <= '1';
                  state <= S_RUN;
                
                when "100" => -- step
                  clk_ena <= '1';
                  ctrl_count <= std_logic_vector(unsigned(ctrl_count) - 1);
                  state <= S_STEP;
                
                when "101" => -- wait
                  timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
                  state         <= S_WAIT_1;
                
                when "110" | "111" => -- scan
                  ctrl_over     <= '1';
                  ctrl_over_val <= ctrl_count(7);
                  timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
                  state         <= S_SCAN_SEL_1;
                
                when others => -- illegal/unknown
                  ctrl_cmd <= "000";
                
              end case;
              
            when s_RUN => -- clock enabled, waiting for command
              case ctrl_cmd is
                when "001" => -- reset
                  clk_ena       <= '0';
                  clk_high      <= '0';
                  reset_phy     <= '0';
                  reset_core    <= '1';
                  mode_s        <= '0';
                  calib_s       <= '0';
                  ctrl_over     <= '0';
                  ctrl_over_val <= '0';
                  timer         <= to_unsigned(10, TIMER_WIDTH);
                  state         <= S_RESET;
                  
                when "010" => -- pause
                  clk_ena <= '0';
                  state <= S_PAUSED;
                
                when others => -- illegal/unknown
                  ctrl_cmd <= "000";
                
              end case;
              
            when S_STEP => -- clock enabled, counting down cycles
              if ctrl_count = (COUNTER_WIDTH-1 downto 0 => '0') then
                ctrl_cmd <= "000";
                clk_ena <= '0';
                state <= S_PAUSED;
              else
                ctrl_count <= std_logic_vector(unsigned(ctrl_count) - 1);
              end if;
            
            when S_SCAN_SEL_1 => -- ctrl_f2a = bit 7, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_2;
              
            when S_SCAN_SEL_2 => -- ctrl_f2a = bit 7, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(6);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_3;
            
            when S_SCAN_SEL_3 => -- ctrl_f2a = bit 6, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_4;
              
            when S_SCAN_SEL_4 => -- ctrl_f2a = bit 6, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(5);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_5;
              
            when S_SCAN_SEL_5 => -- ctrl_f2a = bit 5, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_6;
              
            when S_SCAN_SEL_6 => -- ctrl_f2a = bit 5, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(4);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_7;
              
            when S_SCAN_SEL_7 => -- ctrl_f2a = bit 4, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_8;
              
            when S_SCAN_SEL_8 => -- ctrl_f2a = bit 4, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(3);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_9;
              
            when S_SCAN_SEL_9 => -- ctrl_f2a = bit 3, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_10;
              
            when S_SCAN_SEL_10 => -- ctrl_f2a = bit 3, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(2);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_11;
              
            when S_SCAN_SEL_11 => -- ctrl_f2a = bit 2, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_12;
              
            when S_SCAN_SEL_12 => -- ctrl_f2a = bit 2, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(1);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_13;
              
            when S_SCAN_SEL_13 => -- ctrl_f2a = bit 1, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_14;
              
            when S_SCAN_SEL_14 => -- ctrl_f2a = bit 1, mode high
              mode_s        <= '0';
              ctrl_over_val <= ctrl_count(0);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_15;
              
            when S_SCAN_SEL_15 => -- ctrl_f2a = bit 0, mode low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_SEL_2;
              
            when S_SCAN_SEL_16 => -- ctrl_f2a = bit 0, mode high, load scan length
              ctrl_over     <= '0';
              ctrl_count    <= std_logic_vector(to_unsigned(SCAN_LENGTH, COUNTER_WIDTH));
              scan_en       := true;
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_LOW;
              
            when S_SCAN_LOW => -- mode high, backup_clk low
              clk_high      <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_SCAN_HIGH;
              
            when S_SCAN_HIGH => -- mode high, backup_clk high, decrement
              clk_high      <= '0';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              if ctrl_count = (COUNTER_WIDTH-1 downto 0 => '0') then
                mode_s      <= '0';
                ctrl_cmd    <= "000";
                state       <= S_PAUSED;
              else
                ctrl_count  <= std_logic_vector(unsigned(ctrl_count) - 1);
                state       <= S_SCAN_LOW;
                scan_en     := true;
              end if;
              
            when S_WAIT_1 => -- mode low, backup_clk low, mode low, increment
              clk_high      <= '1';
              ctrl_count    <= std_logic_vector(unsigned(ctrl_count) + 1);
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_WAIT_2;
              
            when S_WAIT_2 => -- mode low, backup_clk high, mode low
              clk_high      <= '0';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_WAIT_3;
              
            when S_WAIT_3 => -- mode low, backup_clk low, mode low
              ctrl_over     <= '1';
              ctrl_over_val <= '0';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_WAIT_4;
              
            when S_WAIT_4 => -- mode low, backup_clk low, mode low, override ctrl_f2a low
              mode_s        <= '1';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              state         <= S_WAIT_5;
              
            when S_WAIT_5 => -- mode low, backup_clk low, mode high, override ctrl_f2a low
              mode_s        <= '0';
              ctrl_over     <= '0';
              timer         <= to_unsigned(SCAN_PRESCALE, TIMER_WIDTH);
              if ctrl_a2f = ctrl_e then
                state       <= S_WAIT_1;
              else
                ctrl_cmd    <= "000";
                state       <= S_PAUSED;
              end if;
              
            when others =>
              state <= S_RESET;
            
          end case;
        end if;
        
        -----------------------------------------------------------------------
        -- Generate the scan setup and sample registers
        -----------------------------------------------------------------------
        if scan_en then
          scan_out <= ctrl_a2f;
          if ctrl_cmd(0) = '1' and scan_wen = '1' then
            scan_in <= scan_wdata;
          else
            scan_in <= ctrl_a2f;
          end if;
        end if;
        
        -----------------------------------------------------------------------
        -- Handle bus writes
        -----------------------------------------------------------------------
        if bus2fsm.writeEnable = '1' and bus2fsm.writeMask = "1111" then
          if ctrl_cmd /= "000" then
            
            -- A command is in progress. Ignore the write unless it is an abort
            -- for the wait command, in which case we only write the E flag.
            if bus2fsm.writeData(31 downto 29) = "101" then
              ctrl_e <= bus2fsm.writeData(22);
            end if;
            
          elsif clk_ena = '1' then
            
            -- Only accept the stop-clock and reset commands if the clock is
            -- running.
            if bus2fsm.writeData(31 downto 29) = "001" then
              ctrl_cmd <= bus2fsm.writeData(31 downto 29);
              ctrl_c <= bus2fsm.writeData(28);
              ctrl_b <= bus2fsm.writeData(27);
              ctrl_a <= bus2fsm.writeData(26);
              ctrl_i <= bus2fsm.writeData(25);
              ctrl_r <= bus2fsm.writeData(24);
            end if;
            if bus2fsm.writeData(31 downto 29) = "010" then
              ctrl_cmd <= bus2fsm.writeData(31 downto 29);
            end if;
            
          else
            
            -- Accept all commands except stop-clock.
            if bus2fsm.writeData(31 downto 29) /= "010" then
              ctrl_cmd <= bus2fsm.writeData(31 downto 29);
            end if;
            
            -- If this is a reset command, write the mode flags.
            if bus2fsm.writeData(31 downto 29) = "001" then
              ctrl_c <= bus2fsm.writeData(28);
              ctrl_b <= bus2fsm.writeData(27);
              ctrl_a <= bus2fsm.writeData(26);
              ctrl_i <= bus2fsm.writeData(25);
              ctrl_r <= bus2fsm.writeData(24);
            end if;
            
            -- If this is a wait command, write the E flag.
            if bus2fsm.writeData(31 downto 29) = "101" then
              ctrl_e <= bus2fsm.writeData(22);
            end if;
            
            -- If this is a step, wait, or scan command, write the counter
            -- register (in the latter case this is used to store the desired
            -- chain).
            if bus2fsm.writeData(31) = '1' then
              ctrl_count <= bus2fsm.writeData(COUNTER_WIDTH-1 downto 0);
            end if;
            
          end if;
        end if;
        
        -----------------------------------------------------------------------
        -- Handle bus reads
        -----------------------------------------------------------------------
        fsm2bus <= BUS_SLV2MST_IDLE;
        fsm2bus.ack <= bus_requesting(bus2fsm);
        fsm2bus.readData(31 downto 29) <= ctrl_cmd;
        fsm2bus.readData(28) <= ctrl_c;
        fsm2bus.readData(27) <= ctrl_b;
        fsm2bus.readData(26) <= ctrl_a;
        fsm2bus.readData(25) <= ctrl_i;
        fsm2bus.readData(24) <= ctrl_r;
        fsm2bus.readData(23) <= clk_ena;
        fsm2bus.readData(22) <= ctrl_e;
        fsm2bus.readData(COUNTER_WIDTH-1 downto 0) <= ctrl_count;
        
      end if;
    end if;
  end process;
  
  -----------------------------------------------------------------------
  -- TODO: scan chain memories
  -----------------------------------------------------------------------
  scan_wen <= '0';
  scan_wdata <= '0';
  
end Behavioral;
