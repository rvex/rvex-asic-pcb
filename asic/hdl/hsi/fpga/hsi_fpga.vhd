library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library asic;
use asic.bus_pkg.all;
use asic.bus_addrConv_pkg.all;

entity hsi_fpga is
  generic (
    
    -- Scan chain length.
    SCAN_LENGTH                 : natural := 256;
    
    -- Number of differential pairs. Each must be 1, 2, 4, or 8.
    N_RX                        : natural := 8;
    N_TX                        : natural := 8;
    
    -- HSI clock generator division factor. This factor is dependent on the
    -- ASIC operational mode.
    HSI_DIV                     : natural := 4
    
  );
  port (
    
    -- AHB clock and reset (100 MHz).
    reset                       : in  std_logic;
    clk_100MHz                  : in  std_logic;
    clk_100MHz_en               : in  std_logic := '1';
    
    -- Memory bus from the ASIC.
    core2mem                    : out bus_mst2slv_type;
    mem2core                    : in  bus_slv2mst_type;
    
    -- Debug bus to the FPGA.
    dbg2core                    : in  bus_mst2slv_type;
    core2dbg                    : out bus_slv2mst_type;
    
    -- GPIO-like stuff transferred over the HSI interface.
    irq                         : in  std_logic_vector(15 downto 0);
    done                        : out std_logic_vector(3 downto 0);
    idle                        : out std_logic_vector(3 downto 0);
    configWord                  : out std_logic_vector(31 downto 0);
    
    -- Core reset signal. This is basically the master reset; if this is
    -- asserted low, everything except the HSI PHY delay calibration is reset.
    -- Note that the core uses a synchronous reset though, so it won't reset
    -- until it gets its first clock somehow.
    pad_aresetn_core            : out std_logic;
    
    -- PHY reset signal and UART receive pin B. If asserted low while core
    -- reset is also asserted low, the HSI PHY delay calibration logic is reset
    -- asynchronously in addition to all the other systems. While core reset is
    -- high, this is disconnected from the PHY reset and used as an additional
    -- FPGA to ASIC UART data pin.
    pad_aresetn_phy             : out std_logic;
    
    -- Backup clock. This is used in UART mode as a backup to the HSI
    -- interface, in case there is a problem with the PHY or DLL.
    pad_backup_clk              : out std_logic;
    
    -- HSI clock pads. The core clock is derived from this when running in HSI
    -- mode: either the core gets the same clock, or the core clock gets HSI
    -- divided by 2.
    pad_hsi_clk_p               : out std_logic;
    pad_hsi_clk_n               : out std_logic;
    
    -- HSI transmitter pads.
    pad_f2a_p                   : out std_logic_vector(N_RX-1 downto 0);
    pad_f2a_n                   : out std_logic_vector(N_RX-1 downto 0);
    
    -- HSI receiver pads.
    pad_a2f_p                   : in  std_logic_vector(N_TX-1 downto 0);
    pad_a2f_n                   : in  std_logic_vector(N_TX-1 downto 0);
    
    -- Mode selection pin. This pin does the following things:
    --  - falling edge of aresetn_phy while aresetn_core is low: low selects
    --    backup interface, high selects HSI.
    --  - rising edge of aresetn_phy while aresetn_core is high: low selects
    --    1:1 clock in HSI mode or reliable UART mode, high selects 1:2 clock
    --    in HSI mode or fast UART mode.
    --  - while aresetn_core is high: low for normal operation, high for scan
    --    chain access.
    --  - rising edge of mode pin: shift ctrl F2A value into scan chain
    --    register LSB.
    pad_mode                    : out std_logic;
    
    -- FPGA to ASIC control pin. This pin does the following things:
    --  - rising edge of mode pin: shifted into scan chain register LSB.
    --  - mode pin high: scan chain input.
    --  - mode pin low, HSI mode: active-high calibration request.
    --  - mode pin low, UART mode: primary UART data (aresetn_phy is
    --    secondary).
    pad_ctrl_f2a                : out std_logic;
    
    -- ASIC to FPGA control pin. This pin does the following things:
    --  - mode pin high: scan chain output.
    --  - mode pin low, HSI mode: active-high lock output.
    --  - mode pin low, UART mode: UART data.
    pad_ctrl_a2f                : in  std_logic
    
  );
end hsi_fpga;

architecture Behavioral of hsi_fpga is
  
  -- Interconnect between the I/Os and the internal logic.
  signal aresetn_phy            : std_logic;
  signal aresetn_core           : std_logic;
  signal mode                   : std_logic;
  signal diff_clk_ena           : std_logic;
  signal diff_clk_q             : std_logic;
  signal f2a_x, f2a_y           : std_logic_vector(N_RX-1 downto 0);
  signal a2f_x, a2f_y           : std_logic_vector(N_TX-1 downto 0);
  signal backup_clk_ena         : std_logic;
  signal backup_clk_q           : std_logic;
  signal ctrl_f2a               : std_logic;
  signal ctrl_a2f               : std_logic;
  
  -- Reset signals.
  signal reset_hsi              : std_logic;
  signal reset_core             : std_logic;
  
  -- MMCM signals.
  signal mmcm_fb_clk            : std_logic;
  signal clk_hsi_local          : std_logic;
  signal clk_core_local         : std_logic;
  signal clk_hsi                : std_logic;
  signal clk_core               : std_logic;
  signal mmcm_locked            : std_logic;
  signal mmcm_reset             : std_logic;
  
  -- Control state machine signals.
  signal reset_ctrl             : std_logic;
  signal lock                   : std_logic;
  signal calib                  : std_logic;
  signal ifsel                  : std_logic;
  signal ratio                  : std_logic;
  signal uart_sample_edge       : std_logic;
  signal uart_setup_edge        : std_logic_vector(1 downto 0);
  signal backup_clk_high        : std_logic;
  
  -- UART signals.
  signal uart_rx                : std_logic;
  signal uart_rx_data           : std_logic_vector(31 downto 0);
  signal uart_rx_first          : std_logic;
  signal uart_rx_valid          : std_logic;
  signal uart_tx                : std_logic_vector(1 downto 0);
  signal uart_tx_data           : std_logic_vector(31 downto 0);
  signal uart_tx_first          : std_logic;
  signal uart_tx_valid          : std_logic;
  signal uart_tx_ack            : std_logic;
  
  -- GPIOs received from the ASIC that are unused.
  signal gpio_unused            : std_logic_vector(27 downto 24);
  
  -- Debug bus demux in AHB clk domain to MMCM.
  signal dema2mmcm              : bus_mst2slv_type;
  signal mmcm2dema              : bus_slv2mst_type;
  
  -- Debug bus demux in AHB clk domain to scan chain memories.
  signal dema2scan              : bus_mst2slv_type;
  signal scan2dema              : bus_slv2mst_type;
  
  -- Debug bus demux in AHB clk domain to f2a xclk unit.
  signal dema2dbgx              : bus_mst2slv_type;
  signal dbgx2dema              : bus_slv2mst_type;
  
  -- Bus from f2a xclk unit to debug bus demux in core clk domain.
  signal dbgx2demc              : bus_mst2slv_type;
  signal demc2dbgx              : bus_slv2mst_type;
  
  -- Debug bus demux in core clk domain to ASIC control FSM.
  signal demc2fsm               : bus_mst2slv_type;
  signal fsm2demc               : bus_slv2mst_type;
  
  -- Debug bus demux in core clk domain to HSI.
  signal demc2core              : bus_mst2slv_type;
  signal core2demc              : bus_slv2mst_type;
  
  -- Bus from HSI to a2f xclk unit.
  signal core2memx              : bus_mst2slv_type;
  signal memx2core              : bus_slv2mst_type;
  
begin
  
  -----------------------------------------------------------------------------
  -- Instantiate all the I/O pads
  -----------------------------------------------------------------------------
  -- Instantiate the reset pads.
  aresetn_phy_pad: OBUF port map (I => aresetn_phy, O => pad_aresetn_phy);
  aresetn_core_pad: OBUF port map (I => aresetn_core, O => pad_aresetn_core);
  
  -- Instantiate the mode pad.
  mode_pad: OBUF port map (I => mode, O => pad_mode);
  
  -- Instantiate the differential clock transmitter.
  diff_clk_ddr: ODDR
    port map (
      Q  => diff_clk_q,
      C  => clk_hsi,
      CE => clk_100MHz_en,
      D1 => '1',
      D2 => '0',
      R  => '0',
      S  => '0'
    );
  diff_clk_pad: OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25"
    )
    port map (
      I  => diff_clk_q,
      O  => pad_hsi_clk_p,
      OB => pad_hsi_clk_n
    );
  
  -- Instantiate the HSI receivers.
  hsi_a2f_inst: entity work.hsi_phy_rx
    generic map (
      N                         => N_RX
    )
    port map (
      aresetn_phy               => aresetn_phy,
      clk_hsi                   => clk_hsi,
      calib                     => calib,
      lock                      => lock,
      hsi_period                => "00000000",
      pad2phy_p                 => pad_a2f_p,
      pad2phy_n                 => pad_a2f_n,
      phy2dll_x                 => a2f_x,
      phy2dll_y                 => a2f_y
    );
  
  -- Instantiate the HSI transmitters.
  hsi_f2a_inst: entity work.hsi_phy_tx
    generic map (
      N                         => N_TX
    )
    port map (
      reset_hsi                 => reset_hsi,
      calib_hsi                 => calib,
      clk_hsi                   => clk_hsi,
      dll2phy_x                 => f2a_x,
      dll2phy_y                 => f2a_y,
      phy2pad_p                 => pad_f2a_p,
      phy2pad_n                 => pad_f2a_n
    );
  
  -- Instantiate the backup clock pad.
  backup_clk_ddr: ODDR
    port map (
      Q  => backup_clk_q,
      C  => clk_core,
      CE => '1',
      D1 => backup_clk_ena,
      D2 => backup_clk_high,
      R  => '0',
      S  => '0'
    );
  backup_clk_pad: OBUF
    port map (
      I  => backup_clk_q,
      O  => pad_backup_clk
    );
  
  -- Instantiate the control pads.
  ctrl_f2a_pad: OBUF
    port map (
      I => ctrl_f2a,
      O => pad_ctrl_f2a
    );
  ctrl_a2f_pad: IBUF
    port map (
      I => pad_ctrl_a2f,
      O => ctrl_a2f
    );
  
  -----------------------------------------------------------------------------
  -- Instantiate reset and clock logic
  -----------------------------------------------------------------------------
  -- Reset logic, same as in the ASIC.
  core_reset_logic: block is
    signal reset_sr_a, reset_sr_b, reset_sr_c : std_logic;
  begin
    reset_shift_reg: process (aresetn_core, clk_core_local) is
    begin
      if aresetn_core = '0' then
        reset_sr_a <= '1';
        reset_sr_b <= '1';
        reset_sr_c <= '1';
        reset_core <= '1';
      elsif rising_edge(clk_core_local) then
        reset_sr_a <= '0';
        reset_sr_b <= reset_sr_a;
        reset_sr_c <= reset_sr_b;
        reset_core <= reset_sr_c;
      end if;
    end process;
  end block;
  hsi_reset_logic: block is
    signal reset_sr : std_logic;
  begin
    reset_shift_reg: process (aresetn_core, clk_hsi_local) is
    begin
      if aresetn_core = '0' then
        reset_sr <= '1';
        reset_hsi <= '1';
      elsif rising_edge(clk_hsi_local) then
        reset_sr <= '0';
        reset_hsi <= reset_sr;
      end if;
    end process;
  end block;
  
  -- Clocking MMCM.
  -- TODO: should be reconfigurable.
  mmcm_reset <= reset or not clk_100MHz_en;
  clk_gen: mmcm_base
    generic map (
      CLKIN1_PERIOD     => 10.0, -- Input clock 100 MHz.
      CLKFBOUT_MULT_F   => 8.0,  -- Multiplier 8 = 800MHz VCO.
      CLKOUT0_DIVIDE_F  => real(8 / HSI_DIV),  -- HSI clock 200/400 MHz.
      CLKOUT1_DIVIDE    => 8     -- Core clock 100 MHz.
    )
    port map (
      CLKFBOUT          => mmcm_fb_clk,
      CLKOUT0           => clk_hsi_local,
      CLKOUT1           => clk_core_local,
      LOCKED            => mmcm_locked,
      CLKFBIN           => mmcm_fb_clk,
      CLKIN1            => clk_100MHz,
      PWRDWN            => '0',
      RST               => mmcm_reset
    );
  
  -- TODO: replace this dummy slave with the MMCM control interface.
--  mmcm_dummy: entity asic.bus_dummySlave
--    port map (
--      reset                     => reset,
--      clk                       => clk_100MHz,
--      clkEn                     => '1',
--      mosi                      => dema2mmcm,
--      miso                      => mmcm2dema
--    );
  mmcm2dema <= BUS_SLV2MST_IDLE;
  
  -- Buffer trees for the core and HSI clocks.
  clk_core_buf: BUFG port map (I => clk_core_local, O => clk_core);
  clk_hsi_buf: BUFG port map (I => clk_hsi_local, O => clk_hsi);
  
  -- Control unit reset generator.
  ctrl_reset_logic: block is
    signal reset_sr : std_logic;
  begin
    reset_shift_reg: process (reset, mmcm_locked, clk_100MHz_en, clk_core) is
    begin
      if reset = '1' or (mmcm_locked = '0' and clk_100MHz_en = '1') then
        reset_sr <= '1';
        reset_ctrl <= '1';
      elsif rising_edge(clk_core) then
        reset_sr <= '0';
        reset_ctrl <= reset_sr;
      end if;
    end process;
  end block;
  
  -----------------------------------------------------------------------------
  -- Instantiate the ASIC control state machine
  -----------------------------------------------------------------------------
  asic_ctrl_fsm_inst: entity work.hsi_fpga_ctrl
    generic map (
      SCAN_LENGTH               => SCAN_LENGTH
    )
    port map (
      clk                       => clk_core,
      reset                     => reset_ctrl,
      bus2fsm                   => demc2fsm,
      fsm2bus                   => fsm2demc,
      aresetn_phy               => aresetn_phy,
      aresetn_core              => aresetn_core,
      mode                      => mode,
      ctrl_a2f                  => ctrl_a2f,
      ctrl_f2a                  => ctrl_f2a,
      diff_clk_ena              => diff_clk_ena,
      backup_clk_ena            => backup_clk_ena,
      backup_clk_high           => backup_clk_high,
      ifsel                     => ifsel,
      ratio                     => ratio,
      uart_sample_edge          => uart_sample_edge,
      uart_setup_edge           => uart_setup_edge,
      calib                     => calib,
      lock                      => lock,
      uart_rx                   => uart_rx,
      uart_tx                   => uart_tx
    );
  
  -----------------------------------------------------------------------------
  -- Instantiate the UART receiver and transmitter
  -----------------------------------------------------------------------------
  uart_rx_inst: entity work.hsi_uart_fpga_rx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      clkEn_core                => backup_clk_ena,
      uart_rx                   => uart_rx,
      uart_mode                 => ratio,
      uart_sample_edge          => uart_sample_edge,
      uart2tl_data              => uart_rx_data,
      uart2tl_first             => uart_rx_first,
      uart2tl_valid             => uart_rx_valid
    );
  
  uart_tx_inst: entity work.hsi_uart_fpga_tx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      clkEn_core                => backup_clk_ena,
      uart_tx                   => uart_tx,
      uart_mode                 => ratio,
      uart_setup_edge           => uart_setup_edge,
      tl2dll_data               => uart_tx_data,
      tl2dll_first              => uart_tx_first,
      tl2dll_valid              => uart_tx_valid,
      dll2tl_ack                => uart_tx_ack
    );
  
  -----------------------------------------------------------------------------
  -- Instantiate the HSI stuff common to ASIC and FPGA
  -----------------------------------------------------------------------------
  hsi_common_inst: entity asic.hsi_common
    generic map (
      N_RX                      => N_RX,
      N_TX                      => N_TX,
      SIDE                      => "FPGA"
    )
    port map (
      
      -- System control.
      reset_hsi                 => reset_hsi,
      reset_core                => reset_core,
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core,
      ifsel                     => ifsel,
      
      -- Data inputs and outputs to HSI PHY.
      rx_x                      => a2f_x,
      rx_y                      => a2f_y,
      tx_x                      => f2a_x,
      tx_y                      => f2a_y,
      
      -- UART interface.
      uart_rx_data              => uart_rx_data,
      uart_rx_first             => uart_rx_first,
      uart_rx_valid             => uart_rx_valid,
      uart_tx_data              => uart_tx_data,
      uart_tx_first             => uart_tx_first,
      uart_tx_valid             => uart_tx_valid,
      uart_tx_ack               => uart_tx_ack,
      
      -- To r-VEX bus master interfave.
      bus_master_m2s            => demc2core,
      bus_master_s2m            => core2demc,
      
      -- To r-VEX bus slave interfave.
      bus_slave_m2s             => core2memx,
      bus_slave_s2m             => memx2core,
      
      -- GPIO data.
      gpio_in(27 downto 16)     => X"000",
      gpio_in(15 downto 0)      => irq,
      gpio_out(27 downto 24)    => gpio_unused,
      gpio_out(23 downto 20)    => idle,
      gpio_out(19 downto 16)    => done,
      gpio_out(15 downto 0)     => configWord(15 downto 0)
      
    );
  
  configWord(31 downto 16) <= (others => '0');
  
  -----------------------------------------------------------------------------
  -- Bus interconnect logic
  -----------------------------------------------------------------------------
  -- Debug bus demuxer in AHB clock domain.
  demux_ahb_inst: entity asic.bus_demux
    generic map (
      ADDRESS_MAP               => (
        1 => addrRangeAndMap(
          match => "------0-------------------------"
        ),
        2 => addrRangeAndMap(
          match => "------10------------------------"
        ),
        3 => addrRangeAndMap(
          match => "------11------------------------"
        )
      )
    )
    port map (
      reset                     => reset,
      clk                       => clk_100MHz,
      clkEn                     => '1',
      mst2demux                 => dbg2core,
      demux2mst                 => core2dbg,
      demux2slv(1)              => dema2dbgx,
      demux2slv(2)              => dema2scan,
      demux2slv(3)              => dema2mmcm,
      slv2demux(1)              => dbgx2dema,
      slv2demux(2)              => scan2dema,
      slv2demux(3)              => mmcm2dema
    );
  
  -- TODO: replace this dummy slave with scan chain memory.
--  scan_dummy: entity asic.bus_dummySlave
--    port map (
--      reset                     => reset,
--      clk                       => clk_100MHz,
--      clkEn                     => '1',
--      mosi                      => dema2scan,
--      miso                      => scan2dema
--    );
  scan2dema <= BUS_SLV2MST_IDLE;
  
  -- Debug bus xclk unit.
  f2a_xclk_inst: entity asic.bus_crossClock
    port map (
      reset                     => reset_ctrl,
      mst_clk                   => clk_100MHz,
      mst2crclk                 => dema2dbgx,
      crclk2mst                 => dbgx2dema,
      slv_clk                   => clk_core,
      crclk2slv                 => dbgx2demc,
      slv2crclk                 => demc2dbgx
    );
  
  -- Debug bus demuxer in core clock domain.
  demux_core_inst: entity asic.bus_demux
    generic map (
      ADDRESS_MAP               => (
        1 => addrRangeAndMap(
          match => "-------0------------------------"
        ),
        2 => addrRangeAndMap(
          match => "-------1------------------------"
        )
      )
    )
    port map (
      reset                     => reset_ctrl,
      clk                       => clk_core,
      clkEn                     => '1',
      mst2demux                 => dbgx2demc,
      demux2mst                 => demc2dbgx,
      demux2slv(1)              => demc2core,
      demux2slv(2)              => demc2fsm,
      slv2demux(1)              => core2demc,
      slv2demux(2)              => fsm2demc
    );
  
  -- Memory bus xclk unit.
  a2f_xclk_inst: entity asic.bus_crossClock
    port map (
      reset                     => reset_ctrl,
      mst_clk                   => clk_core,
      mst2crclk                 => core2memx,
      crclk2mst                 => memx2core,
      slv_clk                   => clk_100MHz,
      crclk2slv                 => core2mem,
      slv2crclk                 => mem2core
    );
  
  
end Behavioral;
