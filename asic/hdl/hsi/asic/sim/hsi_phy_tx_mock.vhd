library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.simUtils_pkg.all;

-- SIMULATION-ONLY MOCKUP VERSION

entity hsi_phy_tx is
  generic (
    
    -- Number of differential pairs.
    N                           : natural := 4
    
  );
  port (
    
    -- System control.
    reset_hsi                   : in  std_logic;
    calib_hsi                   : in  std_logic;
    clk_hsi                     : in  std_logic;
    
    -- Data input. X is transmitted while clk_hsi is high, Y is transmitted
    -- while it is low.
    dll2phy_x                   : in  std_logic_vector(N-1 downto 0);
    dll2phy_y                   : in  std_logic_vector(N-1 downto 0);
    
    -- Data input.
    phy2pad_p                   : out std_logic_vector(N-1 downto 0);
    phy2pad_n                   : out std_logic_vector(N-1 downto 0)
    
  );
end hsi_phy_tx;

architecture Behavioral of hsi_phy_tx is
  signal xp, xn, yp, yn         : std_logic_vector(N-1 downto 0);
begin
  
  process (clk_hsi) is
  begin
    if rising_edge(clk_hsi) then
      if reset_hsi = '1' then
        xp <= (others => '0');
        xn <= (others => '1');
        yp <= (others => calib_hsi);
        yn <= (others => not calib_hsi);
      else
        xp <= dll2phy_x;
        xn <= not dll2phy_x;
        yp <= dll2phy_y;
        yn <= not dll2phy_y;
      end if;
    end if;
  end process;
  
  phy2pad_p <= xp when clk_hsi = '1' else yp;
  phy2pad_n <= xn when clk_hsi = '1' else yn;
  
end Behavioral;

