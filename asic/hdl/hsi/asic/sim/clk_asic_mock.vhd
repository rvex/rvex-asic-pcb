library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SIMULATION-ONLY MOCKUP VERSION

entity clk_asic is
  port (
    diff_clk                    : in  std_logic;
    backup_clk                  : in  std_logic;
    ifsel                       : in  std_logic;
    ratio                       : in  std_logic;
    reset_core_local            : in  std_logic;
    reset_mem_local             : in  std_logic;
    reset_hsi_local             : in  std_logic;
    aresetn_phy_local           : in  std_logic;
    scan_enable_local           : in  std_logic;
    
    clk_hsi                     : out std_logic;
    clk_hsi_local               : out std_logic;
    clk_core                    : out std_logic;
    clk_core_local              : out std_logic;
    reset_core                  : out std_logic;
    reset_mem                   : out std_logic;
    reset_hsi                   : out std_logic;
    aresetn_phy                 : out std_logic;
    scan_enable                 : out std_logic
  );
end clk_asic;

architecture Behavioral of clk_asic is
  
  signal clk_slow               : std_logic := '0';
  signal clk_slow_r             : std_logic := '0';
  signal clk_hsi_local_s        : std_logic;
  signal clk_core_local_s       : std_logic;
  
begin
  
  -- Clock divider.
  process (diff_clk) is
  begin
    if rising_edge(diff_clk) then
      if ratio = '0' then
        clk_slow <= not clk_slow after 300 ps;
      else
        clk_slow <= not clk_slow_r after 300 ps;
      end if;
      clk_slow_r <= clk_slow after 300 ps;
    end if;
  end process;
  
  -- Core clock select mux.
  clk_core_local_s <= clk_slow_r after 150 ps when ifsel = '1' else backup_clk after 150 ps;
  clk_core_local <= clk_slow_r after 150 ps when ifsel = '1' else backup_clk after 150 ps;
  
  -- HSI clock select mux.
  clk_hsi_local_s <= diff_clk after 150 ps when ifsel = '1' else backup_clk after 150 ps;
  clk_hsi_local <= diff_clk after 150 ps when ifsel = '1' else backup_clk after 150 ps;
  
  -- Buffer trees.
  clk_hsi     <= transport clk_hsi_local_s after 5 ns;
  reset_hsi   <= transport reset_hsi_local after 5 ns;
  clk_core    <= transport clk_core_local_s after 5 ns;
  reset_mem   <= transport reset_mem_local after 5 ns;
  reset_core  <= transport reset_core_local after 5 ns;
  aresetn_phy <= transport aresetn_phy_local after 5 ns;
  scan_enable <= transport scan_enable_local after 5 ns;
  
end Behavioral;

