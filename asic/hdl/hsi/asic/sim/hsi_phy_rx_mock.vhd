library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.simUtils_pkg.all;

-- SIMULATION-ONLY MOCKUP VERSION

entity hsi_phy_rx is
  generic (
    
    -- Unit delay of the time-to-digital convertor delay elements.
    TTD_UNIT_DELAY              : time := 120 ps;
    
    -- Number of TTD bits.
    TTD_NUM_BITS                : natural := 6;
    
    -- Number of differential pairs.
    N                           : natural := 4
    
  );
  port (
    
    -- System control.
    aresetn_phy                 : in  std_logic;
    clk_hsi                     : in  std_logic;
    
    -- calib is asserted high while the transmitters are generating a 10101010
    -- pattern to do delay calibration. Aside from potentially doing some small
    -- adjustments to compensate for temperature effects, the delay may not
    -- change significantly while calib_hsi is low.
    calib                       : in  std_logic;
    
    -- Lock is asserted high when all receivers have locked on.
    lock                        : out std_logic;
    
    -- Clock period register contents. Can be between assertion of aresetn_phy
    -- and lock
    hsi_period                  : in  std_logic_vector(7 downto 0);
    
    -- Data input.
    pad2phy_p                   : in  std_logic_vector(N-1 downto 0);
    pad2phy_n                   : in  std_logic_vector(N-1 downto 0);
    
    -- Synchronized data output.
    phy2dll_x                   : out std_logic_vector(N-1 downto 0);
    phy2dll_y                   : out std_logic_vector(N-1 downto 0)
    
  );
end hsi_phy_rx;

architecture Behavioral of hsi_phy_rx is
  signal clk_period       : std_logic_vector(TTD_NUM_BITS-1 downto 0);
  signal clk_lock         : std_logic;
  signal clk_half_period  : std_logic_vector(TTD_NUM_BITS-1 downto 0);
  signal clk_quart_period : std_logic_vector(TTD_NUM_BITS-1 downto 0);
  signal lock_n           : std_logic_vector(N-1 downto 0);
begin
  
  -- Detect the time between two rising edges of the clock in terms of delay
  -- line elements.
  clk_period_detector: entity work.hsi_ttd
    generic map (
      UNIT_DELAY                => TTD_UNIT_DELAY,
      NUM_BITS                  => TTD_NUM_BITS,
      DETECT_FALLING            => false,
      MIN_DELAY                 => 8
    )
    port map (
      clk                       => clk_hsi,
      d                         => clk_hsi,
      taps                      => open,
      q                         => clk_period,
      valid                     => clk_lock
    );
  
  -- Store the clock period.
  process (aresetn_phy, clk_hsi) is
  begin
    if aresetn_phy = '0' then
      clk_half_period <= (others => '0');
    elsif rising_edge(clk_hsi) then
      if clk_lock = '1' and calib = '1' then
        clk_half_period(TTD_NUM_BITS-1) <= '0';
        clk_half_period(TTD_NUM_BITS-2 downto 0) <= clk_period(TTD_NUM_BITS-1 downto 1);
      end if;
    end if;
  end process;
  
  clk_quart_period(TTD_NUM_BITS-1) <= '0';
  clk_quart_period(TTD_NUM_BITS-2 downto 0) <= clk_half_period(TTD_NUM_BITS-1 downto 1);
  
  receiver_gen: for i in 0 to N-1 generate
    signal data               : std_logic;
    signal data_taps          : std_logic_vector(2**TTD_NUM_BITS downto 0);
    signal data_edge          : std_logic_vector(TTD_NUM_BITS-1 downto 0);
    signal data_lock          : std_logic;
    signal data_tap_x         : std_logic_vector(TTD_NUM_BITS-1 downto 0);
    signal data_tap_y         : std_logic_vector(TTD_NUM_BITS-1 downto 0);
    signal data_x_next        : std_logic;
    signal data_y_next        : std_logic;
  begin
    
    -- Model the differential receiver.
    process (pad2phy_p(i), pad2phy_n(i)) is
    begin
      if to_x01(pad2phy_p(i)) = '1' and to_x01(pad2phy_n(i)) = '0' then
        data <= '1';
      elsif to_x01(pad2phy_p(i)) = '0' and to_x01(pad2phy_n(i)) = '1' then
        data <= '0';
      end if;
    end process;
    
    -- Detect edges on the data signal.
    data_detector: entity work.hsi_ttd
      generic map (
        UNIT_DELAY                => TTD_UNIT_DELAY,
        NUM_BITS                  => TTD_NUM_BITS,
        DETECT_FALLING            => true,
        MIN_DELAY                 => 0
      )
      port map (
        clk                       => clk_hsi,
        d                         => data,
        taps                      => data_taps,
        q                         => data_edge,
        valid                     => data_lock
      );
    
    -- Detect only edges up to half a clock period away from the rising clock
    -- edge.
    process (clk_hsi) is
    begin
      if aresetn_phy = '0' then
        data_tap_x <= std_logic_vector(to_unsigned(natural(1250 ps / TTD_UNIT_DELAY), data_tap_x'length));
        data_tap_y <= std_logic_vector(to_unsigned(natural(3750 ps / TTD_UNIT_DELAY), data_tap_y'length));
        lock_n(i) <= '0';
      elsif rising_edge(clk_hsi) then
        if data_lock = '1' and calib = '1' then
          if unsigned(data_edge) <= unsigned(clk_half_period) then
            data_tap_x <= std_logic_vector(unsigned(data_edge) + unsigned(clk_quart_period) + unsigned(clk_half_period));
            data_tap_y <= std_logic_vector(unsigned(data_edge) + unsigned(clk_quart_period));
            lock_n(i) <= '1';
          end if;
        end if;
      end if;
    end process;
    
    -- Select the right delay line tap.
    data_x_next <= data_taps(to_integer(unsigned(data_tap_x))+1);
    data_y_next <= data_taps(to_integer(unsigned(data_tap_y))+1);
    
    -- Register the data.
    process (aresetn_phy, clk_hsi) is
    begin
      if aresetn_phy = '0' then
        phy2dll_x(i) <= '0';
        phy2dll_y(i) <= '0';
      elsif rising_edge(clk_hsi) then
        phy2dll_x(i) <= data_x_next;
        phy2dll_y(i) <= data_y_next;
      end if;
    end process;
    
  end generate;
  
  lock_and: process (lock_n) is
  begin
    lock <= '1';
    for i in 0 to N-1 loop
      if lock_n(i) /= '1' then
        lock <= '0';
      end if;
    end loop;
  end process;
  
end Behavioral;

