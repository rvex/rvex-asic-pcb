library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SIMULATION-ONLY MOCKUP VERSION

entity hsi_ttd is
  generic (
    
    -- Unit delay for one delay element.
    UNIT_DELAY                  : time := 120 ps;
    
    -- Number of output bits.
    NUM_BITS                    : natural := 5;
    
    -- When set, falling edges are detected in addition to rising edges.
    DETECT_FALLING              : boolean := true;
    
    -- The shortest delay that the convertor will detect.
    MIN_DELAY                   : natural := 0
    
  );
  port (
    
    -- Clock input.
    clk                         : in  std_logic;
    
    -- Input signal under test.
    d                           : in  std_logic;
    
    -- Delay line tap outputs.
    taps                        : out std_logic_vector(2**NUM_BITS downto 0);
    
    -- Output.
    q                           : out std_logic_vector(NUM_BITS-1 downto 0);
    valid                       : out std_logic
    
  );
end hsi_ttd;

architecture Behavioral of hsi_ttd is
  
  -- Delay taps.
  signal taps_s                 : std_logic_vector(2**NUM_BITS downto 0);
  
  -- XORs between delay taps.
  signal edge                   : std_logic_vector(2**NUM_BITS-1 downto 0);
  
begin
  
  -- Simulate the delay line.
  taps_s(0) <= d;
  tap_gen: for i in 1 to 2**NUM_BITS generate
  begin
    taps_s(i) <= taps_s(i-1) after UNIT_DELAY;
  end generate;
  taps <= taps_s;
  
  -- Simulate the edge detectors.
  process (clk) is
  begin
    if rising_edge(clk) then
      for i in 0 to 2**NUM_BITS-1 loop
        if DETECT_FALLING then
          edge(i) <= to_x01(taps_s(i)) xor to_x01(taps_s(i+1));
        else
          edge(i) <= to_x01(taps_s(i)) and not to_x01(taps_s(i+1));
        end if;
      end loop;
    end if;
  end process;
  
  -- Simulate thermometer to binary.
  process (edge) is
  begin
    valid <= '0';
    q <= (others => '0');
    for i in 2**NUM_BITS-1 downto MIN_DELAY loop
      if edge(i) = '1' then
        valid <= '1';
        q <= std_logic_vector(to_unsigned(i, NUM_BITS));
      end if;
    end loop;
  end process;
  
end Behavioral;

