library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.simUtils_pkg.all;

-- SIMULATION-ONLY MOCKUP VERSION

entity hsi_clk_rx is
  port (
    clk_p                       : in  std_logic;
    clk_n                       : in  std_logic;
    clk_out                     : out std_logic
  );
end hsi_clk_rx;

architecture Behavioral of hsi_clk_rx is
begin
  process (clk_p, clk_n) is
  begin
    if to_x01(clk_p) = '1' and to_x01(clk_n) = '0' then
      clk_out <= '1';
    elsif to_x01(clk_p) = '0' and to_x01(clk_n) = '1' then
      clk_out <= '0';
    end if;
  end process;
end Behavioral;

