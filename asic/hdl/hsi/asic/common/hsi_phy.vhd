library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;

entity hsi_phy is
  generic (
    
    -- Number of differential pairs. Each must be 1, 2 or 4.
    N_RX                        : natural := 4;
    N_TX                        : natural := 4;
    
    -- Number of clocks, may differ for each design
    N_CK                        : natural := 1
    
  );
  port (
    
    -- Core reset signal. This is basically the master reset; if this is
    -- asserted low, everything except the HSI PHY delay calibration is reset.
    -- Note that the core uses a synchronous reset though, so it won't reset
    -- until it gets its first clock somehow.
    pad_aresetn_core            : in  std_logic;
    aresetn_core                : out std_logic;
    
    -- PHY reset signal and UART receive pin B. If asserted low while core
    -- reset is also asserted low, the HSI PHY delay calibration logic is reset
    -- asynchronously in addition to all the other systems. While core reset is
    -- high, this is disconnected from the PHY reset and used as an additional
    -- FPGA to ASIC UART data pin.
    pad_aresetn_phy             : in  std_logic;
    aresetn_phy_uart            : out std_logic;
    
    -- Reset signal for the physical interface before the core.
    aresetn_phy_local           : out std_logic;
    aresetn_phy                 : in  std_logic;
    
    -- Backup clock. This is used in UART mode as a backup to the HSI
    -- interface, in case there is a problem with the PHY or DLL.
    pad_backup_clk              : in  std_logic;
    backup_clk                  : out std_logic;
    
    -- HSI clock pads. The core clock is derived from this when running in HSI
    -- mode: either the core gets the same clock, or the core clock gets HSI
    -- divided by 2.
    pad_hsi_clk_p               : in  std_logic_vector(N_CK-1 downto 0);
    pad_hsi_clk_n               : in  std_logic_vector(N_CK-1 downto 0);
    diff_clk                    : out std_logic;
    
    -- Reference current pads.
    pad_Iref_tx                 : in  std_logic_vector(N_TX-1 downto 0);
    pad_Iref_rx                 : out std_logic_vector(N_RX-1 downto 0);
    
    -- HSI receiver pads.
    pad_f2a_p                   : in  std_logic_vector(N_RX-1 downto 0);
    pad_f2a_n                   : in  std_logic_vector(N_RX-1 downto 0);
    
    -- HSI transmitter pads.
    pad_a2f_p                   : out std_logic_vector(N_TX-1 downto 0);
    pad_a2f_n                   : out std_logic_vector(N_TX-1 downto 0);
    
    -- Mode selection pin. This pin does the following things:
    --  - falling edge of aresetn_phy while aresetn_core is low: low selects
    --    backup interface, high selects HSI.
    --  - rising edge of aresetn_phy while aresetn_core is low: low selects
    --    1:2 clock in HSI mode or reliable UART mode, high selects 1:4 clock
    --    in HSI mode or fast UART mode.
    --  - while aresetn_core is high: low for normal operation, high for scan
    --    chain access.
    --  - rising edge of mode pin while aresern_core is low: shift ctrl F2A
    --    value into HSI clock period register LSB.
    --  - rising edge of mode pin while aresern_core is high: shift ctrl F2A
    --    value into scan chain register LSB.
    pad_mode                    : in  std_logic;
    mode                        : out std_logic;
    
    -- FPGA to ASIC control pin. This pin does the following things:
    --  - rising edge of mode pin while aresetn_core is low: shifted into HSI
    --    clock period register LSB.
    --  - rising edge of mode pin while aresetn_core is high: shifted into scan
    --    chain register LSB.
    --  - mode pin high: scan chain input.
    --  - mode pin low, HSI mode: active-high calibration request.
    --  - mode pin low, UART mode: primary UART data (aresetn_phy is
    --    secondary).
    pad_ctrl_f2a                : in  std_logic;
    
    -- ASIC to FPGA control pin. This pin does the following things:
    --  - mode pin high: scan chain output.
    --  - mode pin low, HSI mode: active-high lock output.
    --  - mode pin low, UART mode: UART data.
    pad_ctrl_a2f                : out std_logic;
    
    -- Interconnect between the I/Os and the internal logic.
    f2a_x                       : out std_logic_vector(N_RX-1 downto 0);
    f2a_y                       : out std_logic_vector(N_RX-1 downto 0);
    
    a2f_x                       : in  std_logic_vector(N_TX-1 downto 0);
    a2f_y                       : in  std_logic_vector(N_TX-1 downto 0);
    
    ctrl_f2a                    : out std_logic;
    ctrl_a2f                    : in  std_logic;
    
    
    -- System control signals.
    calib                       : in  std_logic;
    lock                        : out std_logic;
    hsi_period                  : in  std_logic_vector(7 downto 0);
  
    -- Buffer tree outputs.
    reset_hsi                   : in  std_logic;
    clk_hsi                     : in  std_logic;
    clk_core                    : in  std_logic
    
  );
end hsi_phy;

architecture Behavioral of hsi_phy is
  signal aresetn_phy_uart_local : std_logic;
  signal aresetn_core_local     : std_logic;
begin
  
  -----------------------------------------------------------------------------
  -- Instantiate all the I/O buffers
  -----------------------------------------------------------------------------
  -- Instantiate the reset input buffers.
  aresetn_phy_buf_inst: entity asic.input_buf
    port map (
      pad => pad_aresetn_phy,
      int => aresetn_phy_uart_local
    );
  aresetn_phy_uart <= aresetn_phy_uart_local;
  
  aresetn_core_buf_inst: entity asic.input_buf
    port map (
      pad => pad_aresetn_core,
      int => aresetn_core_local
    );
  aresetn_core <= aresetn_core_local;
    
  -- Activate aresetn_phy only if the core is also being reset.
  aresetn_phy_local <= aresetn_phy_uart_local or aresetn_core_local;
  
  -- Instantiate the mode input buffer.
  mode_buf_inst: entity asic.input_buf
    port map (
      pad => pad_mode,
      int => mode
    );
  
  -- Instantiate the differential clock receiver.
  -- Note that only one clock can be used for this design.
  clk_rx_inst: entity asic.hsi_clk_rx
    port map (
      clk_p                     => pad_hsi_clk_p(0),
      clk_n                     => pad_hsi_clk_n(0),
      clk_out                   => diff_clk
    );
  
  -- Instantiate the HSI receivers.
  hsi_f2a_inst: entity asic.hsi_phy_rx
    generic map (
      N                         => N_RX
    )
    port map (
      aresetn_phy               => aresetn_phy,
      clk_hsi                   => clk_hsi,
      calib                     => calib,
      lock                      => lock,
      hsi_period                => hsi_period,
      pad2phy_p                 => pad_f2a_p,
      pad2phy_n                 => pad_f2a_n,
      phy2dll_x                 => f2a_x,
      phy2dll_y                 => f2a_y
    );
  
  -- Instantiate the HSI transmitters.
  hsi_a2f_inst: entity asic.hsi_phy_tx
    generic map (
      N                         => N_TX
    )
    port map (
      reset_hsi                 => reset_hsi,
      calib_hsi                 => calib,
      clk_hsi                   => clk_hsi,
      dll2phy_x                 => a2f_x,
      dll2phy_y                 => a2f_y,
      phy2pad_p                 => pad_a2f_p,
      phy2pad_n                 => pad_a2f_n
    );
  
  -- Instantiate the backup clock input buffer.
  backup_clk_buf_inst: entity asic.clk_input_buf
    port map (
      pad => pad_backup_clk,
      int => backup_clk
    );
  
  -- Instantiate the control buffers.
  ctrl_f2a_buf_inst: entity asic.input_buf
    port map (
      pad => pad_ctrl_f2a,
      int => ctrl_f2a
    );
  
  ctrl_a2f_buf_inst: entity asic.output_buf
    port map (
      pad => pad_ctrl_a2f,
      int => ctrl_a2f
    );
  
end Behavioral;
