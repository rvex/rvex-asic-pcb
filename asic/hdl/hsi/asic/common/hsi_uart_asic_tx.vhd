library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity hsi_uart_asic_tx is
  port (
    
    -- System control.
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- Data output.
    uart_tx                     : out std_logic;
    
    -- Mode input: 0=reliable, 1=fast.
    uart_mode                   : in  std_logic;
    
    -- Word stream input.
    tl2dll_data                 : in  std_logic_vector(31 downto 0);
    tl2dll_first                : in  std_logic;
    tl2dll_valid                : in  std_logic;
    dll2tl_ack                  : out std_logic
    
  );
end hsi_uart_asic_tx;

architecture Behavioral of hsi_uart_asic_tx is
  
  -- Counter for divide-by-7 clock.
  signal bit_clk_div            : unsigned(2 downto 0);
  
  -- Clock enable signal for the UART shift register.
  signal bit_clk_en             : std_logic;
  
  -- Data shift register.
  signal shift_reg              : std_logic_vector(32 downto 0);
  
  -- Number of bits remaining before we can start the next packet. If this is
  -- at 1 we can start the next frame if it belongs to the same packet.
  signal bit_counter            : unsigned(5 downto 0);
  
  -- Strobe signal which initiates a new transfer.
  signal start                  : std_logic;
  
begin
  
  -- Generate the clock enable signal for the bit clock. This is always high
  -- when the UART is in fast mode, and high every 7th cycle when the UART is
  -- in reliable mode.
  bit_clock_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' then
        bit_clk_div <= "000";
      else
        if bit_clk_div = "000" then
          if uart_mode = '0' then
            bit_clk_div <= "110";
          else
            bit_clk_div <= "000";
          end if;
        else
          bit_clk_div <= bit_clk_div - 1;
        end if;
      end if;
    end if;
  end process;
  bit_clk_en <= '1' when bit_clk_div = "000" else '0';
  
  -- Generate the data shift registers and bit counter.
  shift_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' then
        bit_counter <= "000000";
        shift_reg <= (others => '1');
      else
        if bit_clk_en = '1' then
          if start = '1' then
            
            -- Load the shift registers with the new data word.
            shift_reg(32) <= '0';
            shift_reg(31 downto 0) <= tl2dll_data;
            
            -- Load the bit counter.
            bit_counter <= "100010";
            
          else
            
            -- Shift the shift registers.
            shift_reg <= shift_reg(31 downto 0) & "1";
            
            -- Decrement the bit counter if it is nonzero.
            if bit_counter /= "000000" then
              bit_counter <= bit_counter - 1;
            end if;
            
          end if;
        end if;
      end if;
    end if;
  end process;
  
  -- Connect the UART output to the shift register.
  uart_tx <= shift_reg(32);
  
  -- Determine if we're ready to start a new transmission and acknowledge an
  -- incoming transfer request.
  start_proc: process (
    tl2dll_first, tl2dll_valid, bit_counter, bit_clk_en
  ) is
  begin
    start <= '0';
    dll2tl_ack <= '0';
    
    -- If we have data pending...
    if tl2dll_valid = '1' then
      
      -- And we're not sending anything anymore...
      if bit_counter(5 downto 1) = "00000" then
        
        -- And the next word is still part of the same packet or we've had at
        -- least one idle cycle since the previous word...
        if tl2dll_first = '0' or bit_counter(0) = '0' then
          
          -- Then get ready to load the new data word into the shift
          -- registers...
          start <= '1';
          
          -- And acknowledge the new word if bit_clk_en is set.
          dll2tl_ack <= bit_clk_en;
          
        end if;
      end if;
    end if;
  end process;
  
end Behavioral;

