library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SoC pad library
library techmap;
use techmap.gencomp.all;

-- ASIC SYNTHESIS VERSION

entity output_buf is
  generic (
    -- Technology to use for pads (ASIC wide).
    padtech                     : integer := umc65;
    -- padlimit dictates wether inline or staggered pads are to be used.
    -- Must be one of 'core_limited' or 'pad_limited'.
    padlimit                    : integer := core_limited
  );
  port (
    int   : in  std_logic;
    pad   : out std_logic
  );
end output_buf;

architecture Behavioral of output_buf is
begin
  
  io_output_buf_inst : entity techmap.outpad
    generic map (
      tech                      => padtech, 
      limit                     => padlimit
    )
    port map (
      pad                       => pad, 
      i                         => int
    );
  
end Behavioral;

