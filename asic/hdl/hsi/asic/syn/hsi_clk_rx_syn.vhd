library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
library techmap;
use techmap.gencomp.all;

-- ASIC SYNTHESIS VERSION

entity hsi_clk_rx is
  generic (
    -- Technology to use for pads (ASIC wide).
    padtech                     : integer := umc65;
    -- padlimit dictates wether inline or staggered pads are to be used.
    -- Must be one of 'core_limited' or 'pad_limited'.
    padlimit                    : integer := core_limited
  );
  port (
    clk_p                       : in  std_logic;
    clk_n                       : in  std_logic;
    clk_out                     : out std_logic;
    
    -- Externally generated biasing signals 
    Vb1                         : in  std_logic
  );
end hsi_clk_rx;

architecture Behavioral of hsi_clk_rx is
  signal clk_p_int, clk_n_int   : std_logic;
  signal outp, outn             : std_logic;
begin
  -- Feed clock networks through input pads
  io_clk_p : techmap.gencomp.inpad
    generic map (
      tech                      => padtech, 
      limit                     => padlimit
    )
    port map (
      pad                       => clk_p, 
      o                         => clk_p_int
    );
  io_clk_n : techmap.gencomp.inpad
    generic map (
      tech                      => padtech, 
      limit                     => padlimit
    )
    port map (
      pad                       => clk_n, 
      o                         => clk_n_int
    );

  -- The actual LVDS receiver instance. Note that this is simply a mockup of
  -- the real LVDS. Only during place and route it is actually replaced.
  receiver_inst: entity asic.LVDS_receiver 
    port map (
      INP                     => clk_p_int,
      INN                     => clk_n_int,
      OUTP                    => outp,
      OUTN                    => outn,
      VB1                     => Vb1
    );
  
  -- TODO: receiver should generate a single signal? Maybe even replace with a
  --       special clock instance instead of reusing a common receiver..
  -- Simply take one of the outputs for now.
  clk_out <= outp;
end Behavioral;

