library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SoC pad library
library techmap;
use techmap.gencomp.all;

-- ASIC SYNTHESIS VERSION

entity diff_output_buf is
  generic (
    -- Technology to use for pads (ASIC wide).
    padtech                     : integer := umc65;
    -- padlimit dictates wether inline or staggered pads are to be used.
    -- Must be one of 'core_limited' or 'pad_limited'.
    padlimit                    : integer := core_limited
  );
  port (
    intp    : in  std_logic;
    intn    : in  std_logic;
    padp    : out std_logic;
    padn    : out std_logic
  );
end diff_output_buf;

architecture Behavioral of diff_output_buf is
begin
  -- Differential output is simply a combination of two pads
  io_output_diff_p_inst : entity techmap.aoutpad
    generic map (
      tech                      => padtech, 
      limit                     => padlimit
    )
    port map (
      pad                       => padp, 
      i                         => intp
    );
    
  io_output_diff_n_inst : entity techmap.aoutpad
    generic map (
      tech                      => padtech, 
      limit                     => padlimit
    )
    port map (
      pad                       => padn, 
      i                         => intn
    );
end Behavioral;

