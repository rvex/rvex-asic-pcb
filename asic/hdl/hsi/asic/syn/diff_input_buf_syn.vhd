library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SoC pad library
library techmap;
use techmap.gencomp.all;

-- ASIC SYNTHESIS VERSION

entity diff_input_buf is
  generic (
    -- Technology to use for pads (ASIC wide).
    padtech                     : integer := umc65;
    -- padlimit dictates wether inline or staggered pads are to be used.
    -- Must be one of 'core_limited' or 'pad_limited'.
    padlimit                    : integer := core_limited
  );
  port (
    padp    : in  std_logic;
    padn    : in  std_logic;
    intp    : out std_logic;
    intn    : out std_logic
  );
end diff_input_buf;

architecture Behavioral of diff_input_buf is
begin
  -- Differential input is simply a combination of two pads
  io_input_diff_p_inst : entity techmap.ainpad
    generic map (
      tech                      => padtech,
      limit                     => padlimit
    )
    port map (
      pad                       => padp,
      o                         => intp
    );
    
  io_input_diff_n_inst : entity techmap.ainpad
    generic map (
      tech                      => padtech,
      limit                     => padlimit
    )
    port map (
      pad                       => padn,
      o                         => intn
    );
end Behavioral;

