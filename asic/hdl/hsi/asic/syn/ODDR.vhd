library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ASIC SYNTHESIS VERSION

entity ODDR is 
  generic (
    -- Same edge or opposite edge mode (see Virtex documentation)
    SAME_EDGE                   : boolean
  );
  port (
    clk                         : in  std_logic;
    D1                          : in  std_logic;
    D2                          : in  std_logic;
    Q                           : out std_logic
  );
end entity ODDR;

architecture Behavioral of ODDR is
  signal D1_int                 : std_logic;
  signal D2_int, D2_int2        : std_logic;
begin
  -- ODDR converts two regular data input signals 'D1' and 'D2' to one Double
  -- Data Rate signal 'Q'. On the rising edge of clk,'Q' holds the data of
  -- 'D1'. On the falling edge of clk, 'Q' holds the data of 'D2'. 
  -- If SAME_EDGE is true, both data inputs are sampled at the rising edge of
  -- clk and output in-order ('D1' first and then 'D2').
  -- If SAME_EDGE is false, 'D1' is sampled at the rising edge and 'D2' at the
  -- falling edge. Both data signals are then forwarded on their respective
  -- edge. This means if timing allows it, 'D2' can be forwarded one cycle
  -- earlier.
  
  
  -- Sample D1 on rising edge of clk
  process (clk) is
  begin
    if rising_edge(clk) then
      D1_int <= D1;
    end if;
  end process;
  
  -- Sample D2 on rising edge if SAME_EDGE is true, or else on falling edge.
  se_gen: if (SAME_EDGE = true) generate
    process (clk) is
    begin
      if rising_edge(clk) then
        D2_int <= D2;
      end if;
    end process;
  end generate;
  -- Sampling will be done on falling edge anyways, so just forward directly.
  de_gen: if (SAME_EDGE = false) generate
    D2_int <= D2;
  end generate;
  
  -- Make sure D2 is never changing during rising edge by registering it on the
  -- falling edge.
  process (clk) is
  begin
    if falling_edge(clk) then
      D2_int2 <= D2_int;
    end if;
  end process;
  
  -- Use the clk to select between 'D1' and 'D2'. During clk high period, 'D1'
  -- should be output, during clk low period 'D2' instead.
  -- TODO delay clk by half a cycle, such that D1 and D2 are stable? Right now
  -- the signals might vary slightly directly after clk switches...
  Q <= D1_int  when clk = '1' else
       D2_int2 when clk = '0';
end Behavioral;
