library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SoC pad library
library techmap;
use techmap.gencomp.all;

library asic;

entity hsi_phy is
  generic (
    -- Technology to use for pads (ASIC wide).
    padtech                     : integer := umc65;
    -- padlimit dictates wether inline or staggered pads are to be used.
    -- Must be one of 'core_limited' or 'pad_limited'.
    padlimit                    : integer := core_limited;
    
    -- Number of differential pairs. Each must be 1, 2 or 4.
    N_RX                        : natural := 4;
    N_TX                        : natural := 4;
    
    -- Number of clocks, may differ for each design
    N_CK                        : natural := 1
    
  );
  port (
    
    -- Core reset signal. This is basically the master reset; if this is
    -- asserted low, everything except the HSI PHY delay calibration is reset.
    -- Note that the core uses a synchronous reset though, so it won't reset
    -- until it gets its first clock somehow.
    pad_aresetn_core            : in  std_logic;
    aresetn_core                : out std_logic;
    
    -- PHY reset signal and UART receive pin B. If asserted low while core
    -- reset is also asserted low, the HSI PHY delay calibration logic is reset
    -- asynchronously in addition to all the other systems. While core reset is
    -- high, this is disconnected from the PHY reset and used as an additional
    -- FPGA to ASIC UART data pin.
    pad_aresetn_phy             : in  std_logic;
    aresetn_phy_uart            : out std_logic;
    
    -- Reset signal for the physical interface before the core.
    aresetn_phy_local           : out std_logic;
    aresetn_phy                 : in  std_logic;
    
    -- Backup clock. This is used in UART mode as a backup to the HSI
    -- interface, in case there is a problem with the PHY or DLL.
    pad_backup_clk              : in  std_logic;
    backup_clk                  : out std_logic;
    
    -- HSI clock pads. The core clock is derived from this when running in HSI
    -- mode: either the core gets the same clock, or the core clock gets HSI
    -- divided by 2.
    pad_hsi_clk_p               : in  std_logic_vector(N_CK-1 downto 0);
    pad_hsi_clk_n               : in  std_logic_vector(N_CK-1 downto 0);
    diff_clk                    : out std_logic;
    
    -- Reference current pads.
    pad_Iref_tx                 : in  std_logic_vector(N_TX-1 downto 0);
    pad_Iref_rx                 : out std_logic_vector(N_RX-1 downto 0);
    
    -- HSI receiver pads.
    pad_f2a_p                   : in  std_logic_vector(N_RX-1 downto 0);
    pad_f2a_n                   : in  std_logic_vector(N_RX-1 downto 0);
    
    -- HSI transmitter pads.
    pad_a2f_p                   : out std_logic_vector(N_TX-1 downto 0);
    pad_a2f_n                   : out std_logic_vector(N_TX-1 downto 0);
    
    -- Mode selection pin. This pin does the following things:
    --  - falling edge of aresetn_phy while aresetn_core is low: low selects
    --    backup interface, high selects HSI.
    --  - rising edge of aresetn_phy while aresetn_core is low: low selects
    --    1:2 clock in HSI mode or reliable UART mode, high selects 1:4 clock
    --    in HSI mode or fast UART mode.
    --  - while aresetn_core is high: low for normal operation, high for scan
    --    chain access.
    --  - rising edge of mode pin while aresern_core is low: shift ctrl F2A
    --    value into HSI clock period register LSB.
    --  - rising edge of mode pin while aresern_core is high: shift ctrl F2A
    --    value into scan chain register LSB.
    pad_mode                    : in  std_logic;
    mode                        : out std_logic;
    
    -- FPGA to ASIC control pin. This pin does the following things:
    --  - rising edge of mode pin while aresetn_core is low: shifted into HSI
    --    clock period register LSB.
    --  - rising edge of mode pin while aresetn_core is high: shifted into scan
    --    chain register LSB.
    --  - mode pin high: scan chain input.
    --  - mode pin low, HSI mode: active-high calibration request.
    --  - mode pin low, UART mode: primary UART data (aresetn_phy is
    --    secondary).
    pad_ctrl_f2a                : in  std_logic;
    
    -- ASIC to FPGA control pin. This pin does the following things:
    --  - mode pin high: scan chain output.
    --  - mode pin low, HSI mode: active-high lock output.
    --  - mode pin low, UART mode: UART data.
    pad_ctrl_a2f                : out std_logic;
    
    -- Interconnect between the I/Os and the internal logic.
    f2a_x                       : out std_logic_vector(N_RX-1 downto 0);
    f2a_y                       : out std_logic_vector(N_RX-1 downto 0);
    
    a2f_x                       : in  std_logic_vector(N_TX-1 downto 0);
    a2f_y                       : in  std_logic_vector(N_TX-1 downto 0);
    
    ctrl_f2a                    : out std_logic;
    ctrl_a2f                    : in  std_logic;
    
    
    -- System control signals.
    calib                       : in  std_logic;
    lock                        : out std_logic;
    hsi_period                  : in  std_logic_vector(7 downto 0);
  
    -- Buffer tree outputs.
    reset_hsi                   : in  std_logic;
    clk_hsi                     : in  std_logic;
    clk_core                    : in  std_logic
    
  );
end hsi_phy;

architecture Behavioral of hsi_phy is
  
  component SSI
    port (
      IREFTX                    : inout std_logic;
      IREFRX                    : inout std_logic;

      EN                        : in  std_logic;
      RST                       : in  std_logic;
      CK_SYN                    : in  std_logic;
      CK_FSM                    : in  std_logic;
      DIN                       : in  std_logic;
      LVDSCKINP                 : in  std_logic;
      LVDSCKINN                 : in  std_logic;
      LVDSDAINP                 : in  std_logic;
      LVDSDAINN                 : in  std_logic;

      LOCK                      : out std_logic;
      CKOUT                     : out std_logic;
      DOUT                      : out std_logic;
      LVDSDAOUTP                : out std_logic;
      LVDSDAOUTN                : out std_logic
    );
  end component;
  
  component SSI_INVALID end component;

  
  -- Internal control signals
  signal aresetn_phy_uart_local : std_logic;
  signal aresetn_core_local     : std_logic;
  signal aresetn_phy_bar        : std_logic;
  
  -- Internal differential clocks
  signal lock_local             : std_logic_vector(N_RX-1 downto 0);
  
  -- Internal PHY to out signals
  signal a2f_x_int              : std_logic_vector(N_TX-1 downto 0);
  signal a2f_y_int              : std_logic_vector(N_TX-1 downto 0);
begin
  
  -----------------------------------------------------------------------------
  -- Instantiate all the I/O buffers
  -----------------------------------------------------------------------------
  -- Instantiate the reset input buffers.
  aresetn_phy_buf_inst: entity asic.input_buf
    generic map (
      padtech                   => padtech, 
      padlimit                  => padlimit
    )
    port map (
      pad                       => pad_aresetn_phy,
      int                       => aresetn_phy_uart_local
    );
  aresetn_phy_uart <= aresetn_phy_uart_local;
  
  aresetn_core_buf_inst: entity asic.input_buf
    generic map (
      padtech                   => padtech, 
      padlimit                  => padlimit
    )
    port map (
      pad                       => pad_aresetn_core,
      int                       => aresetn_core_local
    );
  aresetn_core <= aresetn_core_local;
    
  -- Activate aresetn_phy only if the core is also being reset.
  aresetn_phy_local <= aresetn_phy_uart_local or aresetn_core_local;
  
  -- Instantiate the mode input buffer.
  mode_buf_inst: entity asic.input_buf
    generic map (
      padtech                   => padtech, 
      padlimit                  => padlimit
    )
    port map (
      pad                       => pad_mode,
      int                       => mode
    );
  
  -- Instantiate the backup clock input buffer.
  backup_clk_buf_inst: entity asic.clk_input_buf
    generic map (
      padtech                   => padtech, 
      padlimit                  => padlimit
    )
    port map (
      pad                       => pad_backup_clk,
      int                       => backup_clk
    );
  
  -- Instantiate the control buffers.
  ctrl_f2a_buf_inst: entity asic.input_buf
    generic map (
      padtech                   => padtech, 
      padlimit                  => padlimit
    )
    port map (
      pad                       => pad_ctrl_f2a,
      int                       => ctrl_f2a
    );
  ctrl_a2f_buf_inst: entity asic.output_buf
    generic map (
      padtech                   => padtech, 
      padlimit                  => padlimit
    )
    port map (
      pad                       => pad_ctrl_a2f,
      int                       => ctrl_a2f
    );

    
    
  -----------------------------------------------------------------------------
  -- HSI physical interface and signal connections
  -----------------------------------------------------------------------------
  -- The new (modular) instance always includes one clock receiver, one data
  -- receiver and one data driver.
  -- For ease of use fix the amount of receivers and drivers to be equal for 
  -- now. Instead we could of course also decide to simply not connect some 
  -- receivers or some drivers.
  
  compile_error: if N_RX /= N_TX generate
    sii_gen: SSI_INVALID;
    assert true
    report "N_RX and N_TX must be equal for this version to work" severity failure;
  end generate;
  
  -- During calibration, we need to inject a 01010101 pattern in the physical
  -- instance. Otherwise, simply feed the x and y signals
  a2f_x_int <= a2f_x when calib = '0' else (others => '0');
  a2f_y_int <= a2f_y when calib = '0' else (others => '1');
  
  -- aresetn_phy is an active low reset. However the SSI requires
  -- a (synchronous) active high reset. Invert it.
  aresetn_phy_bar <= not aresetn_phy;
  
  -- 'N_RX' and 'N_TX' must be equal here, generate that many HSI instances and
  -- accompanying input/output pads.
  ssi_gen: for inst in 0 to N_RX-1 generate
    ssi_block: block
      -- Biasing circuit signals
      signal Iref_rx, Iref_tx       : std_logic;
      signal f2a_p, f2a_n           : std_logic;
      signal a2f_p, a2f_n           : std_logic;
      signal f2a, a2f               : std_logic;
      signal hsi_clk_p, hsi_clk_n   : std_logic;
      signal diff_clk_local         : std_logic;
    begin
      -- Analog driver input reference current.
      io_Iref_tx: entity techmap.ainpad
        generic map (
          tech                      => padtech, 
          limit                     => padlimit
        )
        port map (
          pad                       => pad_Iref_tx(inst),
          o                         => Iref_tx
        );
      -- Analog receiver input reference current.
      io_Iref_rx: entity techmap.aoutpad  
        generic map (
          tech                      => padtech, 
          limit                     => padlimit
        )
        port map (
          pad                       => pad_Iref_rx(inst),
          i                         => Iref_rx
        );
    
    
    -- Only connect N_CK clock pairs
    diff_clk_buf_gen: if inst < N_CK generate
      -- Instantiate the differential clock receiver.
      clk_buf_inst: entity asic.diff_input_buf
        generic map (
          padtech                   => padtech,
          padlimit                  => padlimit
        )
        port map (
          padp                      => pad_hsi_clk_p(inst),
          padn                      => pad_hsi_clk_n(inst),
          intp                      => hsi_clk_p,
          intn                      => hsi_clk_n
        );
      end generate;
      -- The other connections are don't care. These should be connected
      -- to VDDANA25
      diff_no_clk_buf_gen: if inst >= N_CK generate
        hsi_clk_p <= '1';
        hsi_clk_n <= '1';
      end generate;
      
    
      -- Instantiate the HSI receiver buffers/pins
      recv_buf_inst: entity asic.diff_input_buf
        generic map (
          padtech                 => padtech,
          padlimit                => padlimit
        )
        port map (
          padp                    => pad_f2a_p(inst),
          padn                    => pad_f2a_n(inst),
          intp                    => f2a_p,
          intn                    => f2a_n
        );
      
      -- Instantiate a DDR output to convert a2f data from x and y.
      driv_ddr_inst: entity asic.ODDR
        generic map (
          SAME_EDGE               => false
        )
        port map (
          clk                     => clk_hsi,
          D1                      => a2f_y_int(inst),
          D2                      => a2f_x_int(inst),
          Q                       => a2f
        );
      
      -- Instantiate the actual HSI physical instance itself
      ssi_inst: SSI
        port map (
          IREFTX                  => Iref_tx,
          IREFRX                  => Iref_rx,
      
          EN                      => calib,
          RST                     => aresetn_phy_bar,
          CK_SYN                  => clk_hsi,
          CK_FSM                  => clk_hsi,
          DIN                     => a2f,
          LVDSCKINP               => hsi_clk_p,
          LVDSCKINN               => hsi_clk_n,
          LVDSDAINP               => f2a_p,
          LVDSDAINN               => f2a_n,
      
          LOCK                    => lock_local(inst),
          CKOUT                   => diff_clk_local,
          DOUT                    => f2a,
          LVDSDAOUTP              => a2f_p,
          LVDSDAOUTN              => a2f_n
        );
        
      -- Output the first differential clock for the rest of the chip
      diff_clk_out_gen: if inst = 0 generate
        diff_clk <= diff_clk_local;
      end generate;
      
      
      -- Instantiate a DDR input to convert f2a data to x and y.
      recv_ddr_inst: entity asic.IDDR
        port map (
          clk                     => clk_hsi,
          D                       => f2a,
          Q1                      => f2a_y(inst),
          Q2                      => f2a_x(inst)
        );
    
      -- Instantiate the HSI driver buffers/pins
      driv_buf_inst: entity asic.diff_output_buf
        generic map (
          padtech                 => padtech,
          padlimit                => padlimit
        )
        port map (
          padp                    => pad_a2f_p(inst),
          padn                    => pad_a2f_n(inst),
          intp                    => a2f_p,
          intn                    => a2f_n
        );
    end block;
  end generate;
  
  
  -- Lock is true iff all instances are locked
  process (lock_local)
    variable lock_v : std_logic;
  begin
    lock_v := '1';
    for i in 0 to N_RX-1 loop
        lock_v := lock_v and lock_local(i);
    end loop;
    lock <= lock_v;
 end process;
  
end Behavioral;
