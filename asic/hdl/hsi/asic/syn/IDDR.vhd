library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ASIC SYNTHESIS VERSION

entity IDDR is 
  port (
    clk                         : in  std_logic;
    D                           : in  std_logic;
    Q1                          : out std_logic;
    Q2                          : out std_logic
  );
end entity IDDR;

architecture Behavioral of IDDR is 
  signal Q2_int                 : std_logic;
begin
  -- IDDR converts a Double Data Rate input signal 'D' to two regular signals
  -- 'Q1' and 'Q2'. 'Q1' is written with the data of 'D' on the rising edge of
  -- 'clk'. 'Q2' is written with the falling edge data.
  -- This unit outputs both Q synchronously on the same clock edge. This means
  -- First 'Q1' is read and written in the same clock cycle. Then 'Q2' is read,
  -- 'Q1' is read and both are written on the next clock cycle etc.
  
  -- Register input data for output 'Q2' on the falling edge of clk:
  process (clk) is
  begin
    if falling_edge(clk) then
      Q2_int <= D;
    end if;
  end process;
  
  -- Register input data for output 'Q1' and output both data on the rising
  -- edge of clk.
  process (clk) is
  begin
    if rising_edge(clk) then
      Q1 <= D;
      Q2 <= Q2_int;
    end if;
  end process;
end Behavioral;
