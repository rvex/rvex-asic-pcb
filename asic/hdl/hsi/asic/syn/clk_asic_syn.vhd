library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SoC pad library
library techmap;
use techmap.gencomp.all;

-- ASIC SYNTHESIS VERSION

entity clk_asic is
  port (
    diff_clk                    : in  std_logic;
    backup_clk                  : in  std_logic;
    ifsel                       : in  std_logic;
    ratio                       : in  std_logic;
    reset_core_local            : in  std_logic;
    reset_mem_local             : in  std_logic;
    reset_hsi_local             : in  std_logic;
    aresetn_phy_local           : in  std_logic;
    scan_enable_local           : in  std_logic;
    
    clk_hsi                     : out std_logic;
    clk_hsi_local               : out std_logic;
    clk_core                    : out std_logic;
    clk_core_local              : out std_logic;
    reset_core                  : out std_logic;
    reset_mem                   : out std_logic;
    reset_hsi                   : out std_logic;
    aresetn_phy                 : out std_logic;
    scan_enable                 : out std_logic
  );
end clk_asic;

architecture Behavioral of clk_asic is
  
  signal diff_clk_div_cnt       : unsigned(1 downto 0);
  
  signal diff_clk_div1          : std_logic;
  signal diff_clk_div2          : std_logic;
  signal diff_clk_div4          : std_logic;
  
  signal clk_core_buf           : std_logic;
  signal clk_hsi_buf            : std_logic;
  
begin
  
  -- Divide the differential input clock by 1, 2, and 4.
  diff_clk_div_p: process(diff_clk, aresetn_phy_local)
  begin
    -- Asynchronous reset to make sure we have an initial value for our clock.
    if aresetn_phy_local = '0' then
      diff_clk_div_cnt <= (others => '0');
    elsif rising_edge(diff_clk) then
      diff_clk_div_cnt <= diff_clk_div_cnt + 1;
    end if;
  end process;
  
  diff_clk_div1 <= diff_clk;
  diff_clk_div2 <= diff_clk_div_cnt(0);
  diff_clk_div4 <= diff_clk_div_cnt(1);
  
  
  -- ratio clock selection stage.
  diff_clk_div_sel: process(ifsel, ratio, backup_clk, 
                            diff_clk_div1, diff_clk_div2, diff_clk_div4)
  begin
    if ifsel = '0' then
      clk_core_buf <= backup_clk;
      clk_hsi_buf  <= backup_clk;
    else
      if ratio = '0' then
        clk_core_buf <= diff_clk_div2;
      else
        clk_core_buf <= diff_clk_div4;
      end if;
      clk_hsi_buf  <= diff_clk_div1;
    end if;
  end process;
  
  -- Forward the clocks.
  clk_core_local <= clk_core_buf;
  clk_hsi_local  <= clk_hsi_buf;
  
  -- No need to buffer the resets in the ASIC design.
  clk_hsi     <= clk_hsi_buf;
  reset_hsi   <= reset_hsi_local;
  clk_core    <= clk_core_buf;
  reset_mem   <= reset_mem_local;
  reset_core  <= reset_core_local;
  aresetn_phy <= aresetn_phy_local;
  scan_enable <= scan_enable_local;
  
end Behavioral;

