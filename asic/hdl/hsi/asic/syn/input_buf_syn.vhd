library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- SoC pad library
library techmap;
use techmap.gencomp.all;

-- ASIC SYNTHESIS VERSION

entity input_buf is
  generic (
    -- Technology to use for pads (ASIC wide).
    padtech                     : integer := umc65;
    -- padlimit dictates wether inline or staggered pads are to be used.
    -- Must be one of 'core_limited' or 'pad_limited'.
    padlimit                    : integer := core_limited
  );
  port (
    pad   : in  std_logic;
    int   : out std_logic
  );
end input_buf;

architecture Behavioral of input_buf is
begin
  
  io_input_buf_inst : entity techmap.inpad
    generic map (
      tech                      => padtech, 
      limit                     => padlimit
    )
    port map (
      pad                       => pad, 
      o                         => int
    );
  
end Behavioral;

