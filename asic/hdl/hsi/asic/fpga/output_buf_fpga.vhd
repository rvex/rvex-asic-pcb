library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

entity output_buf is
  port (
    int   : in  std_logic;
    pad   : out std_logic
  );
end output_buf;

architecture Behavioral of output_buf is
begin
  
  obuf_inst: OBUF
    generic map (
      DRIVE       => 12,
      IOSTANDARD  => "LVCMOS25",
      SLEW        => "SLOW"
    )
    port map (
      I => int,
      O => pad
    );
  
end Behavioral;

