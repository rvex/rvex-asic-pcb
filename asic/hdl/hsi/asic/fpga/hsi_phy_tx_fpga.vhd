library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

entity hsi_phy_tx is
  generic (
    
    -- Number of differential pairs.
    N                           : natural := 4
    
  );
  port (
    
    -- System control.
    reset_hsi                   : in  std_logic;
    calib_hsi                   : in  std_logic;
    clk_hsi                     : in  std_logic;
    
    -- Data input. X is transmitted while clk_hsi is high, Y is transmitted
    -- while it is low.
    dll2phy_x                   : in  std_logic_vector(N-1 downto 0);
    dll2phy_y                   : in  std_logic_vector(N-1 downto 0);
    
    -- Data output.
    phy2pad_p                   : out std_logic_vector(N-1 downto 0);
    phy2pad_n                   : out std_logic_vector(N-1 downto 0)
    
  );
end hsi_phy_tx;

architecture Behavioral of hsi_phy_tx is
begin
  sig_gen: for i in 0 to N-1 generate
    
    -- The incoming positive-edge-aligned parallelized DDR data signals.
    signal x, y                 : std_logic;
    
    -- The DDR data signal coming from the ODDR.
    signal d                    : std_logic;
    
  begin
    
    -- Override x and y when calibrating.
    x <= dll2phy_x(i) when calib_hsi = '0' else '0';
    y <= dll2phy_y(i) when calib_hsi = '0' else '1';
    
    -- Instantiate the DDR unit.
    diff_clk_ddr: ODDR
      port map (
        Q  => d,
        C  => clk_hsi,
        CE => '1',
        D1 => y,
        D2 => x,
        R  => '0',
        S  => '0'
      );
    
    -- Instantiate the output buffer.
    diff_clk_pad: OBUFDS
      generic map (
        IOSTANDARD => "LVDS_25"
      )
      port map (
        I  => d,
        O  => phy2pad_p(i),
        OB => phy2pad_n(i)
      );
    
  end generate;
end Behavioral;

