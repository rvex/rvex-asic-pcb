library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

entity clk_asic is
  port (
    diff_clk                    : in  std_logic;
    backup_clk                  : in  std_logic;
    ifsel                       : in  std_logic;
    ratio                       : in  std_logic;
    reset_core_local            : in  std_logic;
    reset_mem_local             : in  std_logic;
    reset_hsi_local             : in  std_logic;
    aresetn_phy_local           : in  std_logic;
    scan_enable_local           : in  std_logic;
    
    clk_hsi                     : out std_logic;
    clk_hsi_local               : out std_logic;
    clk_core                    : out std_logic;
    clk_core_local              : out std_logic;
    reset_core                  : out std_logic;
    reset_mem                   : out std_logic;
    reset_hsi                   : out std_logic;
    aresetn_phy                 : out std_logic;
    scan_enable                 : out std_logic
  );
end clk_asic;

architecture Behavioral of clk_asic is
  
  signal diff_clk_div1          : std_logic;
  signal diff_clk_div2          : std_logic;
  signal diff_clk_div4          : std_logic;
  
  signal diff_clk_direct        : std_logic;
  signal diff_clk_div           : std_logic;
  
  signal clk_core_buf           : std_logic;
  signal clk_hsi_buf            : std_logic;
  
begin
  
  -- Divide the differential input clock by 1, 2, and 4.
  diff_clk_div1_inst: BUFR
    generic map (
      BUFR_DIVIDE => "1",
      SIM_DEVICE => "VIRTEX6"
    )
    port map (
      CE  => '1',
      CLR => '0',
      I   => diff_clk,
      O   => diff_clk_div1
    );
  
  diff_clk_div2_inst: BUFR
    generic map (
      BUFR_DIVIDE => "2",
      SIM_DEVICE => "VIRTEX6"
    )
    port map (
      CE  => '1',
      CLR => '0',
      I   => diff_clk,
      O   => diff_clk_div2
    );
  
  diff_clk_div4_inst: BUFR
    generic map (
      BUFR_DIVIDE => "4",
      SIM_DEVICE => "VIRTEX6"
    )
    port map (
      CE  => '1',
      CLR => '0',
      I   => diff_clk,
      O   => diff_clk_div4
    );
  
  -- ratio clock selection stage.
  diff_clk_direct_inst: BUFG
    port map (
      I  => diff_clk_div1,
      O  => diff_clk_direct
    );
  
  diff_clk_div_inst: BUFGMUX
    port map (
      I0 => diff_clk_div2,
      I1 => diff_clk_div4,
      S  => ratio,
      O  => diff_clk_div
    );
  
  -- ifsel clock selection stage.
  clk_core_buf_inst: BUFGMUX
    port map (
      I0 => backup_clk,
      I1 => diff_clk_div,
      S  => ifsel,
      O  => clk_core_buf
    );
  
  clk_hsi_buf_inst: BUFGMUX
    port map (
      I0 => backup_clk,
      I1 => diff_clk_direct,
      S  => ifsel,
      O  => clk_hsi_buf
    );
  
  -- Forward the clocks.
  clk_core_local <= clk_core_buf;
  clk_hsi_local  <= clk_hsi_buf;
  
  -- No need to buffer the resets in the FPGA design.
  clk_hsi     <= clk_hsi_buf;
  reset_hsi   <= reset_hsi_local;
  clk_core    <= clk_core_buf;
  reset_mem   <= reset_mem_local;
  reset_core  <= reset_core_local;
  aresetn_phy <= aresetn_phy_local;
  scan_enable <= scan_enable_local;
  
end Behavioral;

