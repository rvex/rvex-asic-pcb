library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- FPGA VERSION

entity hsi_clk_rx is
  port (
    clk_p                       : in  std_logic;
    clk_n                       : in  std_logic;
    clk_out                     : out std_logic
  );
end hsi_clk_rx;

architecture Behavioral of hsi_clk_rx is
begin
  
  -- Instantiate the LVDS IBUF.
  IBUFDS_inst : IBUFGDS
    generic map (
      DIFF_TERM     => true,
      IBUF_LOW_PWR  => false,
      IOSTANDARD    => "LVDS_25"
    )
    port map (
      O  => clk_out,
      I  => clk_p,
      IB => clk_n
    );
  
end Behavioral;

