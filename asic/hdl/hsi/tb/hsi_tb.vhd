library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.bus_pkg.all;

entity hsi_tb is
end hsi_tb;

architecture Behavioral of hsi_tb is
  
  constant N_F2A                    : natural := 1;
  constant N_A2F                    : natural := 1;
  
  constant TLINE_DELAY              : time := 1 ns;
  
  signal fpga_reset                 : std_logic := '1';
  signal fpga_clk                   : std_logic := '1';
  
  signal asic_reset                 : std_logic;
  signal asic_clk                   : std_logic;
  
  signal core2mem_fpga              : bus_mst2slv_type;
  signal mem2core_fpga              : bus_slv2mst_type;
  signal core2mem_asic              : bus_mst2slv_type;
  signal mem2core_asic              : bus_slv2mst_type;
  signal dbg2core_fpga              : bus_mst2slv_type;
  signal core2dbg_fpga              : bus_slv2mst_type;
  signal dbg2core_asic              : bus_mst2slv_type;
  signal core2dbg_asic              : bus_slv2mst_type;
  signal irq_fpga                   : std_logic_vector(15 downto 0);
  signal irq_asic                   : std_logic_vector(15 downto 0);
  signal done_fpga                  : std_logic_vector(3 downto 0);
  signal done_asic                  : std_logic_vector(3 downto 0);
  signal idle_fpga                  : std_logic_vector(3 downto 0);
  signal idle_asic                  : std_logic_vector(3 downto 0);

  signal sig_aresetn_phy_fpga       : std_logic;
  signal sig_aresetn_phy_asic       : std_logic;
  signal sig_aresetn_core_fpga      : std_logic;
  signal sig_aresetn_core_asic      : std_logic;
  signal sig_mode_fpga              : std_logic;
  signal sig_mode_asic              : std_logic;
  signal sig_diff_clk_p_fpga        : std_logic;
  signal sig_diff_clk_p_asic        : std_logic;
  signal sig_diff_clk_n_fpga        : std_logic;
  signal sig_diff_clk_n_asic        : std_logic;
  signal sig_f2a_p_fpga             : std_logic_vector(N_F2A-1 downto 0);
  signal sig_f2a_p_asic             : std_logic_vector(N_F2A-1 downto 0);
  signal sig_f2a_n_fpga             : std_logic_vector(N_F2A-1 downto 0);
  signal sig_f2a_n_asic             : std_logic_vector(N_F2A-1 downto 0);
  signal sig_a2f_p_fpga             : std_logic_vector(N_A2F-1 downto 0);
  signal sig_a2f_p_asic             : std_logic_vector(N_A2F-1 downto 0);
  signal sig_a2f_n_fpga             : std_logic_vector(N_A2F-1 downto 0);
  signal sig_a2f_n_asic             : std_logic_vector(N_A2F-1 downto 0);
  signal sig_backup_clk_fpga        : std_logic;
  signal sig_backup_clk_asic        : std_logic;
  signal sig_backup_f2a_calib_fpga  : std_logic;
  signal sig_backup_f2a_calib_asic  : std_logic;
  signal sig_backup_a2f_lock_fpga   : std_logic;
  signal sig_backup_a2f_lock_asic   : std_logic;

begin
  
  -- Generate the FPGA clock and reset.
  clk_proc: process is
  begin
    wait for 5 ns;
    fpga_clk <= '0';
    wait for 5 ns;
    fpga_clk <= '1';
  end process;
  
  reset_proc: process is
  begin
    wait until rising_edge(fpga_clk);
    wait until rising_edge(fpga_clk);
    wait until rising_edge(fpga_clk);
    wait until rising_edge(fpga_clk);
    wait until rising_edge(fpga_clk);
    fpga_reset <= '0';
    wait;
  end process;
  
  -- FPGA stimuli.
  dbg2core_fpga <= BUS_MST2SLV_IDLE;
  irq_fpga <= "0000000000000000";
  
  fpga_mem_mock: entity asic.bus_ramBlock_singlePort
    generic map (
      DEPTH_LOG2B               => 12
    )
    port map (
      reset                     => fpga_reset,
      clk                       => fpga_clk,
      clkEn                     => '1',
      mst2mem_port              => core2mem_fpga,
      mem2mst_port              => mem2core_fpga
    );
  
  -- Instantiate the FPGA side of the HSI.
  uut_fpga: entity work.hsi_fpga
    generic map (
      N_RX                      => N_A2F,
      N_TX                      => N_F2A
    )
    port map (
      
      -- AHB clock and reset (100 MHz).
      reset                     => fpga_reset,
      clk_100MHz                => fpga_clk,
      
      -- Memory bus from the ASIC.
      core2mem                  => core2mem_fpga,
      mem2core                  => mem2core_fpga,
      
      -- Debug bus to the FPGA.
      dbg2core                  => dbg2core_fpga,
      core2dbg                  => core2dbg_fpga,
      
      -- GPIO-like stuff transferred over the HSI interface.
      irq                       => irq_fpga,
      done                      => done_fpga,
      idle                      => idle_fpga,
      
      -- Interconnect signals.
      pad_aresetn_phy           => sig_aresetn_phy_fpga,
      pad_aresetn_core          => sig_aresetn_core_fpga,
      pad_mode                  => sig_mode_fpga,
      pad_diff_clk_p            => sig_diff_clk_p_fpga,
      pad_diff_clk_n            => sig_diff_clk_n_fpga,
      pad_f2a_p                 => sig_f2a_p_fpga,
      pad_f2a_n                 => sig_f2a_n_fpga,
      pad_a2f_p                 => sig_a2f_p_fpga,
      pad_a2f_n                 => sig_a2f_n_fpga,
      pad_backup_clk            => sig_backup_clk_fpga,
      pad_backup_f2a_calib      => sig_backup_f2a_calib_fpga,
      pad_backup_a2f_lock       => sig_backup_a2f_lock_fpga
      
    );
  
  -- Instantiate the transmission lines.
  sig_aresetn_phy_asic      <= transport sig_aresetn_phy_fpga       after TLINE_DELAY;
  sig_aresetn_core_asic     <= transport sig_aresetn_core_fpga      after TLINE_DELAY;
  sig_mode_asic             <= transport sig_mode_fpga              after TLINE_DELAY;
  sig_diff_clk_p_asic       <= transport sig_diff_clk_p_fpga        after TLINE_DELAY;
  sig_diff_clk_n_asic       <= transport sig_diff_clk_n_fpga        after TLINE_DELAY;
  sig_f2a_p_asic            <= transport sig_f2a_p_fpga             after TLINE_DELAY;
  sig_f2a_n_asic            <= transport sig_f2a_n_fpga             after TLINE_DELAY;
  sig_a2f_p_fpga            <= transport sig_a2f_p_asic             after TLINE_DELAY;
  sig_a2f_n_fpga            <= transport sig_a2f_n_asic             after TLINE_DELAY;
  sig_backup_clk_asic       <= transport sig_backup_clk_fpga        after TLINE_DELAY;
  sig_backup_f2a_calib_asic <= transport sig_backup_f2a_calib_fpga  after TLINE_DELAY;
  sig_backup_a2f_lock_fpga  <= transport sig_backup_a2f_lock_asic   after TLINE_DELAY;
  
  -- Instantiate the ASIC side of the HSI.
  uut_asic: entity work.hsi_asic
    generic map (
      N_RX                      => N_F2A,
      N_TX                      => N_A2F
    )
    port map (
      
      -- System control for the core.
      reset                     => asic_reset,
      clk                       => asic_clk,
      
      -- Scan interface.
      scan_enable               => open,
      scan_in                   => open,
      scan_out                  => (others => '0'),
      
      -- Memory bus to FPGA.
      core2mem                  => core2mem_asic,
      mem2core                  => mem2core_asic,
      
      -- Debug bus from FPGA.
      dbg2core                  => dbg2core_asic,
      core2dbg                  => core2dbg_asic,
      
      -- GPIO-like stuff transferred over the HSI interface.
      irq                       => irq_asic,
      done                      => done_asic,
      idle                      => idle_asic,
      
      -- interconnect signals.
      pad_aresetn_phy           => sig_aresetn_phy_asic,
      pad_aresetn_core          => sig_aresetn_core_asic,
      pad_mode                  => sig_mode_asic,
      pad_diff_clk_p            => sig_diff_clk_p_asic,
      pad_diff_clk_n            => sig_diff_clk_n_asic,
      pad_f2a_p                 => sig_f2a_p_asic,
      pad_f2a_n                 => sig_f2a_n_asic,
      pad_a2f_p                 => sig_a2f_p_asic,
      pad_a2f_n                 => sig_a2f_n_asic,
      pad_backup_clk            => sig_backup_clk_asic,
      pad_backup_f2a_calib      => sig_backup_f2a_calib_asic,
      pad_backup_a2f_lock       => sig_backup_a2f_lock_asic
      
    );
  
  -- ASIC stimuli.
  core2mem_asic <= (
    address                     => (others => '0'),
    readEnable                  => not asic_reset,
    writeEnable                 => '0',
    writeMask                   => "1111",
    writeData                   => (others => '0'),
    flags                       => (
      burstEnable               => '1',
      burstStart                => '1',
      lock                      => '1'
    )
  );
  done_asic <= "0000";
  idle_asic <= "1111";
  
  asic_dbg_mock: entity asic.bus_ramBlock_singlePort
    generic map (
      DEPTH_LOG2B               => 12
    )
    port map (
      reset                     => asic_reset,
      clk                       => asic_clk,
      clkEn                     => '1',
      mst2mem_port              => dbg2core_asic,
      mem2mst_port              => core2dbg_asic
    );
  
  
end Behavioral;
