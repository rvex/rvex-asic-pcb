library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.simUtils_pkg.all;

entity hsi_uart_tb is
end hsi_uart_tb;

architecture Behavioral of hsi_uart_tb is
  signal reset_core             : std_logic;
  signal clk_core               : std_logic;
  signal clkEn_core             : std_logic;
  signal uart                   : std_logic_vector(1 downto 0);
  signal uart_mode              : std_logic;
  signal uart_edge              : std_logic_vector(1 downto 0);
  signal tx_data                : std_logic_vector(31 downto 0);
  signal tx_first               : std_logic;
  signal tx_valid               : std_logic;
  signal tx_ack                 : std_logic;
  signal rx_data                : std_logic_vector(31 downto 0);
  signal rx_first               : std_logic;
  signal rx_valid               : std_logic;
begin
  
  clk_proc: process is
  begin
    wait for 5 ns;
    clk_core <= '0';
    wait for 5 ns;
    clk_core <= '1';
  end process;
  
  reset_proc: process is
  begin
    reset_core <= '1';
    clkEn_core <= '1';
    wait until rising_edge(clk_core);
    wait until rising_edge(clk_core);
    wait until rising_edge(clk_core);
    wait until rising_edge(clk_core);
    wait until rising_edge(clk_core);
    reset_core <= '0';
    wait;
  end process;
  
  uart_mode <= '1';
  uart_edge <= "00";
  
  stim_proc: process is
    procedure send(
      word  : std_logic_vector(31 downto 0);
      first : std_logic
    ) is
    begin
      tx_data <= word;
      tx_first <= first;
      tx_valid <= '1';
      wait until rising_edge(clk_core) and clkEn_core = '1' and tx_ack = '1';
      tx_data <= (others => 'U');
      tx_first <= 'U';
      tx_valid <= '0';
    end send;
    procedure waitc(
      cycles : natural
    ) is
    begin
      for i in 1 to cycles loop
        wait until rising_edge(clk_core) and clkEn_core = '1';
      end loop;
    end waitc;
  begin
    wait until reset_core = '0';
    loop
      waitc(50);
      --send("HHHHHHHHLLLLLLLLHHHHLLLLHHLLHLHL", '1');
      send(X"12345678", '1');
      send(X"DEADC0DE", '0');
      send(X"F00DF00D", '1');
    end loop;
  end process;
  
--  uut_tx: entity work.hsi_uart_fpga_tx
--    port map (
--      reset_core                => reset_core,
--      clk_core                  => clk_core,
--      clkEn_core                => clkEn_core,
--      uart_tx                   => uart,
--      uart_mode                 => uart_mode,
--      uart_setupEdge            => uart_edge,
--      tl2dll_data               => tx_data,
--      tl2dll_first              => tx_first,
--      tl2dll_valid              => tx_valid,
--      dll2tl_ack                => tx_ack
--    );
--  
--  uut_rx: entity work.hsi_uart_asic_rx
--    port map (
--      reset_core                => reset_core,
--      clk_core                  => clk_core,
--      uart_rx                   => uart,
--      uart_mode                 => uart_mode,
--      uart2tl_data              => rx_data,
--      uart2tl_first             => rx_first,
--      uart2tl_valid             => rx_valid
--    );
  
  uut_tx: entity work.hsi_uart_asic_tx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      uart_tx                   => uart(0),
      uart_mode                 => uart_mode,
      tl2dll_data               => tx_data,
      tl2dll_first              => tx_first,
      tl2dll_valid              => tx_valid,
      dll2tl_ack                => tx_ack
    );
  
  uut_rx: entity work.hsi_uart_fpga_rx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      clkEn_core                => clkEn_core,
      uart_rx                   => uart(0),
      uart_mode                 => uart_mode,
      uart_sampleEdge           => uart_edge(0),
      uart2tl_data              => rx_data,
      uart2tl_first             => rx_first,
      uart2tl_valid             => rx_valid
    );
  
  rx_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if rx_valid = '1' then
        if rx_first = '1' then
          dumpStdOut("");
        end if;
        dumpStdOut("rx " & rvs_hex(rx_data, 8));
      end if;
    end if;
  end process;
  
end Behavioral;
