library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.simUtils_pkg.all;

entity hsi_dll_rx_tb is
end hsi_dll_rx_tb;

architecture Behavioral of hsi_dll_rx_tb is
  constant TTD_UNIT_DELAY : time := 120 ps;
  constant TTD_NUM_BITS   : natural := 6;
  constant N              : natural := 4;
  constant NUM_BITS       : natural := 32;
  
  constant hsi_period     : time := 2.5 ns;
  constant core_period    : time := 10 ns;
  signal aresetn_phy      : std_logic := '0';
  signal reset_hsi        : std_logic := '1';
  signal clk_hsi          : std_logic := '1';
  signal clk_core         : std_logic := '1';
  signal calib            : std_logic := '0';
  signal lock             : std_logic;
  signal data             : std_logic_vector(N-1 downto 0);
  signal data_delayed     : std_logic_vector(N-1 downto 0);
  signal data_inverted    : std_logic_vector(N-1 downto 0);
  signal phy2dll_X        : std_logic_vector(N-1 downto 0);
  signal phy2dll_Y        : std_logic_vector(N-1 downto 0);
  signal dll2tl_data      : std_logic_vector(NUM_BITS-1 downto 0);
  signal dll2tl_first     : std_logic;
  signal dll2tl_valid     : std_logic;
  
begin
  
  clk_hsi_proc: process is
  begin
    wait for hsi_period / 2;
    clk_hsi <= '0';
    wait for hsi_period / 2;
    clk_hsi <= '1';
  end process;
  
  clk_core_proc: process is
  begin
    wait for core_period / 2;
    clk_core <= '0';
    wait for core_period / 2;
    clk_core <= '1';
  end process;
  
  stim_proc: process is
    
    procedure send(
      word  : std_logic_vector(N*8-1 downto 0);
      first : boolean
    ) is
    begin
      if first then
        data <= (others => '0');
        wait for hsi_period/2;
      end if;
      data <= (others => '1');
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+7);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+5);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+6);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+4);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+3);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+1);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+2);
      end loop;
      wait for hsi_period/2;
      for i in N-1 downto 0 loop
        data(i) <= word(i*8+0);
      end loop;
      wait for hsi_period/2;
      data <= (others => '0');
    end send;
    
    variable data_v : std_logic_vector(N*8-1 downto 0);
    
  begin
    data <= (others => '0');
    aresetn_phy <= '0';
    reset_hsi <= '1';
    calib <= '0';
    wait for hsi_period * 3;
    aresetn_phy <= '1';
    wait for hsi_period * 3;
    calib <= '1';
    wait for hsi_period * 3;
    loop
      data <= (others => '1');
      wait for hsi_period/2;
      data <= (others => '0');
      wait for hsi_period/2;
      exit when calib = '1';
    end loop;
    data <= (others => '1');
    wait for hsi_period/2;
    data <= (others => '0');
    wait for hsi_period/2;
    data <= (others => '1');
    wait for hsi_period/2;
    data <= (others => '0');
    wait for hsi_period/2;
    wait for hsi_period * 3;
    calib <= '0';
    wait for hsi_period * 3;
    reset_hsi <= '0';
    wait for hsi_period * 3;
    data_v := X"12345678";
    loop
      for i in 0 to 4 loop
        send(data_v, i = 0);
        data_v := std_logic_vector(unsigned(data_v) + 1);
      end loop;
      for i in 0 to 4 loop
        send(data_v, i = 0);
        data_v := std_logic_vector(unsigned(data_v) + 1);
      end loop;
      for i in 0 to 4 loop
        send(data_v, i = 0);
        data_v := std_logic_vector(unsigned(data_v) + 1);
      end loop;
      wait for hsi_period * 3;
    end loop;
  end process;
  
  data_delayed(0) <= transport data(0) after 1 ns;
  data_delayed(1) <= transport data(1) after 2 ns;
  data_delayed(2) <= transport data(2) after 3 ns;
  data_delayed(3) <= transport data(3) after 4 ns;
  data_inverted <= not data_delayed;
  
  uut_phy_rx: entity work.hsi_phy_rx
    generic map (
      TTD_UNIT_DELAY            => TTD_UNIT_DELAY,
      TTD_NUM_BITS              => TTD_NUM_BITS,
      N                         => N
    )
    port map (
      aresetn_phy               => aresetn_phy,
      clk_hsi                   => clk_hsi,
      calib                     => calib,
      lock                      => lock,
      pad2phy_p                 => data_delayed,
      pad2phy_n                 => data_inverted,
      phy2dll_x                 => phy2dll_X,
      phy2dll_y                 => phy2dll_Y
    );
  
  uut_dll_rx: entity work.hsi_dll_rx
    generic map (
      N                         => N,
      NUM_BITS                  => NUM_BITS
    )
    port map (
      reset_hsi                 => reset_hsi,
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core,
      phy2dll_X                 => phy2dll_X,
      phy2dll_Y                 => phy2dll_Y,
      dll2tl_data               => dll2tl_data,
      dll2tl_first              => dll2tl_first,
      dll2tl_valid              => dll2tl_valid
    );
  
  dump_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if dll2tl_valid = '1' then
        dumpStdOut("Received " & rvs_hex(dll2tl_data) & ", first = " & std_logic'image(dll2tl_first));
      end if;
    end if;
  end process;
  
end Behavioral;

