library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library asic;
use asic.bus_pkg.all;

library work;
use work.hsi_pkg.all;

-- pragma translate_off
use work.simUtils_pkg.all;
-- pragma translate_on

entity hsi_common is
  generic (
    
    -- Number of differential pairs. Each must be 1, 2, 4, or 8.
    N_RX                        : natural := 8;
    N_TX                        : natural := 8;
    
    -- Indicates whether this is the "ASIC" or "FPGA" side.
    SIDE                        : string
    
  );
  port (
    
    -- System control.
    reset_hsi                   : in  std_logic;
    reset_core                  : in  std_logic;
    clk_hsi                     : in  std_logic;
    clk_core                    : in  std_logic;
    ifsel                       : in  std_logic;
    
    -- Data inputs and outputs to HSI PHY.
    rx_x                        : in  std_logic_vector(N_RX-1 downto 0);
    rx_y                        : in  std_logic_vector(N_RX-1 downto 0);
    tx_x                        : out std_logic_vector(N_TX-1 downto 0);
    tx_y                        : out std_logic_vector(N_TX-1 downto 0);
    
    -- UART interface.
    uart_rx_data                : in  std_logic_vector(31 downto 0);
    uart_rx_first               : in  std_logic;
    uart_rx_valid               : in  std_logic;
    uart_tx_data                : out std_logic_vector(31 downto 0);
    uart_tx_first               : out std_logic;
    uart_tx_valid               : out std_logic;
    uart_tx_ack                 : in  std_logic;
    
    -- To r-VEX bus master interfave.
    bus_master_m2s              : in  bus_mst2slv_type;
    bus_master_s2m              : out bus_slv2mst_type;
    
    -- To r-VEX bus slave interfave.
    bus_slave_m2s               : out bus_mst2slv_type;
    bus_slave_s2m               : in  bus_slv2mst_type;
    
    -- GPIO data.
    gpio_in                     : in  std_logic_vector(27 downto 0);
    gpio_out                    : out std_logic_vector(27 downto 0)
    
  );
end hsi_common;

architecture Behavioral of hsi_common is
  
  -- Interfaces between link layer and transport layer.
  signal rx_data_hsi            : std_logic_vector(31 downto 0);
  signal rx_data                : std_logic_vector(31 downto 0);
  signal rx_first_hsi           : std_logic;
  signal rx_first               : std_logic;
  signal rx_valid_hsi           : std_logic;
  signal rx_valid               : std_logic;
  
  signal tx_data                : std_logic_vector(31 downto 0);
  signal tx_first               : std_logic;
  signal tx_valid_hsi           : std_logic;
  signal tx_valid               : std_logic;
  signal tx_ack_hsi             : std_logic;
  signal tx_ack                 : std_logic;
  
  -- Transport layer internal signals.
  signal tlbm2tltx_req          : hsi_bus_req_type;
  signal tltx2tlbm_ack          : std_logic;
  signal tlrx2tlbs_req          : hsi_bus_req_type;
  signal tlbs2tltx_resp         : hsi_bus_resp_type;
  signal tlrx2tlbm_resp         : hsi_bus_resp_type;
  
begin
  
  -- Instantiate the link layer protocol handlers.
  rx_link_layer_inst: entity work.hsi_dll_rx
    generic map (
      N                         => N_RX,
      NUM_BITS                  => 32
    )
    port map (
      reset_hsi                 => reset_hsi,
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core,
      phy2dll_X                 => rx_x,
      phy2dll_Y                 => rx_y,
      dll2tl_data               => rx_data_hsi,
      dll2tl_first              => rx_first_hsi,
      dll2tl_valid              => rx_valid_hsi
    );
  
  tx_link_layer_inst: entity work.hsi_dll_tx
    generic map (
      N                         => N_TX,
      NUM_BITS                  => 32
    )
    port map (
      reset_hsi                 => reset_hsi,
      clk_hsi                   => clk_hsi,
      clk_core                  => clk_core,
      tl2dll_data               => tx_data,
      tl2dll_first              => tx_first,
      tl2dll_valid              => tx_valid_hsi,
      dll2tl_ack                => tx_ack_hsi,
      dll2phy_X                 => tx_x,
      dll2phy_Y                 => tx_y
    );
  
  -- Switch between the HSI and the UART based on ifsel.
  rx_data <= rx_data_hsi when ifsel = '1' else uart_rx_data;
  rx_first <= rx_first_hsi when ifsel = '1' else uart_rx_first;
  rx_valid <= rx_valid_hsi when ifsel = '1' else uart_rx_valid;
  uart_tx_data <= tx_data;
  uart_tx_first <= tx_first;
  tx_ack <= tx_ack_hsi when ifsel = '1' else uart_tx_ack;
  tx_valid_hsi <= tx_valid and ifsel;
  uart_tx_valid <= tx_valid and not ifsel;
  
  -- Print activity...
  -- pragma translate_off
--   debug_print_proc: process (clk_core) is
--     function prefix_tx return string is
--     begin
--       if SIDE = "ASIC" then
--         return "";
--       else
--         return "|            |            ";
--       end if;
--     end prefix_tx;
--     function suffix_tx return string is
--     begin
--       if SIDE = "ASIC" then
--         return "            |            |            |";
--       else
--         return "            |";
--       end if;
--     end suffix_tx;
--     function prefix_rx return string is
--     begin
--       if SIDE = "ASIC" then
--         return "|            |            |            ";
--       else
--         return "|            ";
--       end if;
--     end prefix_rx;
--     function suffix_rx return string is
--     begin
--       if SIDE = "ASIC" then
--         return "";
--       else
--         return "            |            |";
--       end if;
--     end suffix_rx;
--   begin
--     if rising_edge(clk_core) then
--       if reset_core = '0' then
--         if tx_ack = '1' then
--           if tx_first = '1' then
--             dumpStdOut(prefix_tx & "|--" & SIDE & "--" & "TX" & "--|" & suffix_tx);
--           end if;
--           dumpStdOut(prefix_tx & "| " & rvs_hex(tx_data) & " |" & suffix_tx);
--         end if;
--         if rx_valid = '1' then
--           if rx_first = '1' then
--             dumpStdOut(prefix_rx & "|--" & SIDE & "--" & "RX" & "--|" & suffix_rx);
--           end if;
--           dumpStdOut(prefix_rx & "| " & rvs_hex(rx_data) & " |" & suffix_rx);
--         end if;
--       end if;
--     end if;
--   end process;
  -- pragma translate_on
  
  -- HSI transport layer transmitter logic.
  hsi_tl_tx_inst: entity work.hsi_tl_tx
    generic map (
      RESP_SR_LEN               => 8 -- TODO: only 1 on ASIC side
    )
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      tl2dll_data               => tx_data,
      tl2dll_first              => tx_first,
      tl2dll_valid              => tx_valid,
      dll2tl_ack                => tx_ack,
      tlbm2tltx_req             => tlbm2tltx_req,
      tltx2tlbm_ack             => tltx2tlbm_ack,
      tlbs2tltx_resp            => tlbs2tltx_resp,
      gpio2tl                   => gpio_in,
      sync_req                  => '0'
    );  
  
  -- HSI transport layer receiver logic.
  hsi_tl_rx_inst: entity work.hsi_tl_rx
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      dll2tl_data               => rx_data,
      dll2tl_first              => rx_first,
      dll2tl_valid              => rx_valid,
      tlrx2tlbs_req             => tlrx2tlbs_req,
      tlrx2tlbm_resp            => tlrx2tlbm_resp,
      tl2gpio                   => gpio_out
    );  
  
  -- HSI transport layer bus master logic.
  hsi_tl_bm_inst: entity work.hsi_tl_bm
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      bm2tl                     => bus_master_m2s,
      tl2bm                     => bus_master_s2m,
      tlbm2tltx_req             => tlbm2tltx_req,
      tltx2tlbm_ack             => tltx2tlbm_ack,
      tlrx2tlbm_resp            => tlrx2tlbm_resp,
      error                     => open
    );  
  
  -- HSI transport layer bus slave logic.
  hsi_tl_bs_inst: entity work.hsi_tl_bs
    port map (
      reset_core                => reset_core,
      clk_core                  => clk_core,
      tl2bs                     => bus_slave_m2s,
      bs2tl                     => bus_slave_s2m,
      tlrx2tlbs_req             => tlrx2tlbs_req,
      tlbs2tltx_resp            => tlbs2tltx_resp,
      error                     => open
    );  
  
end Behavioral;

