library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.hsi_pkg.all;

library asic;
use asic.bus_pkg.all;

entity hsi_tl_bm is
  port (
    
    -- System control.
    reset_core                  : in  std_logic;
    clk_core                    : in  std_logic;
    
    -- r-VEX bus interface.
    bm2tl                       : in  bus_mst2slv_type;
    tl2bm                       : out bus_slv2mst_type;
    
    -- Bus master request.
    tlbm2tltx_req               : out hsi_bus_req_type;
    tltx2tlbm_ack               : in  std_logic;
    
    -- Bus master response.
    tlrx2tlbm_resp              : in  hsi_bus_resp_type;
    
    -- Asserted for one cycle when a response packet is received but no command
    -- or a different command type was given.
    error                       : out std_logic
    
  );
end hsi_tl_bm;

architecture Behavioral of hsi_tl_bm is
  signal pkt_req                : hsi_bus_req_type;
  signal data                   : std_logic_vector(31 downto 0);
  signal fault                  : std_logic;
  signal busy                   : std_logic;
  signal ack                    : std_logic;
begin
  
  -- Registers and logic.
  reg_proc: process (clk_core) is
  begin
    if rising_edge(clk_core) then
      if reset_core = '1' then
        pkt_req.valid <= '0';
        busy <= '0';
        ack <= '0';
        error <= '0';
      else
        ack <= '0';
        error <= '0';
        
        -- Request packet transmission.
        if bus_requesting(bm2tl) = '1' and busy = '0' then
          
          -- Send the packet.
          pkt_req.valid <= '1';
          pkt_req.wr    <= bm2tl.writeEnable;
          pkt_req.mask  <= bm2tl.writeMask;
          pkt_req.burst <= bm2tl.flags.burstEnable;
          pkt_req.addr  <= bm2tl.address(31 downto 2);
          pkt_req.data  <= bm2tl.writeData;
          
          -- Remember that we're busy.
          busy <= '1';
          
        elsif tltx2tlbm_ack = '1' then
          
          -- Stop requesting packet transmission when the request is
          -- acknowledged.
          pkt_req.valid <= '0';
          
        end if;
        
        -- Response packet handling.
        if tlrx2tlbm_resp.valid = '1' then
          if busy = '0' then
            
            -- If we weren't busy, we can't handle response packets.
            error <= '1';
            
          else
            
            -- If this packet has unexpected flags, assert the error signal.
            if (
              (tlrx2tlbm_resp.wr /= pkt_req.wr) or
              (tlrx2tlbm_resp.burst /= pkt_req.burst)
            ) then
              error <= '1';
            end if;
            
            -- Register the data and fault flag and acknowledge the bus
            -- request.
            data <= tlrx2tlbm_resp.data;
            fault <= tlrx2tlbm_resp.fault;
            ack <= '1';
            
            -- Release busy if this is the last beat.
            if tlrx2tlbm_resp.burst = '0' or tlrx2tlbm_resp.last = '1' then
              busy <= '0';
            end if;
            
          end if;
        end if;
        
      end if;
    end if;
  end process;
  
  tlbm2tltx_req <= pkt_req;
  
  -- Drive the bus response.
  bus_resp_proc: process (data, fault, busy, ack) is
  begin
    tl2bm <= BUS_SLV2MST_IDLE;
    tl2bm.readData <= data;
    tl2bm.fault <= fault;
    tl2bm.busy <= busy and not ack;
    tl2bm.ack <= ack;
  end process;
  
end Behavioral;

