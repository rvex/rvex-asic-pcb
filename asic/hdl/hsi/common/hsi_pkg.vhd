-- r-VEX processor
-- Copyright (C) 2008-2016 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2016 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;

package hsi_pkg is
  
  -- Random HSI protocol notes
  -- 
  -- Observation: no assumptions made on phase relationship between channels and
  -- between channel and clock. Therefore, signals that were transmitted on the
  -- same half-clock cycle may not be received simultaneously.
  -- 
  -- Synchronization sequence required on each channel: try to minimize overhead
  -- as much as possible.
  -- 
  -- Considered 8b10b with a control sequence to mark the start of a packet
  --  - While 8b10b can handle byte/10b-symbol synchronization using control
  --    sequences, it cannot do bit-level synchronization out of the box. However,
  --    because 8b10b guarantees edges, sync can be achieved by not having an edge
  --    for a while and then synchronizing with a start bit.
  --  - 2 bits extra for every 8 bits of data = 20% overhead, not counting 10bit
  --    overhead per (small) packet per channel
  --  - relatively complex encoding/decoding logic/lookup tables
  -- 
  -- Observation: UART can also do synchronization at 20% overhead (start bit +
  -- stop bit for every byte) so might as well use that instead to simplify logic.
  -- 
  -- Observation: UART requires both a start and a stop bit to resynchronize after
  -- every byte in case the receiver and transmitter do not share the same clock.
  -- We do share the same clock, therefore we don't need the stop bit.
  -- 
  -- Line-level protocol:
  --  - idle low
  --  - 5bit per flit (2 1/2 HSI clock cycle): 4-bit data with high start bit
  -- 
  -- Example:
  -- _-_-_-_-_-_-_-
  -- 001ABCD1EFGH00
  -- 
  -- Transmit state machine:
  --  - Waits for 4 bits in its input FIFO, then transmits a start bit, followed by
  --    the data.
  --  - FIFO empty -> transmit 0
  -- 
  -- Receive state machine:
  --  - Wait for a 1 bit, then receive 4 bits and stick them in the output FIFO.
  --  - If more than 3 idle bits are received between flits, the receiver logic is
  --    reset up to and including the packet detection logic. In practice this will
  --    happen very often because either side has to wait for a response from the
  --    other side before the next packet can be sent.
  -- 
  -- Because each state machine provides/takes 2 bits per HSI clock cycle (DDR) and
  -- each flit is 2 1/2 clock cycle long, the state machines have separate states for
  -- packets that start at a rising edge and packets that start at a falling edge.
  -- At the receivers this logic is needed anyway because the phase relationship
  -- between clock and data is not known.
  -- 
  -- Each line has its own RX FIFO. Only when the FIFO of every line contains data
  -- should the next-level protocol pop data. This ensures that all data is
  -- resynchronized. If one of the channels misses a flit due to some bit error,
  -- the FIFOs will be reset automatically in the following idle time to prevent
  -- permanent data misalignment.
  -- 
  -- The FIFOs cross the core and HSI clock domain. To keep things simple, the HSI
  -- clock must be 2x as fast as the core clock.
  -- 
  -- After the data link layer, data is abstracted to a wordstream, with slightly
  -- less than one word per core clock cycle. The word size is line count * 4 bits.
  -- Preferred width = 32 bit, as this is the max throughput that the r-VEX busses
  -- can handle, giving the HSI similar (slightly lower) throughput as an r-VEX bus
  -- with a couple stage registers in it. That means 8 lines up and downstream, i.e.
  -- 16 differential pairs, plus one differential pair for the clock.
  -- 
  -- 28 differential pairs are available in the pinout I initially came up with,
  -- leaving plenty of unused pins for analog power supply etc.
  -- 
  -- Because the interface between DLL and TL is a wordstream, it can be easily
  -- rerouted to a UART-style link using whatever the wordsize is as data size
  -- instead of the usual 8. Suggest implementing such a UART as a backup mode in
  -- case the differential pins, HSI logic, cross-clock logic, whatever doesn't
  -- work. This allows the ASIC to retain full functionality, although the bus is
  -- obviously one to two orders of magnitude slower. If we can run from local
  -- memories in the ASIC this is not a problem. Also, for radiation testing, it
  -- would be good to use this interface anyway, because it would be much less
  -- prone to bit errors, running at a lower frequency and having greater voltage
  -- swing.
  -- 
  -- Note that in order to do and receive full benefit, we also need a "backup"
  -- single-ended clock from FPGA to ASIC. Otherwise we lose the property that we
  -- could still use the ASIC if the differential pins don't end up working right.
  -- 
  -- 
  -- 
  -- 
  -- Lin7 Lin6 Lin5 Lin4  Lin3 Lin2 Lin1 Lin0  Lin7 Lin6 Lin5 Lin4  Lin3 Lin2 Lin1 Lin0
  -- ---- ---- ---- ----  ---- ---- ---- ----  ---- ---- ---- ----  ---- ---- ---- ----
  -- 0000 0LSS LLSS LLSS  LLSS LLSS LLSS LLSS                                               : line idle, power save
  -- 0000 1AAA AAAA AAAA  AAAA AAAA AAAA AAAA                                               : 248-bit read request
  -- 0001 000S LLSS LLSS  LLSS LLSS LLSS LLSS, DDDD DDDD DDDD DDDD  DDDD DDDD DDDD DDDD     : 32-bit read acknowledge
  -- 0001 001S LLSS LLSS  LLSS LLSS LLSS LLSS, FFFF FFFF FFFF FFFF  FFFF FFFF FFFF FFFF     : 32-bit read fault
  -- 0001 010S LLSS LLSS  LLSS LLSS LLSS LLSS                                               : x-bit write acknowledge
  -- 0001 011S LLSS LLSS  LLSS LLSS LLSS LLSS, FFFF FFFF FFFF FFFF  FFFF FFFF FFFF FFFF     : x-bit write fault
  -- 0001 100S LLSS LLSS  LLSS LLSS LLSS LLSS, DDDD DDDD DDDD DDDD  DDDD DDDD DDDD DDDV >=8x: 248-bit read acknowledge
  -- 0001 101S LLSS LLSS  LLSS LLSS LLSS LLSS, DDDD DDDD DDDD DDDD  DDDD DDDD DDDD DDDV >=8x: (248-bit read fault; illegal)
  -- 0010 GGGG GGGG GGGG  GGGG GGGG GGGG GGGG                                               : 28-bit GPIO data update
  -- 0011 LLSS LLSS LLSS  LLSS LLSS LLSS LLSS                                               : line idle, sync
  -- 01aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA                                               : 32-bit read request
  -- 10aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, DDDD DDDD DDDD DDDD  DDDD DDDD DDDD DDDD     : 32-bit write request
  -- 11aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, LLHH LLHH LLHH 1100  DDDD DDDD DDDD DDDD     : 16-bit write request 0
  -- 11aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, LLHH LLHH LLHH 0011  DDDD DDDD DDDD DDDD     : 16-bit write request 1
  -- 11aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, LLHH LLHH LLHH 1000  DDDD DDDD ---- ----     : 8-bit write request 0
  -- 11aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, LLHH LLHH LLHH 0100  ---- ---- DDDD DDDD     : 8-bit write request 1
  -- 11aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, LLHH LLHH LLHH 0010  DDDD DDDD ---- ----     : 8-bit write request 2
  -- 11aa aAAA AAAA AAAA  AAAA AAAA AAAA AAAA, LLHH LLHH LLHH 0001  ---- ---- DDDD DDDD     : 8-bit write request 3
  -- 
  -- Key:
  --   0 = transmit low, must be received low
  --   1 = transmit high, must be received high
  --   S = transmit high for sync, transmit low for power-save, receiver don't care
  --   L = transmit low, receiver don't care
  --   A = 256-bit-word address bit
  --   a = 32-bit word address within 256-bit block addressed by A
  --   D = data bit
  --   V = valid bit (the r-VEX does not use bit 0 of the instruction stream)
  --   F = fault bit
  --   G = GPIO bit
  -- 
  -- logical nibble ABCD is transmitted as 1ACBD, resulting in 10101 on the
  -- line when syncing for maximum amount of edges.
  
  -- Bus request between bus master/slave interface and packet interface.
  type hsi_bus_req_type is record
    
    -- Valid bit.
    valid                       : std_logic;
    
    -- Write mode bit, high for writes, low for reads.
    wr                          : std_logic;
    
    -- Write mask.
    mask                        : std_logic_vector(3 downto 0);
    
    -- Burst mode bit, high for instruction bursts, low for data.
    burst                       : std_logic;
    
    -- Word address.
    addr                        : std_logic_vector(31 downto 2);
    
    -- Write data.
    data                        : std_logic_vector(31 downto 0);
    
  end record;
  
  -- Bus response between bus master/slave interface and packet interface.
  type hsi_bus_resp_type is record
    
    -- Valid bit.
    valid                       : std_logic;
    
    -- Write mode bit, high for writes, low for reads.
    wr                          : std_logic;
    
    -- Fault bit, high for fault, low for OK.
    fault                       : std_logic;
    
    -- Burst mode bit, high for instruction bursts, low for data.
    burst                       : std_logic;
    
    -- Last beat in burst flag.
    last                        : std_logic;
    
    -- Read data/fault code.
    data                        : std_logic_vector(31 downto 0);
    
  end record;
  
  -- Array types for the above records.
  type hsi_bus_req_array is array (natural range <>) of hsi_bus_req_type;
  type hsi_bus_resp_array is array (natural range <>) of hsi_bus_resp_type;
  
end hsi_pkg;

package body hsi_pkg is
end hsi_pkg;
