
r-VEX ASIC prototype PCB
========================

This repository contains resources for the r-VEX ASIC prototyping PCB.

Installation/requirements
-------------------------

The following semi-nonstandard things are used by this repository:

 - Vivado 2017.4 (everything is TCL scripts, so other versions may work)
 - Modelsim
 - python3 with the plumbum and prompt_toolkit pip packages.

Before anything else, you need to compile the r-VEX toolchain. Refer to the
readme file in the `toolchain` directory for more information.

Optional stuff:

 - To modify the Teensy firmware, you need Arduino 1.8.5 with Teensyduino
   board support and drivers (other versions may work).
 - To modify the PCB you need Altium Designer '16 (other versions may work).
 - To compile Linux (don't) you need Petalinux 2017.4. But that version doesn't
   even work. Yes, I'm salty about this.

Getting started
---------------

Here's a rough idea of what you should do to get started with
(testing/debugging) the ASIC.

 - Build the r-VEX toolchain. Refer to the `toolchain` readme file.
 - Read the PCB overview section below to get to know the PCB. I strongly
   suggest you do this attentively before powering the board, so you know what
   you're dealing with. This is hardly a commercial, extensively tested and
   certified FPGA board with warranty.
 - Read the `zynq/pl` folder readme and build the PL project in emulator mode.
   This also builds the IP blocks.
 - Get a hang of the r-VEX toolchain, in particular the ASIC-specific programs.
   You need to source the `toolchain/sourceme` file (after building the
   toolchain) for anything to work. `xsdb-rvsrv` or `arvsrv` starts an `xsdb`
   batch mode session that runs an `rvd`-compatible TCP server in TCL, allowing
   `rvd` to access the PL through the TE0703 USB-JTAG port. `abit` will load the
   first bitstream it finds in the working directory onto the FPGA. (Before you
   do this, you must let the bootloader load its initial bitstream. This takes
   about a minute, which is stupidly long; I have no idea why it's this slow.)
   `actrl` is an interactive CLI for controlling the ASIC board. (It has
   reasonable built-in help and command completion, so you should be able to
   figure it out.)
 - Get the emulator working first (select this in `zynq/pl/project.tcl` before
   generating the Vivado project). Communication works, but the r-VEX isn't
   executing code properly. See the `rvex-sw` folder readme for more
   information.
 - Once you've done all that, you can try to get the ASIC itself to work. If you
   can, try to double-check that the pinout of the ASIC is correct; the power
   input pins should read as short-circuits with continuity mode of a
   multimeter. The expected pinout is archived in the `asic/pinout` directory.
   The first time you power up the board with the ASIC you might also want to
   use a lab power supply with current limit, just in case the on-board current
   limits aren't good enough. Note: this is mostly just my paranoia. It SHOULD
   work out of the box. Also, arguably, if the ASIC blows up the board because
   it's pinned wrong, the board would be of little use anyway, unless you can
   get the remaining dies rebonded in the right way or something. Again, mostly
   my paranoia.
 - It's just debugging and measurements from here onwards. Good luck!

Path to ASIC DOOM
-----------------

The PCB was designed with a DOOM port in mind, in case everything indeed happens
to work. This was originally my goal, but alas, the ASIC didn't even arrive
before I left and I gravely underestimated the amount of time it would take.

 - Add the necessary peripherals to the r-VEX in the PL project. That would be
   a UART, access to PS DDR, a timer for repititive interrupts/gettimeofday,
   a second UART for keyboard/mouse input from the Teensy, video output, and
   optionally audio output. I actually already have a video output designed for
   DOOM (320x240 8bpp upscaled to 1280x720 24bpp). Look through the commit log
   of this repository if you want it, the VHDL is in there somewhere even though
   I never had time to turn it into a proper IP block.
 - Program keyboard/mouse input in the Teensy. Get a proper UART working as well
   so you have a stdout sink for debugging.
 - Make drivers for newlib for the aforementioned peripherals. You can use the
   `plat-ml605-demo` and `rvex-drivers` GIT repository code as reference.
 - Finish debugging the `plat-ml605-demo-doom` code. You probably want to do
   that on an ML605 (which is what it was made for). I think you should be able
   to get it up and running in a day or two, especially since we already have a
   working DOOM port without nice peripheral drivers. (you could, of course,
   also base ASIC DOOM on that).
 - Stick it on the ASIC and hopefully get playable performance!

PCB overview
------------

Before you do anything with the ASIC board, please read through this list so you
know what you're dealing with.

### Underneath the FPGA board

![below-te0703](below-te0703.jpg)

1.  12V input. It's designed for the ML605/VC707 power bricks. If those are all
    in use, you could use the one from the VC707 on the perspex r-VEX demo board
    (ask Stephan); it's cable is long enough to reach the ASIC PCB if they're
    side by side without undoing any tie-raps. Failing all that, you could use
    the convertor cable to a good old HDD power connector that I included in the
    box. Either use it for the intended purpose, or just cut it in half and
    connect it to some other power supply using screw terminal blocks or
    something. The board should be okay with 8-15V input at around 3A rated
    current (in practice it uses less than 1A at 12V). It should be
    reverse-protected, but don't count on it.

2.  5V-5A switching regulator with overvoltage protection. This is what powers
    everything else.

3.  Main power switch. This just switches the enable pin of the 5V regulator. I
    would always turn this board off at the 230V side when you leave it for long
    periods of time because of this. After all, it's not exactly CE certified or
    something.

4.  5V regulator status. The red overvoltage LED takes precedence over the green
    OK LED. You need to manually power-cycle the board to clear the overvoltage
    latch. If you get overvoltages often, it may be because of very rapid changes
    in current consumption, causing a significant voltage to appear across L6
    (the output filter inductor on the bottom of the PCB). You can safely short
    this inductor with a wire if this is causing you lots of headaches. The only
    downside is that the rest of the power lines will be more noisy.

5.  Teensy 3.6 for r-VEX power management and USB host for keyboard/mouse. Both
    of these things could have been done by the Zynq processing system as well,
    but a Teensy seemed easier since it can be programmed with Arduino, as
    opposed to Vivado's terribly convoluted and broken system for using the ARM
    PS.

6.  Program buttons for the Teensy (both buttons do the same thing). You can
    press either of these to force the Teensy into the bootloader if it doesn't
    do this automatically.

7.  Teensy USB port. Currently only used for programming the Teensy with Arduino.
    It was originally intended to be used as a software USB-UART for the r-VEX as
    well.

8.  2-port USB hub for keyboard/mouse, since the Teensy only has one host port.

9.  USB host connectors for keyboard and mouse.

10. Status LEDs for keyboard and mouse. LED A will turn on if a keyboard is
    detected. LED B should turn on if a mouse is connected, but doesn't for some
    reason; the Teensy mouse driver appears to be broken. Haven't had time to
    debug this, or implement the communication with the FPGA for this.

11. 3x UART interconnect between Teensy and FPGA (bottom side). One channel is
    currently used to let the HSI IP control the power management functions of
    the Teensy. The others haven't been tested, but they're the same circuit, so
    unless I messed up the soldering they should be fine. They're intended to be
    used for keyboard/mouse data and a UART for the r-VEX (with the Teensy
    functioning as a USB-UART bridge).

12. HDMI input. This circuit is copied from the PYNQ board, so its IP is
    compatible with this board. ***DO NOT*** LEAVE THIS CONNECTED TO AN ACTIVE
    SOURCE WHEN THE BOARD IS POWERED DOWN. This causes the HDMI source to
    parasitically power the Zynq I/O bank through its termination resistors,
    which may or may not damage the Zynq (eventually).

13. HDMI output. Same story as the input. The current PL project has a loopback
    function for these connectors, which I used to test the PCB, and which may
    serve as an example for using them for real. ***DO NOT*** LEAVE THIS
    CONNECTED TO A POWERED DISPLAY WHEN THE BOARD IS POWERED DOWN. This causes
    the display to parasitically power the Zynq I/O bank through its termination
    resistors, which may or may not damage the Zynq (eventually).

14. PMOD header (unpopulated because I couldn't find the connector on Farnell).
    If anyone ever needs to expand the peripherals of the board, you can use
    this. There is room for a standard 12-pin female PMOD header, or you can put
    a wider connector on to get twice the I/Os.

### Trenz TE0703 breakout board

![te0703](te0703.jpg)

1.  Trenz TE0720 (XC7Z020 484-pin) with heatsink. This is the same FPGA as the
    PYNQ board, but a different package, so unfortunately I couldn't make it
    pin-compatible. The primary function of the FPGA is to provide the memory
    and peripherals for the r-VEX ASIC.

2.  5V barrel jack. ***DO NOT USE THIS,*** ALWAYS USE THE 12V INPUT! THE
    BASEBOARD WILL PROVIDE THE TE0703 WITH POWER.

3.  USB host connector, available to Zynq PS.

4.  Ethernet connector, available to Zynq PS.

5.  I/O bank voltage jumpers. The jumper for bank A is freely configurable; it
    is used exclusively for the PMOD connector. THERE ***MUST*** BE A JUMPER ON
    HEADER B TO SELECT 3.3V FOR BANK B. THERE ***MUST NOT*** BE JUMPERS ON THE
    C AND D HEADERS; THESE BANKS ARE POWERED BY THE BASEBOARD.

6.  Micro-SD socket for the Zynq PS to boot from.

7.  USB mini-B connector allowing access to the PS UART, and to the FPGA JTAG
    port through `xsdb` or `arvsrv`.

8.  Reset button for the Zynq.

9.  (below heatsink) Green LED indicating that the FPGA bitstream has NOT been
    loaded. YOU MUST WAIT FOR THE BOOTLOADER TO LOAD THE INITIAL BITSTREAM
    BEFORE LOADING YOUR OWN THROUGH THE JTAG. THIS TAKES ABOUT A MINUTE FOR SOME
    INANE REASON.

10. (fortunately mostly below heatsink) Annoyingly blinky lights of which I'm
    not sure what the functions are.

### ASIC area

![asic-area](asic-area.jpg)

1.  ZIF socket for the r-VEX ASIC PGA-84 package. Lift the lever to open up the
    socket, insert the ASIC, and push the lever back down until it clicks. The
    ASIC must be *CENTERED* in the socket (that is, there will be a single line
    of unused pins surrounding the ASIC), and pin 1 should be at the bottom
    right. You should be able to identify pin 1 by the corner of the ASIC that
    has an extra pin in the middle. If you're unsure, check the bottom of the
    PCB; only the socket pins that are physical pins in the ASIC are soldered.

2.  Loopback interface for the HSI. This allows the r-VEX ASIC HSI to be
    implemented in FPGA fabric to test the HSI independently of the ASIC. This
    is referred to as the ASIC emulator. Note that the r-VEX must NOT be in the
    ZIF socket when this interface is used. The A2F control line from the r-VEX
    would interfere with the loopback A2F line.

3.  Individually configurable current references for the r-VEX LVDS
    transceiver pairs 2 and 3. You can access these with the `actrl power`
    commands.

4.  Stereo audio output (PWM) connected to the FPGA. Unfortunately no IP block
    for this yet, and I haven't had the time to test it yet either. It should be
    trivial to use though. Maybe the PYNQ has some IPs you can use, it has a
    similar output (although this one is stereo, I think the PYNQ output is
    mono).

5.  High performance ADC (bottom side, 18bit 333kSPS) for monitoring one of the
    supply voltages or currents at a time.

6.  ADC channel selection leads. This allows you to select between the shunts of
    the four ASIC supply rails, or to a zero reference (this reference only
    works in the current measurement modes).

7.  ADC lowpass filter cutoff selection jumpers. The right one is for the input
    of the preamplifier, so if you're not using the preamp it doesn't do
    anything.

8.  ADC preamplifier gain selection jumper.

9.  ADC mode selection leads. From bottom to top: current measurement through
    preamplifier gain stage, current measurement through the complete
    preamplifier (should theoretically be more accurate but has much more
    noise), direct current measurement (withour preamp), direct voltage
    measurement (without preamp), and zero input for offset calibration.

10. Individually monitored and trimmable voltage regulators for all four r-VEX
    power domains. These regulators feature a controlled rise time of ~1ms. You
    can access these with the `actrl power` commands. Note that if an
    overvoltage is detected (~5% above DAC setpoint) the entire board is shut
    down to prevent damage. This means that you can't rapidly change the DAC
    values while the regulators are powered up; if you lower the setpoint too
    fast the entire board will shut down. The `actrl ramp` commands will ramp
    the DACs slow enough to prevent this.

11. Status LEDs for the r-VEX voltage regulators. Both LEDs are off if the
    Teensy is configured not to power the ASIC. The green OK LED turns on if all
    the voltage regulators are within ~5% of their DAC setpoints. The red fault
    LED turns on if the voltage regulators are out of range or if the Teensy has
    enabled overcurrent protection.

12. Powerup sequence header. The default jumper configuration powers uses the
    following sequence: I/O -> core -> analog 2V5 -> analog 1V2. If this
    sequence results in problems, you can replace the jumpers with a custom
    header that connects the enable/OK signals in a different way.

13. Individually configurable current references for the r-VEX LVDS
    transceiver pairs 0 and 1. You can access these with the `actrl power`
    commands.

### Bottom side

![bottom](bottom.jpg)

1.  5V output filter inductor. You can safely short this if you're getting
    spurious overvoltage issues that may be due to rapidly changing currents.

2.  Test points for the main voltage regulator.

3.  Test points for the current references. Naming: `I<A><B><C>`. `<A>` is `r`
    for a receiver current reference or `t` for a transmitter. `<B>` is the
    channel number. <C> is empty for the output of the current reference, `'`
    for the output before the 1k sense resistor, or `d` for the DAC voltage.
    The DAC to current ratio is 2V/mA referenced to 0V (the easiest way to probe
    0V is to push the probe into one of the rubber foot screws). The actual
    current can be measured by probing the voltage between the `Ixx'` and `Ixx`
    test points, at a 1V/mA ratio. It might be interesting to verify Shizhao's
    simulated voltage vs. current plots at some point using these test points.

4.  Hack to work around missing pullups for the USB subsystem (see `pcb` folder
    readme for more information).

5.  Hack to allow the Teensy to rapidly disable the voltage regulators in case
    it detects overcurrent.

6.  Hack to work around me cutting a trace with a knife while trying to fix a
    solder bridge. Hopefully this won't short with the via next to the lead at
    some point. Without this wire/trace, the 1V2A rail cannot be trimmed.

7.  1.0R shunt resistors for measuring current. These will be the first to
    go if there is a hard short that the Teensy can't detect fast enough. They
    should be good up to about 200 to 300 mA. If the ASIC goes above that for
    some reason, you should replace them with 0.2R resistors (they should be in
    the spare components bag) and update all the software that works with
    current measures to get correct readouts.

8.  ~200mA polyfuse for the HDMI output 5V line. This is not within HDMI spec,
    so it may or may not work with all displays. Like everything else to do
    with HDMI, it was just copied from the PYNQ. In any case, this will be the
    first to go if there is a short circuit somewhere downstream. There are
    spares in the spare parts bag.

